/*
* Authors	   :  Atul Hinge
* Date created :  24/10/2017
* Purpose      :  To create new attachment.
* Dependencies :   SelectAttachment.cmp,Email.cmp(Aura component)
* JIRA ID      :  PUX-335,PUX-266,PUX-265,PUX-252	
* -----------------------------------------------------------
* Modifications: 
*        Date:  
*        Purpose of modification:  
*        Method/Code segment modified:    
*/
({
    /* Authors: Atul Hinge || Purpose: send message from Lightning to VF page */ 
  sendToVF : function(component,event) {
        var message={"relatedId":component.get("v.relatedId"),"status":component.get("v.status"),"allowedSize":component.get("v.allowedSize"),"allowedExt":component.get("v.allowedExt")};
        component.set("v.message",message);
		console.log("relatedId "+component.get("v.relatedId"));
		console.log("status "+component.get("v.status"));
		console.log("allowedSize "+component.get("v.allowedSize"));
		console.log("allowedExt "+component.get("v.allowedExt"));
	
		
        var message = component.get("v.message");
        var vfOrigin = "https://" + component.get("v.vfHost");
		console.log("vfOrigin "+vfOrigin);
        var vfWindow = component.find("vfFrame").getElement().contentWindow;
        //vfWindow.postMessage(message, vfOrigin);
        vfWindow.postMessage(message, "*");
    },
    /* Authors: Atul Hinge || Purpose: receive message from VF page to Lightning */
    receiveFromVF: function(component,event) {
	console.log("receiveFromVF ");
        var vfOrigin = "https://" + component.get("v.vfHost");
		console.log("vfOrigin "+vfOrigin);
        window.addEventListener("message", function(event) {
            if (event.origin !== vfOrigin) {
			console.log("event.origin !== vfOrigin ");
                return;
            }
            if(event.data.response != undefined){

             //   component.set("v.contentDocumentLinks",event.data.response.contentDocumentLinks);
				  component.set("v.contentVersions",event.data.response.contentVersions);
				//	console.log("event.data.response.contentDocumentLinks "+event.data.response.contentDocumentLinks);
						console.log("event.data.response.contentVersions "+event.data.response.contentVersions);


            }
           console.log("event.data.status "+event.data.status);
            component.set("v.status",event.data.status);
        }, false);
    },
})