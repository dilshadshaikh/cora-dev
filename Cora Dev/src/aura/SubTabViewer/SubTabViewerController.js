/*
* Authors	   :  Atul Hinge
* Date created :  01/09/2017
* Purpose      :  Used to display attachment and case details in seperate tabs.
* Dependencies :  CaseDetailSectionDock,AttachmentListView.cmp,AttachmentViewer.cmp,SelectAttachment.cmp
* JIRA ID      :  PUX-38
* -----------------------------------------------------------
* Modifications: 
*        Date:  
*        Purpose of modification:  
*        Method/Code segment modified:    
*/
({
    /* Authors: Atul Hinge || Purpose: To open new Tab  */      
    openSubTab : function(component, event, helper) {
      var tabs=component.get("v.subTabList");
        if(event.getParam('parentTabName')==component.get("v.name")){
            var newTab=true;
            for(var i=0;i<tabs.length;i++){
                if(tabs[i].id==event.getParam('id')){
                	newTab=false;    
                }
            }
            if(newTab){
                
                tabs.push({"label":event.getParam('label'),"id":event.getParam('id'),object:event.getParam('object')});
                component.set("v.subTabList",tabs);
                $A.util.removeClass(component.find('subTab'), 'hideFirstTab');
               
            }
             try {
                    component.find("subTab").set("v.selectedTabId",event.getParam('id'));
                }catch(err) {
                    
                }
           
        }
       
   },
    /* Authors: Atul Hinge || Purpose: To close any Tab  */ 
    close : function(component, event, helper) {
        var tabs=component.get("v.subTabList");
        var opentabs=[];
        var comp=component.find("subTab").get('v.selectedTabId');
        for(var i=0;i<tabs.length;i++){
            if(event.currentTarget.id !== tabs[i].id){
                opentabs.push(tabs[i]);
            }
        }
        component.find("subTab").set("v.selectedTabId",'default');
        component.set("v.subTabList", opentabs);
        if(opentabs.length===0){
            $A.util.addClass(component.find('subTab'), 'hideFirstTab');
        }
    },
    
})