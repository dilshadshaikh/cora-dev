({
	 init :function(component, event, helper) {
	
       	helper.getSobjectDetails(component, event);
		helper.getLoginUser(component,event);
      },
	afterScriptsLoaded :function(component, event, helper) {
		window.globalvalues.pagemode = component.get("v.pagemode");
		component.set("v.JSAvailable",true);
	},
	navtoparent  :function(component, event, helper) {
		component.set("v.isDisplay",false);
		//component.set("v.pagemode",'EDIT');
		window.globalvalues.pagemode = 'EDIT';
	},
	editable:function(component, event, helper) {
		component.set("v.pagemode",'EDIT');
		component.set("v.isEdit",true);
		component.set("v.isDisplay",false);
        
       /*  var editRecordEvent = $A.get("e.force:editRecord");
    		editRecordEvent.setParams({
         	"recordId": component.get('v.recordId')
   		});
    editRecordEvent.fire(); */
       // window.open("https://esmdev-dev-ed.lightning.force.com/lightning/r/"+component.get('v.sObjectName')+"/"+component.get('v.recordId')+"/edit","_self");
		
	},
	handleRefresh : function(component, event, helper) {
		if(event.getParam("callOnChangeManager") != undefined && event.getParam("callOnChangeManager") == true)
		{
			//Call on change manager
			var action = component.get("c.callOnChangeManager");
			action.setParams({
				className: component.get("v.layoutJson")["PageLayout"]["onChangeManagerClass"],
				childSobject: component.get("v.sObject"),
				invLineItemList: component.get("v.invLineList"),
				puchaseOrder: component.get("v.POList"),
				//grnLineItemList: component.get("v.grnLineList"),
				field: event.getParam("fieldName"),
				//stringifyVal: JSON.stringify(component.get("v.sObject")),
				value : event.getParam("fieldValue"),
				childNameSpace: component.get("v.childNameSpace"),
				//extraParamMap: undefined
			});
			action.setCallback(this, function(response) {
				var state =response.getState();
				if (state === "SUCCESS") {
					if(response.getReturnValue().error!=null && response.getReturnValue().error!=undefined && response.getReturnValue().error!=''){
						helper.showToast("Validation Alert!","error","sticky",response.getReturnValue().error);
					}else{
						component.set("v.sObject", response.getReturnValue().currentObject);
					}
				}
				else if (state === "INCOMPLETE") {
					//Logic
				} else if (state === "ERROR") {
					var errors = response.getError();
					if (errors) {
						if (errors[0] && errors[0].message) {
							helper.showToast("Validation Alert!","error","sticky",errors[0].message);
						}
					} else {
						helper.showToast("Validation Alert!","error","sticky","Unknown error");
                		}
					}
				});	
			$A.enqueueAction(action);
		}
		var jsonData = component.get("v.layoutJson");
		if(jsonData.PageLayout.POReference.POReferenceRequired == true && jsonData.PageLayout.POReference.criteria != null && jsonData.PageLayout.POReference.criteria != undefined && jsonData.PageLayout.POReference.criteria != "")
		{
			var criJson = jsonData.PageLayout.POReference.criteria;
			if(criJson.criteriagroup != undefined && criJson.criteriagroup.rules != undefined && criJson.criteriagroup.rules.length > 0)
			{
				jsonData.PageLayout.POReference.render = _criteriahelper.validate(criJson,component.get("v.sObject"),component.get("v.fieldMetadata"));
			}
		}
		if(jsonData.PageLayout.lineItemComponents != undefined && jsonData.PageLayout.lineItemComponents.length > 0)
		{
			for(var i = 0; i < jsonData.PageLayout.lineItemComponents.length; i++)
			{
				var criJson = jsonData.PageLayout.lineItemComponents[i].criteria;
				if(criJson != undefined && criJson.criteriagroup != undefined && criJson.criteriagroup.rules != undefined && criJson.criteriagroup.rules.length > 0)
				{
					jsonData.PageLayout.lineItemComponents[i].render = _criteriahelper.validate(criJson,component.get("v.sObject"),component.get("v.fieldMetadata"));
				}
			}
		}
		if(jsonData.PageLayout.headerLevelComponents != undefined && jsonData.PageLayout.headerLevelComponents.length > 0)
		{
			for(var i = 0; i < jsonData.PageLayout.headerLevelComponents.length; i++)
			{
				var criJson = jsonData.PageLayout.headerLevelComponents[i].criteria;
				if(criJson != undefined && criJson.criteriagroup != undefined && criJson.criteriagroup.rules != undefined && criJson.criteriagroup.rules.length > 0)
				{
					jsonData.PageLayout.headerLevelComponents[i].render = _criteriahelper.validate(criJson,component.get("v.sObject"),component.get("v.fieldMetadata"));
				}
			}
		}
		component.set("v.layoutJson",jsonData);
	},
   	openModel: function(component, event, helper) {
		window.scroll(0, 0);
	   	document.getElementById("modalPopup").style.display = "block";
      	component.set("v.isOpen", true);	
   	},
 
   	closeModel: function(component, event, helper) {
    	document.getElementById("modalPopup").style.display = "none";
      	component.set("v.isOpen", false);
   	},
	handleActive : function(cmp, event, helper) {
        //Code start for PUX-358
		if(event.getSource().get("v.id")!='Notes Attachments')
			window.overDueIn=''; 
        if(event.getSource().get("v.id") != window.selectedTab)	{
			window.inputFilterFirst=null;	
			window.inputFilterSecond=null;	
			window.selectFieldSecond=null;	
			window.inputFilterThird=null;	
			window.selectFieldThird=null;
		}	
        var tab = event.getSource();
        cmp.set("v.selectedTabId", tab.get('v.id'));
    	window.selectedTab=event.getSource().get("v.id");
    },
   	renderComponent:function(component,event,helper){
        //alert('render--');
		var tab = event.getSource().get('v.id');
        //alert(tab);
		helper.renderComponentHelper(component,event,tab);
   },

   renderbtnComponent:function(component,event,helper){
		var buttonName  = event.getSource().getLocalId();
		if(document.getElementById("InvoiceLineItemsDiv"))
		{
			document.getElementById("InvoiceLineItemsDiv").style.display = "none";
		}
		if(document.getElementById("AdditionalChargesDiv"))
		{
			document.getElementById("AdditionalChargesDiv").style.display = "none";
		}
		if(document.getElementById(buttonName+'Div'))
		{
			document.getElementById(buttonName+'Div').style.display = "block";
		}
   },
	submitRecordData: function(component,event,helper){
		
        var childQualityCmp = component.find("qualityCheck");
        //var test = childQualityCmp.qualityFun();
   
		helper.submitRecordData(component, event);
		
		
       // helper.qualityControls(component,event);
	},
	
	/* End : PUX-340  : Add Notes event*/
		/*
	 * Authors: Parth Lukhi
	 * Purpose:  Handling Case Action Event when Clicked on Reply ,Reply All
	 * Dependencies:  InterActionListViewHelper.js
	 * 
	 * Start : Handling Case Action Event when Clicked on Reply ,Reply All
	 */
	handleCaseActionEvent: function (component, event, helper) {
		var action = event.getParam("Action");
		var case1 = event.getParam("Case");
		component.set("v.Case", event.getParam("Case"));
		case1 = component.get("v.Case");
		var interactionId = event.getParam("interactionId");
		if (action == 'reply' || action == 'replyall' || action == 'forward') {
			var emailTitle = '';
			if (action == 'reply') {
				emailTitle = 'Reply';
			} else if (action == 'replyall') {
				emailTitle = 'Reply All';
			} else if (action == 'forward') {
				emailTitle = 'Forward';
			}
			component.set("v.emailTitle", emailTitle);
			component.set("v.interactionId", interactionId);
			component.set("v.showReplyScreen", !component.get("v.showReplyScreen"));
			component.set("v.showInteractionList", false);
			event.stopPropagation();
		}

	},
	 AddInvoiceLine: function(component, event, helper) {
		 helper.AddInvoiceLineHelper(component, event);
   },
	handleInvoiceChange: function(component, event, helper) {
		///$A.get('e.force:refreshView').fire();
   },
   handleRefreshEvent : function(cmp, event, helper) {
        //Code start for PUX-358
		alert("handle event");
    },
	handleRefreshAmount: function(cmp, event, helper) {
		var childCmpInvLine = cmp.find("invlineDet");
		if(childCmpInvLine){
			childCmpInvLine.handleRefreshAmount(function(v){
				if(v!=null && v!=undefined && v!=0){
					cmp.set("v.sObject.Amount__c",v);
					cmp.set("v.sObject", cmp.get("v.sObject"));
				}
				
			});
		}
        var childCmpInvLine = cmp.find("addlineDet");
		if(childCmpInvLine){
			childCmpInvLine.handleRefreshAmount(function(v){
				if(v!=null && v!=undefined && v!=0){
					cmp.set("v.sObject.Other_Charges_Surcharges__c",v);
					cmp.set("v.sObject", cmp.get("v.sObject"));
				}
				
			});
		}
		helper.calulateTotalAmount(cmp, event);
	},
	closeToast: function(component, event, helper) {
		document.getElementById("showToast").style.display = "none";
   	},
	saveRecordData: function(component, event, helper) {
	  
		var action = component.get("c.closeToast");
		$A.enqueueAction(action);	    
		helper.saveRecordData(component, event);

   	},
	dragModal: function(component,event,helper){
		$('#modalPopup').draggable();
	},

	// Author  || vandita || ESMPROD-1059 
	toggleSpinner: function (component,event,helper) {
	
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, 'slds-hide');
    },
})