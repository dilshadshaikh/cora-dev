({
    getLoginUser: function(component, event) {
        var action = component.get("c.getUserInfo");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.UserName", response.getReturnValue().UserName);
                window.InstanceName = response.getReturnValue().InstanceName;
                component.set("v.Instance", InstanceName);
                component.set("v.renderComponents", true);
            } else if (state === "INCOMPLETE") {
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        this.showToast("Validation Alert!","error","sticky",errors[0].message);
                    }
                } else {
					this.showToast("Validation Alert!","error","sticky","Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    getSobjectDetails: function(component, event) {
        var action = component.get("c.getSObjectDetails");
        action.setParams({
            layout: component.get("v.pagemode"),
            recId: component.get("v.recordId"),
            sObjectName: component.get("v.sObjectName"),
			dataModal: component.get("v.sObject"),
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            		if (state === "SUCCESS") {
		    component.set("v.esmNameSpace",response.getReturnValue().esmNameSpace);
		    if (response.getReturnValue().isAccesible == true) {
			   /*if(response.getReturnValue().currentObject.Vendor__r != '' || response.getReturnValue().currentObject.Vendor__r != 'undefined' || response.getReturnValue().currentObject.Vendor__r != null )
				{
					  var changeVendorEvent = $A.get("e.c:changeVendor");
						changeVendorEvent.setParams({
							"vendorId": response.getReturnValue().currentObject.Vendor__r.Id,
							"objectName": response.getReturnValue().currentObject.Vendor__r.Name,				
						});
					 changeVendorEvent.fire();
				} */
		        var layoutjson = JSON.parse(response.getReturnValue().layoutJson);
				console.log('layoutjson ----------');
				console.log(layoutjson);
		        if (layoutjson["layoutJson"][0] == undefined) {
		            //Show toast - Blocker
		            var toastEvent = $A.get("e.force:showToast");
		            toastEvent.setParams({
		                "title": "Validation Alert!",
		                "type": "error",
		                "mode": "sticky",
		                "message": "Layout configuration not found."
		            });
		            toastEvent.fire();
		        } else {
		            //if (component.get("v.pagemode") != 'NEW') {
		                component.set("v.sObject", response.getReturnValue().currentObject);
		                component.set("v.currentObject", response.getReturnValue().currentObject);					
		           // }
		            var jsonCon = JSON.parse(response.getReturnValue().layoutJson);
		            //component.set("v.sObjectName", jsonCon["sObjectName"]);
		            var jsonData = JSON.parse(jsonCon["layoutJson"][0]["Config_Json__c"]);

		            component.set("v.layoutJson", jsonData);
		            component.set("v.objMetadata", jsonCon["sObjectMetadata"]);
		            component.set("v.fieldMetadata", jsonCon["sObjectFieldMetadata"]);

		            component.set("v.layoutJsonAvailable", true);
		            var intervalId;
		            var checkTheAction = function() {
		                if (component.get("v.layoutJsonAvailable") == true && component.get("v.JSAvailable") == true) {
		                    window.clearInterval(intervalId);
		                    //Check rendering criteria
		                    jsonData.PageLayout.POReference.render = true;
		                    if (jsonData.PageLayout.POReference.POReferenceRequired == true && jsonData.PageLayout.POReference.criteria != null && jsonData.PageLayout.POReference.criteria != undefined && jsonData.PageLayout.POReference.criteria != "") {
		                        var criJson = jsonData.PageLayout.POReference.criteria;
		                        if (criJson.criteriagroup != undefined && criJson.criteriagroup.rules != undefined && criJson.criteriagroup.rules.length > 0) {
		                            jsonData.PageLayout.POReference.render = _criteriahelper.validate(criJson, component.get("v.sObject"), component.get("v.fieldMetadata"));
		                        }
		                    }
		                    if (jsonData.PageLayout.lineItemComponents != undefined && jsonData.PageLayout.lineItemComponents.length > 0) {
		                        for (var i = 0; i < jsonData.PageLayout.lineItemComponents.length; i++) {
		                            jsonData.PageLayout.lineItemComponents[i].render = true;
		                            var criJson = jsonData.PageLayout.lineItemComponents[i].criteria;
		                            if (criJson != undefined && criJson.criteriagroup != undefined && criJson.criteriagroup.rules != undefined && criJson.criteriagroup.rules.length > 0) {
		                                jsonData.PageLayout.lineItemComponents[i].render = _criteriahelper.validate(criJson, component.get("v.sObject"), component.get("v.fieldMetadata"));
		                            }
		                        }
		                    }
		                    if (jsonData.PageLayout.headerLevelComponents != undefined && jsonData.PageLayout.headerLevelComponents.length > 0) {
		                        for (var i = 0; i < jsonData.PageLayout.headerLevelComponents.length; i++) {
		                            jsonData.PageLayout.headerLevelComponents[i].render = true;
		                            var criJson = jsonData.PageLayout.headerLevelComponents[i].criteria;
		                            if (criJson != undefined && criJson.criteriagroup != undefined && criJson.criteriagroup.rules != undefined && criJson.criteriagroup.rules.length > 0) {
		                                jsonData.PageLayout.headerLevelComponents[i].render = _criteriahelper.validate(criJson, component.get("v.sObject"), component.get("v.fieldMetadata"));
		                            }
		                        }
		                    }
		                    component.set("v.layoutJson", jsonData);
		                }
		            };
		            intervalId = window.setInterval(checkTheAction, 2000);
		        }
		    }else{
				component.set("v.recordError","Insufficient previlage to Edit Record");
			}
		}
			else if (state === "INCOMPLETE") {
                //Logic
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                       this.showToast("Validation Alert!","error","sticky",errors[0].message);
                    }
                } else {
						this.showToast("Validation Alert!","error","sticky","Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    submitRecordData: function(component, event) {
    	
        var attachList = component.get("v.attachList");;
        var primaryDocJson = component.get("v.primaryDocJson");

        //--- check user action is "Validate" or not ---//
        var childCmpInvLine = component.find("invlineDet");
		var childInvDetail = component.find("invDetailComp");
		var childOtherDetail = component.find("addlineDet");
        var invLineValid = false;
		var invDetailValid = false;
		var isOtherValid = false;
		if(childCmpInvLine!=null && childCmpInvLine!=undefined && component.get("v.invLineList")!=null && childCmpInvLine.validate!=null)
        {
			childCmpInvLine.validate(function(t) {
				if(t){
					invLineValid = t;
				}
			});
		}else{
			invLineValid = true;
		}
		if(childInvDetail!=null && childInvDetail!=undefined)
        {
			childInvDetail.validate(function(t) {
				if(t){
					invDetailValid = t;
				}
			});
		}else{
			invDetailValid = true;
		}
		if(childOtherDetail!=null && childOtherDetail!=undefined && component.get("v.additionalChargesList")!=null && childOtherDetail.validate!=null)
        {
			childOtherDetail.validate(function(t) {
				if(t){
					isOtherValid = t;
				}
			});
		}else{
			isOtherValid = true;
		}
		if(invLineValid && invDetailValid && isOtherValid){		
		    console.log(component.get("v.sObjectName"));
			var mainObjAPI = component.get("v.sObjectName") ; 
			var esmNmSpace = component.get("v.esmNameSpace");
			if( mainObjAPI == esmNmSpace+'Invoice__c'){
				this.callSaveOrValidate(component, event);
			}
			else{
				this.saveObjectDetail(component,event);
			}
		}else{
			var toastEvent = $A.get("e.force:showToast");
			toastEvent.setParams({
				"title": "Validation Alert!",
				"type": "error",
				"mode": "sticky",
				"message": "Please fill all required details"
			});
			toastEvent.fire();
		}
		
    },
	saveObjectDetail : function(component, event){
		var confirm = false;
		var block = false;
		var alertMsg = '';
		customImplementationOnSaveClass(component.get("v.sObject"),component.get("v.pagemode"),confirm,block,alertMsg);
		if(alertMsg !=null){
			this.showToast("Validation Alert!","error","sticky",alertMsg);
		}
		if (!block) {
			this.saveObjectDetailApexCall(component, event);
		}
	},

    callSaveOrValidate: function(component, event) {
	console.log(' callSaveOrValidate called ');
        if (component.get("v.sObject.User_Action__c") == "Validate") {
            this.validateRecordData(component, event);
        }
		else if (component.get("v.sObject.User_Action__c") == "QC Reject") 
		{
            var qcComp = component.find("qualityCheck");
			var qcObject = [];
			qcComp.getQcObject(function (resObj){
				qcObject =resObj;
			});
			if(qcObject !=null && qcObject!= undefined){
				var allEmpty=true;
				for(var k in qcObject){
					if(qcObject[k]!=null && qcObject[k]!= undefined && qcObject[k]!=''){
						allEmpty =false;
						break;
					}
				}
				if(allEmpty){
					this.renderComponentHelper(component, event,'QualityCheck');
					this.showToast("Validation Alert!","error","sticky","Please enter atleast one qc value");
				}
                else 
                {
                    this.saveRecordData(component, event);
                }
			}
			else{
				this.showToast("Validation Alert!","error","sticky","Please enter atleast one qc value");
			}
        }
		else 
		{
            this.saveRecordData(component, event);
        }
    },
    validateRecordData: function(component, event) {
	    component.set("v.sObject.Exception_Reason__c",null);
		var action = component.get("c.validateInvoiceData");
        action.setParams({
            invoiceObject: component.get("v.sObject"),
            invLineItemList: component.get("v.invLineList"),
			otherChargesList: component.get("v.additionalChargesList"),
            poList: component.get("v.POList"),
            childNameSpace: component.get("v.childNameSpace")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				var map = response.getReturnValue();
				if(map == null)
				{
					this.showToast("Validation Alert!","Server side error occurred. Please contact Administrator");
				}
				if(map != null && map != "")
				{
					if(map.currentObject != null && map.currentObject != "")
					{
						console.log('map.currentObject:'+map.currentObject);
						console.log(map.currentObject);
						console.log('map.currentObject:'+map.currentObject.Exception_Reason__c);					  									
						component.set("v.sObject",map.currentObject); 			
						if(component.get("v.sObject.Exception_Reason__c") == null || component.get("v.sObject.Exception_Reason__c") == undefined){
							component.set("v.sObject.Exception_Reason__c",null);						
						}
						
					}
                    if(map.invLines != null && map.invLines != "")
					{
						var invNew = new Object;
						var invLinesNew = new Array();
						for(var k in map.invLines)
						{
							invNew = map.invLines[k];
							invNew["sobjectType"] = 'Invoice_Line_Item__c';
							invLinesNew[k] = invNew;
						}
						//component.set("v.invLineList",invLinesNew);
						component.set("v.invLineListdummy",null);
						var isEmptydummy11 = $A.util.isEmpty(component.get("v.invLineListdummy"));
						console.log('isEmptydummy11 :');
						console.log(isEmptydummy11);
						component.set("v.invLineListdummy",invLinesNew);
						var isEmptydummy1 = $A.util.isEmpty(component.get("v.invLineListdummy"));
						console.log('isEmptydummy1 :');
						console.log(isEmptydummy1);
					}
					if(map.validationMap != null && map.validationMap != "")
					{
						if((map.validationMap.tempExceptionReason == null || map.validationMap.tempExceptionReason == "" || map.validationMap.tempExceptionReason == "{}") 
						&& map.validationMap.jsonValidationExceptionMsg != null && map.validationMap.jsonValidationExceptionMsg != "" && map.validationMap.jsonValidationExceptionMsg != "{}")
						{
							//Show toast - Blocker
							this.showToast("Validation Alert!","error","sticky",JSON.parse(map.validationMap.jsonValidationExceptionMsg).Invoice_Validation);
						}
						else if(map.validationMap.tempExceptionReason != null && map.validationMap.tempExceptionReason != "" && map.validationMap.tempExceptionReason != "{}")
						{
							//Show toast
							document.getElementById("divToastMsg").innerHTML = JSON.parse(map.validationMap.jsonValidationExceptionMsg).Invoice_Exception;
							// -- Added by Varsha | 11-04-2018 | ESMPROD-1007 | Start -- //
							component.set("v.exceptionString", JSON.parse(map.validationMap.jsonValidationExceptionMsg).Invoice_Exception);
							// -- Added by Varsha | 11-04-2018 | ESMPROD-1007 | End -- //
							document.getElementById("showToast").style.display = "block";
						}
						else
						{
							this.saveRecordData(component, event);
						}
					}
					else if(map.currentObject != null && map.currentObject != "")
					{
						console.log('map.currentObject -----------');
						console.log(map.currentObject);
						this.saveRecordData(component, event);
					}
				}
				else{
					this.showToast("Validation Alert!","error","sticky",map.error);
				}
            }
            else if (response.getState() === 'ERROR') 
		{
				var errors = response.getError(response.getReturnValue());
				if (errors) {
					if (errors[0] && errors[0].message) {
						   this.showToast("Validation Alert!","error","sticky",errors[0].message);
					}
				}
        }
        });
        $A.enqueueAction(action);
    },
    saveRecordData: function(component, event) {
	    console.log(' saveRecordData method called');
        var invoiceObjName = component.get("v.childNameSpace") + "Invoice__c";
        var sObjectName = component.get("v.sObjectName");

        if (invoiceObjName == sObjectName) {
			var qcObject = [];
			if (component.get("v.sObject.Current_State__c") == "Ready For QC" ) 
			{
				var qcComp = component.find("qualityCheck");
					qcComp.getQcObject(function (resObj){
					qcObject =resObj;
				});
			}else{
				qcObject =null;
			}
            var invLineDetail = component.get("v.invLineList");
            var PODetail = component.get("v.POList");
            var POLineDetail = component.get("v.POLineListNew");
            //component.set("v.invObj",component.get("v.sObject"));
            var action = component.get("c.SaveInvoiceData");
            var invObject = component.get("v.sObject");
			component.set("v.invObj",component.get("v.sObject"));			
			console.log('sObject data ::');
			console.log(component.get("v.invObj"));
			// set the sobjectType!
			//component.set("v.additionalChargesList",component.get("v.additionalChargessend"));
			// var addChargers=component.get('v.additionalChargesList');
			//addChargers.sobjectType='Invoice_Line_Item__c';
			var isEmptydummy = $A.util.isEmpty(component.get("v.invLineListdummy"));			
			console.log('isEmptydummy :');
			console.log(isEmptydummy);
			if(isEmptydummy == false)
			{
				var linelist = component.get("v.invLineListdummy");
				component.set("v.invLineList",linelist);
			}
			console.log(component.get("v.invLineListdummy"));			
			var isEmptymain = $A.util.isEmpty(component.get("v.invLineList"));
			console.log('isEmptymain :');
			console.log(isEmptymain);
			console.log(component.get("v.invLineList"));
			
		  
			action.setParams({
                invoiceObject: component.get("v.invObj"),
                invLineItemList: component.get("v.invLineList"),
                poList: component.get("v.POList"),
                otherChargesList: component.get("v.additionalChargesList"),
                pageMode: component.get("v.pagemode"),
                primaryDocJsonAtt : component.get("v.primaryDocJson"),
                NewAttachmentsAtt  : component.get("v.attachList"),
				qcObject : qcObject				

            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    //cmp.set("v.objectList", response.getReturnValue());
                    //alert("Message:" + response.getReturnValue());
					this.saveAlert(component, event,response.getReturnValue());
					
                }
				else if (response.getState() === 'ERROR') {
					var errors = response.getError(response.getReturnValue());
					if (errors) {
						if (errors[0] && errors[0].message) {
						    this.showToast("Validation Alert!","error","sticky",errors[0].message);
						}
                } else {
                    this.showToast("Validation Alert!","error","sticky","Unknown error");
                }
            } else {
					this.showToast("Validation Alert!","error","sticky","Something went wrong, Please check with your admin");
            }
            });
            $A.enqueueAction(action);

        }
    },

    AddInvoiceLineHelper: function(component, event) {
        var poLine;
        var poLineList;
        var childCmpPOLine = component.find("PolineItemDet");
        var validForm = true;
        childCmpPOLine.addPOLinesMain(function(result) {
            poLine = result["polines"];
            poLineList = result["polinesobj"];
        });
        for (var key in poLine) {
            for (var key1 in poLineList) {
                if (poLineList[key1]["Id"] == poLine[key]) {
                    //-- Added by Vandita | 9-5-2018 | ESMPROD-1066 | Start --//
                     if (poLineList[key1]["Quanity_To_Invoice__c"] === null || poLineList[key1]["Quanity_To_Invoice__c"] === undefined || poLineList[key1]["Quanity_To_Invoice__c"] === 0 || poLineList[key1]["Quanity_To_Invoice__c"] <= 0) {
                        validForm = false;
                    }                                                           
                     //-- Added by Vandita | 9-5-2018 | ESMPROD-1066 | end --//
                    if (poLineList[key1]["Rate__c"] === null || poLineList[key1]["Rate__c"] === undefined || poLineList[key1]["Rate__c"] === 0) {
                        validForm = false;
                    }
                }
            }
        }
		console.log('validForm ----'+validForm);
        if (validForm) {
            var childCmpInvLine = component.find("invlineDet");
			console.log('MainPage Js childCmpInvLine --------');
			console.log(childCmpInvLine);
            var grnList = component.get("v.grnLineList");
			console.log('grnList --------');
			console.log(grnList);
			console.log('poLine ------');
			console.log(poLine);
			console.log('poLineList -------');
			console.log(poLineList);
            // call the aura:method in the child component
            childCmpInvLine.setInvoiceLineFromPO(poLine, poLineList, grnList);
            component.set("v.isOpen", false);
            document.getElementById("modalPopup").style.display = "none";
			this.calulateTotalAmount(component, event);
			//component.set("v.sObject",component.get("v.sObject"));
        } else {
			this.showToast("Validation Alert!","error","sticky","Please enter proper poline detail.");
        }
		
    },
	calulateTotalAmount: function(component, event) {
		component.set("v.sObject", component.get("v.sObject"));
		if(component.get("v.pagemode") === 'NEW'){
			var totalamt = 0;
			if(component.get("v.sObject.Amount__c")!=null && component.get("v.sObject.Amount__c")!= undefined)
			{
				totalamt = totalamt + Number(component.get("v.sObject.Amount__c"));
			}
			if(component.get("v.sObject.Other_Charges_Surcharges__c")!=null && component.get("v.sObject.Other_Charges_Surcharges__c")!= undefined)
			{
				totalamt = totalamt +  Number(component.get("v.sObject.Other_Charges_Surcharges__c"));
			}
			if(component.get("v.sObject.Tax__c")!=null && component.get("v.sObject.Tax__c")!= undefined)
			{
				totalamt = totalamt +  Number(component.get("v.sObject.Tax__c"));
			}
			component.set("v.sObject.Total_Amount__c",totalamt);
		}
	},
	showToast: function(title, type,mode,message) {
		var toastEvent = $A.get("e.force:showToast");
							toastEvent.setParams({
								"title": title,
								"type": type,
								"mode": mode,
								"message": message
							});
							toastEvent.fire();
	},
	renderComponentHelper: function(component, event,tab) {
		if(document.getElementById("NotesAttachmentsDiv"))
		{
			document.getElementById("NotesAttachmentsDiv").style.display = "none";
		}
		if(document.getElementById("InvoiceHistoryDiv"))
		{
			document.getElementById("InvoiceHistoryDiv").style.display = "none";
		}
		if(document.getElementById("InteractionDiv"))
		{
			document.getElementById("InteractionDiv").style.display = "none";
		}
		if(document.getElementById("QualityCheckDiv"))
		{
			document.getElementById("QualityCheckDiv").style.display = "none";
		}
        if(document.getElementById("routeDiv")){
            document.getElementById("routeDiv").style.display = "none";
        }
		var layOutJson = component.get("v.layoutJson").PageLayout.headerLevelComponents.length;
		for(var i=0;i<=layOutJson;i++){
			var dynamicDiv = "RelatedListDetails"+i+"Div";
			if(document.getElementById(dynamicDiv))
			{
				document.getElementById(dynamicDiv).style.display = "none";
			}
		}
		if(document.getElementById(tab+'Div'))
		{
			document.getElementById(tab+'Div').style.display = "block";
		}
	},
	saveAlert: function(component, event,tab) {
		if(tab !=null && tab !=undefined && tab['success']!=null){
			this.showToast("Success!",null,null,tab['success']);
		}
		else if(tab !=null && tab !=undefined && tab['displayError']!=null){
			this.showToast("Validation Alert!","error","sticky",tab['displayError']);
		}
		else if(tab !=null && tab !=undefined && tab['failure']!=null){
			this.showToast("Validation Alert!","error","sticky",tab['failure']);
		}
		else{
			this.showToast("Success!",null,null,"Error in saving data.");
		}
		component.set("v.isDisplay",false);
		window.globalvalues.pagemode =component.get("v.pagemode");
	/*	for(var kSucess in tab){
						if(kSucess == 'true' ){
							this.showToast("Success!",null,null,"Record has been saved.");
							component.set("v.isDisplay",false);
							//component.set("v.pagemode",'EDIT');
							window.globalvalues.pagemode =component.get("v.pagemode");
							/*var navEvt = $A.get("e.force:navigateToSObject");
								navEvt.setParams({
								"recordId": retId,
								"slideDevName": "related"
							});
							navEvt.fire();*/
						/*}else{
							this.showToast("Error!","error","sticky",response.getReturnValue()[kSucess]);
						}
					}*/
	},
	saveObjectDetailApexCall: function(component, event) {
		var action = component.get("c.SaveObjectData") ;
		var sObjectConv = component.get("v.sObject");
		    if (sObjectConv != null)
		        sObjectConv["sobjectType"] = component.get("v.sObjectName");
		    action.setParams({
		        customObject: sObjectConv,
		        pageMode: component.get("v.pagemode"),
		        primaryDocJsonAtt: component.get("v.primaryDocJson"),
		        NewAttachmentsAtt: component.get("v.attachList"),
		        qcObject: null,
		        objectName: component.get("v.sObjectName"),
		    });
		    action.setCallback(this, function(response) {
		        var state = response.getState();
		        if (state === "SUCCESS") {
		            //cmp.set("v.objectList", response.getReturnValue());
		            //alert("Message:" + response.getReturnValue());
		            this.saveAlert(component, event, response.getReturnValue());

		        } else if (response.getState() === 'ERROR') {
		            var errors = response.getError(response.getReturnValue());
		            if (errors) {
		                if (errors[0] && errors[0].message) {
		                    this.showToast("Validation Alert!", "error", "sticky", errors[0].message);
		                }
		            } else {
		                this.showToast("Validation Alert!", "error", "sticky", "Unknown error");
		            }
		        } else {
		            this.showToast("Validation Alert!", "error", "sticky", "Something went wrong, Please check with your admin");
		        }

		    });
		    $A.enqueueAction(action);
	}
})