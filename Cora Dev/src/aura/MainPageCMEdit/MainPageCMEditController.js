/*
    Name           : CaseDetailViewOverrideController.js
    Author         : Ashish Kr.
    Description    : Controller JS used for getting Case Detail View  
*/
({
    /* Start : Init  function  for Case Detail View  component*/
    init :function(component, event, helper) {
        console.log(component.get("v.recordId"));
    	component.set("v.caseid",component.get("v.recordId"));  
       	helper.getCaseDetail(component, event);
		helper.getLoginUser(component, event);
        
      //  helper.handleCaseAction(component,event);
        
 	},
 	handleCaseActionEvent: function(component, event, helper) {
        
        var action=  event.getParam("Action");
        var caseObj=event.getParam("Case");
        var InteractionId=event.getParam("interactionId");
        var filesList=event.getParam("Files");
        var plmUtility = component.find('plmUtility');
        var callback=event.getParam("callback");
        if(action == 'reply' || action == 'replyall' || action == 'forward'){
            plmUtility.reply(event.getParam("Case"), InteractionId, action,callback);
        }else if(action == 'update'){
            var callback=event.getParam("callback");
            plmUtility.update(event.getParam("Case"),callback);
        }/*Code Start For PUX-295 */else if(action == 'updateOwner'){
            var callback=event.getParam("callback");
            plmUtility.updateOwner(event.getParam("Case"),callback);
        }
        
    }
})