({
	  getRelatedHistory: function(component, event) {		
		//debugger;
		var objName =  component.get("v.objName") ; 
		console.log('objName :'+objName);
		var recordId = component.get("v.obj.Id") ;		
		var sectionsJson = component.get("v.layOutJson");		
		var relationField = component.get("v.relationField") ; 
		var whereClause = component.get("v.whereClause");
		var orderBy = component.get("v.orderBy");
		console.log('relationField :'+relationField);
		console.log('whereClause :'+whereClause);
		console.log('orderBy :'+orderBy);
		var columns;
		columns = sectionsJson["columns"];
		this.getRelatedListData(component, event, recordId,objName,columns,relationField,whereClause,orderBy);
	},

	getRelatedListData : function(component, event,recordId,objName,columns,relationField,whereClause,orderBy) {
		var curPageNo = component.get("v.pageNumber")
		var action1 = component.get("c.getRelatedListDataServer");
		//Set the Object parameters and Field Set name 
        action1.setParams({          
            objrecordId: recordId,        
			objectName : objName , 
			jsonObj: JSON.stringify(columns),
			pageNumber: component.get("v.pageNumber"),
            recordsToDisplay: component.get("v.recordsToDisplay"),
			relationField : relationField,
			whereClauseJson : whereClause,
			orderByJson : orderBy
        });
        action1.setCallback(this, function(response) {
            console.log('response');
            console.log('@@@@@' + response.getState());
            if (response.getState() === 'SUCCESS') {
				var resp = response.getReturnValue();
				if(resp.serverError != null && resp.serverError != ""){
					this.showToast("Related List Error!","error","sticky",resp.serverError);
				} else {
					console.log('res related list');
					console.log(resp.cases);
					component.set("v.objList", resp.cases.DataList);
					component.set("v.columns", resp.cases.Columns);
					component.set("v.maxPage", Math.ceil((resp.total) / resp.pageSize));            
					component.set("v.totalRecords", resp.total);
				}
            } else if (response.getState() === 'ERROR') {
                var errors = response.getError(response.getReturnValue().cases);
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Error message: " + 
                            errors[0].message);
                    }
                } else {
                    alert("Unknown error");
                }
            } else {
                alert('Something went wrong, Please check with your admin');
            }
        });
        $A.enqueueAction(action1);
		
	},
	showToast: function(title, type,mode,message) {
		var toastEvent = $A.get("e.force:showToast");
							toastEvent.setParams({
								"title": title,
								"type": type,
								"mode": mode,
								"message": message
							});
							toastEvent.fire();
	},

})