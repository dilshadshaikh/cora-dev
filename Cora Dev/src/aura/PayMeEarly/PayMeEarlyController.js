({
   
	sectionOne : function(component, event, helper) {
	   helper.helperFun(component,event,'articleOne');
	},
	
   sectionTwo : function(component, event, helper) {
	  helper.helperFun(component,event,'articleTwo');
	},
   
  
	init :function(component, event, helper) {
		var conEle = component.find("MLDiv");
        if ($A.util.hasClass(conEle, 'slds-is-open')){ 
			$A.util.removeClass(conEle, 'slds-is-open');
		} 
		component.set("v.isOpen", true);
		helper.getSobjectDetails(component, event);
		//helper.getobjNameSpace(component,event);
		return false;
	  },
	   
	dateUpdate : function(component, event, helper) {
	
		console.log("Your Onchange logic goes here");
		helper.getcomponentdata(component, event);
	 
	},

	saveInvoicefun:function(component, event, helper) {

		helper.updateinvdata(component, event);
	} ,

	navigatetoparent:function(component,event,helper){
	
	helper.navigateToParentComponent(component,helper);
	
	},
	

})