/*
  * Authors		 : Parth Lukhi
  * Date created : 30/08/2017 
  * Purpose      :InteractionListViewController JS used for getting Interactions based on Case
  * Dependencies : InteractionListViewHelper.js
  * -------------------------------------------------
  * Modifications:
                Date: 
                Purpose of modification:  
                Method/Code segment modified: 
*/
({
	/*
	 * Authors: Parth Lukhi
	 * Purpose:   Getting Interaction on CaseDetail View
	 * Dependencies:  InterActionListViewHelper.js
	 * 
	 */
	doInit: function (component, event, helper) {
		helper.getInterbyCase(component, event);
		var callBack = function () {
			helper.getInterbyCase(component, event);
		};
		component.set("v.callBack", callBack);

	},

	/*
		Authors: Atul Hinge
		Purpose: It will be called when HTTP Req is being processed at server side	
        Jiar : 
	*/
	waiting: function (component, event, helper) {
		if (document.getElementById("Accspinner") != null || document.getElementById("Accspinner") != undefined) {
			document.getElementById("Accspinner").style.display = "block";
		}
	},
	/*
		Authors: Atul Hinge
		Purpose: It will be called when HTTP Res is received at client side	
	*/
	doneWaiting: function (component, event, helper) {
		if (document.getElementById("Accspinner") != null || document.getElementById("Accspinner") != undefined) {
			document.getElementById("Accspinner").style.display = "none";
		}
	},
	/*
	 * Authors: Parth Lukhi
	 * Purpose:  Handling Case Action Event when Clicked on Reply ,Reply All
	 * Dependencies:  InterActionListViewHelper.js
	 * 
	 * Start : Handling Case Action Event when Clicked on Reply ,Reply All
	 */
	handleCaseActionEvent: function (component, event, helper) {
		var action = event.getParam("Action");
		var interactionId = event.getParam("interactionId");
		if (action == 'reply' || action == 'replyall' || action == 'forward') {
			var emailTitle = '';
			if (action == 'reply') {
				emailTitle = 'Reply';
			} else if (action == 'replyall') {
				emailTitle = 'Reply All';
			} else if (action == 'forward') {
				emailTitle = 'Forward';
			}
			component.set("v.emailTitle", emailTitle);
			component.set("v.interactionId", interactionId);
			component.set("v.showReplyScreen", !component.get("v.showReplyScreen"));
			component.set("v.showInteractionList", false);
			event.stopPropagation();
		}

	},
	/* End : Getting Interaction on CaseDetail View*/
	/*
	 * Authors: Parth Lukhi
	 * Purpose:   PUX-302  :Compose Mail event
	 * Dependencies: NA
	 * 
	 *  Start :  PUX-302  :Compose Mail event*/
	newMailEvent: function (component, event, helper) {
		component.set("v.emailTitle", 'Compose Mail');
		component.set("v.interactionId", null);
		component.set("v.showReplyScreen", "true");
		component.set("v.showInteractionList", "false");
	},
	/*
	 * Authors: Parth Lukhi
	 * Purpose:   Enabling interaction List
	 * Dependencies: NA
	 * 
	 *  Start :  Enabling interaction List*/
	enableInteractionList: function (component, event, helper) {
		if (!component.get("v.showReplyScreen")) {
			component.set("v.showInteractionList", "true");
		}
	},
	/* End : PUX-302  : Compose Mail event*/

	/*
	 * Authors: Tejas Patel
	 * Purpose:   PUX-340  :Add Notes event
	 * Dependencies: NA
	 * 
	 * Start :  PUX-340  :Add Notes event
	 */
	addNotesEvent: function (component, event, helper) {
		component.set("v.emailTitle", 'Add Note');
		component.set("v.showNotesScreen", "true");
		component.set("v.showInteractionList", "false");
       // alert('addNote');
	},

	 sectionOne : function(component, event, helper) {
	 console.log(event.currentTarget.dataset.value);
	 var headerSecId = event.currentTarget.dataset.value
         var acc = component.find(headerSecId);	
		 console.log(); 
			for(var cmp in acc) {
				$A.util.toggleClass(acc[cmp], 'slds-show');  
				$A.util.toggleClass(acc[cmp], 'slds-hide');  
	  		}
	},
	
})