/*
  Authors	   :   Rahul Pastagiya
  Date created :   10/08/2017
  Purpose      :   Client side controller to handle all actions of component
  Dependencies :   HighlightPanelHelper.js
  __________________________________________________________________________
  Modifications:
		    Date:  
		    Purpose of modification:  
		    Method/Code segment modified: 
 */
({
	/*
		Authors: Rahul Pastagiya
		Purpose: To get record from given object on basis of given field set and record id
	*/
	doInit: function(component, event, helper) {
		helper.getRecord(component, event);
	},
	/*
		Authors: Ashish Kumar
		Purpose: To fetch record when change event call
	*/
	doChange: function(component, event, helper) {
		if (component.get("v.callChange") == true) {
			helper.getRecord(component, event);
		}
		component.set("v.callChange", true);
	},
	/*
		Authors: Rahul Pastagiya
		Purpose: To expand and collapse the section
	*/
	expandToggle: function(component) {
		var conEle1 = component.find("highlightPanelContainer");
		$A.util.toggleClass(conEle1, 'showMe');
		$A.util.toggleClass(conEle1, 'hideMe');
		var conEle2 = component.find("highlightPanelOneRowContainer");
		$A.util.toggleClass(conEle2, 'showMe');
		$A.util.toggleClass(conEle2, 'hideMe');
		var expandIcon = component.find("expandIcon");
		if (expandIcon.get('v.iconName') == 'utility:chevrondown') {
			expandIcon.set('v.iconName', 'utility:chevronup');
		} else {
			expandIcon.set('v.iconName', 'utility:chevrondown');
		}
	}
})