({
	selectRecord : function(component, event, helper) {
		component.set("v.lock",false);
	//console.log('---------------- selectRecord ----------------');
		// get the selected record from list  
      var getSelectRecord = component.get("v.oRecord");
    // call the event   
      var compEvent = component.getEvent("oSelectedRecordEvent");
    // set the Selected sObject Record to the event attribute.  
         compEvent.setParams({"recordByEvent" : getSelectRecord });
		 compEvent.setParams({"eventBy" : component.get("v.eventBy") });
    // fire the event  
         compEvent.fire();
	}
})