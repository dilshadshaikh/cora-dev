/*
    Name           : CaseDetailViewOverrideHelper.js
    Author         : Ashish Kr.
    Description    : Helper JS used for getting Case Detail View 
    JIRA ID        : 
*/
({
    getCaseDetail: function(component, event) {
        var action = component.get("c.getCaseDetail");
        action.setParams({
            caseId: component.get("v.caseid")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.Case", response.getReturnValue());
                //alert(1);
                component.set("v.renderComponents", 'true');

            } else if (state === "INCOMPLETE") {
                //Logic
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getLoginUser: function(component, event) {
        var action = component.get("c.getUserInfo");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.UserName", response.getReturnValue().UserName);
                window.InstanceName = response.getReturnValue().InstanceName;
                component.set("v.Instance", InstanceName);
                component.set("v.renderComponents", true);
            } else if (state === "INCOMPLETE") {
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
    
})