({
	doInit: function(component, event, helper) {
		component.set('v.myVal', '<b>Hello! Testing</b>');
		helper.getinitialData(component, event);
    },
    
	handleCancel: function(component, event, helper) {
        helper.handleCancel(component, event);
    },

	toggle: function (component, event, helper) {
         var sel = component.find("priority");
         var opt =	sel.get("v.value");
		 component.set('v.selectedpri', opt);
		 component.set("v.case.Priority__c",component.get("v.selectedpri"));
		 //console.log('Selected Prio options -- '+opt);
    },

	SubmitRecordData: function(component,event,helper){
		helper.SaveRecordData(component, event,component.get("v.case"));
	},

	CloseRecord: function(component,event,helper){
		helper.CloseRecord(component, event,component.get("v.case"));
	},
})