({
	getinitialData: function(component, event) {
		var action1 = component.get("c.getinitialData");
		var action2 = component.get("c.getUserid");
		var action3 = component.get("c.getPicklistvalues");
		var action4 = component.get("c.getCase");
		var action5 = component.get("c.getattachementData");

		action1.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                //console.log('records--'+response.getReturnValue());
                component.set("v.interactionList", response.getReturnValue());
			    $A.enqueueAction(action2);

			} 
			else if (response.getState() === 'ERROR') {
                var errors = response.getError(response.getReturnValue());
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            } else {
                console.log('Something went wrong, Please check with your admin');
            }
        });

		action2.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                component.set("v.userid", response.getReturnValue());
				$A.enqueueAction(action3);
			}
        });

		action3.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
				//console.log('options -- '+response.getReturnValue());
                component.set("v.options", response.getReturnValue());
				$A.enqueueAction(action4);
			}
        });

		action4.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
				//console.log('Caseobj -- '+response.getReturnValue());
                component.set("v.case", response.getReturnValue());
				component.set("v.selectedpri",component.get("v.case.Priority__c"));
				console.log('v.selectedpri -- '+component.get("v.selectedpri"));
				component.set("v.fileloaded", true);
				$A.enqueueAction(action5);
			}
        });
		action5.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                console.log('attachment--'+response.getReturnValue());
                component.set("v.attachmentList", response.getReturnValue());
				console.log('attachment--'+component.get("v.attachmentList"));
			} 
		});

		$A.enqueueAction(action1);  
	},
	handleCancel: function(component, event) {
		component.set("v.isDisplay",false);
	},

	SaveRecordData : function(component, event,CaseObj) {
		
		component.set("v.caseObj.Current_State__c",'Closed'); 
		component.set("v.caseObj.Priority__c",component.get("v.selectedpri"));
		//component.set("v.caseObj.Status__c",'Closed'); 
		var emailbody=component.get("v.myVal");
		var action = component.get("c.saveCaseData");
			action.setParams({
				CaseObj: CaseObj,
				mailbody:emailbody
			});
			action.setCallback(this, function(response){
				var state = response.getState();
				if (state === "SUCCESS") {
					//console.log("Mail Message:"+response.getReturnValue());
					component.set("v.email", response.getReturnValue()[0]);
					console.log("Mail Message:"+component.get("v.email[0].Id"));
					component.set("v.fileUploadStatus",'create');
					var toastEvent = $A.get("e.force:showToast");
					toastEvent.setParams({
						"title": "Success!",
						"message": "The record has been updated successfully."
					});
					toastEvent.fire();
					$A.get('e.force:refreshView').fire();
                }
			});
			$A.enqueueAction(action);
	},

	CloseRecord: function(component, event,CaseObj) {
		
		component.set("v.caseObj.Current_State__c",'Closed'); 
		component.set("v.caseObj.Status__c",'Closed'); 
		component.set("v.caseObj.Priority__c",component.get("v.selectedpri"));
		var emailbody=component.get("v.myVal");
		var action = component.get("c.saveCaseData");
			action.setParams({
				CaseObj: CaseObj,
			});
			action.setCallback(this, function(response){
				var state = response.getState();
				if (state === "SUCCESS") {
					console.log("Message:"+response.getReturnValue());
					var toastEvent = $A.get("e.force:showToast");
					toastEvent.setParams({
						"title": "Success!",
						"message": "The record has been updated successfully."
					});
					toastEvent.fire();
					component.set("v.isDisplay",false);
                }
			});
			$A.enqueueAction(action);
	},

})