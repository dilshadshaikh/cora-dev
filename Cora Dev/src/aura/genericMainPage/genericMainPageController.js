({
	doInit: function(component, event, helper) {
	console.log("doInit");
		if(component.get("v.recordId")!=null && component.get("v.recordId")!=undefined)
		{
			var action = component.get("c.getCloneSobjectDetails");
			action.setParams({
				recordId: component.get("v.recordId"),
				objName:  component.get("v.sObjectName"),
			});
			action.setCallback(this, function(response) {
				var state = response.getState();
					console.log(state);
					console.info('response', response);
				if (state === "SUCCESS") {
				console.log(response.getReturnValue().attachment);

					component.set("v.cloneAttachment",response.getReturnValue().attachment);
					component.set("v.simpleNewContact",response.getReturnValue().Invoice);
					component.set("v.simpleNewContact.id","");
					component.set("v.invObject",component.get("v.simpleNewContact"));
				}
				else if (state === "ERROR") {
                alert('e');
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
			});
			$A.enqueueAction(action);
		}else{
		console.log("doInit else");
			component.find("invoiceRecordCreator").getNewRecord(
				component.get("v.sObjectName"),null,false,$A.getCallback(function() {
				var poFlipList =  component.get("v.poFlipList");
					if(poFlipList !=null && poFlipList !=undefined && poFlipList.length >0){
						var pol = '';
						for(var k in poFlipList){
							pol += poFlipList[k]["Id"] + ',';
						}
						if(pol!='')
						{
							pol = pol.substring(0,pol.length -1);							
							component.set("v.poFlip",pol);
						}
					}
					var invoiceObj =new Object();
					invoiceObj["sobjectType"] = component.get("v.sObjectName");
					for(var k in component.get("v.simpleNewContact")){
						invoiceObj[k] = component.get("v.simpleNewContact")[k];
					}
					component.set("v.invObject",invoiceObj);
			})
			);
		}
	},
})