({
   /* doInit: function(component,event,helper){
        helper.createLayout(component, event, helper);
    },
	*/
	doInit: function(component,event,helper){
        helper.getFieldSetMember(component, event, helper);
    },
    
    cancel:function(component,event){
        component.set("v.isFilterDisplay",false); 
    },
    hideModal: function(component,event){ 
        component.set("v.isFilterDisplay",false); 
    },
	doFilter: function(component,event,helper){         
		helper.filterCase(component, event,component.get("v.invObj"));									 
    }
})