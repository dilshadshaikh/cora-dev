({
  	getFieldSetMember: function(component, event) {
		var action1 = component.get("c.getFieldsetMember");
		
		//console.log(component.get("v.myVal"));
        action1.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                console.log('records--'+response.getReturnValue().record);
                //console.log('columns--'+response.getReturnValue().Columns);
                component.set("v.invObj", response.getReturnValue().record);
			    component.set("v.columns", response.getReturnValue().Columns);				
			} 
			else if (response.getState() === 'ERROR') {
                var errors = response.getError(response.getReturnValue());
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            } else {
                console.log('Something went wrong, Please check with your admin');
            }
        });
		$A.enqueueAction(action1);   
	},
	 filterCase: function(component, event, invObj) {
		var action = component.get("c.getCases");       

        action.setParams({
			whereClause:'All',
            //PUX-207
			searchConfigName :  'Invoice_Search',
            sortField: 'Name',
            sortDirection: 'Desc',
            pageNumber: 1,
            recordsToDisplay: 15,
            //PUX-207
            /* start : PUX-305 PUX-421 */
            invObj:invObj,
            overDueIn: ''

        });
        component.set("v.showSpinner", true);
        action.setCallback(this, function(response) {
            component.set("v.showSpinner", false);
            var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") {
                //PUX-678
                component.set("v.caseList", response.getReturnValue().cases);                
              
            } else if (state === "INCOMPLETE") {
                //Logic
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        //console.log("Error message: " +
                        //  errors[0].message);
                    }
                } else {
                    //console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
	},
	
})