/*
Authors: Niraj Prajapati
Date created: 01/11/2017
Purpose: This Helper will call from controller and insert Quality check record.
Dependencies: 
-------------------------------------------------
Modifications:
                Date: 
                Purpose of modification: 
                Method/Code segment modified: 
                
*/ 
({
    /*
		Authors: Niraj Prajapati
		Purpose: insertQC function will used to validate mandatory fields and create Quality Check record.
		Dependencies: QualityControlController.cmp
                
    */
    insertQC : function(component, event) {
         var np=component.toString().split('":"')[1].split('"}')[0]; 
      var  nameSpace=(np=='c')?'':np+'__';
        var eventNameSpace=(np=='c')?'c':np;
        component.set("v.qc."+nameSpace+"CaseManager__c", component.get("v.ct.Id"));
        component.find("qualitycheckRecordCreator").saveRecord(function(saveResult) {
            //alert(saveResult.state);
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                // record is saved successfully
                 //alert(saveResult.state);
                component.set('v.render',false);
				component.set("v.rCnt",component.get("v.rCnt")+1);
                var resultsToast = $A.get("e."+eventNameSpace+":ShowToast");
                resultsToast.setParams({
                    "title": "Saved",
                    "message": "The record was saved."
                });
                resultsToast.fire();
                //alert(component.get("v.rCnt"));
                component.set("v.rCnt",component.get("v.rCnt")+1);
                //component.find("inputField").getElement().value='';
                //component.set("v.qc",'');
                component.find("qualitycheckRecordCreator").getNewRecord(
                    nameSpace+"QualityCheck__c", // sObject type (objectApiName)
                    null,      // recordTypeId
                    false,     // skip cache?
                    $A.getCallback(function() {
                        var rec = component.get("v.newQualityCheck");
                        var error = component.get("v.newQualityCheckError");
                        component.set('v.render',true);
                        if(error || (rec === null)) {
                            console.log("Error initializing record template: " + error);
                            return;
                        }
                        console.log("Record template initialized: " + rec.sobjectType);
                    })
                );
                
            } else if (saveResult.state === "INCOMPLETE") {
                // handle the incomplete state
                console.log("User is offline, device doesn't support drafts.");
            } else if (saveResult.state === "ERROR") {
                // handle the error state
                console.log('Problem saving Quality Record, error: ' + JSON.stringify(saveResult.error));
            } else {
                console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
            }
        });
    },
    /*
		Authors: Niraj Prajapati
		Purpose: Spinner symbol will display till all fields value will fetch
		Dependencies: CaseDetailSection.cmp
                
    */
    toggleSpinner: function (cmp, event) {
        var spinner = cmp.find("mySpinner1");
        $A.util.toggleClass(spinner, "slds-hide");
    },
    /*
		Authors: Niraj Prajapati
		Purpose: getQC function will be used to get all the Quality Check record for that case
		Dependencies: CaseDetailSection.cmp
                
    */
    getQC : function (component,event){
        var action = component.get("c.getQualityControls");
        var key = [];
        var lblMap = [];
        var valueMap = [];
		var fs='';
		if(component.get("v.qcInfo.qcFormFieldsetName")!=''){
			fs=component.get("v.qcInfo.qcFormFieldsetName");
		}else{
			fs='QC';
		}
        //Code start for PUX-512
        //alert(component.get("v.qcInfo.qcFormFieldsetName"));
        action.setParams({ caseId :  component.get("v.invoiceId"),
                           fsName : fs});
        //Code End for PUX-512
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var qcList=response.getReturnValue().QualityControls;
                var qcFields=response.getReturnValue().FieldMap;
                //alert(qcList);
                component.set('v.qcRecordList',qcList);
                component.set('v.qcFieldsMap',qcFields);
                //for(var i=0;i<qcList.length;i++){
                //var qc=qcList[i];
                for (var property in qcFields) {
                    key.push(property);
                    console.log(qcFields[property]);
                    //lblMap.push
                    //alert(property+'###'+qcFields[property]+'###'+qc[property]);
                }
                // }
                component.set('v.keyList',key);
               // component.set("v.isDisabled",false);
            }else if (state === "INCOMPLETE") {
                //alert('e');
            }else if (state === "ERROR") {
                //alert('e');
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },

	getFields: function(component, event) {
		//alert(component.get("v.sObjectName"));
		//alert(component.get("v.invoiceId"));
		var action1 = component.get("c.getFieldsApex");
		action1.setParams({ caseId : component.get("v.invoiceId")
                           });
		console.log(component.get("v.myVal"));
        action1.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                //console.log('records--'+response.getReturnValue().record);
                //console.log('columns--'+response.getReturnValue().Columns);
                component.set("v.caseObj", response.getReturnValue().record);
                //alert(response.getReturnValue().Columns);
			    component.set("v.columns", response.getReturnValue().Columns);
				
			} 
			else if (response.getState() === 'ERROR') {
                var errors = response.getError(response.getReturnValue());
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            } else {
                console.log('Something went wrong, Please check with your admin');
            }
        });
        $A.enqueueAction(action1);    
    },
	SaveRecordData : function(component, event,CaseObj) {
			//console.log(component.get("v.myVal"));
			//console.log('After -- '+component.get("v.caseObj.Comment__c"));
			//component.set("v.caseObj.Comment__c",component.get("v.myVal"));
			//console.log('After -- '+component.get("v.caseObj.Comment__c"));
			var action = component.get("c.saveQualityData");
			action.setParams({
				CaseObj: CaseObj,
			});
			action.setCallback(this, function(response){
				var state = response.getState();
				//alert(state);
				if (state === "SUCCESS") {
					component.set("v.rCnt",component.get("v.rCnt")+1);
					console.log("Message:"+response.getReturnValue());
					var toastEvent = $A.get("e.force:showToast");
				toastEvent.setParams({
					"title": "Success!",
					"message": "The record has been inserted successfully."
				});
					toastEvent.fire();
                    //component.set("v.caseObj", response.getReturnValue());
				}
			});
			$A.enqueueAction(action);
	},
})