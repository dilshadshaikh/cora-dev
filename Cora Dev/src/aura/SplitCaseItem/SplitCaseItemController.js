({
    doInit: function(component, event, helper) {
		/*try{
			var caseManager = component.get('v.splitCase');
	  component.set("v.showSpinner",true);
			var action = component.get("c.getSplitCaseFields");
			action.setParams({"caseManager":caseManager ,"sessionId": component.get("v.sessionId")});
			action.setCallback(this, function(response) {
                component.set("v.showSpinner",false);
				var state = response.getState();
				if (state === "SUCCESS") {
					var layoutMeta=response.getReturnValue().sectionFields;
					component.set("v.layoutMeta",layoutMeta);
				}
				else{
            
					var errorMessage ='';
                    if (state === "INCOMPLETE"){
                        errorMessage = $A.get("$Label.c.Error_Message_Incomplete");
                    }else if(state === "ERROR"){
                        errorMessage = action.getError()[0].message;
                    }
                    //this.showToast(component,'New Case:', errorMessage, 'error');
				}
        }); 
        	$A.enqueueAction(action);
		}
		catch (e) {
            //this.showToast(component,'JS Error: ', e.message, 'error');
        }*/
    },
    toggleSubCase: function(component, event, helper) {
    	try{
    	var subCaseContainer = component.find("subCaseContainer");
        var toggleBtn = component.find("toggleBtn");
        if(toggleBtn.get('v.iconName') == 'utility:chevronright'){        	
        	toggleBtn.set('v.iconName', 'utility:switch');   
        }else{
            toggleBtn.set('v.iconName', 'utility:chevronright');  
        }
        $A.util.toggleClass(subCaseContainer, 'hide');
		}
		catch (e) {
            //this.showToast(component,'JS Error: ', e.message, 'error');
        }
    },
   
    selectAttachment: function(component, event, helper) {
    	helper.attachmentHelper(component, event, helper);
    },
    handleAttachments : function(component, event, helper) {
		try{
			if(component.get("v.isFirstCall")){
				component.set("v.isFirstCall",false);
	        var attachmentList = component.get('v.attachments');
	        //console.log(attachmentList);
	    	var attachmentLabel = component.find("checkboxMenuLabel");
	    	//console.log(attachmentList.length);
	        
				if (attachmentList.length > 0) {
					attachmentLabel.set("v.label", "All Attachments Selected");
					component.set("v.selectedAttachments", component.get('v.attachments'));
		        }
	        
	        
        }
		}
		catch (e) {
            //this.showToast(component,'JS Error: ', e.message, 'error');
        }
    }
    
})