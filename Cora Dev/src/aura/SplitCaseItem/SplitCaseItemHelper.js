({
	attachmentHelper: function(component, event, helper) {
		 try{
			 var allMenuItems = component.find("checkboxList");
	
    	var allAttachments = component.get('v.attachments');

			 var selectedAttachments = [];

			 $.each(allMenuItems,function(index,menuItem){
				if (menuItem.get("v.selected")){
					 $.each(allAttachments,function(innerIndex,attachment){
						  if(attachment.ContentDocument.Title == menuItem.get("v.label")){
							  selectedAttachments.push(attachment);
                    }
					 });
        		}
			 });
			 component.set("v.selectedAttachments",selectedAttachments);

			var resultCmp = component.find("checkboxMenuLabel");
			if (selectedAttachments.length == allAttachments.length) {
				resultCmp.set("v.label", 'All Attachments Selected');
			} else if (selectedAttachments.length == 0) {
				resultCmp.set("v.label", 'Select Attachments');
			} else if (selectedAttachments.length == 1) {
				resultCmp.set("v.label", selectedAttachments[0].ContentDocument.Title);
			} else if (selectedAttachments.length >= 2) {
				resultCmp.set("v.label", selectedAttachments.length + ' Attachments Selected');
        }
       
	                    }
		catch (e) {
            //this.showToast(component,'JS Error: ', e.message, 'error');
	                }
	            }
})