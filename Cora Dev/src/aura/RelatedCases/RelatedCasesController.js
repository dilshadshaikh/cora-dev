/*
  Authors	   :   Mohit Garg
  Date created :   13/02/2018
  Purpose      :   To show the related cases for any case.
  Dependencies :   CaseDetailView.cmp, RelatedCasesController.cls
  __________________________________________________________________________
  Modifications:
		    Date:  
		    Purpose of modification:  
		    Method/Code segment modified: 
 */
({
    doInit: function(component, event, helper) {
        console.log('doinit called');
        helper.getRelatedCases(component, event);
    },

 /*   waiting: function(component, event, helper) {
        document.getElementById("Accspinner").style.display = "block";
    },

    doneWaiting: function(component, event, helper) {
        if (document.getElementById("Accspinner") != null || document.getElementById("Accspinner") != undefined) {
            document.getElementById("Accspinner").style.display = "none";
        }
    },*/

    redirectRelatedCase: function(component, event, helper) {
       // var target = event.getSource().get("v.name");
       var attr = 'data-value' ;
       var target = event.target.attributes[attr].value;
        component.set("v.relatedCase", target);
        console.log(component.get("v.relatedCase"));
       helper.handleCaseAction(component, event);
    },
    

})