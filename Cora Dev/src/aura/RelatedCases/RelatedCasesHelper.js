({
		getRelatedCases: function(component, event) {
		//console.log('Helper called' +component.get("v.mainCaseId"));
		 component.set("v.showSpinner",true);
		var action = component.get("c.getRelatedCasesData");
		action.setParams({
			mainCaseId: component.get("v.mainCaseId")
		});
		action.setCallback(this, function(response) {
			console.log('Response : ' +response.getReturnValue());
			var state = response.getState();
             component.set("v.showSpinner",false);
			if (state === "SUCCESS") {
				var storeResponse = response.getReturnValue();
				//To set internal attributes value 
				component.set('v.relatedCasesList', storeResponse);
			console.log('storeResponse : ' +storeResponse);
				if (storeResponse.length != 0) {
					component.set('v.hasData', true);
				}
				component.set('v.resReceived', true);
			} else if (state === "INCOMPLETE") {
				//Logic
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + errors[0].message);
					}
				} else {
					console.log("Unknown error");
				}
			}
		});
		$A.enqueueAction(action);
	},
	   //handle Case Action on click of Case
    handleCaseAction: function(component, event) {
        	var compEvent = component.getEvent("caseAction");
        	console.log(component.get('v.relatedCase'));
        	compEvent.setParams({
    				"Action": "relatedCaseRedirect",
    				"Case": component.get("v.relatedCase")
			});
      		compEvent.fire();
    	},
})