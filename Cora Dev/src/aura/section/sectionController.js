({
	init : function(component, event, helper) {
	
		if(component.get("v.section")["criteria"] != null && component.get("v.section")["criteria"] != undefined && component.get("v.section")["criteria"] != "")
        {
            var criJson = component.get("v.section")["criteria"];
            if(criJson.criteriagroup != undefined && criJson.criteriagroup.rules != undefined && criJson.criteriagroup.rules.length > 0)
            {
                component.set("v.render",helper.validatecriteria(criJson, component.get("v.criteriaObj"), component.get("v.fieldsMetadata")));
            }
        }

		var section = component.get("v.section");
		for(var i = 0; i < section.columns.length; i++)
		{
			section.columns[i].className = "slds-show";
		}
		component.set("v.section",section);
		component.set("v.isOpen", true);
	},
    
    validate: function(component, event, helper) {
		var isValid = true;
		var allCmp = component.find('fieldform');	 
        console.log('allCmp :'+allCmp);
		for(var k in allCmp) {
			if(allCmp[k].validate !=null && allCmp[k].validate!=undefined){
				allCmp[k].validate(function(value){
					if (!value) 
					{
						isValid = false;
					}
				});
			}
		}
		var params = event.getParam('arguments');
        var callback;
        if (params) {
            callback = params.callback;
        }
        if (callback) callback(isValid);
	},
	sectionOne : function(component, event, helper) {
	   helper.helperFun(component,event,'articleOne');
	},
	
})