({
	getVendorInfo : function(component,event) {
	    console.log('Vendor Information Helper Called');
		var vendorId  = event.getParam("vendorId");
		console.log('vendorId :----------------'+vendorId);
		var objectName = event.getParam("objectName");
		console.log('objectName :----------------'+objectName);

		var action = component.get("c.fetchVendorDetail");
        action.setParams({
            'recId': vendorId,
            'objName' : objectName
          });

		action.setCallback(this, function(response) {
		
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
				console.log('storeResponse vendor info ------------');
				console.log(storeResponse);
                if (storeResponse.length == 0) {
                    component.set("v.Message", 'No Result Found...');
                } else {
                    component.set("v.Message", '');
                }
                component.set("v.mainObject",storeResponse[0].record);	
                component.set("v.listOfSearchRecords", storeResponse[0].Columns);
               
            } 
			else if (response.getState() === 'ERROR') 
			{
				var errors = response.getError(response.getReturnValue());
				if (errors) {
					if (errors[0] && errors[0].message) {
						   console.log(errors[0].message);

					}
				}
			}
        });
        $A.enqueueAction(action);

	}
})