({
   
    toggleSpinner: function (cmp) {
	//alert('toggleSpinner');
        var spinner = cmp.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
    },
    
  
    getAttachments :function(component,helper){
	console.log("getAttachments");
        var action = component.get("c.getAttachmentsESM");
     //   component.set("v.Case.Id","")
        action.setParams({ caseId :component.get("v.CaseId")});
		
        action.setCallback(this, function(response) {
            var state = response.getState();
			console.log("state "+state);
            if (state === "SUCCESS") {
			
			var att=response.getReturnValue().attachments;
			
			console.log("Attachments  size"+att.length);
			 component.set('v.Attachments',helper.addIcon(att));

			  var object;
			   for(var i=0;i<att.length;i++){
                if(att[i].ContentDocument.LatestPublishedVersion.Is_Primary_Doc__c){
                    object=att[i];

                    break;
                }
           	}
			if(object)
			{
			console.log("object "+object);
			console.log("object.ContentDocument.LatestPublishedVersionId "+object.ContentDocument.LatestPublishedVersionId);
			console.log("object.ContentDocument.Title "+object.ContentDocument.Title);
		
		
			var appEvent = $A.get("e.c:OpenSubTab");
            appEvent.setParams({
                "parentTabName": "attachment",
                "id" :object.ContentDocument.LatestPublishedVersionId,
                "label":object.ContentDocument.Title,
                "object":object,
            });
            appEvent.fire(); 
			
			}
			if(att.length!=0)
			{              
		    	component.set('v.ifAttachments',true);
		    }
			var NewAttachments = component.get("v.NewAttachments");
			  console.log("NewAttachments "+NewAttachments);
			if(component.get("v.NewAttachments")!=null && component.get("v.NewAttachments")!='')
			{
			 helper.getCloneAttachments(component,helper);
			}
            helper.toggleSpinner(component);
           }else if (state === "INCOMPLETE") {
                alert('e');
            }else if (state === "ERROR") {
                alert('e');
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    deleteAttachments :function(component,Id,helper){
	 console.log("deleteAttachments "+Id );
        helper.toggleSpinner(component);
        var action = component.get("c.deleteAttachmentsESM");
        action.setParams({ docId :Id,caseId:component.get("v.CaseId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
			console.log("Attachments "+response.getReturnValue().attachments);
			console.log("Attachments  size"+response.getReturnValue().attachments.length);
			//

			  var updatedAttachments =component.get("v.NewAttachments");
		     console.log("NewAttachments "+ updatedAttachments);

			    var oldAttachments=helper.addIcon(response.getReturnValue().attachments);
			
               for(var i=0;i<oldAttachments.length;i++){
                updatedAttachments.push(oldAttachments[i]);
               }
			    component.set('v.Attachments',updatedAttachments);
			   console.log("updatedAttachments.length "+updatedAttachments.length);
			
			 //
			 if(updatedAttachments.length!=0)
			   component.set('v.ifAttachments',true);
			else
               component.set('v.ifAttachments',false);

            helper.toggleSpinner(component);

            }else if (state === "INCOMPLETE") {
                alert('e');
            }else if (state === "ERROR") {
                alert('e');
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },
 
   deleteAttachmentsTemp :function(component,event,helper){
	 console.log("deleteAttachmentsTemp");
        helper.toggleSpinner(component);
       
        var NewAttachments =component.get("v.NewAttachments");
		 console.log("NewAttachments "+ NewAttachments);
		var  versionId=event.getSource().get("v.name");
		  console.log("versionId "+ versionId);
		   var Attachments =component.get("v.Attachments");
		  var UpdatedNewAttachments=[];
		  var UpdatedAttachments=[];
        for(var i=0;i<NewAttachments.length;i++){

		if(NewAttachments[i].Id!=versionId)
		    UpdatedNewAttachments.push(NewAttachments[i]);

		}

		 for(var i=0;i<Attachments.length;i++){

		if(Attachments[i].Id!=versionId)
		    UpdatedAttachments.push(Attachments[i]);

		}

		console.log("UpdatedNewAttachments "+ UpdatedNewAttachments);
		 console.log("UpdatedAttachments "+ UpdatedAttachments);
		component.set("v.NewAttachments",UpdatedNewAttachments);
		component.set("v.Attachments",UpdatedAttachments)

		if(UpdatedAttachments.length==0)
			{
               
		    	component.set('v.ifAttachments',false);
		    }
		  helper.toggleSpinner(component);

    },
 
    
    uploadJs: function(component, file, base64Data, callback) {
         sforce.connection.sessionId = component.get("v.SessionId");
        var attachment         = new sforce.SObject('Attachment');
        attachment.Name        = file.name;
        attachment.IsPrivate   = false;
        attachment.ContentType = file.type;
        attachment.Body        = base64Data;
        attachment.Description = 'xxx';
        attachment.ParentId    = component.get("v.CaseId");
        sforce.connection.create([attachment]);
    },
    addIcon :function(attachments) {

        for(var i=0;i<attachments.length;i++){
		console.log("is prime --"+attachments[i].ContentDocument.LatestPublishedVersion.Is_Primary_Doc__c);
		attachments[i]['Title']=attachments[i].ContentDocument.Title;
		attachments[i]['Is_Primary_Doc__c']=attachments[i].ContentDocument.LatestPublishedVersion.Is_Primary_Doc__c;
		attachments[i]['LastModifiedDate']=attachments[i].ContentDocument.LastModifiedDate;
		attachments[i]['FileExtension']=attachments[i].ContentDocument.FileExtension;
		attachments[i]['Owner']=attachments[i].ContentDocument.Owner.Name;
		attachments[i]['LatestPublishedVersionId']=attachments[i].ContentDocument.LatestPublishedVersionId;
		 attachments[i]['isUploaded']=true;
            var extension =attachments[i].ContentDocument.FileExtension;
			 attachments[i]['isPreviewAvailable']=false;
             attachments[i]['display']=true;
            var size=attachments[i].ContentDocument.ContentSize;
            if((size/1000).toFixed(2)<1){
                size=size+' BYTES';
            }else if(((size/1000)/1024).toFixed(2)<1){
                size=(size/1000).toFixed(2)+' KB';
            }else{
                size=((size/1000)/1024).toFixed(2)+' MB';
            }
            attachments[i].Size=size;
           
            if(extension=='pdf'){
                attachments[i]['icon']='doctype:pdf';
				attachments[i]['isPreviewAvailable']=true;
                
            }else if(extension=='jpg' || extension=='png' || extension=='gif' || extension=='jpg' || extension=='tiff'){
                attachments[i]['icon']='doctype:image';
                attachments[i]['isPreviewAvailable']=true;
            }else if(extension=='html' || extension=='htm'){
                attachments[i]['icon']='doctype:html';
                attachments[i]['isPreviewAvailable']=true;
            }else if(extension=='xlsx'){
                attachments[i]['icon']='doctype:excel';
                
            }else{
                attachments[i]['icon']='doctype:attachment';
            }
            
        }
        return attachments;
    },

	 addIconTemp :function(attachments,component) {
	 	console.log("UserName "+component.get("v.UserName"));
		var UserName=component.get("v.UserName");

        for(var i=0;i<attachments.length;i++){
		
		console.log("attachments[i].Title --"+attachments[i].Title);
		attachments[i]['Title']=attachments[i].Title;
		attachments[i]['Is_Primary_Doc__c']=attachments[i].Is_Primary_Doc__c;
		attachments[i]['LastModifiedDate']=attachments[i].LastModifiedDate;
		attachments[i]['FileExtension']=attachments[i].FileExtension;
		attachments[i]['Owner']=UserName;
		attachments[i]['LatestPublishedVersionId']=attachments[i].Id;
	
            var extension =attachments[i].FileExtension;
			 attachments[i]['isPreviewAvailable']=false;
			 attachments[i]['isUploaded']=false;
             attachments[i]['display']=true;
            var size=attachments[i].ContentSize;
            if((size/1000).toFixed(2)<1){
                size=size+' BYTES';
            }else if(((size/1000)/1024).toFixed(2)<1){
                size=(size/1000).toFixed(2)+' KB';
            }else{
                size=((size/1000)/1024).toFixed(2)+' MB';
            }
            attachments[i].Size=size;
           
            if(extension=='pdf'){
                attachments[i]['icon']='doctype:pdf';
				
                
            }else if(extension=='jpg' || extension=='png' || extension=='gif' || extension=='jpg' || extension=='tiff'){
                attachments[i]['icon']='doctype:image';
              
            }else if(extension=='html' || extension=='htm'){
                attachments[i]['icon']='doctype:html';
               
            }else if(extension=='xlsx'){
                attachments[i]['icon']='doctype:excel';
                
            }else{
                attachments[i]['icon']='doctype:attachment';
            }
            
        }
        return attachments;
    },



    filterAttachment :function(attachments,filterValue) {
        for(var i=0;i<attachments.length;i++){
            if(attachments[i].ContentDocument.LatestPublishedVersion.Flag__c==filterValue){
                attachments[i]['display']=true;
            }else{
                attachments[i]['display']=false;
            }
        }   
        return attachments;
    },

	 showToast: function(component,title,message,type){
        //   console.log(type);
        var  type = type;
        if(type == undefined){
            type = 'success';
        }
        var  css = 'toast-top-center';
        var  msg = message;
        toastr.options.positionClass = 'toast-top-full-width';
        toastr.options.extendedTimeOut = 0; //1000;
        toastr.options.timeOut = 3000;
        toastr.options.fadeOut = 250;
        toastr.options.fadeIn = 250;
        toastr.options.positionClass = css;
        toastr[type](msg);
    },
    

	  getCloneAttachments :function(component,helper){
	console.log("getCloneAttachments");
    var NewAttachments=component.get("v.NewAttachments");
	var NewAttachmentsUpdated=[];
	  var attachments=[];
		console.log("NewAttachments before"+NewAttachments);
		//	 var uploadedAttachments=component.get("v.fileToBeUploaded");
           var uploadedAttachments=helper.addIconTemp(NewAttachments,component);
			console.log("uploadedAttachments "+uploadedAttachments);
            for(var i=0;i<uploadedAttachments.length;i++){
                attachments.push(uploadedAttachments[i]);
				NewAttachmentsUpdated.push(uploadedAttachments[i])
            }
console.log("NewAttachmentsUpdated after"+NewAttachmentsUpdated);
 component.set('v.NewAttachments',NewAttachmentsUpdated);
           // attachments.push(uploadedAttachment);
           
			 component.set('v.Attachments',attachments);
			 console.log("attachments.length "+attachments.length);
			
			if(attachments.length!=0)
			  { component.set('v.ifAttachments',true);}
			else
              { component.set('v.ifAttachments',false);}
			   
		
    },

})