({


    doInit : function(component, event, helper) {
	  
		console.log("object ID in AttachmentListViewESM "+component.get("v.CaseId"));
		if(window.globalvalues && window.globalvalues.pagemode == 'VIEW')
		{
			
			component.set("v.isEditMode", false);
		}
	
		var sectionsJson = component.get("v.layOutJson");	
		

		var Component = sectionsJson["Component"];	
		var configuration=Component["configuration"];
		var allowedSize=configuration["allowedSize"];
		var allowedExt=configuration["allowedExt"];
		var allowAttachPrime=configuration["allowAttachPrime"];
		var allowedExtForPrime=configuration["allowedExtForPrime"];
	
		configuration=JSON.stringify(configuration);
		
	
		component.set("v.allowedSize",allowedSize);
		component.set("v.allowedExt",allowedExt);
		component.set("v.allowAttachPrime",allowAttachPrime);
		component.set("v.allowedExtForPrime",allowedExtForPrime);
		
    
	//	if(component.get("v.CaseId")!=null && component.get("v.CaseId")!='')
	//	{
		    helper.toggleSpinner(component);
             helper.getAttachments(component,helper);
	/*	}
		else
		{
			 alert('clone');
			 
			// helper.getCloneAttachments(component,helper);
		}*/
    },
   
    /* Authors: Atul Hinge || Purpose: toggleView is used to switch view from list view to thumbneil view and vice-versa  */
    toggleView : function(component, event, helper) {
        component.set("v.isListView",!component.get("v.isListView"));
    },
    /* Authors: Atul Hinge || Purpose: Display file uploder  */
    ShowFileUploder: function(component, event, helper) {
        var toggleText = component.find("fileUpload");
        $A.util.toggleClass(toggleText, "slds-hide");
		
    },
    /* Authors: Atul Hinge || Purpose: On file upload status change  */
/*    onFileUploadStatusChange:function(component, event, helper){
        var status=component.get("v.fileUploadStatus");
        if(status=="complete"){
            var attachments=[];
            var uploadedAttachments=helper.addIcon(component.get("v.fileToBeUploaded"));
            for(var i=0;i<uploadedAttachments.length;i++){
                attachments.push(uploadedAttachments[i]);
            }
           // attachments.push(uploadedAttachment);
            var oldAttachments=component.get('v.Attachments');
            for(var i=0;i<oldAttachments.length;i++){
                attachments.push(oldAttachments[i]);
            }
			 component.set('v.Attachments',attachments);
			 console.log("attachments.length "+attachments.length);
			
			if(attachments.length!=0)
			   component.set('v.ifAttachments',true);
			else
               component.set('v.ifAttachments',false);

            $A.util.toggleClass(component.find("fileUpload"), "slds-hide");
            var appEvent = $A.get("e.c:ShowToast");
            appEvent.setParams({ "title" : "Success !","message":" File is uploded successfully!" });
            appEvent.fire();
            component.set('v.fileUploadStatus','new');
            
        }
        
    },*/
	  onFileUploadStatusChange:function(component, event, helper){
	  //  component.set("v.displaySaveButton",true);
        var status=component.get("v.fileUploadStatus");
			console.log("status "+status);
        if(status=="complete"){
            var attachments=[];
		var NewAttachments=component.get("v.NewAttachments");
		console.log("NewAttachments before"+NewAttachments);
		//	 var uploadedAttachments=component.get("v.fileToBeUploaded");
           var uploadedAttachments=helper.addIconTemp(component.get("v.fileToBeUploaded"),component);
			console.log("uploadedAttachments "+uploadedAttachments);
            for(var i=0;i<uploadedAttachments.length;i++){
                attachments.push(uploadedAttachments[i]);
				NewAttachments.push(uploadedAttachments[i])
            }
console.log("NewAttachments after"+NewAttachments);
 component.set('v.NewAttachments',NewAttachments);
           // attachments.push(uploadedAttachment);
            var oldAttachments=component.get('v.Attachments');
			
               for(var i=0;i<oldAttachments.length;i++){
                attachments.push(oldAttachments[i]);
               }
			    component.set('v.Attachments',attachments);
			   console.log("attachments.length "+attachments.length);
			
			
			if(attachments.length!=0)
			   component.set('v.ifAttachments',true);
			else
               component.set('v.ifAttachments',false);

            $A.util.toggleClass(component.find("fileUpload"), "slds-hide");
          
            component.set('v.fileUploadStatus','new');
            
        }
        
    },

    changeFlag : function (component, event,helper) {
   //   component.set("v.displaySaveButton",true);
        var changeItem=event.currentTarget.dataset.value;
         var flag =event.currentTarget.dataset.flag;
		 console.log(flag);
		 var changed=false;
        var attachments =component.get("v.Attachments");
		var parameters={};
        for(var i=0;i<attachments.length;i++){
            if(attachments[i].LatestPublishedVersionId==changeItem){
                if(flag=='true'){
                    attachments[i].Is_Primary_Doc__c=false;
					parameters[attachments[i].LatestPublishedVersionId]=attachments[i].Is_Primary_Doc__c;
					
                }else{
				 var ext ='.'+attachments[i].FileExtension;

				 var extns = component.get("v.allowedExtForPrime").split(",")
           console.log("array "+extns);
            for(var j=0;j<extns.length;j++)
           {
               if(ext==extns[j])
              {
                flag=1;
                break;
               }
             else
             {
                flag=0;
             }
          }

				  console.log("flag "+flag);
				  if(flag==1)
				  {
                    attachments[i].Is_Primary_Doc__c=true;
					 parameters[attachments[i].LatestPublishedVersionId]=attachments[i].Is_Primary_Doc__c;
					changed=true;
					}
				  else
				  {
				var message = "That format is not allowed for primary attachment";
               //  statusMessage = response.getReturnValue()[0].status__c;
				helper.showToast(component,'error',message,"error");
				  }

                }
				break;
            }
        }

		 

		if(changed)
		{
		for(var i=0;i<attachments.length;i++){
		if(attachments[i].LatestPublishedVersionId!=changeItem)
		{
			   attachments[i].Is_Primary_Doc__c=false;
			    parameters[attachments[i].LatestPublishedVersionId]=attachments[i].Is_Primary_Doc__c;
				}
			   }
		 }

		 console.log("primaryDocJsonTemp :"+JSON.stringify(parameters));
		 component.set("v.primaryDocJsonTemp",JSON.stringify(parameters));

        component.set("v.Attachments",attachments);
    },

	saveData :function (component, event,helper) {
	 helper.toggleSpinner(component);
	 
	   
	 var NewAttachments =component.get("v.NewAttachments");
	  console.log("NewAttachments "+ NewAttachments);
	  var parentId=component.get("v.CaseId");

	var primaryDocJsonTemp=component.get("v.primaryDocJsonTemp");
	 console.log("primaryDocJsonTemp "+ primaryDocJsonTemp);

	  var action = component.get("c.insertData");
     
	  action.setParams({
            "primaryDocJson": primaryDocJsonTemp,
			"NewAttachments":NewAttachments,
			"parentId":parentId
        });
       
		
        action.setCallback(this, function(response) {
            var state = response.getState();
			console.log("state"+ state);
            if (state === "SUCCESS") {
		 

		  
		  var att=response.getReturnValue().attachments;
			
			console.log("Attachments  size"+att.length);
			 component.set('v.Attachments',helper.addIcon(att));

			
			if(att.length!=0)
			{
               
		    	component.set('v.ifAttachments',true);
		    }
			 component.set("v.displaySaveButton",false);
            helper.toggleSpinner(component);

         
           }else if (state === "INCOMPLETE") {
                alert('e');
            }else if (state === "ERROR") {
                alert('e');
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);




/*	  var parentId=component.get("v.CaseId");
	     var ContentDocumentLinks=[];
            for(var i=0;i<attachments.length;i++){
                var cntent  = new sforce.SObject('ContentDocumentLink');
                cntent.ContentDocumentId=attachments[i].ContentDocumentId;
     console.log("queryResult[i].ContentDocumentId "+ queryresult[i].ContentDocumentId);
                cntent.LinkedEntityId=parentId;
                cntent.ShareType= 'V';
                cntent.Visibility='AllUsers';
                ContentDocumentLinks.push(cntent);
            }
     console.log("ContentDocumentLinks  before"+ ContentDocumentLinks);
            ContentDocumentLinks = sforce.connection.create(ContentDocumentLinks);

            var contentDocumentLinksIds=[];
            for(var i=0;i<ContentDocumentLinks.length;i++){
                contentDocumentLinksIds.push(ContentDocumentLinks[i].id);
     console.log("ContentDocumentLinks[i].id "+ ContentDocumentLinks[i].id);
            }
           // alert("contentDocumentLinksIds "+contentDocumentLinksIds );
            ContentDocumentLinks =  sforce.connection.query('SELECT ContentDocument.LatestPublishedVersion.Is_Primary_Doc__c,ContentDocument.Owner.Name,ContentDocument.Id,ContentDocument.Title,ContentDocument.FileType,ContentDocument.LatestPublishedVersionId, LinkedEntityId, Id,ContentDocument.LastModifiedDate,ContentDocument.FileExtension,ContentDocument.ContentSize FROM ContentDocumentLink where Id in (\'' + contentDocumentLinksIds.join('\',\'') + '\')' ).getArray("records");
     console.log("ContentDocumentLinks after"+ ContentDocumentLinks);
      
    */      
           

	},
    
    /* Authors: Atul Hinge || Purpose: handleMenuSelect is called when action is selected from menu button on list view  */
    handleMenuSelect : function (component, event,helper) {
        var selectedMenuItemValue = event.getParam("value");
        var id=event.getSource().get("v.name");
        var att=component.get("v.Attachments");
        if(selectedMenuItemValue=="view"){
            var appEvent = $A.get("e.c:OpenSubTab");
            var object={};
            for(var i=0;i<att.length;i++){
                if(att[i].LatestPublishedVersionId==id){
                    object=att[i];
                    break;
                }
           	}
            
            appEvent.setParams({
                "parentTabName": "attachment",
                "id" :id,
                "label":event.getSource().get("v.title"),
                "object":object,
            });
            appEvent.fire();
        }else if(selectedMenuItemValue=="download"){
            var att =component.get("v.Attachments");
            for(var i=0;i<att.length;i++){
                if(att[i].LatestPublishedVersionId==id){
                    window.open('/sfc/servlet.shepherd/document/download/'+att[i].ContentDocument.Id,'_self');        
                    break;
                }
           }
        }else if(selectedMenuItemValue=="delete"){

            helper.deleteAttachments(component,id,helper);
         
        }
		else if(selectedMenuItemValue=="deleteTemp"){

            helper.deleteAttachmentsTemp(component,event,helper);
         
        }
    },
    /* Authors: Atul Hinge || Purpose: to open attachment  */
    openAttachViewer : function(component, event, helper) {
	 // helper.toggleSpinner(component);
	 
        var attachId= event.currentTarget.id;
	//	alert(attachId);
		
        var object={};
		var isUploaded;
        var att=component.get("v.Attachments");
            for(var i=0;i<att.length;i++){
                if(att[i].LatestPublishedVersionId==attachId){
                    object=att[i];
					isUploaded=att[i].isUploaded;
                    break;
                }
           	}
			console.log("attachId "+attachId);
           console.log("event.currentTarget.dataset.value "+event.currentTarget.dataset.value);

        if(event.currentTarget.dataset.visible=='true'){
            var appEvent = $A.get("e.c:OpenSubTab");
            appEvent.setParams({
                "parentTabName": "attachment",
                "id" :attachId,
                "label":event.currentTarget.dataset.value,
                "object":object,
            });
            appEvent.fire(); 
        }else if(isUploaded){
            window.open('/sfc/servlet.shepherd/document/download/'+event.currentTarget.dataset.id,'_self');        
        }
		//  helper.toggleSpinner(component);
    },
  
})