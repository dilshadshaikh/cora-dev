({
    doInit: function(component, event, helper) {
		helper.getFields(component, event);
    },
    
	handleCancel: function(component, event, helper) {
        helper.handleCancel(component, event);
    },
    SubmitRecordData: function(component,event,helper){
		helper.SaveRecordData(component, event,component.get("v.caseObj"));
	},
})