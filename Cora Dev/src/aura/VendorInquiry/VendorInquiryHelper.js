({
	getFields: function(component, event) {
		var action1 = component.get("c.getFields");
		var action2 = component.get("c.getUsernameEmail");
		if(component.get("v.invoiceid")!='na')
		{
			var action3 = component.get("c.getInvoicenumber");
		}
		

		//console.log(component.get("v.myVal"));
        action1.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                //console.log('records--'+response.getReturnValue().record);
                //console.log('columns--'+response.getReturnValue().Columns);
                component.set("v.caseObj", response.getReturnValue().record);
			    component.set("v.columns", response.getReturnValue().Columns);
				if(component.get("v.invoiceno")!='na')
				{
					component.set("v.caseObj.Invoice__r.Id",component.get("v.invoiceid"));
					component.set("v.caseObj.Invoice__c",component.get("v.invoiceid"));
					//component.set("v.caseObj.Invoice__r.Name",component.get("v.invoiceno"));
					//component.set("v.caseObj.Subject__c",component.get("v.invoiceno"));
				}
				 component.set("v.caseObj",component.get("v.caseObj"));
				$A.enqueueAction(action2);
			} 
			else if (response.getState() === 'ERROR') {
                var errors = response.getError(response.getReturnValue());
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            } else {
                console.log('Something went wrong, Please check with your admin');
            }
        });

		action2.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                //console.log('records--'+response.getReturnValue().UserName);
				//console.log('records--'+response.getReturnValue().Email);
				component.set("v.username", response.getReturnValue().UserName);
				component.set("v.usermailid", response.getReturnValue().Email);
				if(component.get("v.invoiceid")!='na')
				{
					$A.enqueueAction(action3);
				}
			}
        });

		if(component.get("v.invoiceid")!='na')
		{
			var invid=component.get("v.invoiceid");
			//console.log('invid--',invid);
			action3.setParams({
					invoiceid: invid,
			});

			action3.setCallback(this, function(response) {
				if (response.getState() === "SUCCESS") {
					component.set("v.invoiceno", response.getReturnValue());
					component.set("v.caseObj.Invoice__r.Name",component.get("v.invoiceno"));
				}
			});
		}
        $A.enqueueAction(action1);    
    },

	SaveRecordData : function(component, event,CaseObj) {
			//console.log(component.get("v.myVal"));
			//console.log(CaseObj);
			//console.log('Before -- '+component.get("v.caseObj.Invoice__c"));
			component.set("v.caseObj.Invoice__c",component.get("v.caseObj.Invoice__r.Id"));
			component.set("v.caseObj.Input_Channel__c",'vendor'); 
			//console.log('After -- '+component.get("v.caseObj.Invoice__c"));
			var action = component.get("c.saveCaseData");
			action.setParams({
				CaseObj: CaseObj,
			});
			action.setCallback(this, function(response){
				var state = response.getState();
				if (state === "SUCCESS") {
					console.log("Message:"+response.getReturnValue());
					var toastEvent = $A.get("e.force:showToast");
					toastEvent.setParams({
						"title": "Success!",
						"message": "The record has been updated successfully."
					});
					toastEvent.fire();
					this.handleCancel(component, event);
					//$A.get('e.force:refreshView').fire();
                }
			});
			$A.enqueueAction(action);
	},

	handleCancel: function(component, event) {
//		$A.get('e.force:refreshView').fire();
		component.set("v.isDisplay",false);
	}
})