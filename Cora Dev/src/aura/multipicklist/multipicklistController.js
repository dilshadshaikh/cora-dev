({
    init: function(component, event, helper) {
        if (window.globalvalues && window.globalvalues.pagemode == 'VIEW') {
            component.set("v.readonly", true);
			component.set("v.required", false);
        }
        if (component.get("v.fieldCriteria")["criteria"] != null && component.get("v.fieldCriteria")["criteria"] != undefined && component.get("v.fieldCriteria")["criteria"] != "") {
            var criJson = component.get("v.fieldCriteria")["criteria"];
			console.log('criJson :');
			console.log(criJson);
			console.log('criteriaObj :');
			console.log(component.get("v.criteriaObj"));
			console.log('objFieldMetadata :');
			console.log(component.get("v.objFieldMetadata"));
            if (criJson.criteriagroup != undefined && criJson.criteriagroup.rules != undefined && criJson.criteriagroup.rules.length > 0) {
                component.set("v.render", _criteriahelper.validate(criJson, component.get("v.criteriaObj"), component.get("v.objFieldMetadata")));
                if (component.get("v.render") == true) {
                    var field = component.get("v.fieldCriteria");
                    field.className = "slds-show";
                    component.set("v.fieldCriteria", field);
                }
                if (component.get("v.render") == false) {
                    var field = component.get("v.fieldCriteria");
                    field.className = "slds-hide";
                    component.set("v.fieldCriteria", field);
                }
            }
        }
        if (window.globalvalues && window.globalvalues.pagemode != 'VIEW') {
			if (component.get("v.fieldCriteria")["requiredCriteria"] != null && component.get("v.fieldCriteria")["requiredCriteria"] != undefined && component.get("v.fieldCriteria")["requiredCriteria"] != "") {
				var criJson = component.get("v.fieldCriteria")["requiredCriteria"];
				if (criJson.criteriagroup != undefined && criJson.criteriagroup.rules != undefined && criJson.criteriagroup.rules.length > 0) {
					component.set("v.required", _criteriahelper.validate(criJson, component.get("v.criteriaObj"), component.get("v.objFieldMetadata")));
				}
			}
        }
        var retvalue = [];
        var obj1 = new Object();
        obj1.value = '';
        obj1.label = '---select---';
        retvalue[0] = obj1;
        for (var key in component.get("v.fieldMetadata.picklistValues")) {
            var obj = new Object();
            obj.value = component.get("v.fieldMetadata.picklistValues")[key]['value'];
            obj.label = component.get("v.fieldMetadata.picklistValues")[key]['label'];
            retvalue[Number(Number(key) + 1)] = obj;
        }
        //console.log(retvalue);
        console.log(component.get("v.fieldMetadata"));
        var opts = retvalue;
		console.log('opts :');
		console.log(opts);
		console.log('Parent Obj'+ component.get("v.fieldMetadata.controllerName"));
        component.set("v.options", opts);
        component.set("v.parentObject", component.get("v.fieldMetadata.controllerName"));
        component.set("v.status", component.getReference("v.obj." + component.get("v.fieldMetadata.name")));
    },
  

    handleRefresh: function(component, event, helper) {
        if (component.get("v.fieldCriteria")["criteria"] != null && component.get("v.fieldCriteria")["criteria"] != undefined && component.get("v.fieldCriteria")["criteria"] != "") {
            var criJson = component.get("v.fieldCriteria")["criteria"];
            if (criJson.criteriagroup != undefined && criJson.criteriagroup.rules != undefined && criJson.criteriagroup.rules.length > 0) {
                component.set("v.render", _criteriahelper.validate(criJson, component.get("v.criteriaObj"), component.get("v.objFieldMetadata")));
                if (component.get("v.render") == true) {
                    var field = component.get("v.fieldCriteria");
                    field.className = "slds-show";
                    component.set("v.fieldCriteria", field);
                }
                if (component.get("v.render") == false) {
                    var field = component.get("v.fieldCriteria");
                    field.className = "slds-hide";
                    component.set("v.fieldCriteria", field);
                }
            }
        }
        if (window.globalvalues && window.globalvalues.pagemode != 'VIEW') {
			if (component.get("v.fieldCriteria")["requiredCriteria"] != null && component.get("v.fieldCriteria")["requiredCriteria"] != undefined && component.get("v.fieldCriteria")["requiredCriteria"] != "") {
				var criJson = component.get("v.fieldCriteria")["requiredCriteria"];
				if (criJson.criteriagroup != undefined && criJson.criteriagroup.rules != undefined && criJson.criteriagroup.rules.length > 0) {
					component.set("v.required", _criteriahelper.validate(criJson, component.get("v.criteriaObj"), component.get("v.objFieldMetadata")));
				}
			}
        }
    },
    validate: function(component, event) {
        var params = event.getParam('arguments');
			console.log('params --------');
		console.log(params);
        var callback;
        if (params) {
            callback = params.callback;
        }

       if(component.get("v.render") && component.get("v.required"))
		{
			var options = component.get('v.options');
			console.log(options);
			var selectVal =  component.find("formValidate").get("v.value");		
			if( typeof selectVal != 'undefined' && typeof selectVal != '' ){	
				console.log('selectVal :'+selectVal);
				var selectValLength = selectVal.split(";").length;
				console.log('selectVal :'+selectValLength);
				if(options.length > 1 && selectValLength >= 1 ){
					if (callback)	callback(true) ; 
				}
				else{
					if (callback) callback(false) ; 
				}	
			}
			else
			{
				callback(false) ; 
				alert('Please select multipicklist value');
			}		
		}else{
			if (callback) callback(true);
		}
    }

})