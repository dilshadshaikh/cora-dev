/*
Authors: Niraj Prajapati
Date created: 01/11/2017
Purpose: This Helper will call from controller and insert Quality check record.
Dependencies: 
-------------------------------------------------
Modifications:
                Date: 
                Purpose of modification: 
                Method/Code segment modified: 
                
*/ 
({
    /*
		Authors: Niraj Prajapati
		Purpose: insertQC function will used to validate mandatory fields and create Quality Check record.
		Dependencies: QualityControlController.cmp
                
    */
    insertQC : function(component, event ,helper) {
         var np=component.toString().split('":"')[1].split('"}')[0]; 
      var  nameSpace=(np=='c')?'':np+'__';
		 var errorMessageVar='';
		 var addQualityTitleVar='Quality Control';
        var eventNameSpace=(np=='c')?'c':np;
        component.set("v.qc."+nameSpace+"CaseManager__c", component.get("v.ct.Id"));
        component.find("qualitycheckRecordCreator").saveRecord(function(saveResult) {
            //alert(saveResult.state);
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                // record is saved successfully
                 //alert(saveResult.state);
                component.set('v.render',false);
                var resultsToast = $A.get("e."+eventNameSpace+":ShowToast");
                resultsToast.setParams({
                    "title": "Saved",
                    "message": "Record saved successfully!"
                });
                resultsToast.fire();
                //alert(component.get("v.rCnt"));
                component.set("v.rCnt",component.get("v.rCnt")+1);
                //component.find("inputField").getElement().value='';
                //component.set("v.qc",'');
                component.find("qualitycheckRecordCreator").getNewRecord(
                    nameSpace+"QualityCheck__c", // sObject type (objectApiName)
                    null,      // recordTypeId
                    false,     // skip cache?
                    $A.getCallback(function() {
                        var rec = component.get("v.newQualityCheck");
                        var error = component.get("v.newQualityCheckError");
                        component.set('v.render',true);
                        if(error || (rec === null)) {
                            console.log("Error initializing record template: " + error);
                            return;
                        }
                        console.log("Record template initialized: " + rec.sobjectType);
                    })
                );
                
            } else{
				 if (saveResult.state === "INCOMPLETE") {
                // handle the incomplete state
					errorMessageVar="User is offline, device doesn't support drafts.";
            } else if (saveResult.state === "ERROR") {
                // handle the error state
					errorMessageVar='Problem saving Quality Record';
            } else {
					errorMessageVar='Unknown problem';
				}
				helper.showToast(component, np, addQualityTitleVar, errorMessageVar, 'error');
            }
        });
    },
    /*
		Authors: Niraj Prajapati
		Purpose: Spinner symbol will display till all fields value will fetch
		Dependencies: CaseDetailSection.cmp
                
    */
    toggleSpinner: function (cmp, event) {
        var spinner = cmp.find("mySpinner1");
        $A.util.toggleClass(spinner, "slds-hide");
    },
    /*
		Authors: Niraj Prajapati
		Purpose: getQC function will be used to get all the Quality Check record for that case
		Dependencies: CaseDetailSection.cmp
                
    */
    getQC : function (component,event,helper){
		var np=component.toString().split('":"')[1].split('"}')[0]; 
		 var errorMessageVar='';
		 var addQualityTitleVar='Quality Control';
        var action = component.get("c.getQualityControls");
        var key = [];
        var lblMap = [];
        var valueMap = [];
        //Code start for PUX-512
        //alert(component.get("v.qcInfo.qcFormFieldsetName"));
        action.setParams({ caseId : component.get("v.ct.Id"),
                           fsName : component.get("v.qcInfo.qcFormFieldsetName")});
        //Code End for PUX-512
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var qcList=response.getReturnValue().QualityControls;
                var qcFields=response.getReturnValue().FieldMap;
                //alert(qcList);
                component.set('v.qcRecordList',qcList);
                component.set('v.qcFieldsMap',qcFields);
                //for(var i=0;i<qcList.length;i++){
                //var qc=qcList[i];
                for (var property in qcFields) {
                    key.push(property);
                    //console.log(qcFields[property]);
                    //lblMap.push
                    //alert(property+'###'+qcFields[property]+'###'+qc[property]);
                }
                // }
                component.set('v.keyList',key);
                component.set("v.isDisabled",false);
            }else{
				 if (state === "INCOMPLETE") {
					errorMessageVar = (np == 'c') ? $A.get("$Label.c.Error_Message_Incomplete") : $A.get("$Label.CoraPLM.Error_Message_Incomplete");
				
            }else if (state === "ERROR") {
                alert('e');
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
							errorMessageVar=errors[0].message;
                    }
                } else {
						errorMessageVar="Unknown error";
                }
				}
				helper.showToast(component, np, addQualityTitleVar, errorMessageVar, 'error');
            }
        });
        
        $A.enqueueAction(action);
    },
	showToast: function(component, nameSpacePerfix, title, message, msgtype){
        var tosterError = $A.get("e."+nameSpacePerfix+":ShowToast");
        tosterError.setParams({"title": title, "message": message, "type": msgtype});
        tosterError.fire();
    }

})