({
    doInit : function(component, event, helper) {
		try{
        component.set("v.showSpinner",true);
			component.set('v.splitComments','');
			component.set('v.noOfSplits',1);

           var emailMsgObj = component.get('v.ct.Emails');
            if (emailMsgObj == undefined) {
                component.set('v.EmailTxtBody', 'No interaction yet!');
            } else if (emailMsgObj[0].TextBody != undefined) {
                component.set('v.EmailTxtBody', emailMsgObj[0].TextBody.substring(0, 100));
            }
			helper.getSplitCaseField(component,event);
            helper.noOfSplitsHelper(component,event);
            helper.getAttachmentHelper(component,event,helper);
            //helper method call ends
            
			component.set("v.showSpinner",false);
		}
		catch (e) {
            helper.showToast(component,namespace,'JS Error: ', e.message, 'error');
        }
        
    },
    
   
    
    noOfSplits : function(component, event, helper){  
 
        helper.noOfSplitsHelper(component,event);
    },
    
    ToggleCollapse : function(component, event, helper) { 
        try{
        var existingText = component.get("v.collapseText"); 
        var container = component.find("containerCollapsable") ;
        
        if(existingText === "View more"){
            component.set("v.collapseText","View less");
            $A.util.toggleClass(container, 'hide');  
        }else{
            component.set("v.collapseText","View more");
            $A.util.toggleClass(container, 'hide');  
        }  
		}
		catch (e) {
            helper.showToast(component,namespace,'JS Error: ', e.message, 'error');
        } 
    },
   //Function called on Cancel button
    closeSplitWindow : function(component, event, helper){
		try{
        var compEventRedirect = component.getEvent("caseAction");
				compEventRedirect.setParams({
					"Action": "redirect",
					"Case": component.get("v.ct")
				});
				compEventRedirect.fire();
			 var setBreadcrumb = component.getEvent("caseAction");
            setBreadcrumb.setParams({
                "Action" : "setBreadCrumb",
                "TabName" : "My Cases" 
            });
            setBreadcrumb.fire();
		}
		catch (e) {
            helper.showToast(component,namespace,'JS Error: ', e.message, 'error');
        }
    },
    splitCases : function (component,event,helper){
		var namespace  = component.get("v.namespace");
		try{
			//
			var casesStr=JSON.stringify(component.get('v.SplitCaseList'));
   
			var splitComments = component.get('v.splitComments');
    	
			var commentComponent=component.find("splitComments");
        
			if(splitComments==null || splitComments==''){
				commentComponent.showHelpMessageIfInvalid();
				return false;
        }
			component.set("v.showSpinner",true);
            var mainCaseTracker = component.get('v.ct');
			var action = component.get("c.performSplitCase");
            action.setParams({
                mainCase : mainCaseTracker,
				splitCasesStr : casesStr,
                comments : splitComments
            });
			action.setCallback(this, function(response) {
				component.set("v.showSpinner",false);
                var state = response.getState();
				if (state === "SUCCESS") {
                 
                
                    var compEventRedirect = component.getEvent("caseAction");
                    compEventRedirect.setParams({
                        "Action": "redirect",
                        "Case": component.get("v.ct")
                    });
                    compEventRedirect.fire();
                    var setBreadcrumb = component.getEvent("caseAction");
                     setBreadcrumb.setParams({
                    "Action" : "setBreadCrumb",
                    "TabName" : "My Cases" 
                    });
                    setBreadcrumb.fire();

					var returnValue=response.getReturnValue();
                    var toastMsg = [];
                    for (var i=0;i<returnValue.length;i++){
                       // toastMsg.push('Case-'+returnValue[i].Name);
					   toastMsg.push(returnValue[i].Name);                    } 
					console.log(toastMsg);
                    var toastMsgString = toastMsg.join(',')+' created successfully.!';
                    var appEvent = $A.get("e.c:ShowToast");
                    	appEvent.setParams({
                    		"title": "Success ",
                    		"message": toastMsgString
                    		});
                        appEvent.fire();
				}
				else{
					var errorMessage ='';
                    if (state === "INCOMPLETE"){
                        errorMessage = $A.get("$Label.c.Error_Message_Incomplete");
                }else if (state === "ERROR"){
                        errorMessage = action.getError()[0].message;
                        }
                    helper.showToast(component,namespace,'Split Case:', errorMessage, 'error');
                }
            });
			$A.enqueueAction(action);
		}
		catch (e) {
            helper.showToast(component,namespace,'JS Error: ', e.message, 'error');
        }  
    },
    //handle Case Action on click of Case Number
    handleCaseAction: function(component, event, helper) {
    	var caseId = component.get("v.ct.Id");
		var url= 'https://'+window.location.hostname+"/one/one.app#/sObject/"+caseId+"/view";
		window.open(url,"_blank")
	},
})