/**
  * 
  * Authors		: Parth Lukhi
  * Date created : 17/08/2017 
  * Purpose      : This InterActionListItemHelper is used for getting Interactions (EmailMessages)
  *  				  records,attachments objects for particular Case Id or Interaction Item
  * Dependencies :  InteractionListItemHelper.js , InteractionListItem.cmp
  * -------------------------------------------------
  *Modifications:
                Date:  09/11/2017 
                Purpose of modification:  PUX-235 : Getting Contact Name on interction 
                Method/Code segment modified: getContactName
 * -------------------------------------------------
 * Modifications:
                Date:  18/12/2017 
                Purpose of modification:  PUX-505 : Maintain attachment @ interaction level  
*/
({
    /*
	 * Authors: Parth Lukhi
	 * Purpose:   Formatiing date based on Interaction Time
	 * Dependencies:  NA
	 * 
     * Start : Formatiing date based on Interaction Time */
	formatAMPM: function(date) {
		var hours = date.getHours();
		var minutes = date.getMinutes();
		var ampm = hours >= 12 ? ' PM' : ' AM';
		hours = hours % 12;
		hours = hours ? hours : 12; // the hour '0' should be '12'
		minutes = minutes < 10 ? '0' + minutes : minutes;
		var strTime = hours + ':' + minutes + ' ' + ampm;
		return strTime;
	},
	foramteWeekDate: function(date) {
		var hours = date.getHours();
		var minutes = date.getMinutes();
		var ampm = hours >= 12 ? ' PM' : ' AM';
		hours = hours % 12;
		hours = hours ? hours : 12; // the hour '0' should be '12'
		minutes = minutes < 10 ? '0' + minutes : minutes;
		var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
		var weekDy = days[date.getDay()];
		var strTime = weekDy + ' ' + hours + ':' + minutes + ' ' + ampm;
		return strTime;
	},
	formatDate: function(date) {
		var hours = date.getHours();
		var minutes = date.getMinutes();
		var ampm = hours >= 12 ? 'PM' : 'AM';
		hours = hours % 12;
		hours = hours ? hours : 12; // the hour '0' should be '12'
		minutes = minutes < 10 ? '0' + minutes : minutes;
		var month = (1 + date.getMonth()).toString();
		month = month.length > 1 ? month : '0' + month;
		var day = date.getDate().toString();
		day = day.length > 1 ? day : '0' + day;
		var year = date.getFullYear();
		return month + "/" + day + "/" + year + ' ' + hours + ':' + minutes + ' ' + ampm;
	},
	/* End : Formatiing date based on Interaction Time */
    
    /*
	 * Authors: Parth Lukhi
	 * Purpose:    Getting Attachment List for Interaction
	 * Dependencies:  InteractionListItemController.apxc
	 * 
     * Start : Getting Attachment List for Interaction */
	getAttachList: function(component, event) {
        component.set("v.attachList",'');
		var action = component.get("c.getAttachments");
		action.setParams({
		// PUX-505 start
			id: component.get("v.Interaction.Id")
		// PUX-505 end
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				//alert(response.getReturnValue());
				//
				//component.set("v.attachList", response.getReturnValue());
				component.set("v.attachList", this.addIcon(response.getReturnValue()));
			} else if (state === "INCOMPLETE") {
				//Logic
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + errors[0].message);
					}
				} else {
					console.log("Unknown error");
				}
			}
		});
		$A.enqueueAction(action);
	},
	/* End : Getting Attachment List for Interaction */
    
    /*
	 * Authors: Parth Lukhi
	 * Purpose:   PUX-235 Getting ContactName List for Interaction
	 * Dependencies:  InteractionListItemController.apxc
	 * 
     *  Start : PUX-235 Getting ContactName List for Interaction */
	getContactName: function(component, event) {
		var action = component.get("c.getContactsByInteraction");
        var address=component.get("v.Interaction.ToAddress")+','+component.get("v.Interaction.CcAddress")+','+component.get("v.Interaction.BccAddress")+','+component.get("v.Interaction.EscAddress__c");
        address=address.replace(/;/g , ",");
        var ToAddress ='';
        var CcAddress ='';
        var BccAddress = '';
        var EscAddress ='';
        if(component.get("v.Interaction.ToAddress")!='' && component.get("v.Interaction.ToAddress")!= null)
    		ToAddress = component.get("v.Interaction.ToAddress").replace(/;/g , "").split(",");
        if(component.get("v.Interaction.CcAddress")!='' && component.get("v.Interaction.CcAddress")!= null)
        	CcAddress = component.get("v.Interaction.CcAddress").replace(/;/g , "").split(",");
        if(component.get("v.Interaction.BccAddress")!='' && component.get("v.Interaction.BccAddress")!= null)
        	BccAddress = component.get("v.Interaction.BccAddress").replace(/;/g , "").split(",");
        if(component.get("v.Interaction.EscAddress__c")!='' && component.get("v.Interaction.EscAddress__c")!= null)
        	EscAddress = component.get("v.Interaction.EscAddress__c").replace(/;/g , "").split(",");
        //console.log(ToAddress);
		var ToAddressList=[];
        var CcAddressList=[];
        var BccAddressList=[];
        var EscAddressList=[];
 		action.setParams({ 
			interactionId: component.get("v.Interaction.Id"),
            emailAdd :address
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				//debugger;
				component.set("v.contactList", response.getReturnValue());
			
                for(var contact in response.getReturnValue()){
                    console.log(response.getReturnValue()[contact].Email);
                    if(ToAddress.includes(response.getReturnValue()[contact].Email)){
                        ToAddressList.push(response.getReturnValue()[contact]);
                    }
                    if(CcAddress.includes(response.getReturnValue()[contact].Email)){
                        CcAddressList.push(response.getReturnValue()[contact]);
                    }
                    if(BccAddress.includes(response.getReturnValue()[contact].Email)){
                        BccAddressList.push(response.getReturnValue()[contact]);
                    }
                    if(EscAddress.includes(response.getReturnValue()[contact].Email)){
                        EscAddressList.push(response.getReturnValue()[contact]);
                    }
                }
                if(ToAddressList.length>0)
                    component.set("v.toContactList",ToAddressList);
                if(CcAddressList.length>0)
                    component.set("v.ccContactList",CcAddressList);
                if(BccAddressList.length>0)
                    component.set("v.bccContactList",BccAddressList);
                if(EscAddressList.length>0)
                    component.set("v.escContactList",EscAddressList);
                
			} else if (state === "INCOMPLETE") {
				//Logic
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + errors[0].message);
					}
				} else {
					console.log("Unknown error");
				}
			}
		});
		$A.enqueueAction(action);
	},
	/* End :  PUX-235  Getting ContactName List for Interaction */
    
    /*
	 * Authors: Parth Lukhi
	 * Purpose:   PUX-211 marking Mail as Read 
	 * Dependencies:  InteractionListItemController.apxc
	 * 
     *  Start : marking Mail as Read */
	makeReadMail: function(component, event) {
        
		var action = component.get("c.makeReadMail");
       	action.setParams({ 
			interactionId: component.get("v.Interaction.Id")           
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
                var interObj=component.get("v.Interaction");
                interObj.Read__c=true;
                component.set("v.Interaction",interObj);
               
            } else if (state === "INCOMPLETE") {
				//Logic
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + errors[0].message);
					}
				} else {
					console.log("Unknown error");
				}
			}
		});
		$A.enqueueAction(action);
	},
	/* End : marking Mail as Read  */
    /* End : PUX-211 */
    
    addIcon :function(attachments) {
        for(var i=0;i<attachments.length;i++){
            var extension =attachments[i].ContentDocument.FileExtension;
            attachments[i]['isPreviewAvailable']=false;
            var size=attachments[i].ContentDocument.ContentSize;
            if((size/1000).toFixed(2)<1){
                size=size+' BYTES';
            }else if(((size/1000)/1024).toFixed(2)<1){
                size=(size/1000).toFixed(2)+' KB';
            }else{
                size=((size/1000)/1024).toFixed(2)+' MB';
            }
            	
            attachments[i].Size=size;
            if(extension=='pdf'){
                attachments[i]['icon']='doctype:pdf';
                attachments[i]['isPreviewAvailable']=true;
            }else if(extension=='xlsx'){
                attachments[i]['icon']='doctype:excel';
                
            }else if(extension=='html'){
                attachments[i]['icon']='doctype:html';
                attachments[i]['isPreviewAvailable']=true;
            }else if(extension=='docx' || extension=='doc'){
                attachments[i]['icon']='doctype:word';
                
            }else if(extension=='txt'){
                attachments[i]['icon']='doctype:txt';
                
            }else if(extension=='csv'){
                attachments[i]['icon']='doctype:csv';
                
            }else if(extension=='jpg' || extension=='png' || extension=='gif' || extension=='jpg'){
                attachments[i]['icon']='doctype:image';
                attachments[i]['isPreviewAvailable']=true;
            }else{
                //alert(extension);
                attachments[i]['icon']='doctype:attachment';
            }
            
        }
        return attachments;
    },
})