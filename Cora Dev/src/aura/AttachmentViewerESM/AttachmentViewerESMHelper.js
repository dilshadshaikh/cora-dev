({
    /* Authors: Atul Hinge || Purpose: To start or close spiner */
     toggleSpinner: function (cmp, event) {
         var spinner = cmp.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
    },
	
})