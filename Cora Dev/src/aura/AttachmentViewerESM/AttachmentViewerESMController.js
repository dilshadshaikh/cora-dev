({
    /* Authors: Atul Hinge || Purpose: To initialized component data  */
    init : function(component, event, helper) {
  /*      var nameSpace =component.getConcreteComponent().getDef().getDescriptor().getNamespace();
        nameSpace=(nameSpace=='c')?'':nameSpace+'__';
		 component.set("v.attachmentURL","/apex/"+nameSpace+"FileViewer?fileId="+component.get("v.attachmentId"));
		 */
        
        console.log("attachmentId "+component.get("v.attachmentId"));
         console.log("v.subTab.label "+component.get("v.subTab.label"));
        
        component.set("v.attachmentURL","/apex/FileViewerESM?fileId="+component.get("v.attachmentId"));
        
        
        component.set("v.doctype",component.get("v.subTab.label").split('.')[1]);



        if(component.get("v.subTab.label").split('.')[1]!='pdf'){
            helper.toggleSpinner(component, event);
        }
    },
    /* Authors: Atul Hinge || Purpose: To close attachment viewer */
    onLoadImage : function(component, event, helper) {
      helper.toggleSpinner(component, event);
    },

	onLoadFrame : function(component, event, helper) {
   $(".loading").hide();
  
    },
    
})