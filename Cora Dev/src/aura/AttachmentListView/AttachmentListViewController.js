/*
* Authors	   :  Atul Hinge
* Date created : 01/09/2017
* Purpose      : This component Displays list of attachments.
* Dependencies :  Email.cmp(Aura component)
* JIRA ID      :  PUX-116,PUX-130
* -----------------------------------------------------------
* Modifications: 
*        Date:  
*        Purpose of modification:  
*        Method/Code segment modified:    
*/
({
    /* 
     *  Authors: Atul Hinge
     *  Purpose: It will be called at very first time when component render
    	 		 To initialised component data
    */
    doInit : function(component, event, helper) {
    	console.log('hi');
        helper.toggleSpinner(component);
        helper.getAttachments(component,helper);
		
    },
    handleFilterSelect : function(component, event, helper) {
        var att=component.get("v.Attachments");
        var val = event.getParam("value");
        component.set('v.selectedFilter',val);
        component.set("v.Attachments",helper.filterAttachment(component,att,val));
    },
    /* Authors: Atul Hinge || Purpose: toggleView is used to switch view from list view to thumbneil view and vice-versa  */
    toggleView : function(component, event, helper) {	
        component.set("v.isListView",!component.get("v.isListView"));
    },
    /* Authors: Atul Hinge || Purpose: Display file uploder  */
    ShowFileUploder: function(component, event, helper) {
        var toggleText = component.find("fileUpload");
        $A.util.toggleClass(toggleText, "slds-hide");
    },
	hideDeleteFileMenu: function(component, event, helper) {
		var toggleText = component.find("Confirmation");
        $A.util.toggleClass(toggleText, "slds-hide");
    },
    /* Authors: Atul Hinge || Purpose: Create file   */
    addAttachments: function(component, event, helper) {
       /*Start : Added for PUX-808*/
	   document.getElementById("Accspinner").style.display = "block";
	   /*End : Added for PUX-808*/
       component.set("v.fileUploadStatus",'create');
    },
    
    /* Authors: Atul Hinge || Purpose: On file upload status change  */
    onFileUploadStatusChange:function(component, event, helper){
        var status=component.get("v.fileUploadStatus");
        if(status=="complete"){
		 /*Start : Added for PUX-808*/
		if (document.getElementById("Accspinner") != null || document.getElementById("Accspinner") != undefined) {
			document.getElementById("Accspinner").style.display = "none";
		}
		/*End : Added for PUX-808*/
            var attachments=[];
			
            if(component.get("v.fileToBeUploaded").length > 0){
                var uploadedAttachments=helper.addIcon(component,component.get("v.fileToBeUploaded"));
            for(var i=0;i<uploadedAttachments.length;i++){
                attachments.push(uploadedAttachments[i]);
            }
           // attachments.push(uploadedAttachment);
            var oldAttachments=component.get('v.Attachments');
            for(var i=0;i<oldAttachments.length;i++){
                attachments.push(oldAttachments[i]);
            }
                component.set('v.Attachments', helper.addIcon(component,attachments));
                /*
                var att = component.get("v.Attachments");
		        var val = component.get('v.selectedFilter');
		        component.set("v.Attachments",helper.filterAttachment(component,att,val));
				*/
		        
				component.set('v.showFlagBtn',true);
            $A.util.toggleClass(component.find("fileUpload"), "slds-hide");
            var appEvent = $A.get("e.c:ShowToast");
            appEvent.setParams({ "title" : "Success !","message":" File is uploded successfully!" });
            appEvent.fire();
            component.set('v.fileUploadStatus','hold');
                component.set("v.fileToBeUploaded",{'sobjectType':'Object'});
            } else{
            	component.set('v.fileUploadStatus','hold');
                var appEvent = $A.get("e.c:ShowToast");
                appEvent.setParams({ "title" : "Error!","message":" Please select a file to upload!","type":"error" });
                appEvent.fire();
            }           
        }
        
    },
    changeFlag : function (component, event) {
      component.set("v.displaySaveButton",true);
        var changeItem=event.currentTarget.dataset.value;
         var flag =event.currentTarget.dataset.flag;
        var attachments =component.get("v.Attachments");
        for(var i=0;i<attachments.length;i++){
            if(attachments[i].ContentDocument.LatestPublishedVersionId==changeItem){
                if(flag=='Flag'){
                    attachments[i].ContentDocument.LatestPublishedVersion.Flag__c="Non Flag";
                }else if(flag=='Non Flag'){
                    attachments[i].ContentDocument.LatestPublishedVersion.Flag__c="Archive";
                }else if(flag=='Archive'){
                    attachments[i].ContentDocument.LatestPublishedVersion.Flag__c="Flag";
                }
            }
        }
        component.set("v.Attachments",attachments);
    },
    updateFlags : function (component, event,helper) {
        component.set("v.displaySaveButton",false);
        helper.toggleSpinner(component);
        var action = component.get("c.updateFlagOnAttachments");
        var parameters={};
        var attachments =component.get("v.Attachments");
        for(var i=0;i<attachments.length;i++){
          //  var cont={};
            parameters[attachments[i].ContentDocument.LatestPublishedVersionId]=attachments[i].ContentDocument.LatestPublishedVersion.Flag__c;
           // parameters.push(cont);
        }
        action.setParams({
            "contentJson": JSON.stringify(parameters)
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                 helper.toggleSpinner(component);
            }else if (state === "INCOMPLETE") {
                alert('e');
            }else if (state === "ERROR") {
                alert('e');
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    /* Authors: Atul Hinge || Purpose: handleMenuSelect is called when action is selected from menu button on list view  */
    handleMenuSelect : function (component, event,helper) {
        var selectedMenuItemValue = event.getParam("value");
        var id=event.getSource().get("v.name");
        var att=component.get("v.Attachments");
        if(selectedMenuItemValue=="view"){
            var appEvent = $A.get("e.c:OpenSubTab");
            var object={};
            for(var i=0;i<att.length;i++){
                if(att[i].ContentDocument.LatestPublishedVersionId==id){
                    object=att[i];
                    break;
                }
           	}
            
            appEvent.setParams({
                "parentTabName": "attachment",
                "id" :id,
                "label":event.getSource().get("v.title"),
                "object":object,
            });
            appEvent.fire();
        }else if(selectedMenuItemValue=="download"){
            var att =component.get("v.Attachments");
            for(var i=0;i<att.length;i++){
                if(att[i].ContentDocument.LatestPublishedVersionId==id){
                    window.open('/sfc/servlet.shepherd/document/download/'+att[i].ContentDocument.Id,'_self');        
                    break;
                }
           }
        }else if(selectedMenuItemValue=="delete"){
            $A.util.toggleClass(component.find("Confirmation"), "slds-hide");
            var callBack = function () {
                helper.deleteAttachments(component,id,helper);
            };
            component.set("v.callBackOnDelete", callBack);
        }
    },
    deleteConform : function(component, event, helper) {
        $A.util.toggleClass(component.find("Confirmation"), "slds-hide");
        var temp=component.get("v.callBackOnDelete");
        temp();
    },
    /* Authors: Atul Hinge || Purpose: to open attachment  */
    openAttachViewer : function(component, event, helper) {
        var attachId= event.currentTarget.id;
         var object={};
        var att=component.get("v.Attachments");
            for(var i=0;i<att.length;i++){
                if(att[i].ContentDocument.LatestPublishedVersionId==attachId){
                    object=att[i];
                    break;
                }
           	}
           
        if(event.currentTarget.dataset.visible=='true'){
            var appEvent = $A.get("e.c:OpenSubTab");
            appEvent.setParams({
                "parentTabName": "attachment",
                "id" :attachId,
                "label":event.currentTarget.dataset.value,
                "object":object,
            });
            appEvent.fire(); 
        }else{
            window.open('/sfc/servlet.shepherd/document/download/'+event.currentTarget.dataset.id,'_self');        
        }
    },
  
})