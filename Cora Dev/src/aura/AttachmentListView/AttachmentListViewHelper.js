({
    show: function (cmp, event) {
        var spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, "slds-show");
    },
    toggleSpinner: function (cmp) {
        var spinner = cmp.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
    },
    
    hide:function (cmp, event) {
        var spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-show");
        $A.util.addClass(spinner, "slds-hide");
    },
    getAttachments :function(component,helper){
        var action = component.get("c.getAttachments");
        
        action.setParams({ caseId :component.get("v.Case.Id")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.Attachments',helper.addIcon(component,response.getReturnValue().attachments));
               
				if(response.getReturnValue().attachments.length>0)
				{
					component.set('v.showFlagBtn',true);
				}else{
					component.set('v.showFlagBtn',false);
				}
                helper.toggleSpinner(component);
           }else if (state === "INCOMPLETE") {
                alert('e');
            }else if (state === "ERROR") {
                alert('e');
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    deleteAttachments :function(component,Id,helper){
		helper.toggleSpinner(component);
        var action = component.get("c.deleteAttachments");
        action.setParams({ docId :Id,caseId:component.get("v.Case.Id")});
        //action.setParams({ caseId :Id});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				if(response.getReturnValue().attachments!=null || response.getReturnValue().attachments!= undefined)
				{
            		component.set('v.Attachments',helper.addIcon(component,response.getReturnValue().attachments));
					if(response.getReturnValue().attachments.length>0)
					{
						component.set('v.showFlagBtn',true);
					}else{
						component.set('v.showFlagBtn',false);
					}
				}
				else
				{
					//alert('Sorry,you have no delete access for following item.');
					var appEvent = $A.get("e.c:ShowToast");
	                appEvent.setParams({ "title" : "Error!","message":"Sorry, you do not have access to delete the item.","type":"error" });
	                appEvent.fire();
				}
            	
                helper.toggleSpinner(component);
				
            }else if (state === "INCOMPLETE") {
                alert('e');
            }else if (state === "ERROR") {
                alert('e');
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    upload: function(component, file, base64Data, callback) {
        var action = component.get("c.uploadFile");
        this.uploadJs(component, file, base64Data, callback);
        action.setParams({
            fileName: file.name,
            base64Data: base64Data,
            contentType: file.type,
            ct:component.get("v.Case"),
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // this.getAttachments(component);
                component.set('v.Attachments',this.addIcon(component,response.getReturnValue().attachments));
				if(response.getReturnValue().attachments.length>0)
				{
					component.set('v.showFlagBtn',true);
				}else{
					component.set('v.showFlagBtn',false);
				}
                var appEvent = $A.get("e.c:ShowToast");
                appEvent.setParams({ "title" : "Success !","message":" File is uploded successfully!" });
                appEvent.fire();
                this.toggleSpinner(component);
                //callback(response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    uploadJs: function(component, file, base64Data, callback) {
         sforce.connection.sessionId = component.get("v.SessionId");
        var attachment         = new sforce.SObject('Attachment');
        attachment.Name        = file.name;
        attachment.IsPrivate   = false;
        attachment.ContentType = file.type;
        attachment.Body        = base64Data;
        attachment.Description = 'xxx';
        attachment.ParentId    = component.get("v.Case.Id");
        sforce.connection.create([attachment]);
    },
    addIcon :function(component,attachments) {
	      component.set("v.counter",0);
	      console.log(attachments);
	      
        for(var i=0;i<attachments.length;i++){
            var extension =attachments[i].ContentDocument.FileExtension;
            attachments[i]['isPreviewAvailable']=false;
            var size=attachments[i].ContentDocument.ContentSize;
            if((size/1000).toFixed(2)<1){
                size=size+' BYTES';
            }else if(((size/1000)/1024).toFixed(2)<1){
                size=(size/1000).toFixed(2)+' KB';
            }else{
                size=((size/1000)/1024).toFixed(2)+' MB';
            }
            attachments[i].Size=size;
            if(attachments[i].ContentDocument.LatestPublishedVersion.Flag__c=='Archive'){
                attachments[i]['display']=false;
            }else{
                attachments[i]['display']=true;
            }
            if(extension=='pdf'){
                attachments[i]['icon']='doctype:pdf';
                attachments[i]['isPreviewAvailable']=true;
				component.set("v.counter",component.get("v.counter") + 1);
				attachments[i]['counter']= component.get("v.counter");
				
            }else if(extension=='xlsx'){
                attachments[i]['icon']='doctype:excel';
                component.set("v.counter",component.get("v.counter") + 1);
				attachments[i]['counter']= component.get("v.counter");
                
            }else if(extension=='jpg' || extension=='png' || extension=='gif'){
                attachments[i]['icon']='doctype:image';
                attachments[i]['isPreviewAvailable']=true;
				component.set("v.counter",component.get("v.counter") + 1);
				attachments[i]['counter']= component.get("v.counter");
				
            }else if(extension=='html'){
				attachments[i]['icon']='doctype:html';
				attachments[i]['isPreviewAvailable']=true;
				component.set("v.counter",component.get("v.counter") + 1);
				attachments[i]['counter']= component.get("v.counter");
				
			}
			else if(extension=='csv'){
				attachments[i]['icon']='doctype:csv';
				component.set("v.counter",component.get("v.counter") + 1);
				attachments[i]['counter']= component.get("v.counter");
				
			}
			else if(extension=='txt'){
				attachments[i]['icon']='doctype:txt';
				component.set("v.counter",component.get("v.counter") + 1);
				attachments[i]['counter']= component.get("v.counter");
				
			}
			else if(extension=='docx' || extension=='doc'){
                attachments[i]['icon']='doctype:word';   
				component.set("v.counter",component.get("v.counter") + 1);
				attachments[i]['counter']= component.get("v.counter");
				
            }
			else if(extension=='tif'){
                attachments[i]['icon']='doctype:image';   
				component.set("v.counter",component.get("v.counter") + 1);
				attachments[i]['counter']= component.get("v.counter");
				
            }
            else if(extension=='xml'){
                attachments[i]['icon']='doctype:xml';   
				component.set("v.counter",component.get("v.counter") + 1);
				attachments[i]['counter']= component.get("v.counter");
				
            } 
            else{
                attachments[i]['icon']='doctype:attachment';
				component.set("v.counter",component.get("v.counter") + 1);
				attachments[i]['counter']= component.get("v.counter");
				
            }
            
        }
        return attachments;
    },
    filterAttachment :function(component,attachments,filterValue) {
		var countValue=component.get("v.counter");
		component.set("v.counter",0);
        for(var i=0;i<attachments.length;i++){
            if(attachments[i].ContentDocument.LatestPublishedVersion.Flag__c==filterValue){
                attachments[i]['display']=true;
				component.set("v.counter",component.get("v.counter") + 1);
				attachments[i]['counter']= component.get("v.counter");
            }else if(filterValue=='All'){
                attachments[i]['display']=true;
				component.set("v.counter",component.get("v.counter") + 1);
				attachments[i]['counter']= component.get("v.counter");
				
            }else{
                attachments[i]['display']=false;
            }
        }   
        return attachments;
    },

})