({

readFile : function(component,fileContents,file) {

	document.getElementById("Accspinner").style.display = "block";
	var action = component.get("c.readTheFile"); 
	console.log('fileData '+encodeURIComponent(fileContents));
        action.setParams({
            fileData : encodeURIComponent(fileContents),
			fileName: file.name,
            contentType: file.type

        });
		action.setCallback(this, function(a) {
           var historyId = (a.getReturnValue().split('---'))[0];
		   var jobId = (a.getReturnValue().split('---'))[1];

		     console.log(jobId);
		 if(jobId=='null')
		 {
		   document.getElementById("Accspinner").style.display = "none";
          var message="Blank csv. Please upload valid csv file. ";
	      this.showToast(component,'error',message,"error");
		
		}
		else if(jobId=='Records not availble.')
		{
		 document.getElementById("Accspinner").style.display = "none";
          var message="Records not availble. Please insert records. ";
	      this.showToast(component,'error',message,"error");
		}
		else{
		
			this.checkBatch(component,jobId,historyId);
		}
        });
		$A.enqueueAction(action);
       
    },


readcsvFile : function(component,helper) {

var fr = new FileReader();   
	var fileInput = component.find("file").getElement();
	console.log(fileInput);
	
    var file = fileInput.files[0];
	console.log(file);
    var self = this;
    fr.onload = function(){
    var fileContents = fr.result;
	console.log("fileContents before"+fileContents);
    var base64Mark = 'base64,';
    var dataStart = fileContents.indexOf(base64Mark)+ base64Mark.length;
	fileContents = fileContents.substring(dataStart);
	console.log("fileContents after"+fileContents);
	helper.readFile(component,fileContents,file);
	};
	
    fr.readAsDataURL(file);
    },

getSampleFiles : function(component,helper) {

var action = component.get("c.getSampleFiles");
    action.setCallback(this, function(response){
        var state = response.getState();
        console.log(state);
        if (state === "SUCCESS") {
              var introFileID = (response.getReturnValue().split('---'))[0];
		   var sampleFileID = (response.getReturnValue().split('---'))[1];
            console.log(introFileID);
			  console.log(sampleFileID);

			   component.set('v.introFileID',introFileID);
		 component.set('v.sampleFileID',sampleFileID);


        }
    });
    $A.enqueueAction(action);
       
    },

 getListViews : function(component,helper) {

var action = component.get("c.getListViews");
    action.setCallback(this, function(response){
        var state = response.getState();
        console.log(state);
        if (state === "SUCCESS") {
            var listviews = response.getReturnValue();
			 console.log(listviews);
			  component.set("v.TabList",listviews);
         /*   var navEvent = $A.get("e.force:navigateToList");
            navEvent.setParams({
                "listViewId": listviews.Id,
                "listViewName": null,
                "scope": "Account"
            });
            navEvent.fire();*/
        }
    });
    $A.enqueueAction(action);
       
    },

	 getRecords : function(cmp,helper) {

       var action = cmp.get("c.getRecords");
       var selectedTabId=cmp.get("v.selectedTabId");
	    var curPageNo = cmp.get("v.pageNumber");
	    cmp.set("v.tempPageNumber", curPageNo);
	   console.log(selectedTabId);
	    console.log(cmp.get("v.pageNumber"));

  action.setParams({
            selectedTabId : selectedTabId,
			recordsToDisplay: cmp.get("v.recordsToDisplay"),
			 pageNumber: cmp.get("v.pageNumber"),
			
        });

    action.setCallback(this, function(response){
        var state = response.getState();
        console.log(state);
        if (state === "SUCCESS") {
            var sw = response.getReturnValue();

			console.log('total '+response.getReturnValue().total)
			 console.log('records '+sw.records)
			  console.log('total '+sw.total);
			  console.log('pageSize '+ sw.pageSize );
			  console.log('page '+sw.page);
			  cmp.set('v.historylist',sw.records);
			   cmp.set("v.maxPage", Math.ceil((response.getReturnValue().total) / 15));
                //Start : PUX-504
                cmp.set("v.totalRecords", response.getReturnValue().total);

			 
         /*   var navEvent = $A.get("e.force:navigateToList");
            navEvent.setParams({
                "listViewId": listviews.Id,
                "listViewName": null,
                "scope": "Account"
            });
            navEvent.fire();*/
        }
    });
    $A.enqueueAction(action);
       
    },

	
	

	checkBatch : function(component, jobId,historyId) {
         console.log("in checkBatch");
		document.getElementById("Accspinner").style.display = "block";
	var action = component.get("c.checkBatch"); 
        action.setParams({
            jobid : jobId

        });

		action.setCallback(this, function(a) {
		document.getElementById("Accspinner").style.display = "block";
		   console.log(a.getReturnValue());
		if(a.getReturnValue()=='Completed')
            this.getCurHistoryDtl(component,historyId);
		else
		{
		
		 setTimeout(function () { 
		$A.enqueueAction(action);
	 }, 100);
		}

			
        });
		$A.enqueueAction(action);
	},

	getCurHistoryDtl : function(component,historyId) {
 console.log("in getCurHistoryDtl");

	var action = component.get("c.getCurHistoryDtl"); 

	action.setParams({
            historyId : historyId,
			
        });

		action.setCallback(this, function(a) {
           var historyDtl = a.getReturnValue();
         	document.getElementById("Accspinner").style.display = "none";
		   console.log("Error_Status__c "+historyDtl.Error_Status__c);
		   console.log("historyDtl 0 "+historyDtl.Attachments[0].Name);
		   component.set('v.originalFileID',historyDtl.Attachments[0].Id);
		     component.set('v.historyDtl',historyDtl);
			   component.set('v.render',true);
		   if(historyDtl.Error_Status__c)
		   {
		  console.log("historyDtl  1 "+historyDtl.Attachments[1].Name);
		   var message="Unable to read file! Please check Error File for details.";
		   this.showToast(component,'error',message,"error");
		 
		 if(historyDtl.Attachments[1].Name==historyDtl.File_Name__c)
		 {
		  component.set('v.originalFileID',historyDtl.Attachments[1].Id);
		 component.set('v.errorFileID',historyDtl.Attachments[0].Id);
		 }
		 else{
		  component.set('v.errorFileID',historyDtl.Attachments[1].Id);
		 }
		   }

		    var tempPageNo = component.get("v.tempPageNumber");
        component.set("v.pageNumber", tempPageNo);
    //    helper.getListViews(component,helper);

		this.getHistoryDtl(component, event);

		   document.getElementById("fileID").value = "";

        });
		$A.enqueueAction(action);

	},

		 showToast: function(component,title,message,type){
        //   console.log(type);
        var  type = type;
        if(type == undefined){
            type = 'success';
        }
        var  css = 'toast-top-center';
        var  msg = message;
        toastr.options.positionClass = 'toast-top-full-width';
        toastr.options.extendedTimeOut = 0; //1000;
        toastr.options.timeOut = 3000;
        toastr.options.fadeOut = 250;
        toastr.options.fadeIn = 250;
        toastr.options.positionClass = css;
        toastr[type](msg);
    },

	
	
	getHistoryDtl : function(component, event) {

	var action = component.get("c.getHistoryDtl"); 

	var curPageNo = component.get("v.pageNumber");
	    component.set("v.tempPageNumber", curPageNo);
	  
	    console.log(component.get("v.pageNumber"));

  action.setParams({
         
			recordsToDisplay: component.get("v.recordsToDisplay"),
			 pageNumber: component.get("v.pageNumber"),
			
        });


		action.setCallback(this, function(response) {

		 var sw = response.getReturnValue();

			console.log('total '+response.getReturnValue().total);
			 console.log('records '+sw.records)
			  console.log('total '+sw.total);
			  console.log('pageSize '+ sw.pageSize );
			  console.log('page '+sw.page);
			  component.set('v.historylist',sw.records);
			   component.set("v.maxPage", Math.ceil((response.getReturnValue().total) / 15));
                //Start : PUX-504
                component.set("v.totalRecords", response.getReturnValue().total);

		

        });
		$A.enqueueAction(action);

	},
	
	
})