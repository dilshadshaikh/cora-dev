({
	doInit: function(component, event, helper) {
	console.log("doInit");
		if(component.get("v.recordId")!=null && component.get("v.recordId")!=undefined)
		{
			var action = component.get("c.getInvoiceDetails");
			action.setParams({
				invoiceId: component.get("v.recordId"),
			});
			action.setCallback(this, function(response) {
				var state = response.getState();
					console.log(state);
					console.info('response', response);
				if (state === "SUCCESS") {
				console.log(response.getReturnValue().attachment);

					component.set("v.cloneAttachment",response.getReturnValue().attachment);
					component.set("v.simpleNewContact",response.getReturnValue().Invoice);
					component.set("v.simpleNewContact.Current_State__c","Start");
					component.set("v.invObject",component.get("v.simpleNewContact"));
				}
				else if (state === "ERROR") {
                alert('e');
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
			});
			$A.enqueueAction(action);
		}else{
		console.log("doInit else");
			component.find("invoiceRecordCreator").getNewRecord(
				"Invoice__c",null,false,$A.getCallback(function() {
				component.set("v.simpleNewContact.Current_State__c","Start");
				component.set("v.simpleNewContact.Document_Type__c","");
				var poFlipList =  component.get("v.poFlipList");
					if(poFlipList !=null && poFlipList !=undefined && poFlipList.length >0){
						var pol = '';
						for(var k in poFlipList){
							pol += poFlipList[k]["Id"] + ',';
						}
						if(pol!='')
						{
							pol = pol.substring(0,pol.length -1);							
							component.set("v.simpleNewContact.Document_Type__c","PO Invoice");
							component.set("v.poFlip",pol);
						}
					}
					var invoiceObj =new Object();
					invoiceObj["sobjectType"] = 'Invoice__c';
					for(var k in component.get("v.simpleNewContact")){
						invoiceObj[k] = component.get("v.simpleNewContact")[k];
					}
					component.set("v.invObject",invoiceObj);
				})
			);
		}
	},
})