({
 	getInvoiceLine: function (cmp, event, helper) { 
 		helper.getInvoiceLine(cmp, event);
 	},
	 echo : function(cmp, event) {
		console.log('enter child');
		var params = event.getParam('arguments');
        var callback;
        if (params) {
            callback = params.callback;
        }
		console.log('child');
		console.log(cmp.get("v.objList"));
		console.log('child1');
		if (callback) callback(cmp.get("v.objList"));
	},
	addMoreFields : function(component,event,helper){
		helper.addMoreInvoiceFields(component, event);
	},
	setInvoiceLineFromPO : function(component,event,helper){
		helper.setInvoiceLineFromPOHelper(component, event);
	},
	deleteField : function(component,event,helper){
		helper.deleteFieldHelper(component, event);
	},
    sectionOne : function(component, event, helper) {
         var acc = component.find('HeaderSec');	 
			for(var cmp in acc) {
				$A.util.toggleClass(acc[cmp], 'slds-show');  
				$A.util.toggleClass(acc[cmp], 'slds-hide');  
	  		}
	},
    handleRefreshAmount: function(component, event, helper) {
        helper.handleRefreshAmountHelper(component, event);
    },
	loadSlimscroll: function(component, event, helper) {
        /*var elem = component.find("slimScrollDiv");
		elem.slimScroll({
			height: '300px'
		});*/
    },
	 validate: function(component, event, helper) {
        var isValid = true;
        var allStrCmp = component.find('formfield');
        for (var k in allStrCmp) {
            allStrCmp[k].validate(function(value) {
                if (!value) {
                    isValid = false;
                }
            });
        }
        var params = event.getParam('arguments');
        var callback;
        if (params) {
            callback = params.callback;
        }
        if (callback) callback(isValid);
    },
})