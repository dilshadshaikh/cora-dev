({
    getInvoiceLine: function(component, event) {
		
		//var columns = JSON.parse(event.getParam("evtlayoutJson"))["PageLayout"]["lineItemComponents"][0]["columns"];
		/*var type ='';
		if(component.get("v.compType")){
			type = 'InvoiceLine';
		}*/
		//alert('inv line');
		var columns;
		var layout =component.get("v.layOutJson");
		console.log('layout');
		console.log(layout);

			//if(layout["type"] === component.get("v.compType")){
				columns = layout["columns"]
				component.set("v.section",layout);
			//}
		/*
		try{
			var tempRec = component.find("recordData");
			tempRec.set("v.recordId", event.getParam("evtId"));
			tempRec.reloadRecord();
		}catch(e){
			console.log(e);
		}*/
		/*$('.slimScrollDiv').slimScroll({
			height: 'auto'
		});*/
		this.getInvoiceLineData(component, event, columns, component.get("v.mainObject.Id"));
    },
    getInvoiceLineData: function(component, event, objectLsts,objId) {
        var action1 = component.get("c.getInvoiceLineDataServer");
        //Set the Object parameters and Field Set name 
        action1.setParams({
            strObjectName: 'Invoice_Line_Item__c',
            jsonObj: JSON.stringify(objectLsts),
		    objId : objId,
			type : component.get("v.compType"),
        });
        action1.setCallback(this, function(response) {
            console.log('response');
            console.log('@@@@@' + response.getState());
            if (response.getState() === 'SUCCESS') {
				if(response.getReturnValue().errmsg != null && response.getReturnValue().errmsg != undefined){
					this.showToast("Error!","error","sticky",response.getReturnValue().errmsg);
				}else{
					component.set("v.objListAdd", response.getReturnValue().DataList);
					component.set("v.columns", response.getReturnValue().Columns);
				}
            } else if (response.getState() === 'ERROR') {
                var errors = response.getError(response.getReturnValue());
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            } else {
                console.log('Something went wrong, Please check with your admin');
            }
        });
        $A.enqueueAction(action1);
    },
	addMoreInvoiceFields : function(component, event) {
	
		/*
		var invLines = component.get("v.objListAdd");
		var invNew = new Object;
		for (var column in component.get("v.columns")) {
    		for (var invFields in component.get("v.columns")[column]) {
        		if (invFields == 'name') {
            		if (component.get("v.columns")[column][invFields] === 'Invoice_Line_No__c') {
                		invNew["Invoice_Line_No__c"] = (Number(invLines.length) + 1);
					}
					else if (component.get("v.columns")[column][invFields] === 'Invoice_Line_Item_No__c') {
						invNew["Invoice_Line_Item_No__c"] = 'Invoice_Line_No__c~'+column+ '~'+ invFields +'~'+ Math.random();
            		} else {
                		invNew[component.get("v.columns")[column][invFields]] = null;
            		}
        		}
    		}
		}*/
		
		this.generateNewLine(component, event);
		/*
		
		 var action4 = component.get("c.getNewRecord");
		  action4.setCallback(this, function(response) {
            console.log('response');
            console.log('@@@@@' + response.getState());
            if (response.getState() === 'SUCCESS') {
				var invLines = component.get("v.objListAdd")
				var inll =response.getReturnValue();
				//inll["Id"] = 'a1737000001Qp0xXXX';
				invLines.push(inll);
				component.set("v.objListAdd", invLines);
			    //component.set("v.columns", response.getReturnValue().Columns);
				
				

            } else if (response.getState() === 'ERROR') {
                var errors = response.getError(response.getReturnValue());
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            } else {
                console.log('Something went wrong, Please check with your admin');
            }
        });
        $A.enqueueAction(action4);*/
	},
	setInvoiceLineFromPOHelper : function(component, event) {
		
	},
	calculateAmount : function(component, event,invLines) {
	var total = 0;
	for(var key in invLines){
		var amt =0;
		if(invLines["Amount__c"]!=null && invLines["Amount__c"]!=undefined){
			amt = Number(invLines["Amount__c"]);
		}
		total = total + Number(amt);
	}
	component.set("v.simpleRecord.Amount__c",'55');
	},
	deleteFieldHelper  : function(component, event) {
		var tab = event.getSource().get('v.value');
		console.log(tab);
		var invLines = component.get("v.objListAdd");
		var invLinesNew =new Array();
		var cnt = 0;
		for(var invkey in invLines){
			if(!((tab).toString() === invkey.toString())){
				invLinesNew[cnt] = invLines[invkey];
				cnt ++;
			}
		}
		component.set("v.objListAdd",invLinesNew);
		var RefreshEvent = $A.get("e.c:RefreshAmount");
	        RefreshEvent.fire();
	},
    handleRefreshAmountHelper: function(component, event) {
        var invLines =  component.get("v.objListAdd");
        var tot=0;
        if(invLines!=null){
        for (var key in invLines) {
            if (invLines[key]["Amount__c"] != null && invLines[key]["Amount__c"] != undefined) {
				tot=tot+Number(invLines[key]["Amount__c"]);
            }
        }
        } 
      var callback;
        var params = event.getParam('arguments');
        if (params) {
            callback = params.callback;
        }
        if (callback){ callback(tot);}
    },
	generateNewLine :function(component, event) {
				component.set("v.newContact",null);
				component.set("v.tempNewObj",null);
				component.set("v.simpleNewContact",null);
				component.set("v.newContactError",null);
				component.find("invoiceLineRecordCreator").getNewRecord(
				"Invoice_Line_Item__c",null,false,$A.getCallback(function() {
					/*
					trial 1
					-------------------------------------------------------------*/
					/*
					component.set("v.tempNewObj",component.get("v.simpleNewContact"));
					var invLines = component.get("v.objListAdd");
					//component.set("v.tempNewObj.Invoice_Line_No__c",(Number(invLines.length) + 1));
					//component.set("v.tempNewObj.Invoice_Line_Item_No__c",'Invoice_Line_No__c~'+ Math.random());
					//newinvLine["Invoice_Line_No__c"] = (Number(invLines.length) + 1);
					//newinvLine["Invoice_Line_Item_No__c"] = 'Invoice_Line_No__c~'+ Math.random();
					invLines.push(component.get("v.tempNewObj"));
					component.set("v.objListAdd", invLines);
					*/
					/*
					trial 2-------------------------------------------------------------*/
					
					var invLines = component.get("v.objListAdd");
					var tempstr='';
					var invNew = new Object;
					invNew["sobjectType"] = 'Invoice_Line_Item__c';
					for(var key in component.get("v.simpleNewContact")){
						if (key === 'Invoice_Line_No__c') {
                			invNew[key] = ''+(Number(invLines.length) + 1);
							//tempstr += invNew[key] +':'  +(Number(invLines.length) + 1)+',';
						}
						else if (key === 'Invoice_Line_Item_No__c') {
							invNew[key] = 'Invoice_Line_No__c~'+component.get("v.mainObject.Id")+ Math.random();
							//tempstr +=  invNew[key] +':' + Math.random()+',';

            			} else {
                			invNew[key] = null;
							//tempstr +=  invNew[key] +':' + null+',';
            			}
					}
				
					invLines.push(invNew);
					component.set("v.objListAdd", invLines);
					/*
					-------------------------------------------------------------
					*/
					
				}));
	},
	showToast: function(title, type,mode,message) {
		var toastEvent = $A.get("e.force:showToast");
							toastEvent.setParams({
								"title": title,
								"type": type,
								"mode": mode,
								"message": message
							});
							toastEvent.fire();
	},
})