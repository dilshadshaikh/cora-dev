({
    getInvoiceLine: function(component, event) {
        var columns;
        var layout = component.get("v.layOutJson");
        console.log('layout');
        console.log(layout);
        columns = layout["columns"]
        component.set("v.section", layout);
        this.getInvoiceLineData(component, event, columns, component.get("v.mainObject.Id"));
    },
    getInvoiceLineData: function(component, event, objectLsts, objId) {
        var action1 = component.get("c.getInvoiceLineDataServer");
        //Set the Object parameters and Field Set name 
        action1.setParams({
            strObjectName: 'Invoice_Line_Item__c',
            jsonObj: JSON.stringify(objectLsts),
            objId: objId,
            type: component.get("v.compType"),
            layout: component.get("v.pagemode")
        });
        action1.setCallback(objectLsts, function(response) {
            if (response.getState() === 'SUCCESS') {
				if(response.getReturnValue().errmsg != null && response.getReturnValue().errmsg != undefined){
					this.showToast("Error!","error","sticky",response.getReturnValue().errmsg);
				}
                else{
					component.set("v.objList", response.getReturnValue().DataList);
					var cls = JSON.parse(JSON.stringify(response.getReturnValue().Columns));
					for (var keyy in cls) {
					    cls[keyy]["criteria"] = objectLsts[keyy]["criteria"];
					}
					component.set("v.columns", cls);
					component.set("v.invPoMapNew", response.getReturnValue().DataListMap);
				}
            } else if (response.getState() === 'ERROR') {
                var errors = response.getError(response.getReturnValue());
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            } else {
                console.log('Something went wrong, Please check with your admin');
            }
			/*var spinner = component.find("mySpinner");
			$A.util.toggleClass(spinner, "slds-hide");*/
        });
        $A.enqueueAction(action1);
    },
    addMoreInvoiceFields: function(component, event) {
		this.generateNewLine(component, event);
    },
    setInvoiceLineFromPOHelper: function(component, event) {
        if (component.get("v.compType") === 'InvoiceLine') {
            var poLines = null;
            var polinesobj = null;
            var grnListObj = null;
            var params = event.getParam('arguments');
            if (params) {
                poLines = params.polines;
                polinesobj = params.polinesobj;
                grnListObj = params.grnList;
            }
            var invTabList = [];
            var invLines = component.get("v.objList");
            console.log('invLines ');
            console.log(invLines);
            for (var val2 in polinesobj) {
                for (var key3 in poLines) {
                    if (poLines[key3] == polinesobj[val2]["Id"]) {
                        var invNew = new Object;
						invNew["sobjectType"] = 'Invoice_Line_Item__c';
                        for (var k54 in component.get("v.invPoMapNew")) {
                            if (polinesobj[val2][component.get("v.invPoMapNew")[k54]]) {
                                invNew[k54] = polinesobj[val2][component.get("v.invPoMapNew")[k54]]
                            }
                        }
                        invNew["Invoice_Line_Item_No__c"] = polinesobj[val2]["Purchase_Order__c"] + '~' + polinesobj[val2]["Id"] + '~' + Math.random();
                        invNew["PO_Line_Item__c"] = polinesobj[val2]["Id"];
                        invNew["PO_Line_Item__r"] = polinesobj[val2];
                        invNew["Invoice_Line_No__c"] =''+ (Number(invLines.length) + 1);
                        invNew["Invoice__c"] = component.get("v.recId");
                        invNew["Purchase_Order__c"] = polinesobj[val2]["Purchase_Order__c"];
                        invNew["Purchase_Order__r"] = polinesobj[val2]["Purchase_Order__r"];
                        invNew["Amount__c"] = Number(Number(polinesobj[val2]["Quanity_To_Invoice__c"]) * Number(polinesobj[val2]["Rate__c"]));
						invNew["Quantity_Calculation_Required__c"] = polinesobj[val2]["Quantity_Calculation_Required_Formula__c"];
                        if (polinesobj[val2]["GRN_Match_Required_Formula__c"] == true || polinesobj[val2]["GRN_Match_Required_Formula__c"] == 'true') {
                            for (var grnKey in grnListObj) {
                                if (grnListObj[grnKey]["PO_Line_Item__c"] === polinesobj[val2]["Id"]) {
                                    if (invNew["GRN__c"] != null || invNew["GRN_Line_Item__c"] != null) {
                                        invNew["GRN__c"] = null;
                                        invNew["GRN__r"] = null;
                                        invNew["GRN_Line_Item__c"] = null;
                                        invNew["GRN_Line_Item__r"] = null;
                                        break;
                                    } else {
                                        invNew["GRN__c"] = grnListObj[grnKey]["GRN__c"];
                                        invNew["GRN__r"] = grnListObj[grnKey]["GRN__r"];
                                        invNew["GRN_Line_Item__c"] = grnListObj[grnKey]["Id"];
                                        invNew["GRN_Line_Item__r"] = grnListObj[grnKey];
                                    }
                                }
                            }
                        }
                        invLines[Number(invLines.length)] = invNew;
                    }
                }
            }
            component.set("v.objList", invLines);
            this.calculateAmount(component, event, invLines);
        }
    },
    calculateAmount: function(component, event, invLines) {
		if(invLines){
			  var total = 0;
        for (var key in invLines) {
            var amt = 0;
            if (invLines[key]["Amount__c"] != null && invLines[key]["Amount__c"] != undefined) {
                amt = Number(invLines[key]["Amount__c"]);
            }
            total = total + Number(amt);
        }
        component.set("v.mainObject.Amount__c", total);
        var params = event.getParam('arguments');
        var callback;
        if (params) {
            callback = params.callback;
        }
        if (callback) callback(total);
		}
    },
    deleteFieldHelper: function(component, event) {
        var tab = event.getSource().get('v.value');
        var invLines = component.get("v.objList");
        var invLinesNew = new Array();
        var cnt = 0;
        for (var invkey in invLines) {
            if (!((tab).toString() === invkey.toString())) {
                invLinesNew[cnt] = invLines[invkey];
                cnt++;
            }
        }
		component.set("v.objList", invLinesNew);
		var RefreshEvent = $A.get("e.c:RefreshAmount");
	    RefreshEvent.fire();
       
    },
    handleRefreshAmountHelper: function(component, event) {
        var invLines =  component.get("v.objList");
        if(invLines!=null){
        for (var key in invLines) {
            if (invLines[key]["Rate__c"] != null && invLines[key]["Rate__c"] != undefined &&
            invLines[key]["Quantity__c"] != null && invLines[key]["Quantity__c"] != undefined) {
                invLines[key]["Amount__c"] = Number(invLines[key]["Rate__c"]) *  Number(invLines[key]["Quantity__c"]);
            }
        }
        component.set("v.objList", invLines);
        this.calculateAmount(component, event,invLines);
        } 
    },
	generateNewLine :function(component, event) {
		component.set("v.newContact",null);
		//component.set("v.tempNewObj",null);
		component.set("v.simpleNewContact",null);
		component.set("v.newContactError",null);
		component.find("invoiceLineRecordCreator").getNewRecord(
				"Invoice_Line_Item__c",null,false,$A.getCallback(function() {
					var invLines = component.get("v.objList");
					var tempstr='';
					var invNew = new Object;
					invNew["sobjectType"] = 'Invoice_Line_Item__c';
					for(var key in component.get("v.simpleNewContact")){
						if (key === 'Invoice_Line_No__c') {
                			invNew[key] = ''+(Number(invLines.length) + 1);
						}
						else if (key === 'Invoice_Line_Item_No__c') {
							invNew[key] = 'Invoice_Line_No__c~'+component.get("v.mainObject.Id")+ Math.random();
            			} else {
                			invNew[key] = null;
            			}
					}
					invLines.push(invNew);
					component.set("v.objList", invLines);
				}));
	},
	showToast: function(title, type,mode,message) {
		var toastEvent = $A.get("e.force:showToast");
							toastEvent.setParams({
								"title": title,
								"type": type,
								"mode": mode,
								"message": message
							});
							toastEvent.fire();
	},
})