({
	addPOHelper : function(component, event) {
		var newPO= '';
		var validatePO=true;
		var childCmp = component.find("poRefId");
        // call the aura:method in the child component
        childCmp.setParData(function(result) {
			if(result!=null && result!=undefined && result["Id"]!=null && result["Id"]!=undefined){
				newPO = result["Id"] ;
			}
        });
		if(newPO ==null ||newPO ==undefined || newPO.trim() ==''){
			this.showToast("Validation Alert!","error","sticky","Please select Purchase Order.");
		}
		var existPO = component.get("v.objList");
		var poList = '';
		for(var key in existPO){
			if(existPO[key]["Id"] ===newPO){
				validatePO =false;
			}
			poList = poList + existPO[key]["Id"]+',';
		}
		if(validatePO){
			poList = poList + newPO;
			if (component.get("v.fieldCriteria.callOnChangeManager"))
			{
				var RefreshEvent = $A.get("e.c:Refresh");
				RefreshEvent.setParams({
					"callOnChangeManager": true,
					"fieldName": 'Purchase_Order__c',
					"fieldValue" : newPO,
				});
				RefreshEvent.fire();
            }
			this.getSelectedPurchaseOrderHelper(component, event,poList);
		}else{
			this.showToast("Validation Alert!","error","sticky","Purchase Order is already added.");
		}
		
	},
	getSelectedPurchaseOrderHelper: function(component, event,poList) {
	  var action2 = component.get("c.getSelectedPurchaseOrderServer");
        //Set the Object parameters and Field Set name 
        action2.setParams({
            'poListDet': poList,
			'strObjectName': 'Purchase_Order__c',
        });
        action2.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                if(response.getReturnValue().errmsg !=null && response.getReturnValue().errmsg!=undefined && response.getReturnValue().errmsg!=''){
					this.showToast("Validation Alert!","error","sticky", response.getReturnValue().errmsg);
				}else{
					component.set("v.objList", response.getReturnValue().DataList);
					var compEvent = $A.get("e.c:loadPoline");;
					compEvent.setParams({'polines' : poList });
					compEvent.fire();
				}
            } else if (response.getState() === 'ERROR') {
                var errors = response.getError(response.getReturnValue());
                if (errors) {
                    if (errors[0] && errors[0].message) {
                      this.showToast("Validation Alert!","error","sticky", errors[0].message);
                    }
                } else {
                    this.showToast("Validation Alert!","error","sticky","Something went wrong, Please check with your admin.");
                }
            } else {
                this.showToast("Validation Alert!","error","sticky","Something went wrong, Please check with your admin.");
            }
        });
        $A.enqueueAction(action2);
	},
	 getPurchaseOrderHelper: function(component, event) {
		var columns;
		var layout =component.get("v.layOutJson");
		columns = layout["columns"];
		this.getPOLineData(component, event, columns, component.get("v.mainObject.Id"));
    },
    getPOLineData: function(component, event, objectLsts,objId) {
        var action1 = component.get("c.getPurchaseOrderServer");
        action1.setParams({
            strObjectName: 'Purchase_Order__c',
            jsonObj: JSON.stringify(objectLsts),
		    objId :  component.get("v.mainObject.Id"),
			poFlip: component.get("v.poFlipCreate"),
        });
        action1.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                component.set("v.objList", response.getReturnValue().DataList);
                 component.set("v.columns", response.getReturnValue().Columns);

            } else if (response.getState() === 'ERROR') {
                var errors = response.getError(response.getReturnValue());
                if (errors) {
                    if (errors[0] && errors[0].message) {
					this.showToast("Validation Alert!","error","sticky", errors[0].message);
                    }
                } else {
					this.showToast("Validation Alert!","error","sticky","Something went wrong, Please check with your admin.");
                }
            } else {
				this.showToast("Validation Alert!","error","sticky","Something went wrong, Please check with your admin.");
            }
        });
        $A.enqueueAction(action1);
    },
	deleteFieldHelper: function(component, event) {
        var tab = event.getSource().get('v.value');
		var deletedPo='';
        var pOrder = component.get("v.objList");
        var pOrderNew = new Array();
        var cnt = 0;
        for (var pokey in pOrder) {
            if (!((tab).toString() === pokey.toString())) {
                pOrderNew[cnt] = pOrder[pokey];
                cnt++;
            }else{
				deletedPo = pOrder[pokey]["Id"];
			}
        }
		var poList ='';
		for (var pokey in pOrderNew) {
			poList = poList + pOrderNew[pokey]["Id"]+',';
		}
        component.set("v.objList", pOrderNew);
		 var compEvent = $A.get("e.c:loadPoline");;
						compEvent.setParams({'polines' : poList ,'deletedPo':deletedPo });
					// fire the event  
					compEvent.fire();
		if (component.get("v.fieldCriteria.callOnChangeManager"))
		{
			var RefreshEvent = $A.get("e.c:Refresh");
			RefreshEvent.setParams({
				"callOnChangeManager": true,
				"fieldName": 'Purchase_Order__c',
			});
			RefreshEvent.fire();
        }
    },
	showToast: function(title, type,mode,message) {
		var toastEvent = $A.get("e.force:showToast");
							toastEvent.setParams({
								"title": title,
								"type": type,
								"mode": mode,
								"message": message
							});
							toastEvent.fire();
	}
	
})