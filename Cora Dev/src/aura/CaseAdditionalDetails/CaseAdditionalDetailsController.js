/*
Authors: Tej Pal Kumawat
Date created: 4-Dec-2017
Purpose: PUX-459: Comment entered by user should store and display in historical maner.
Dependencies: CaseAdditionalDetails.cmp, CaseAdditionalDetailsHelper.js, CaseAdditionalDetails.cls             
*/ 
({
	/*
	Authors: Tej Pal Kumawat
	Purpose: PUX-459: Comment entered by user should store and display in historical maner.
	Dependencies: None
    */
    doInit: function(component, event, helper) {
		helper.getRecord(component, event);
	},
})