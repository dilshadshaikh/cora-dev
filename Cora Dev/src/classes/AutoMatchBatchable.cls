global class AutoMatchBatchable implements Database.Batchable<SObject>,Schedulable {
    
    global SchedulableContext objSchedulableContext;
    global Set<string> InvIdSet;
    global List<PO_Line_Item__c> poLineItemList;
    global List<GRN_Line_Item__c> grnLineItemList;
    global String esmNameSpace;
    global Invoice_Configuration__c invConf;
    global Map<String,string> polinvlMap;
    public Map<string,set<string>> InvPOSet;
    
    global AutoMatchBatchable() {
        try {
            system.debug('=====AutoMatchBatchable constructor======');
            polinvlMap = new Map<String,string>(); 
            InvPOSet = new Map<string,set<string>>();
            esmNameSpace = UtilityController.esmNameSpace;
            if(Invoice_Configuration__c.getInstance(Userinfo.getUserid()) != null) {
                invConf = Invoice_Configuration__c.getInstance(Userinfo.getUserid());
            } else {
                invConf = Invoice_Configuration__c.getOrgDefaults();
            }
            system.debug('invConf.Auto_Match_Current_State__c:'+invConf.Auto_Match_Current_State__c);
            if(invConf.Auto_Match_Current_State__c != null && invConf.Auto_Match_Current_State__c != ''){
                Set<string> invCurrentStateSet = new Set<string>();  
                if(invConf.Auto_Match_Current_State__c.contains(',')){
                    for(String currentstate:invConf.Auto_Match_Current_State__c.split(',')){
                        invCurrentStateSet.add(currentstate);
                    }
                } else {
                    invCurrentStateSet.add(invConf.Auto_Match_Current_State__c);
                }
                System.debug('Log invCurrentStateSet'+invCurrentStateSet);
                InvIdSet = new Set<string>();
                if(Invoice__c.getSObjectType().getDescribe().isAccessible()) {
                    for(Invoice__c inv: [SELECT Id, Name FROM Invoice__c WHERE Current_State__c IN:invCurrentStateSet]){
                        InvIdSet.add(inv.Id);
                    }
                } else {
                    throw new CustomCRUDFLSException(UtilityController.getObjectAccessExceptionMessage(esmNameSpace + 'Invoice__c'));
                }
                
                Set<String> poIdSet = new Set<String>(); 
                if(Invoice_PO_Detail__c.getSObjectType().getDescribe().isAccessible()
                    && Schema.sObjectType.Invoice_PO_Detail__c.fields.Invoice__c.isAccessible()
                    && Schema.sObjectType.Invoice_PO_Detail__c.fields.Purchase_Order__c.isAccessible()){
                    set<string> poset;
                    system.debug('=======INVIDset'+InvIdSet);
                    for(Invoice_PO_Detail__c invpo: [SELECT Id, Name, Invoice__c, Purchase_Order__c FROM Invoice_PO_Detail__c WHERE Invoice__c IN:InvIdSet]){
                        InvIdSet.add(invpo.Invoice__c);
                        poIdSet.add(invpo.Purchase_Order__c);
                        system.debug('POID set==========='+poIdset);
                        
                        if(InvPOSet.get(invpo.Invoice__c) != null) {
                            poset = InvPOSet.get(invpo.Invoice__c);
                        } else {
                            poset = new set<string>();
                        }
                        poset.add(invpo.Purchase_Order__c);
                        InvPOSet.put(invpo.Invoice__c,poset);
                    }
                } else {
                    throw new CustomCRUDFLSException(UtilityController.getObjectAccessExceptionMessage(esmNameSpace + 'Invoice_PO_Detail__c'));
                }
                
                
                System.debug('Log InvIdSet'+InvIdSet);
                System.debug('Log InvIdSet'+poIdSet);
                System.debug('Log InvPOSet'+InvPOSet);
                string polQuery = 'SELECT Id,Name';
                Map<String, String> mpp = new Map<String, String> ();
                List<PO_GRN_Line_Invoice_Line_Mappings__c> cusmdt = [select Invoice_Line_Item_Field_Name__c, PO_GRN_Line_Item_Field_Name__c from PO_GRN_Line_Invoice_Line_Mappings__c where Used_In_Auto_Match__c = true];
                system.debug('---------cusmdt====='+cusmdt);
                for (PO_GRN_Line_Invoice_Line_Mappings__c cml : cusmdt) {
                    polinvlMap.put(String.valueOf(cml.PO_GRN_Line_Item_Field_Name__c), String.valueOf(cml.Invoice_Line_Item_Field_Name__c));
                    polQuery += ',' + cml.PO_GRN_Line_Item_Field_Name__c;
                }
                if(!polQuery.containsIgnoreCase('Purchase_Order__c')){
                    polQuery += ','+esmNameSpace+'Purchase_Order__c';
                }
                polQuery += ','+esmNameSpace+'GRN_Match_Required_Formula__c,Quantity_Calculation_Required_Formula__c,PO_Line_No__c';
                polQuery += ' FROM '+esmNameSpace+'PO_Line_Item__c WHERE '+esmNameSpace+'Purchase_Order__c IN:poIdSet'; 
                system.debug('polQuery====='+polQuery);
                poLineItemList = new List<PO_Line_Item__c>();
                if(UtilityController.isFieldAccessible(esmNameSpace+'PO_Line_Item__c',null)){
                    poLineItemList = Database.Query(polQuery);
                }
                system.debug('Log poLineItemList:==========='+poLineItemList);
                grnLineItemList = new List<GRN_Line_Item__c>();
                if(UtilityController.isFieldAccessible(esmNameSpace+'GRN_Line_Item__c',null)){
                    //grnLineItemList = Database.Query(CommonFunctionController.getGRNLineItemQuery(null, esmNameSpace, esmNameSpace, '', poIdSet));
                    grnLineItemList = (CommonFunctionController.getGRNline(poIdSet));
                }
                system.debug('Log grnLineItemList:'+grnLineItemList);
            }
        } catch(Exception ex){
             UtilityController.customDebug(ex, 'AutoMatchBatchable');
        }
    }
    
    /**
     * @description gets invoked when the batch job starts
     * @param context contains the job ID
     * @returns the record set as a QueryLocator object that will be batched for execution
     */ 
    global List<Invoice_Line_Item__c> start(Database.BatchableContext context) {
        try{
            system.debug('=====AutoMatchBatchable start======');
            return Database.Query(CommonFunctionController.getInvLineItemQuery(null, esmNameSpace, esmNameSpace, '', InvIdSet));            
        } catch(Exception ex){
             UtilityController.customDebug(ex, 'AutoMatchBatchable');
        }
        return null;
    }

    /**
     * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
     * @param context contains the job ID
     * @param scope contains the batch of records to process.
     */ 
    global void execute(Database.BatchableContext context, List<Invoice_Line_Item__c> scope) {
        //---- Auto Matching | Starts ---//
        try {
            system.debug('=====AutoMatchBatchable execute======');
        system.debug('Log invConf.Auto_Match_Flow_Method__c:'+invConf.Auto_Match_Flow_Method__c);
        if(invConf.Auto_Match_Flow_Method__c != null && invConf.Auto_Match_Flow_Method__c != ''){
               
            Map<string,string> InvMap = new Map<string,string>();
            
            
            Map<String,List<GRN_Line_Item__c>> GRNLMap = new Map<String,List<GRN_Line_Item__c>>();
            
            Map<String,String> GRNLineMap = new Map<String,String>();
            Map<String,String> GRNMap = new Map<String,String>();
            system.debug('grnlineItemlist========='+grnLineItemList);
            if(grnLineItemList != null && grnLineItemList.size() > 0){
                List<GRN_Line_Item__c> grnlTempList;
                for(GRN_Line_Item__c grnl : grnLineItemList){
                    grnlTempList = new List<GRN_Line_Item__c>();
                    if(grnl.PO_Line_Item__r.GRN_Match_Required_Formula__c){
                        if(GRNLMap.get(grnl.PO_line_Item__c)!= null){
                            grnlTempList = GRNLMap.get(grnl.PO_line_Item__c);
                        }
                        grnlTempList.add(grnl);
                        GRNLMap.put(grnl.PO_line_Item__c,grnlTempList);
                    }
                }
            }
            system.debug('Log GRNLMap:-----------'+GRNLMap);
            system.debug('Log scope:-----------'+scope);
            for(Invoice_Line_Item__c invl : scope){ 
             system.debug('Log invConf.Enable_Auto_Flipping_With_GRN__c:-----------'+invConf.Enable_Auto_Flipping_With_GRN__c);
                system.debug('Log invl.PO_Line_Item__c:-----------'+invl.PO_Line_Item__c);

                if((!invConf.Enable_Auto_Flipping_With_GRN__c && invl.PO_Line_Item__c == null) || (invConf.Enable_Auto_Flipping_With_GRN__c && invl.GRN_Line_Item__c == null)){
                    invl.Is_Mismatch__c = true;
                    boolean Matched = true;
                    system.debug('Log invConf.Auto_Match_Flow_Method__c:-----------'+invConf.Auto_Match_Flow_Method__c);    
                    if(invConf.Auto_Match_Flow_Method__c.equalsIgnoreCase('Sequence')){
                        for(String polFieldName:polinvlMap.keySet()){
                            String invlFieldName = polinvlMap.get(polFieldName);
                            if(invConf.Enable_Auto_Flipping_With_GRN__c){
                             system.debug('Log grnl:grnLineItemList-----------'+grnLineItemList);

                                for(GRN_Line_Item__c grnl:grnLineItemList){
                                    if(InvPOSet.get(invl.Invoice__c) != null && InvPOSet.get(invl.Invoice__c).contains(grnl.Purchase_Order__c) && grnl.get(polFieldName) != null && grnl.get(polFieldName).equals(invl.get(invlFieldName))){
                                        invl.GRN_Line_Item__c = grnl.Id;
                                        invl.GRN__c = grnl.GRN__c;
                                        invl.PO_Line_Item__c = grnl.PO_Line_Item__c;
                                        break;                                           
                                    }
                                }
                            } else {
                            system.debug('pol:poLineItemList-----------'+poLineItemList);
                                for(PO_Line_Item__c pol:poLineItemList){
                                    if(InvPOSet.get(invl.Invoice__c) != null && InvPOSet.get(invl.Invoice__c).contains(pol.Purchase_Order__c) && pol.get(polFieldName) != null && pol.get(polFieldName).equals(invl.get(invlFieldName))){
                                        invl.PO_Line_Item__c = pol.Id;
                                        system.debug('GRNLMap.get(invl.PO_Line_Item__c):======='+GRNLMap.get(pol.Id));
                                        if(GRNLMap != null && GRNLMap.size() > 0 && GRNLMap.get(pol.Id) != null && GRNLMap.get(pol.Id).size() == 1){
                                            invl.GRN_Line_Item__c = GRNLMap.get(pol.Id)[0].Id;
                                            invl.GRN__c = GRNLMap.get(pol.Id)[0].GRN__c;
                                        }
                                        invl.Quantity_Calculation_Required__c = pol.Quantity_Calculation_Required_Formula__c;
                                        break;                                           
                                    }
                                }
                            }
                            if((invConf.Enable_Auto_Flipping_With_GRN__c && invl.GRN_Line_Item__c != null) || (!invConf.Enable_Auto_Flipping_With_GRN__c && invl.PO_Line_Item__c != null)){
                                break;                                      
                            }
                        }
                    } else if(invConf.Auto_Match_Flow_Method__c.equalsIgnoreCase('Parallel')) {
                        if(invConf.Enable_Auto_Flipping_With_GRN__c){
                            for(GRN_Line_Item__c grnl:grnLineItemList){
                                if(InvPOSet.get(invl.Invoice__c) != null && InvPOSet.get(invl.Invoice__c).contains(grnl.Purchase_Order__c)){
                                    Matched = true;
                                    for(String polFieldName:polinvlMap.keySet()){
                                        String invlFieldName = polinvlMap.get(polFieldName);
                                        if((grnl.get(polFieldName) != null && invl.get(invlFieldName) == null) || (grnl.get(polFieldName) == null && invl.get(invlFieldName) != null)){
                                            Matched = false;
                                        } else if(grnl.get(polFieldName) != null && !grnl.get(polFieldName).equals(invl.get(invlFieldName))){
                                            Matched = false;
                                        }
                                    }
                                    if(Matched){
                                        invl.GRN_Line_Item__c = grnl.Id;
                                        invl.GRN__c = grnl.GRN__c;
                                        invl.PO_Line_Item__c = grnl.PO_Line_Item__c;
                                        break;                                      
                                    }
                                }
                            }
                        } else {
                        system.debug('*************pol:poLineItemList'+poLineItemList);
                            for(PO_Line_Item__c pol:poLineItemList){
                                if(InvPOSet.get(invl.Invoice__c) != null && InvPOSet.get(invl.Invoice__c).contains(pol.Purchase_Order__c)){
                                    Matched = true;
                                    for(String polFieldName:polinvlMap.keySet()){
                                        String invlFieldName = polinvlMap.get(polFieldName);
                                        if((pol.get(polFieldName) != null && invl.get(invlFieldName) == null) || (pol.get(polFieldName) == null && invl.get(invlFieldName) != null)){
                                            Matched = false;
                                        } else if(pol.get(polFieldName) != null && !pol.get(polFieldName).equals(invl.get(invlFieldName))){
                                            Matched = false;
                                        }
                                    }
                                    if(Matched){
                                        invl.PO_Line_Item__c = pol.Id;
                                        if(GRNLMap != null && GRNLMap.size() > 0 && GRNLMap.get(invl.PO_Line_Item__c) != null && GRNLMap.get(invl.PO_Line_Item__c).size() == 1){
                                            invl.GRN_Line_Item__c = GRNLMap.get(invl.PO_Line_Item__c)[0].Id;
                                            invl.GRN__c = GRNLMap.get(invl.PO_Line_Item__c)[0].GRN__c;
                                        }
                                        invl.Quantity_Calculation_Required__c = pol.Quantity_Calculation_Required_Formula__c;
                                        break;                                      
                                    }
                                }
                            }
                        }
                    }
                    if(invConf.Enable_Auto_Flipping_With_GRN__c && invl.GRN_Line_Item__c == null){
                        invl.Exception_Reason__c = 'GRN Line Not Found';
                        invl.Is_Mismatch__c = true;
                        InvMap.put(invl.Invoice__c, 'GRN Match Fail');
                    } else if(!invConf.Enable_Auto_Flipping_With_GRN__c && invl.PO_Line_Item__c == null){
                        invl.Exception_Reason__c = 'PO Line Not Found';
                        invl.Is_Mismatch__c = true;
                        InvMap.put(invl.Invoice__c, 'PO Match Fail');
                    } else {
                        invl.Is_Mismatch__c = false;
                        if(InvMap.get(invl.Invoice__c) == null || (InvMap.get(invl.Invoice__c) != null && (InvMap.get(invl.Invoice__c) != 'GRN Match Fail' && InvMap.get(invl.Invoice__c) != 'PO Match Fail'))){
                            InvMap.put(invl.Invoice__c, 'Pass');
                        } 
                    }
                }
                
            }
            Savepoint sp = Database.setSavepoint();
            try {
                if(UtilityController.isFieldUpdateable(esmNameSpace+'Invoice_Line_Item__c',null)){
                    update scope;
                }
            } catch(System.DmlException e) {
                Database.rollback(sp);
                return;
            }
                
            if(InvMap != null && InvMap.size() > 0){
                List<Invoice__c> invList = new List<Invoice__c>();

            // -- Added by Shital Rathod | 30-05-2018 | ESMPROD-1091 | Start -- // 
            List<User_Master__c> UserMasterList = new List<User_Master__c> ();
            if (User_Master__c.getSObjectType().getDescribe().isAccessible()
                && Schema.sObjectType.User_Master__c.fields.User_Name__c.isAccessible()) {
                UserMasterList = [SELECT Id, Name FROM User_Master__c WHERE User_Name__c = :UserInfo.getUserName()];
            } else {
                throw new CustomCRUDFLSException(UtilityController.getAccessExceptionMessage(esmNamespace + 'User_Master__c'));
            }
            system.debug('------------Debug-------------------'+UserMasterList);
            // -- Added by Shital Rathod | 30-05-2018 | ESMPROD-1091 | End -- // 
                if(UtilityController.isFieldAccessible(esmNameSpace+'Invoice__c','Id,Name,Last_Modified_By__c,'+esmNameSpace+'Exception_Reason__c,'+esmNameSpace+'User_Action__c,'+esmNameSpace+'Current_State__c')){
                    invList = [SELECT Id,Name,Exception_Reason__c,User_Action__c,Current_State__c,Last_Modified_By__c FROM Invoice__c WHERE Id IN:InvMap.keySet()];
                    System.debug('*********INVList******'+invList);
                }
                
                if(invList != null && invList.size() > 0){
                    for(Invoice__c inv:InvList){
                    System.debug('*************inv'+inv.Last_Modified_By__c);
                        if(InvMap.get(inv.Id).equalsIgnoreCase('GRN Match Fail') || (inv.Exception_Reason__c != null && inv.Exception_Reason__c.contains('GRN Line Not Found'))){
                            if(inv.Exception_Reason__c != null && !inv.Exception_Reason__c.contains('GRN Line Not Found')){
                                inv.Exception_Reason__c = inv.Exception_Reason__c+';GRN Line Not Found';
                            } else {
                                inv.Exception_Reason__c = 'GRN Line Not Found';
                            }
                            inv.User_Action__c = 'Auto Match Fail';
                        } else if(InvMap.get(inv.Id).equalsIgnoreCase('PO Match Fail') || (inv.Exception_Reason__c != null && inv.Exception_Reason__c.contains('PO Line Not Found'))){
                            if(inv.Exception_Reason__c != null && !inv.Exception_Reason__c.contains('PO Line Not Found')){
                                inv.Exception_Reason__c = inv.Exception_Reason__c+';PO Line Not Found';
                            } else {
                                inv.Exception_Reason__c = 'PO Line Not Found';
                            }
                            inv.User_Action__c = 'Auto Match Fail';
                        } else {
                            inv.User_Action__c = 'Auto Match Pass';
                        }
                        // -- Added by Shital Rathod | 25-05-2018 | ESMPROD-1091 | Start -- // 
                        system.debug('------------Debug-------------------'+UserMasterList);
                        system.debug('------------Debug inv-------------------'+inv);
                        if (UserMasterList.size() > 0) {
                          inv.put(esmNamespace + 'Last_Modified_By__c', UserMasterList.get(0).Id);
                        }
                        system.debug('------------Debug inv-------------------'+inv);
                        //-- Added by Shital Rathod | 25-05-2018 | ESMPROD-1091 | end -- // 
                    }
                    system.debug('----InvMap:'+InvMap);
                    if(Invoice__c.getSObjectType().getDescribe().isUpdateable()
                    && Schema.sObjectType.Invoice__c.fields.Exception_Reason__c.isAccessible()
                    && Schema.sObjectType.Invoice__c.fields.User_Action__c.isAccessible()){
                        update invList;
                    } else {
                        throw new CustomCRUDFLSException(UtilityController.getUpdateExceptionMessage(esmNameSpace + 'Invoice__c'));
                    }
                    system.debug('------------Debug invList-------------------'+invList);
                }
            }
        }
        } catch(Exception ex){
             UtilityController.customDebug(ex, 'AutoMatchBatchable');
        }
        //---- Auto Matching | Ends ---//
    }
    
    /**
     * @description gets invoked when the batch job finishes. Place any clean up code in this method.
     * @param context contains the job ID
     */ 
    global void finish(Database.BatchableContext context) {
        system.debug('=====AutoMatchBatchable finish======');
        
    }

    global void execute(SchedulableContext objSchedulableContext) {
        system.debug('=====AutoMatchBatchable schedule======');
        AutoMatchBatchable bat = new AutoMatchBatchable();
        system.database.executebatch(bat, 50); 
    }
}