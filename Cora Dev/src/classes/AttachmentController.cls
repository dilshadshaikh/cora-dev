/*
* Authors      :  Atul Hinge
* Date created :  13/09/2017
* Purpose      :  A common controller for selectAttachment,AttachmentListView 
* Dependencies :  Email.cmp(Aura component)
* JIRA ID      :  PUX-335,PUX-266,PUX-265,PUX-252
* -----------------------------------------------------------
* Modifications: 
*        Date:  
*        Purpose of modification:  
*        Method/Code segment modified:    
*/

public class AttachmentController {
    /*
        Authors: Atul Hinge
        Purpose: To get a list of Attachment
        Dependencies:   AttachmentListView.cmp.   
    */
    @AuraEnabled
    public static Map<String,Object> getAttachments(String caseId) {
        Map<String,Object> resp=new Map<String,Object>();
        String errorMessage='Exception has occurred.';
        List<ContentDocumentLink> cdl = CaseService.getAttachmentLinks(caseId);
            resp.put('attachments',cdl);
        return resp;
    }
    
    
    
      @AuraEnabled
    public static Map<String,Object> getAttachmentsESM(String caseId) {
     System.debug('getAttachments');
        Map<String,Object> resp=new Map<String,Object>();
        List<ContentDocumentLink> contentDocumentLinks=new List<ContentDocumentLink>();
       
        try{
        
    
            if (UtilityController.isFieldAccessible('ContentDocumentLink',null))
            {
               contentDocumentLinks=[SELECT  ContentDocument.Owner.Name,ContentDocument.LatestPublishedVersion.PathOnClient,ContentDocument.LatestPublishedVersion.Flag__c,ContentDocument.LatestPublishedVersion.Is_Primary_Doc__c,ContentDocument.Id,ContentDocument.Title,ContentDocument.FileType,ContentDocument.LatestPublishedVersionId, LinkedEntityId, Id,ContentDocument.LastModifiedDate,ContentDocument.FileExtension,ContentDocument.ContentSize FROM ContentDocumentLink where LinkedEntityId=:caseId order by SystemModstamp desc];

            }
        
        } catch (Exception e) {
           System.debug('msg :'+e.getMessage());
           System.debug('trace :'+e.getStackTraceString());
            
         }
           System.debug('attachments : '+contentDocumentLinks);  
        resp.put('attachments',contentDocumentLinks);
        return resp;
    }
    
 /*    @AuraEnabled
    public static Map<String,Object> insertData(String primaryDocJson, List<ContentVersion> NewAttachments, String parentId){
     System.debug('parentId'+parentId);
     
     System.debug('attachments'+NewAttachments.size());
     System.debug('primaryDocJson '+primaryDocJson);

    if (primaryDocJson!=null && primaryDocJson!='')
    {
         Map<String,Boolean> content= (Map<String,Boolean>)JSON.deserialize(primaryDocJson, Map<String,Boolean>.class);
        system.debug('#####'+content);
        List<ContentVersion> cv=new List<ContentVersion>();

       for(string s:content.keySet()){
       System.debug('flag '+content.get(s));
     
            cv.add(new ContentVersion(Id=s,Is_Primary_Doc__c=content.get(s)));
        }
         
            if (UtilityController.isFieldUpdateable('ContentVersion',null))
            {
             update cv;
            }
       
    }
    
    List<ContentDocumentLink> contentDocumentLinkList=new List<ContentDocumentLink>();
    
    
         for(Integer i=0;i<NewAttachments.size();i++)
         {
            System.debug('attachments[i].ContentDocumentId'+NewAttachments[i].ContentDocumentId);
            contentDocumentLinkList.add(new ContentDocumentLink(
           ContentDocumentId=NewAttachments[i].ContentDocumentId,
           LinkedEntityId=parentId,
           ShareType='V',
            Visibility='AllUsers'));
      }
            System.debug('ContentDocumentLinks  before'+ contentDocumentLinkList);
            
        if (UtilityController.isFieldUpdateable('ContentDocumentLink',null) && UtilityController.isFieldCreateable('ContentDocumentLink',null))
        {
        upsert contentDocumentLinkList;
        }
            
            System.debug('ContentDocumentLinks  after'+ contentDocumentLinkList);

            return getAttachments(parentId);
    }
     */
     
     
      @AuraEnabled
    public static Map<String,Object> deleteAttachmentsESM(List<String> docId,String caseId) {
      
        try{
            if (UtilityController.isDeleteable('ContentDocument',null))
            {
             delete [SELECT Id FROM ContentDocument where LatestPublishedVersionId In :docId];
            }
           
          
        }catch (Exception e) {
           
            ExceptionHandlerUtility.writeException('Attachment', e.getMessage(), e.getStackTraceString());
        } 
        return getAttachments(caseId);
       
    }
    
    
    @AuraEnabled
    public static Map<String,Object> updateFlagOnAttachments(String contentJson){
        Map<String,Object> resp=new Map<String,Object>();
        system.debug('#####'+contentJson);
        Map<String,String> content= (Map<String,String>)JSON.deserialize(contentJson, Map<String,String>.class);
        system.debug('#####'+content);
        List<ContentVersion> cv=new List<ContentVersion>();
        for(string s:content.keySet()){
            cv.add(new ContentVersion(Id=s,flag__c=content.get(s)));
        }
        update cv;
        return resp;
    }
    /*
        Authors: Atul Hinge
        Purpose: To get delete Attachment
        Dependencies:   AttachmentListView.cmp   
    */
    @AuraEnabled
     public static Map<String,Object> deleteAttachments(List<String> docId,String caseId) {
        Map<String,Object> resp=new Map<String,Object>();
        String errorMessage='The following exception has occurred: ';
        try{
            delete [SELECT Id FROM ContentDocument where LatestPublishedVersionId In :docId];
            resp.put('success',true);
            resp.put('attachments', CaseService.getAttachmentLinks(caseId));
        }catch (Exception e) {
            resp.put('error', errorMessage+e.getMessage());
            ExceptionHandlerUtility.writeException('Attachment', e.getMessage(), e.getStackTraceString());
        } 
        return resp;
       
    }
}