public class triggerHelper {
    public static void insertUpdateHistory(List<sObject> newValues, List<sObject> oldValues, Boolean isInsert, Boolean isUpdate,
                                           Map<Id, sObject> newMap, Map<Id, sObject> oldMap, String childobjectname)
    {
        try
        {
            Boolean isRecHistoryEnable = false;
            System_Configuration__c sc = [select Value__c from System_Configuration__c where Module__c = 'Custom_Record_History' limit 1];
            if (sc.Value__c !=null)
            {
                for(String valSel :sc.Value__c.split(',')){
                    if (valSel.equalsIgnoreCase(childobjectname))
                    {
                        isRecHistoryEnable = true;
                    }
                }
            }
            if (isRecHistoryEnable) 
            {
                List<Record_History__c> recordHistoryList = new List<Record_History__c> ();
                List<sObject> recordHistoryUpdateList = new List<sObject> ();
                List<sObject> recordHistoryDeleteList = new List<sObject> ();
                String esmNameSpace = UtilityController.esmNameSpace;
                Set<String> fieldSet = new Set<String> ();
                Map<String, String> labelMap = new Map<String, String> ();
                Map<String, String> fieldAPIMap = new Map<String, String> ();

                List<Schema.FieldSetMember> fieldSetMemberList = CommonFunctionController.getFieldsFromFieldSet(childobjectname, esmNameSpace + 'Custom_Record_History_Fields');
                System.debug('fieldSetMemberList16-->' + fieldSetMemberList);
                if (fieldSetMemberList != null && fieldSetMemberList.size() > 0) {
                    for (Schema.FieldSetMember fldMember : fieldSetMemberList) {
                        if ((fldMember.getFieldPath() != 'Name') && (!String.ValueOf(fldMember.getType()).containsIgnoreCase('REFERENCE'))) {
                            fieldSet.add(fldMember.getFieldPath().toLowerCase());
                            labelMap.put(fldMember.getFieldPath().toLowerCase(), fldMember.getLabel());
                            fieldAPIMap.put(fldMember.getFieldPath().toLowerCase(), String.valueof(fldMember.getFieldPath()));
                        }
                    }
                    //-- Add by Rahul(ESMPROD-78)(15-09-2015) -- inserting record history -- End --//                
                    system.debug('------------History--------'+newValues);
                    
                    system.debug('------------History--------'+oldValues);
                    sObject oldRecHist;
                    Record_History__c recHistory;
                    for (sObject obj : newValues) {
                        system.debug('------------History--------'+fieldSet);
                        for (string s : fieldSet) {
                            if (isInsert) {
                                System.debug('ininsert');
                                if (obj.get(s) != null) {
                                    recHistory = new Record_History__c();
                                    recHistory.User__c = String.valueOf(obj.get(esmNameSpace+'Last_Modified_By__c'));
                                    recHistory.CreatedDate__c = datetime.now();
                                    recHistory.FieldLabel__c = Labelmap.get(s);
                                    recHistory.FieldName__c = fieldAPIMap.get(s);
                                    recHistory.NewValue__c = String.valueOf(obj.get(s));
                                    recHistory.oldValue__c = null;
                                    recHistory.put(esmNameSpace + childobjectname, obj.Id);

                                    //recordHistoryList.add(recHistory);

                                    if (TriggerHelperClass.firstRecHist.isEmpty()) {
                                        System.debug('recHistory5710' + recHistory);
                                        recordHistoryList.add(recHistory);
                                    } else {
                                        oldRecHist = TriggerHelperClass.firstRecHist.get(fieldAPIMap.get(s) + '~' + String.valueOf(obj.Id));
                                        //first time history is captured
                                        if (oldRecHist != null) {
                                            System.debug('oldRecHist45' + oldRecHist);
                                            if (oldRecHist.get(esmNameSpace + 'NewValue__c') != String.valueOf(obj.get(s))) {
                                                oldRecHist.put(esmNameSpace + 'NewValue__c', String.valueOf(obj.get(s)));
                                                recordHistoryUpdateList.add(oldRecHist);
                                            }
                                            //this is new history after workflow
                                        } else {
                                            System.debug('recHistory5711' + recHistory);
                                            recordHistoryList.add(recHistory);
                                        }
                                    }
                                }
                            } else if (isUpdate) {
                                System.debug('updat334e');
                                System.debug('update : '+newMap.get(obj.id).get(s)+':'+oldMap.get(obj.id).get(s));
                                System.debug('update : '+!TriggerHelperClass.firstRecHist.isEmpty());
                                if (newMap.get(obj.id).get(s) != oldMap.get(obj.id).get(s) || !TriggerHelperClass.firstRecHist.isEmpty()) {
                                    recHistory = new Record_History__c();
                                    recHistory.CreatedDate__c = datetime.now();
                                    recHistory.User__c = String.valueOf(obj.get(esmNameSpace+'Last_Modified_By__c'));
                                    recHistory.FieldLabel__c = Labelmap.get(s);
                                    recHistory.FieldName__c = fieldAPIMap.get(s);
                                    recHistory.NewValue__c = newMap.get(obj.id).get(s) != null ? String.valueOf(newMap.get(obj.id).get(s)) : null;
                                    System.debug('newMap--->' + newMap);
                                    System.debug('oldMap---->' + oldMap);
                                    recHistory.oldValue__c = oldMap.get(obj.id).get(s) != null ? String.valueOf(oldMap.get(obj.id).get(s)) : null;
                                    recHistory.put(esmNameSpace + childobjectname, obj.Id);

                                    if (TriggerHelperClass.firstRecHist.isEmpty()) {
                                        System.debug('recHistory5741' + recHistory);
                                        recordHistoryList.add(recHistory);
                                    }
                                    if (!TriggerHelperClass.firstRecHist.isEmpty()) {
                                        //this is new history after workflow
                                        oldRecHist = TriggerHelperClass.firstRecHist.get(fieldAPIMap.get(s) + '~' + String.valueOf(obj.Id));
                                        if (oldRecHist != null) {
                                            recordHistoryDeleteList.add(oldRecHist);
                                            recHistory.put(esmNameSpace + 'oldValue__c', oldRecHist.get(esmNameSpace + 'OldValue__c'));

                                            if (recHistory.get(esmNameSpace + 'oldValue__c') != recHistory.get(esmNameSpace + 'NewValue__c')) {
                                                recordHistoryList.add(recHistory);
                                            }
                                        } else {
                                            if (Trigger.newMap.get(obj.id).get(s) != Trigger.oldMap.get(obj.id).get(s)) {
                                                recordHistoryList.add(recHistory);
                                            }
                                        }
                                    }
                                }
                            }


                        }
                    }

                    ///add update here
                    System.debug('recordHistoryListfinal---->' + recordHistoryList);
                    System.debug('recordHistoryUpdateListfinal---->' + recordHistoryUpdateList);
                    Database.SaveResult[] srList;
                    if (recordHistoryList.Size() > 0) {
                        System.debug('recordHistoryListfinalEnter');
                        if (UtilityController.isFieldCreateable(esmNameSpace + 'Record_History__c', null)) {
                            System.debug('recordhistooryiscreatble');
                            srList = database.insert(recordHistoryList, false);
                        } else {
                            System.debug('recordhistooryisnotcreatble');
                        }

                        for (Record_History__c recHist : recordHistoryList) {
                            TriggerHelperClass.firstRecHist.put(recHist.get(esmNameSpace + 'FieldName__c') + '~' + String.valueOf(recHist.get(esmNameSpace + childobjectname)), recHist);
                        }
                    }
                    if (recordHistoryUpdateList.Size() > 0) {
                        if (UtilityController.isFieldUpdateable(esmNameSpace + 'Record_History__c', null)) {
                            database.update(recordHistoryUpdateList, false);
                        }
                    }

                    if (recordHistoryDeleteList.Size() > 0) {
                        if (Record_History__c.getSObjectType().getDescribe().isDeletable())
                        {
                            database.delete(recordHistoryDeleteList, false);
                        } else
                        {
                            throw new CustomCRUDFLSException('Insufficient delete rigts');
                        }
                    }
                    /*if (srList != null)
                      {
                      for (Database.SaveResult sr : srList) {
                      if (sr.isSuccess()) {
                      // Operation was successful, so get the ID of the record that was processed
                      System.debug('Successfully inserted account. Account ID: ' + sr.getId());
                      }
                      else {
                      // Operation failed, so get all errors                
                      for (Database.Error err : sr.getErrors()) {
                      System.debug('The following error has occurred.');
                      System.debug(err.getStatusCode() + ': ' + err.getMessage());
                      System.debug('Account fields that affected this error: ' + err.getFields());
                      }
                      }
                      }
                      }*/

                }
            }
        }
        catch(Exception e)
        {
            System.debug('exception545--->' + e + '---' + e.getLineNumber());
            throw new CustomCRUDFLSException(e);
        }
    }
}