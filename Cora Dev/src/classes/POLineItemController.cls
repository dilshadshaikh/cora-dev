public class POLineItemController {
	@AuraEnabled
	public static EsmHelper.dataHelper getPOLineDataServer(String strObjectName, String jsonObj, String objId, String poFlip) {
		EsmHelper.dataHelper dh = new EsmHelper.dataHelper();
		try
		{
			List<EsmHelper.Columns> cols = UtilityController.getHelperColumnsForWrapper(jsonObj);
			String selectedRows = UtilityController.getAllSelectedFields(cols);
			String whereClause = 'where Invoice__c = :objId and Invoice__c!=null';
			String query = 'SELECT Purchase_Order__c FROM Invoice_PO_Detail__c ' + whereClause;
			List<Invoice_PO_Detail__c> invPO = new List<Invoice_PO_Detail__c> ();
			Set<String> poList = new Set<String> ();
			if (poFlip != null) {
				for (String pol : poFlip.split(',')) {
					poList.add(pol);
				}
			} else {
				invPO = Database.query(query);
				for (Invoice_PO_Detail__c pol : invPO) {
					poList.add(pol.Purchase_Order__c);
				}
			}
			whereClause = ' Purchase_Order__c = :poList';
			query = CommonFunctionController.getCreatableFieldsSOQL(strObjectName, whereClause, true);
			List<sObject> objList = new List<sObject> ();
			if (UtilityController.isFieldAccessible(strObjectName, selectedRows))
			{
				objList = Database.query(query);
			}
			dh = UtilityController.getWrapperFromJson(objList, strObjectName, cols);
		}
		catch(Exception e)
		{
			dh.errmsg = ExceptionHandlerUtility.customDebug(e, 'UtilityController');
		}
		return dh;
	}
	@AuraEnabled
	public static EsmHelper.dataHelper getPOLineFromPODataServer(String strObjectName, String objId) {
		Set<String> poset = new Set<String> ();
		EsmHelper.dataHelper dh = new EsmHelper.dataHelper();
		try {
			for (String pol : objId.split(',')) {
				poset.add(pol);
			}
			String whereClause = ' Purchase_Order__c = :poset';
			String query = CommonFunctionController.getSOQLQuery(strObjectName) + ' ' + whereClause;
			query = CommonFunctionController.getCreatableFieldsSOQL(strObjectName, whereClause, true);
			List<sObject> objList = new List<sObject> ();
			if (UtilityController.isFieldAccessible(strObjectName, null))
			{
				dh.DataList = Database.query(query);
			}
		}
		catch(Exception e)
		{
			dh.errmsg = ExceptionHandlerUtility.customDebug(e, 'UtilityController');
		}
		return dh;
	}
}