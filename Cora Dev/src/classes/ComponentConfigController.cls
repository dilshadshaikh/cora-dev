public class ComponentConfigController {

    public class LayoutInfo {
        public string ObjectName {
            get; Set;
        }
        public string LayoutType {
            get; Set;
        }
        public Boolean isActive {
            get; Set;
        }
        //public Boolean isReadOnly {
        //get; Set;
        //}
        public string id {
            get; Set;
        }
        public PageLayout PageLayout { get; set; }

    }
    public class PageLayout {
        public string objectName {
            get; Set;
        }
        public string objectLabel {
            get; Set;
        }
        public string objectNameSpace {
            get; Set;
        }



        public List<RelatedList> headerLevelComponents {
            get; Set;
        }

        public List<RelatedList> lineItemComponents {
            get; Set;
        }
    }

    public class Option {
        public string value { get; set; }
        public string text { get; set; }

        public Option() {
        }
        public Option(string value, string text) {
            this.value = value;
            this.text = text;
        }
    }

    public class FieldInfo {
        public string label {
            get; set;
        }
        public string apiName {
            get; set;
        }
        public string type {
            get; set;
        }
    }

    //-----------------------------------------------------Added by hitakshi start

    public class ComponentInfo {
        public string ObjectName {
            get; Set;
        }
        public string ComponentType {
            get; Set;
        }
        public string ComponentTitle {
            get; Set;
        }
        public Boolean isActive {
            get; Set;
        }

        public string ChildObject {
            get; Set;
        }
        public string id {
            get; Set;
        }
        public PageComponent PageComponent { get; set; }

        public RelatedList RelatedList{get; Set;}

    }
    public class column {

        public string name {
            get; Set;
        }
        public string label {
            get; Set;
        }
        public string reference {
            get; Set;
        }

    }
    public class PageComponent {

        public JsonConfiguration configuration {
            get; Set;
            }
        
    }

    public class JsonConfiguration {
        public Integer allowedSize {
            get; Set;
        }
        public String allowedExt {
            get; Set;
        }
        public Boolean allowAttachPrime {
            get; Set;
        }
        public String allowedExtForPrime {
            get; Set;
        }

    }
    public class RelatedList {

        public List<column> columns {
            get; Set;
        }
    }

    public class ChildObjectDetail {
        public string objectName {
            get; set;
        }
        public string label {
            get; set;
        }
        public List<CmptFieldInfo> columns {
            get; set;
        }
    }
    public class CmptFieldInfo {
        public string label {
            get; set;
        }
        public string apiName {
            get; set;
        }
        public string type {
            get; set;
        }
        public List<String> ReferenceTo {
            get; set;
        }
    }

    @RemoteAction
    public static String saveComponent(string cmpname) {
        List<Option> options = new List<Option> ();
        System.debug('cmpname' + cmpname);
        String cmptype = cmpname;
        Component_Config__c cmpconfig = new Component_Config__c();
        System.debug('cmptype' + cmptype);

        cmpconfig.Component_Type__c = cmptype;
        if (cmptype == 'Attachment')
        {
            cmpconfig.ChildObjectName__c = 'Attachment';
            PageComponent jsonField = new PageComponent();
            // jsonField.catagory = cmptype;
            jsonField.configuration = new JsonConfiguration();
            jsonField.configuration.allowedSize = 0;
            jsonField.configuration.allowedExt = '';
            jsonField.configuration.allowAttachPrime = false;
            jsonField.configuration.allowedExtForPrime = '';
            System.debug('jsonField:' + jsonField);

            cmpconfig.Config_Json__c = JSON.serialize(jsonField);
            System.debug('cmpconfig:' + cmpconfig);
        }
        else if (cmptype == 'InteractionHistory') {
            cmpconfig.ChildObjectName__c = 'CaseManager__c';
        }
        else if (cmptype == 'Route') {
            cmpconfig.ChildObjectName__c = 'Invoice__c';
        }
        else if (cmptype == 'RecordHistory') {
            cmpconfig.ChildObjectName__c = 'Record_History__c';
        }
        else if (cmptype == 'QualityCheck') {
            cmpconfig.ChildObjectName__c = 'QualityCheck__c';
        }
		else if (cmptype == 'VendorInformation') {
            cmpconfig.ChildObjectName__c = 'Vendor_Maintenance__c';
        }

        System.debug('cmptype' + cmptype);

        if (cmptype != 'RelatedList')
        {
            string childobj = cmpconfig.ChildObjectName__c;
            //System.debug('childobj' + childobj);
            //List<Component_Config__c> cmptconfiglst = new List<Component_Config__c> ();
            //cmptconfiglst = [SELECT Id, Name, Object_Name__c, ChildObjectName__c, Component_Type__c, IsRead_only__c, IsActive__c, Title__c, Config_Json__c FROM Component_Config__c where Component_Type__c = :cmptype and ChildObjectName__c = :childobj];
            //System.debug('cmptconfiglst' + cmptconfiglst);
            //System.debug('cmptconfiglst.size()' + cmptconfiglst.size());
            //if (cmptconfiglst.size() >= 1)
            //{
                //return 'Fail';
            //}
            //else
            //{
                insert cmpconfig;
                return 'SUCCESS';
            
        }
        else
        {
            insert cmpconfig;
            return 'SUCCESS';
        }


    }

    @RemoteAction
    public static List<CmptFieldInfo> getObjectFields(string objectName) {


        List<CmptFieldInfo> fieldNames = new List<CmptFieldInfo> ();

        Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
        for (Schema.SObjectField sfield : fieldMap.Values())
        {
            schema.describefieldresult dfield = sfield.getDescribe();

            string type = String.valueOf(dfield.getType());
            CmptFieldInfo fieldInfo = new CmptFieldInfo();
            fieldInfo.label = dfield.getLabel();
            fieldInfo.apiName = dfield.getName();
            fieldInfo.type = type;

            fieldInfo.ReferenceTo = new List<String> ();
            //System.debug('reference to'+dfield.getReferenceTo()+','+dfield.getReferenceTo().isEmpty());
            List<Schema.sObjectType> reference = new List<Schema.sObjectType> ();
            reference = dfield.getReferenceTo();
            System.debug('reference to' + reference);
            if (!reference.isEmpty())
            {
                System.debug('here');
                for (Integer i = 0; i < reference.size(); i++)
                {
                    System.debug('here' + String.valueOf(reference[i]));
                    fieldInfo.ReferenceTo.add(String.valueOf(reference[i]));
                }
            }
            fieldNames.add(fieldInfo);

        }
        System.debug(fieldNames);
        return fieldNames;
    }
    @RemoteAction
    public static List<ComponentInfo> getAllComponents() {
        try {
            System.debug('---');
            List<ComponentInfo> cmpinfo = new List<ComponentInfo> ();

            List<Component_Config__c> cmpconfig = [SELECT Id, Name, Object_Name__c, ChildObjectName__c, Component_Type__c, IsActive__c, Title__c, Config_Json__c FROM Component_Config__c order by Object_Name__c];
            System.debug('cmpconfig' + cmpconfig);

            for (Component_Config__c cmpconfigObj : cmpconfig) {
                ComponentInfo cmpObj = new ComponentInfo();
                if (cmpconfigObj.Config_Json__c != null && cmpconfigObj.Component_Type__c != 'RelatedList')             
                {
                    System.debug('cmpconfigObj.Config_Json__c' + cmpconfigObj.Config_Json__c);
                    PageComponent jsonField = (PageComponent) System.JSON.deserialize(cmpconfigObj.Config_Json__c, PageComponent.class);

                    cmpObj.PageComponent = jsonField;
                    System.debug('cmpObj.PageComponent' + cmpObj.PageComponent);
                }
                else if(cmpconfigObj.Config_Json__c != null && cmpconfigObj.Component_Type__c == 'RelatedList'){
                System.debug('cmpconfigObj.Config_Json__c' + cmpconfigObj.Config_Json__c);
                    RelatedList jsonField = (RelatedList) System.JSON.deserialize(cmpconfigObj.Config_Json__c, RelatedList.class);

                    cmpObj.RelatedList = jsonField;
                    System.debug('cmpObj.RelatedList' + cmpObj.RelatedList);
                }
                cmpObj.ObjectName = cmpconfigObj.Object_Name__c;
                cmpObj.ComponentType = cmpconfigObj.Component_Type__c;
                cmpObj.ComponentTitle = cmpconfigObj.Title__c;
                cmpObj.isActive = cmpconfigObj.IsActive__c;
                cmpObj.ChildObject = cmpconfigObj.ChildObjectName__c;
                //cmpObj.isReadOnly=cmpconfigObj.IsRead_only__c;
                cmpObj.id = cmpconfigObj.id;
                cmpinfo.add(cmpObj);
            }
            System.debug('cmpinfo' + cmpinfo);
            return cmpinfo;
        }
        catch(Exception ex) {
            System.debug('Exception:' + ex);
            //ExceptionHandlerUtility.writeException('TATRuleController', ex.getMessage(), ex.getStackTraceString());
            return null;
        }
    }

    @RemoteAction
    public static void removeComponent(string id) {
        List<Component_Config__c> cmpdel = [select id from Component_Config__c where id = :id];
        System.debug('-----cmpdel-----' + cmpdel);

        delete cmpdel;
    }

    @RemoteAction
    public static void changecmpState(string recid) {
        List<Component_Config__c> qcRule = [select id, IsActive__c from Component_Config__c where id = :recid];
        System.debug('-----qcRule is active-----' + qcRule);
        for (Component_Config__c cmpconfig : qcRule)
        {
            Boolean isact = cmpconfig.IsActive__c;

            if (isact == true)
            {
                cmpconfig.IsActive__c = false;
            }
            else if (isact == false)
            {
                cmpconfig.IsActive__c = true;
            }

            System.debug('cmpconfig' + cmpconfig);
            update cmpconfig;
        }


    }

    @RemoteAction
    public static List<ComponentConfigHelper.objectsAvailaible> getAllObjects() {
        List<ComponentConfigHelper.objectsAvailaible> objects = new List<ComponentConfigHelper.objectsAvailaible> ();
        Schema.DescribeFieldResult fieldResult = Component_Config__c.Object_Name__c.getDescribe();
        System.debug('fieldResult :' + fieldResult);
        List<Schema.PicklistEntry> plvalues = fieldResult.getPicklistValues();
        ComponentConfigHelper.objectsAvailaible obj = new ComponentConfigHelper.objectsAvailaible();
        for (Schema.PicklistEntry f : plvalues)
        {
            obj = new ComponentConfigHelper.objectsAvailaible();
            obj.label = f.getLabel();
            obj.apiName = f.getValue();
            objects.add(obj);

        }
        System.debug('objects :' + objects);
        return objects;
    }
    @RemoteAction
    public static List<ComponentConfigHelper.objectsAvailaible> getAllChildObjects() {

        List<ComponentConfigHelper.objectsAvailaible> objects = new List<ComponentConfigHelper.objectsAvailaible> ();
        Schema.DescribeFieldResult fieldResult = Component_Config__c.ChildObjectName__c.getDescribe();
        //System.debug('fieldResult :' + fieldResult);
        List<Schema.PicklistEntry> plvalues = fieldResult.getPicklistValues();
        ComponentConfigHelper.objectsAvailaible obj = new ComponentConfigHelper.objectsAvailaible();
        for (Schema.PicklistEntry f : plvalues)
        {
            obj = new ComponentConfigHelper.objectsAvailaible();
            obj.label = f.getLabel();
            obj.apiName = f.getValue();
            if (obj.apiName == 'GRN_Line_Item__c' || obj.apiName == 'GRN__c' || obj.apiName == 'PO_Line_Item__c' || obj.apiName == 'Purchase_Order__c')
            {
                objects.add(obj);
            }


        }
        //System.debug('objects :' + objects);


        return objects;

    }

    @RemoteAction
    public static string editComponentChange(ComponentInfo cmpt, String sobj) {
        try {
            System.debug('cmpt' + cmpt);
            System.debug('sobj' + sobj);

            Component_Config__c componentConfig = new Component_Config__c();
            System.debug('cmpt.PageComponent' + cmpt.PageComponent);
            System.debug('cmpt.PageComponent.configuration');

            if (cmpt.ComponentType != 'RelatedList')
            {
                if (cmpt.PageComponent != null)
                {
                    componentConfig.Config_Json__c = JSON.serialize(cmpt.PageComponent);
                }


                //componentConfig.IsRead_only__c = cmpt.isReadOnly;
                componentConfig.IsActive__c = cmpt.isActive;
                componentConfig.Object_Name__c = sobj;
                componentConfig.Title__c = cmpt.ComponentTitle;
                componentConfig.Component_Type__c = cmpt.ComponentType;
                componentConfig.Id = cmpt.Id;
                System.debug('componentConfig' + componentConfig);

                update componentConfig;
                return 'SUCCESS';

            }

            else
            {
                String cmpttype = cmpt.ComponentType;
                System.debug('cmpttype' + cmpttype);
                String cmptchldobj = cmpt.ChildObject;
                String cmptobj = sobj;
                System.debug('cmptchldobj' + cmptchldobj);
                List<Component_Config__c> cmptconfiglst = new List<Component_Config__c> ();
                cmptconfiglst = [SELECT Id, Name, Object_Name__c, ChildObjectName__c, Component_Type__c, IsRead_only__c, IsActive__c, Title__c, Config_Json__c FROM Component_Config__c where Component_Type__c = :cmpttype and ChildObjectName__c = :cmptchldobj];
                System.debug('cmptconfiglst' + cmptconfiglst);
                System.debug('cmptconfiglst.size()' + cmptconfiglst.size());
                if (cmptconfiglst.size() >= 1)
                {
                    return 'Fail';
                }
                else
                {
                    //componentConfig.IsRead_only__c = cmpt.isReadOnly;
                    componentConfig.IsActive__c = cmpt.isActive;
                    componentConfig.Object_Name__c = sobj;
                    componentConfig.Title__c = cmpt.ComponentTitle;
                    componentConfig.Component_Type__c = cmpt.ComponentType;
                    componentConfig.ChildObjectName__c = cmpt.ChildObject;
                    componentConfig.Id = cmpt.Id;
                    System.debug('componentConfig' + componentConfig);

                    update componentConfig;
                    return 'SUCCESS';
                }


            }
        }
        catch(Exception ex) {
            System.debug(ex);
            //ExceptionHandlerUtility.writeException('TATRuleController', ex.getMessage(), ex.getStackTraceString());
            return '';
        }

    }
    @RemoteAction
    public static string editComponentRelatedList(ComponentInfo cmpt, String sobj, RelatedList clmnlist) {
        try {
            System.debug('cmpt' + cmpt);
            System.debug('sobj' + sobj);
            System.debug('clmnlist' + clmnlist);
            Component_Config__c componentConfig = new Component_Config__c();
            System.debug('cmpt.PageComponent' + cmpt.PageComponent);
            System.debug('cmpt.PageComponent.configuration');


            String cmpttype = cmpt.ComponentType;
            System.debug('cmpttype' + cmpttype);
            String cmptchldobj = cmpt.ChildObject;
            String cmptobj = sobj;
            System.debug('cmptchldobj' + cmptchldobj);
            List<Component_Config__c> cmptconfiglst = new List<Component_Config__c> ();
            cmptconfiglst = [SELECT Id, Name, Object_Name__c, ChildObjectName__c, Component_Type__c, IsRead_only__c, IsActive__c, Title__c, Config_Json__c FROM Component_Config__c where Component_Type__c = :cmpttype and ChildObjectName__c = :cmptchldobj and Id !=:cmpt.Id];
            System.debug('cmptconfiglst' + cmptconfiglst);
            System.debug('cmptconfiglst.size()' + cmptconfiglst.size());
            if (cmptconfiglst.size() >= 1)
            {
                return 'Fail';
            }
            else
            {
                componentConfig.Config_Json__c = JSON.serialize(clmnlist);
                //componentConfig.IsRead_only__c = cmpt.isReadOnly;
                componentConfig.IsActive__c = cmpt.isActive;
                componentConfig.Object_Name__c = sobj;
                componentConfig.Title__c = cmpt.ComponentTitle;
                componentConfig.Component_Type__c = cmpt.ComponentType;
                componentConfig.ChildObjectName__c = cmpt.ChildObject;
                componentConfig.Id = cmpt.Id;
                System.debug('componentConfig' + componentConfig);

                update componentConfig;
                return 'SUCCESS';
            }

            //}

        }
        catch(Exception ex) {
            System.debug(ex);
            //ExceptionHandlerUtility.writeException('TATRuleController', ex.getMessage(), ex.getStackTraceString());
            return '';

        }

    }

    //----------------------------------------------Added by Hitakshi end
}