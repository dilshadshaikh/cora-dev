/*
  Authors: Niraj Prajapati
  Date created: 01/11/2017
  Purpose: This controller will get the fields as per field set configured on Quality Check Object.
  Dependencies: 
  -------------------------------------------------
  Modifications: Niraj Prajapati
  Date: 14/12/2017
  Purpose of modification: Pass Fieldset Name Dynamically for PUX-512
  Method/Code segment modified: getQualityControls

*/
public with sharing class QualityControlController {
	/*
	  Authors: Niraj Prajapati
	  Purpose: This method will be used to get QualityControls records.
	  Dependencies: QualityControl.cmp


	*/
	
	
	@AuraEnabled
	public static Map<String, Object> getQualityControls(String caseId, String fsName) {
		
		fsName = ObjectUtil.getValidFieldSetName(ObjectUtil.getWithNameSpace('QualityCheck__c'), fsName);
		Map<String, Object> resp = new Map<String, Object> ();
		String errorMessage = 'The following exception has occurred: ';
		system.debug('fsName' + fsName);
		try {
			Map<String, Object> fieldMap = new Map<String, Object> ();
			resp.put('QualityControls', CaseService.getQualityControls(caseId, fsName));

			for (Schema.FieldSetMember fld : SObjectType.QualityCheck__c.FieldSets.getMap().get(fsName).getFields()) {
				fieldMap.put(fld.getFieldPath(), fld.getLabel());

			}
            System.debug('hello fileds' +fieldMap);
			resp.put('FieldMap', fieldMap);
		} catch(Exception e) {
			resp.put('error', errorMessage + e.getMessage());
			ExceptionHandlerUtility.writeException('QualityControlController', e.getMessage(), e.getStackTraceString());
		}
		return resp;
	}
/*
	public class PageLayout {
         @AuraEnabled
		public string objectName {
			get; Set;
		}
         @AuraEnabled
		public List<LayoutSection> headerLevelComponents {
			get; Set;
		}
	}
	public class LayoutSection {
         @AuraEnabled
		public string objectName {
			get; Set;
		}
         @AuraEnabled
		public string type {
			get; Set;
		}
         @AuraEnabled
		public string title {
			get; Set;
		} 
        @AuraEnabled
		public string criteria {
			get; Set;
		}
         @AuraEnabled
		public List<column> columns {
			get; Set;
		}
         @AuraEnabled
		public integer order {
			get; Set;
		}
         @AuraEnabled
		public string RelationField {
			get; Set;
		}
         @AuraEnabled
		public boolean readonly {
			get; Set;
		}
         @AuraEnabled
		public boolean active {
			get; Set;
		}
	}
	public class column {
 @AuraEnabled
		public string name {
			get; Set;
		}
         @AuraEnabled
        public string label{
            get;set;
        }
         @AuraEnabled
		public boolean reference {
			get; Set;
		}
       
         @AuraEnabled
		public integer order {
			get; Set;
		}
	}
   @AuraEnabled
	public static List<column> parseJSON(){
        Map<String,String>invoiceFields = new Map<String,String>();
		string json='{"objectName": "Invoice__c", "headerLevelComponents": [{"objectName": "QualityCheck__c","type": "QCSampling","title": "QC Sampling","criteria": null,"order": 5,"RelationField": "Invoice__c","readonly": true,"active": true,"columns": [{"name": "VendorName__c", "reference": null,"order": 0 },{"name": "Test__c","reference": null, "order": 1 }, { "name": "VendorNumber__c","reference": null,"order": 2 }  ]   }    ]  }';
		PageLayout jsonField = (PageLayout) System.JSON.deserialize(json, PageLayout.class);

		List<column> columns=jsonField.headerLevelComponents[0].columns;
       String type='QualityCheck__c';

        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(type);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
        
        
        for (String fieldName: fieldMap.keySet()) {
         String labelName = fieldMap.get(fieldName).getDescribe().getLabel();
            
        System.debug('##Field API Name='+fieldName);// list of all field API name
        invoiceFields.put(fieldName,labelName);
      }
        System.debug('final map of invoice' +invoiceFields);
      // List<column> col=jsonField.headerLevelComponents[0].columns.name;
		System.debug(columns);
	    return columns;
	}*/
	@AuraEnabled
    public static EsmHelper.dataHelperRecord getFieldsApex(String caseId) {
		String objName = 'QualityCheck__c';
		QCHelper.QCFormResponse res=QCCalculation.getQCFormFieldInfo(caseId);
		System.debug('ResetPasswordResult'+res.qcFormFieldsetName);
		String fsname='';
		if (res.qcFormFieldsetName!='' && res.qcFormFieldsetName!= null)
		{
			fsname=res.qcFormFieldsetName;
		}else{
				 fsname='QC';
				}
		System.debug('fsname'+fsname);
		List<EsmHelper.Columns> cols =new List<EsmHelper.Columns>();
		String selectedRows = UtilityController.getAllSelectedFields(cols);
		QualityCheck__c CaseObj =new QualityCheck__c();

		Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objName);
        Schema.DescribeSObjectResult describe = targetType.getDescribe();
        Map<String, Schema.FieldSet> fsMap = describe.fieldSets.getMap();
		System.debug('fsMap'+fsMap);
        Schema.FieldSet fs = fsMap.get(fsName);
		
		List<Schema.FieldSetMember> fieldSet = fs.getFields();
        List<FieldSetMember> fset = new List<FieldSetMember>();
		for (Schema.FieldSetMember f: fieldSet) {
			EsmHelper.Columns col =new EsmHelper.Columns();
			col.name = f.getFieldPath();
			if (String.valueOf(f.getType())=='REFERENCE')
			{
				col.reference='NAME';
				col.type = 'reference';
			}
			cols.add(col);
        }
		EsmHelper.dataHelperRecord dh = UtilityController.getWrapperFromJson(CaseObj, objName, cols);
		System.debug('Dh---'+ dh);
		return dh;
    }
	@AuraEnabled
	public static Map<String, object> getQCFieldSetName(String caseId) {
		Map<String, object> response = new Map<String, object> ();
		if (caseId!=null)
		{
			Schema.SObjectType token = ((Id) caseId).getSobjectType();
			String sObjName=token.getDescribe().getName();
			response.put('qcInfo', QCCalculation.getQCFormFieldInfo(caseId));
			response.put('sObjName',sObjName);
		 System.debug('response are' +response);
			return response;
		}else{
			return null;
		}
		
	}
	@AuraEnabled
	public static String saveQualityData(QualityCheck__c CaseObj) {
		System.debug('CaseObj -- '+CaseObj);

		String startToken = ESMConstants.SUBJECT_START_IDENTIFIER;
		String endToken = ESMConstants.SUBJECT_END_IDENTIFIER;
		//String emailSubject = (String)CaseObj.get(Schema.CaseManager__c.Subject__c);
		//String emailBody = (String)CaseObj.get(Schema.CaseManager__c.Comments__c);
		
		upsert CaseObj;
		
	return 'Success';	
}
}