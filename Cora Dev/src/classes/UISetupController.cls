public class UISetupController {

	public class LayoutInfo {
		public string ObjectName {
			get; Set;
		}
		public string LayoutType {
			get; Set;
		}
		public Boolean isActive {
			get; Set;
		}
		public string id {
			get; Set;
		}
		public string LayoutSystem {
			get; Set;
		}

		public PageLayout1 PageLayout { get; set; }

	}
	public class PageLayout1 {
		public PageLayout PageLayout { get; set; }
	}
	public class PageLayout {
		public string objectName {
			get; Set;
		}
		public string objectLabel {
			get; Set;
		}
		public string objectNameSpace {
			get; Set;
		}
		public string additionalCharge {
			get; Set;
		}
		public Boolean ShowPrimaryDocument {
			get; Set;
		}
		public List<LayoutDetailSection> layoutDetailSections {
			get; Set;
		}
		public List<LayoutSection> layoutHeaderSections {
			get; Set;
		}
		public LayoutPORefSection POReference {
			get; Set;
		}
		public List<RelatedList> headerLevelComponents {
			get; Set;
		}


		public List<RelatedList> lineItemComponents {
			get; Set;
		}
	}
	public class LayoutSection {
		public LayoutHeaderSection headerSection {
			get; Set;
		}
		public LayoutAdditionalHeaderSection additionalHeaderSection {
			get; Set;
		}
		public string title {
			get; Set;
		}
		public Boolean readonly {
			get; Set;
		}
		public Boolean active {
			get; Set;
		}
	}
	public class LayoutDetailSection {
		public Criteria criteria {
			get; Set;
		}
		public string title {
			get; Set;
		}
		public Boolean readonly {
			get; Set;
		}
		public Boolean active {
			get; Set;
		}
		public List<column> columns {
			get; Set;
		}
	}
	public class LayoutHeaderSection {
		public Criteria criteria {
			get; Set;
		}
		public string title {
			get; Set;
		}
		public Boolean readonly {
			get; Set;
		}
		public Boolean active {
			get; Set;
		}
		public List<column> columns {
			get; Set;
		}
	}
	public class LayoutAdditionalHeaderSection {
		public Criteria criteria {
			get; Set;
		}
		public string title {
			get; Set;
		}
		public Boolean readonly {
			get; Set;
		}
		public Boolean active {
			get; Set;
		}
		public List<column> columns {
			get; Set;
		}
	}


	public class LayoutPORefSection {
		public Criteria criteria {
			get; Set;
		}
		public string title {
			get; Set;
		}
		public Boolean POReferenceRequired {
			get; Set;
		}
		public string searchBy {
			get; Set;
		}
		public string searchFilter {
			get; Set;
		}
	}
	public class Criteria {
		public Groups criteriagroup;
	}
	public class Groups {
		public string operator {
			get; Set;
		}
		public List<Rules> rules {
			get; Set;
		}
	}
	public class Rules {
		public Groups criteriagroup;
		public string condition {
			get; Set;
		}
		public Field field {
			get; Set;
		}
		public Map<String, String> data {
			get; Set;
		}
	}

	public class Field {

		public string apiName {
			get; Set;
		}

		public string label {
			get; Set;
		}

		public string ReferenceTo {
			get; Set;
		}

		public string type {
			get; Set;
		}
		public List<String> picklistVal {
			get; Set;
		}
	}
	//public class PickListVal {
	//public string label {
	//get; Set;
	//}
	//public string value {
	//get; Set;
	//}
	//}
	//public class LayoutSection {
	//public Criteria criteria {
	//get; Set;
	//}
	//public string title {
	//get; Set;
	//}
	//public Boolean readonly {
	//get; Set;
	//}
	//public Boolean active {
	//get; Set;
	//}
	//public List<column> columns {
	//get; Set;
	//}
	//}
	public class column {

		public string name {
			get; Set;
		}
		public string label {
			get; Set;
		}
		public string type {
			get; Set;
		}
		public string defaultValue {
			get; Set;
		}
		public boolean readonly {
			get; Set;
		}
		public boolean required {
			get; Set;
		}
		public boolean currentUserSelected {
			get; Set;
		}
		public boolean excludeCurrentUser {
			get; Set;
		}
		public Criteria requiredCriteria {
			get; Set;
		}
		public Criteria readonlyCriteria {
			get; Set;
		}
		public Criteria criteria {
			get; Set;
		}
		public string searchFilter {
			get; Set;
		}
		public string searchBy {
			get; Set;
		}
		/*public string fromfield {
		  get; Set;
		  }
		  public string tofield {
		  get; Set;
		  }*/
	}

	public class RelatedList {
		public string objectName {
			get; Set;

		}
		public string title {
			get; Set;
		}
		public boolean active {
			get; Set;
		}
		public boolean readonly {
			get; Set;
		}
		public boolean allowButton {
			get; Set;
		}
		public boolean additionalCharge {
			get; Set;
		}

		public Component Component {
			get; Set;
		}
		public Criteria criteria {
			get; Set;
		}
		public List<column> columns {
			get; Set;
		}

		public string RelationField {
			get; Set;
		}
		public String whereClause {
			get; Set;
		}
		public string groupBy {
			get; Set;
		}
		public String orderBy {
			get; Set;
		}
		public String label {
			get; Set;
		}
		public Integer index {
			get; Set;
		}
		public String type {
			get; Set;
		}
	}
	/*
	  Authors: Chandresh Koyani
	  Purpose: Wrapper class to store option like value,text.
	  Dependencies: 
	 */
	public class Option {
		public string value { get; set; }
		public string text { get; set; }

		public Option() {
		}
		public Option(string value, string text) {
			this.value = value;
			this.text = text;
		}
	}

	public class FieldInfo {
		public string label {
			get; set;
		}
		public string apiName {
			get; set;
		}
		public string type {
			get; set;
		}
		public List<String> ReferenceTo {
			get; set;
		}
		public List<String> picklistVal {
			get; Set;
		}
	}

	@RemoteAction
	public static List<Option> getObjects() {
		List<Option> options = new List<Option> ();

		Schema.DescribeFieldResult fieldResult = Layout_Configuration__c.Object_Name__c.getDescribe();
		List<Schema.PicklistEntry> pickListValues = fieldResult.getPicklistValues();
		for (Schema.PicklistEntry valueEntry : pickListValues)
		{
			options.add(new Option(valueEntry.getValue(), valueEntry.getLabel()));
		}
		return options;
	}
	@RemoteAction
	public static List<Option> getLayoutTypeValue() {
		List<Option> options = new List<Option> ();

		Schema.DescribeFieldResult fieldResult = Layout_Configuration__c.Page_Layout__c.getDescribe();
		List<Schema.PicklistEntry> pickListValues = fieldResult.getPicklistValues();
		for (Schema.PicklistEntry valueEntry : pickListValues)
		{
			options.add(new Option(valueEntry.getValue(), valueEntry.getLabel()));
		}
		return options;
	}

	@RemoteAction
	public static List<LayoutInfo> getAllLayouts() {
		try {
			List<LayoutInfo> qcRules = new List<LayoutInfo> ();

			List<Layout_Configuration__c> tatConfiges = [select id, Name, Object_Name__c, Page_Layout__c, Config_Json__c, System__c, Is_Active__c from Layout_Configuration__c order by Object_Name__c];

			for (Layout_Configuration__c tatConfigObj : tatConfiges) {
				System.debug('tatConfigObj json ' + tatConfigObj.Config_Json__c);
				PageLayout1 jsonField = (PageLayout1) System.JSON.deserialize(tatConfigObj.Config_Json__c, PageLayout1.class);
				LayoutInfo tatRuleObj = new LayoutInfo();
				System.debug('jsonField ' + jsonField);
				tatRuleObj.PageLayout = jsonField;
				//tatRuleObj.ruleName = Name;
				tatRuleObj.ObjectName = tatConfigObj.Object_Name__c;
				tatRuleObj.LayoutType = tatConfigObj.Page_Layout__c;
				System.debug('tatConfigObj.System__c' + tatConfigObj.System__c);
				tatRuleObj.LayoutSystem = tatConfigObj.System__c;
				tatRuleObj.isActive = tatConfigObj.Is_Active__c;
				tatRuleObj.id = tatConfigObj.id;
				qcRules.add(tatRuleObj);
				System.debug('qcRules' + qcRules);
			}
			return qcRules;
		}
		catch(Exception ex) {
			ExceptionHandlerUtility.writeException('UISetupController', ex.getMessage(), ex.getStackTraceString());
			return null;
		}
	}

	@RemoteAction
	public static List<ChildObjectDetail> getCurrentObjectChild(String objectName, String currentObject) {

		List<ChildObjectDetail> childObjectDetails = new List<ChildObjectDetail> ();

		Schema.DescribeSObjectResult describeSobjectResult = Schema.getGlobalDescribe().get(objectName).getDescribe();

		for (Schema.SObjectField sfield : describeSobjectResult.fields.getMap().Values())
		{
			System.debug('child ===' + sfield);
			schema.describefieldresult dfield = sfield.getDescribe();
			System.debug('String.valueOf(dfield.getType() ' + String.valueOf(dfield.getType()) + ',' + dfield.getName());
			if (String.valueOf(dfield.getType()) == 'REFERENCE' && dfield.getName().equals(currentObject)) {
				ChildObjectDetail objectDetails = new ChildObjectDetail();
				objectDetails.objectName = dfield.getName();
				objectDetails.label = dfield.getLabel();
				childObjectDetails.add(objectDetails);
			}

		}


		System.debug('childObjectDetails' + childObjectDetails);
		return childObjectDetails;
	}
	@RemoteAction
	public static List<UISetupController.ComponentConfig> getActivewidget(String objectName) {
		List<Object> widList = new List<Object> ();
		List<UISetupController.ComponentConfig> valRules = new List<UISetupController.ComponentConfig> ();

		List<Component_Config__c> cmpConfiges = [SELECT Id, IsRead_only__c, Title__c, ChildObjectName__c, Config_Json__c, Component_Type__c, Object_Name__c, IsActive__c FROM Component_Config__c where Object_Name__c = :objectName and IsActive__c = :true];
		System.debug('cmpConfiges' + cmpConfiges);
		for (Component_Config__c cmpConfig : cmpConfiges) {
			UISetupController.ComponentConfig compCong = new UISetupController.ComponentConfig();
			if (cmpConfig.Component_Type__c != 'RelatedList')
			{
				System.debug('JSON c ' + cmpConfig.Config_Json__c + ',' + (cmpConfig.Config_Json__c != null));
				if (cmpConfig.Config_Json__c != null)
				{
					System.debug('jsoncc' + cmpConfig.Config_Json__c);
					System.debug('jsoncc1' + System.JSON.deserialize(cmpConfig.Config_Json__c, JSONValue.class));
					compCong.ChildObjectName = cmpConfig.ChildObjectName__c;
					compCong.JSONValue = (JSONValue) System.JSON.deserialize(cmpConfig.Config_Json__c, JSONValue.class);
				}
				System.debug('compCong');
				System.debug(compCong);
				compCong.Title = cmpConfig.Title__c;
				compCong.isActive = cmpConfig.IsActive__c;
				compCong.IsReadonly = cmpConfig.IsRead_only__c;
				compCong.ObjectName = cmpConfig.ChildObjectName__c;
				compCong.ComponentType = cmpConfig.Component_Type__c;
				valRules.add(compCong);
			}
			else
			{
				Schema.DescribeSObjectResult describeSobjectResult = Schema.getGlobalDescribe().get(cmpConfig.ChildObjectName__c).getDescribe();
				Schema.DescribeSObjectResult describeSobjectObj = Schema.getGlobalDescribe().get(objectName).getDescribe();
				
				compCong.objName = describeSobjectResult.getName();
				compCong.label = describeSobjectResult.getLabel();
				compCong.columns = new List<FieldInfo> ();
				compCong.Title = cmpConfig.Title__c;
				compCong.isActive = cmpConfig.IsActive__c;
				compCong.IsReadonly = cmpConfig.IsRead_only__c;
				//	compCong.ObjectName = cmpConfig.ChildObjectName__c;
				compCong.ComponentType = cmpConfig.Component_Type__c;

				compCong.ObjectName = cmpConfig.Object_Name__c;
				compCong.ChildObjectName = cmpConfig.ChildObjectName__c;
				for (Schema.SObjectField sfield : describeSobjectResult.fields.getMap().Values())
				{
					schema.describefieldresult dfield = sfield.getDescribe();

					string type = String.valueOf(dfield.getType());
					FieldInfo fieldInfo = new FieldInfo();
					fieldInfo.label = dfield.getLabel();
					fieldInfo.apiName = dfield.getName();
					fieldInfo.type = type;

					compCong.columns.add(fieldInfo);
					System.debug('objectDetails' + compCong);
				}

				valRules.add(compCong);
				
			}
			System.debug('widList ' + valRules);
			System.debug('widList size' + valRules.size());
		}
		return valRules;
	}
	@RemoteAction
	public static string saveRule(LayoutInfo layout) {
		try {
			Layout_Configuration__c tatConfig = new Layout_Configuration__c();
			System.debug('layout.PageLayoutSSSSSS' + layout.PageLayout);
			System.debug(' JSON.serialize(layout.PageLayout)' + JSON.serialize(layout.PageLayout));
			tatConfig.Config_Json__c = JSON.serialize(layout.PageLayout);
			tatConfig.Is_Active__c = layout.isActive;
			tatConfig.Object_Name__c = layout.ObjectName;
			tatConfig.Page_Layout__c = layout.LayoutType;
			tatConfig.System__c = layout.LayoutSystem;
			if (!string.isEmpty(layout.id)) {
				tatConfig.id = layout.id;
			}
			System.debug('tatConfig' + tatConfig);
			upsert tatConfig;
			return 'SUCCESS';
		}
		catch(Exception ex) {
			ExceptionHandlerUtility.writeException('UISetupController', ex.getMessage(), ex.getStackTraceString());
			return '';
		}

	}

	public class JSONValue {
		public Component1 configuration { get; set; }
	}
	public class Component1 {
		public Integer allowedSize { get; set; }
		public string allowedExt { get; set; }
		public Boolean allowAttachPrime { get; set; }
		public string allowedExtForPrime { get; set; }
	}
	public class Configuration {
		public Integer allowedSize { get; set; }
		public string allowedExt { get; set; }
		public Boolean allowAttachPrime { get; set; }
		public string allowedExtForPrime { get; set; }
	}
	public class Component {
		public Configuration configuration { get; set; }

	}
	public class ComponentConfig {

		public JSONValue JSONValue { get; set; }
		public string ChildObjectName { get; set; }
		public String ConfigJson { get; set; }
		public string Title { get; set; }
		public boolean isActive { get; set; }
		public boolean IsReadonly { get; set; }
		public string ObjectName { get; set; }
		public string ComponentType { get; set; }
		public string objName {
			get; set;
		}
		public string label {
			get; set;
		}
		public List<FieldInfo> columns {
			get; set;
		}
	}

	@RemoteAction
	public static Boolean checkLayoutTypeObj(string LayoutType, String ObjectName, string layoutSystem, String idVal) {
		List<Layout_Configuration__c> existLayout = [select System__c, Id from Layout_Configuration__c where Page_Layout__c = :LayoutType and Object_Name__c = :ObjectName and System__c = :layoutSystem limit 1];
		if (existLayout.size() == 1 && !idVal.equals(existLayout.get(0).Id))
		{
			return false;
		}

		return true;

	}
	@RemoteAction
	public static List<FieldInfo> getObjectFields(string objectName) {

		System.debug('object Name' + objectName);
		List<FieldInfo> fieldNames = new List<FieldInfo> ();

		Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
		System.debug('fieldMap' + fieldMap);
		System.debug('fieldMap value' + fieldMap.values());
		for (Schema.SObjectField sfield : fieldMap.Values())
		{
			schema.describefieldresult dfield = sfield.getDescribe();
			System.debug('dfield data' + dfield);
			string type = String.valueOf(dfield.getType());
			FieldInfo fieldInfo = new FieldInfo();
			fieldInfo.label = dfield.getLabel();
			fieldInfo.apiName = dfield.getName();
			fieldInfo.type = type.toLowerCase();

			fieldInfo.ReferenceTo = new List<String> ();
			System.debug('reference to' + dfield.getReferenceTo() + ',' + dfield.getReferenceTo().isEmpty());
			List<Schema.sObjectType> reference = new List<Schema.sObjectType> ();
			reference = dfield.getReferenceTo();
			System.debug('reference to' + reference);
			if (!reference.isEmpty())
			{
				System.debug('here');
				for (Integer i = 0; i < reference.size(); i++)
				{
					System.debug('here' + String.valueOf(reference[i]));
					fieldInfo.ReferenceTo.add(String.valueOf(reference[i]));
				}
			}
			fieldInfo.picklistVal = new List<String> ();
			if (type == 'PICKLIST') {
				for (Integer j = 0; j < dfield.getPicklistValues().size(); j++)
				{
					fieldInfo.picklistVal.add(dfield.getPicklistValues() [j].getLabel());
				}
				System.debug('fieldInfo.picklistVal ' + fieldInfo.picklistVal);
			}

			fieldNames.add(fieldInfo);

		}
		System.debug(fieldNames);
		return fieldNames;
	}
	@RemoteAction
	public static void removeLayout(string id) {
		List<Layout_Configuration__c> qcRule = [select id from Layout_Configuration__c where id = :id];
		delete qcRule;
	}


	public class ChildObjectDetail {
		public string objectName {
			get; set;
		}
		public string label {
			get; set;
		}
		public List<FieldInfo> columns {
			get; set;
		}
	}
	@RemoteAction
	public static List<String> getAllPickListValue(String fieldName, string objectName) {
		List<String> childObjectDetails = new List<String> ();
		System.debug('we aere hee');
		return null;
	}
	@RemoteAction
	public static List<ComponentConfigHelper.objectsAvailaible> getsystem() {

		List<ComponentConfigHelper.objectsAvailaible> objects = new List<ComponentConfigHelper.objectsAvailaible> ();
		Schema.DescribeFieldResult fieldResult = Layout_Configuration__c.System__c.getDescribe();
		//System.debug('fieldResult :' + fieldResult);
		List<Schema.PicklistEntry> plvalues = fieldResult.getPicklistValues();
		ComponentConfigHelper.objectsAvailaible obj = new ComponentConfigHelper.objectsAvailaible();
		for (Schema.PicklistEntry f : plvalues)
		{
			obj = new ComponentConfigHelper.objectsAvailaible();
			obj.label = f.getLabel();
			obj.apiName = f.getValue();
			objects.add(obj);

		}
		return objects;

	}
	@RemoteAction
	public static List<ChildObjectDetail> getChildObjectDetails(string objectName) {
		List<ChildObjectDetail> childObjectDetails = new List<ChildObjectDetail> ();

		Schema.DescribeSObjectResult describeSobjectResult = Schema.getGlobalDescribe().get(objectName).getDescribe();

		for (Schema.ChildRelationship childRelationshipObj : describeSobjectResult.getChildRelationships())
		{
			Schema.SObjectType sobjType = childRelationshipObj.getChildSObject();
			System.debug('sobjType' + sobjType);
			Schema.DescribeSObjectResult describResult = sobjType.getDescribe();
			System.debug('describResult' + describResult);
			if (describResult.isCustom()) {
				ChildObjectDetail objectDetails = new ChildObjectDetail();
				objectDetails.objectName = describResult.getName();
				objectDetails.label = describResult.getLabel();

				objectDetails.columns = new List<FieldInfo> ();

				for (Schema.SObjectField sfield : describResult.fields.getMap().Values())
				{
					schema.describefieldresult dfield = sfield.getDescribe();

					string type = String.valueOf(dfield.getType());
					FieldInfo fieldInfo = new FieldInfo();
					fieldInfo.label = dfield.getLabel();
					fieldInfo.apiName = dfield.getName();
					fieldInfo.type = type;
					objectDetails.columns.add(fieldInfo);
					System.debug('objectDetails' + objectDetails);
				}

				childObjectDetails.add(objectDetails);
			}
		}

		return childObjectDetails;
	}
	@RemoteAction
	public static List<ChildObjectDetail> getObjectFieldsData(string objectName) {
		List<ChildObjectDetail> childObjectDetails = new List<ChildObjectDetail> ();

		Schema.DescribeSObjectResult describeSobjectResult = Schema.getGlobalDescribe().get(objectName).getDescribe();

		//for (Schema.ChildRelationship childRelationshipObj : describeSobjectResult.getChildRelationships())
		//{
		//Schema.SObjectType sobjType = childRelationshipObj.getChildSObject();
		//System.debug('sobjType' + sobjType);
		//Schema.DescribeSObjectResult describResult = sobjType.getDescribe();
		//System.debug('describResult' + describResult);
		//if (describResult.isCustom()) {
		ChildObjectDetail objectDetails = new ChildObjectDetail();
		objectDetails.objectName = describeSobjectResult.getName();
		objectDetails.label = describeSobjectResult.getLabel();

		objectDetails.columns = new List<FieldInfo> ();

		for (Schema.SObjectField sfield : describeSobjectResult.fields.getMap().Values())
		{
			schema.describefieldresult dfield = sfield.getDescribe();

			string type = String.valueOf(dfield.getType());
			FieldInfo fieldInfo = new FieldInfo();
			fieldInfo.label = dfield.getLabel();
			fieldInfo.apiName = dfield.getName();
			fieldInfo.type = type;

			objectDetails.columns.add(fieldInfo);
			System.debug('objectDetails' + objectDetails);
			childObjectDetails.add(objectDetails);
		}

		return childObjectDetails;
	}
	@RemoteAction
	public static void changeState(string id, boolean isActive) {
		try {
			Layout_Configuration__c config = new Layout_Configuration__c();
			config.Id = id;
			config.Is_Active__c = isActive;
			update config;
		}
		catch(Exception ex) {
			ExceptionHandlerUtility.writeException('UISetupController1', ex.getMessage(), ex.getStackTraceString());
		}
	}
}