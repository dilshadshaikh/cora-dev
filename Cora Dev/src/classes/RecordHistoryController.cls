public class RecordHistoryController {

    public static String RecordId;
    public static boolean isRecHistoryEnable;
    public static String ChildObjectvalue;
    public static String Childhistory;
    public static String LookupFieldID;
    public static List<SObject> recordHistoryList;
    public static Map<String, String> recordHistoryMap;
    public static List<Map<String, String>> newrecordHistoryList;

    @AuraEnabled
    public static RecordHistoryWrapper getRecordHistoryDataServer(String ObjRecordId, String sObjName, Integer pageNumber, Integer recordsToDisplay) {

        RecordHistoryWrapper sw = new RecordHistoryWrapper();
        try {
            System.debug('ObjRecordId : ' + ObjRecordId);
            isRecHistoryEnable = false;
            Integer offset = (Integer.valueOf(pageNumber) - 1) * Integer.valueOf(recordsToDisplay);
            String totalCaseCountQuery = '';
            RecordId = ObjRecordId;
            LookupFieldID = sObjName; // LookUP Id used in Record history
            ChildObjectvalue = sObjName;

            System_Configuration__c sc = [select Value__c from System_Configuration__c where Module__c = 'Custom_Record_History' limit 1];
            if (sc.Value__c != null) {
                for (String valSel : sc.Value__c.split(',')) {
                    if (valSel.equalsIgnoreCase(ChildObjectvalue)) {
                        isRecHistoryEnable = true;
                    }
                }
            }
            System.debug('@@isRecHistoryEnable : ' + isRecHistoryEnable);

            if (isRecHistoryEnable) {
                Childhistory = 'Record_History__c';
                system.debug('@@Childhistory :' + Childhistory);
            } else {
                ChildObjectvalue = ChildObjectvalue.replace('__c', '');
                Childhistory = ChildObjectvalue + '__History';
                system.debug('@@Childhistory  :' + Childhistory);
                ChildObjectvalue = ChildObjectvalue + '__c';
                system.debug('@@ChildObjectvalue :' + ChildObjectvalue);
            }

            system.debug('@@ChildObjectvalue :' + ChildObjectvalue);
            system.debug('@@Childhistory  :' + Childhistory);
            system.debug('@@isRecHistoryEnable  :' + isRecHistoryEnable);
            system.debug('@@LookupFieldID  :' + LookupFieldID);
            if (!isRecHistoryEnable) {

                String[] fields = new List<String> {
                    'Field',
                    'OldValue',
                    'NewValue',
                    'CreatedDate',
                    'User',
                    'Id'
                };
                recordHistoryMap = new Map<String, String> ();
                recordHistoryList = new List<SObject> ();
                newrecordHistoryList = new List<Map<String, String>> ();

                if (RecordId != null) {
                    if (UtilityController.isFieldAccessible(Childhistory, 'Id,ParentId,Field,NewValue,OldValue,CreatedDate,CreatedById,CreatedBy')) {
                        String query = 'SELECT Field, NewValue, OldValue, CreatedDate,CreatedBy.Name,CreatedById FROM ' + Childhistory + ' WHERE ParentId = :RecordId ORDER BY CreatedDate DESC, Id DESC';

                        query += ' Limit ' + recordsToDisplay + ' OFFSET ' + offset;
                        totalCaseCountQuery = 'select count() from ' + Childhistory + ' where ParentId = :RecordId '; // id != null
                        recordHistoryList = Database.query(query);
                        System.debug('----Query List Invoice History ---' + recordHistoryList);
                        for (SObject a : recordHistoryList) {
                            recordHistoryMap = new Map<String, String> ();
                            String CreatedByName = String.valueOf(a.getSobject('CreatedBy').get('Name'));
                            recordHistoryMap.put(fields.get(0), String.valueOf(a.get('Field')));
                            recordHistoryMap.put(fields.get(1), String.valueOf(a.get('oldValue')));
                            recordHistoryMap.put(fields.get(2), String.valueOf(a.get('NewValue')));
                            recordHistoryMap.put(fields.get(3), String.valueOf(a.get('CreatedDate')));
                            recordHistoryMap.put(fields.get(4), CreatedByName);
                            recordHistoryMap.put(fields.get(5), String.valueOf(a.get('CreatedById')));
                            newrecordHistoryList.add(recordHistoryMap);
                        }
                    }
                }

            } else {

                String[] fields = new List<String> {
                    'Field',
                    'OldValue',
                    'NewValue',
                    'CreatedDate',
                    'User',
                    'Id'
                };
                recordHistoryMap = new Map<String, String> ();
                recordHistoryList = new List<SObject> ();
                List<sObject> lsstr = new List<sObject> ();
                newrecordHistoryList = new List<Map<String, String>> ();
                if (RecordId != null) {

                    if (UtilityController.isFieldAccessible(Childhistory, null)) {
                        String query = 'SELECT FieldLabel__c,NewValue__c,oldValue__c,CreatedDate__c,User__c,User__r.Name  FROM ' + Childhistory + ' WHERE ' + LookupFieldID + ' = :RecordId ORDER BY CreatedDate DESC, Id DESC';

                        query += ' Limit ' + recordsToDisplay + ' OFFSET ' + offset;
                        totalCaseCountQuery = 'select count() from ' + Childhistory + ' where ' + LookupFieldID + ' = : RecordId '; // id!=null ';
                        recordHistoryList = Database.query(query);
                        System.debug('----Query List Record History ---' + recordHistoryList);
                        for (SObject a : recordHistoryList) {
                            recordHistoryMap = new Map<String, String> ();
                            recordHistoryMap.put(fields.get(0), String.valueOf(a.get('FieldLabel__c')));
                            recordHistoryMap.put(fields.get(1), String.valueOf(a.get('oldValue__c')));
                            recordHistoryMap.put(fields.get(2), String.valueOf(a.get('NewValue__c')));
                            recordHistoryMap.put(fields.get(3), String.valueOf(a.get('CreatedDate__c')));
                            String CreatedByName = a.get('User__c') != null ? String.valueOf(a.getSobject('User__r').get('Name')) : '';
                            recordHistoryMap.put(fields.get(4), CreatedByName);
                            recordHistoryMap.put(fields.get(5), String.valueOf(a.get('User__c')));
                            newrecordHistoryList.add(recordHistoryMap);
                        }

                    }
                }
            }
            System.debug('newrecordHistoryListList :' + totalCaseCountQuery);
            sw.total = (database.countQuery(totalCaseCountQuery)); //2 total records passed;
            system.debug('-----sw.total'+sw.total);
            system.debug('-----------database.countQuery(totalCaseCountQuery)'+database.countQuery(totalCaseCountQuery));
            sw.pageSize = recordsToDisplay;
            sw.page = pageNumber;
            sw.cases = newrecordHistoryList;


        } catch(Exception ex) {
            //String error = UtilityController.customDebug(ex, 'RecordHistoryController'); 
            //System.debug('Exception:'+error);
            //throw new CustomCRUDFLSException(error);
            sw.serverError = UtilityController.customDebug(ex, 'RecordHistoryController'); 
        }
        return sw;
    }

    public class RecordHistoryWrapper {
        @AuraEnabled public Integer pageSize {
            get;
            set;
        }
        @AuraEnabled public Integer page {
            get;
            set;
        }
        @AuraEnabled public Integer total {
            get;
            set;
        }
        @AuraEnabled public List<Map<String, String>> cases {
            get;
            set;
        }
        @AuraEnabled public String serverError {
            get;
            set;
        }
    }
}