public with sharing class FileController {

  @AuraEnabled
  public static SearchWrapper getRecords(String selectedTabId,Integer recordsToDisplay,Integer pageNumber) {
     System.debug('getRecords '+recordsToDisplay+' '+pageNumber);
       

          SearchWrapper sw = new SearchWrapper();  
        
          List<CSV_Load_History__c> historyDtl=new List<CSV_Load_History__c>();
        try{
         Integer offset = (Integer.valueOf(pageNumber) - 1) * Integer.valueOf(recordsToDisplay);
    System.debug('offset '+offset);
      List<Id> ids=getRecordIdsAgainstView(selectedTabId );
       System.debug('ids '+ids);
        //String query = 'select Id,name from CSV_Load_History__c where id=:ids ';

    String query = 'Select S.Id,S.Name, S.File_Name__c, S.Date_Time__c, S.Error_Status__c,(Select Id,Parentid From Attachments ) from CSV_Load_History__c S where id=:ids Order By LastModifiedDate DESC  ';

       query += ' Limit ' + recordsToDisplay + ' OFFSET ' + offset;
       System.debug('query : '+query);  
        String totalCaseCountQuery = 'select count() from CSV_Load_History__c where id=:ids  ';

         totalCaseCountQuery += ' limit 2010';

         historyDtl = database.query(query);
         System.debug('records : '+historyDtl);  
         sw.total = (database.countQuery(totalCaseCountQuery));
         sw.records=historyDtl;
            sw.pageSize = recordsToDisplay;
            sw.page = pageNumber;

        } catch (Exception e) {
           System.debug('msg :'+e.getMessage());
           System.debug('trace :'+e.getStackTraceString());
            
         }
           
    
        return sw;
    }


 @AuraEnabled
    public static SearchWrapper getHistoryDtl(Integer recordsToDisplay,Integer pageNumber) {
	 System.debug('getHistoryDtl'+recordsToDisplay+' '+pageNumber);
	 
		  SearchWrapper sw = new SearchWrapper();  
	
          List<CSV_Load_History__c> historyDtl=new List<CSV_Load_History__c>();
        try{

		 Integer offset = (Integer.valueOf(pageNumber) - 1) * Integer.valueOf(recordsToDisplay);
    System.debug('offset '+offset);
   
	  //   historyDtl  = [Select S.Id,S.Name, S.File_Name__c, S.Date_Time__c, S.Error_Status__c,(Select Id,Parentid From Attachments ) from CSV_Load_History__c S Order By LastModifiedDate DESC LIMIT 999];       
				String query = 'Select S.Id,S.Name, S.File_Name__c, S.Date_Time__c, S.Error_Status__c,(Select Id,Parentid From Attachments  where Name like \'%Error%\') from CSV_Load_History__c S Order By LastModifiedDate DESC  ';

			  query += ' Limit ' + recordsToDisplay + ' OFFSET ' + offset;
			   System.debug('query : '+query);  
		String totalCaseCountQuery = 'select count() from CSV_Load_History__c ';

		 totalCaseCountQuery += ' limit 2010';
		  historyDtl = database.query(query);
		 System.debug('records : '+historyDtl);  
		 sw.total = (database.countQuery(totalCaseCountQuery));
		 sw.records=historyDtl;
		    sw.pageSize = recordsToDisplay;
            sw.page = pageNumber;

        } catch (Exception e) {
           System.debug('msg :'+e.getMessage());
		   System.debug('trace :'+e.getStackTraceString());
		    
         }
          
        return sw;
    }


  /*  @AuraEnabled
    public static List <Map<String,String>> getListViews() {

    List<System.SelectOption> listViews = new List<System.SelectOption>();
    List<Map<String,String>> listOfMap = new List<Map<String,String>>();
    

    listViews = [SELECT Id, Name FROM ListView WHERE SobjectType = 'CSV_Load_History__c'];
     System.debug('listViews' + listViews );
        for (Integer i = 0; i < listViews.size(); i++)
        {
            Map<String,String> viewValueLabel = new Map<String,String>();
                 System.debug('listViews[i].getLabel()' + listViews[i].getLabel() );
                     System.debug('listViews[i].getValue()' + listViews[i].getValue() );
            viewValueLabel.put('label', listViews[i].getLabel());
            viewValueLabel.put('value', listViews[i].getValue());
            listOfMap.add(viewValueLabel);
        }
        System.debug('listOfMap' + listOfMap );
        return listOfMap;

 
   }*/

   @AuraEnabled
    public static List<ListView> getListViews() {

    
    List<Map<String,String>> listOfMap = new List<Map<String,String>>();
    

     List<ListView> listviews  = [SELECT Id, Name FROM ListView WHERE SobjectType = 'CSV_Load_History__c'];
     System.debug('listViews' + listviews );
    
        return listviews;

 
   }
    
    @AuraEnabled
    public static CSV_Load_History__c getCurHistoryDtl(String historyId) {
     System.debug('getCurHistoryDtl' + historyId );
       
        String prefix = getNamespace('FileController');
          CSV_Load_History__c historyDtl=new CSV_Load_History__c();
        try{

         historyDtl  = [Select S.Id,S.Name, S.File_Name__c, S.Date_Time__c, S.Error_Status__c,(Select Id,Parentid,Name From Attachments ) from CSV_Load_History__c S where Id=:historyId];       
            
        } catch (Exception e) {
           System.debug('msg :'+e.getMessage());
           System.debug('trace :'+e.getStackTraceString());
            
         }
           System.debug('historyDtl : '+historyDtl);  
        
        return historyDtl;
    }

	@AuraEnabled
	public static boolean checkFileNameNew(string strFileName){

		boolean DuplicateFile=false;
		String prefix = getNamespace('FileController');
		List<CSV_Load_History__c> historyList = new List<CSV_Load_History__c>();
		

		if(CSV_Load_History__c.getSObjectType().getDescribe().isAccessible()){
			historyList=[SELECT id from CSV_Load_History__c where File_Name__c=:strFileName];
			DuplicateFile=historyList.size()>0;
		}
		return DuplicateFile;
	}
    

    @AuraEnabled
    public static String readTheFile(String fileData,String fileName,String contentType)
    {
        
        
        String parentId ;
        datetime myDateTime = datetime.now();
        CSV_Load_History__c csvHis = new CSV_Load_History__c();
        csvHis.File_Name__c = fileName;
        csvHis.Error_Status__c = false;
        csvHis.Date_Time__c = myDateTime;
        insert csvHis;
        parentId = csvHis.Id;
        system.debug(' @@ parentId : ' + parentId);


         fileData = EncodingUtil.urlDecode(fileData, 'UTF-8');
		  system.debug(' @@ fileData : ' + fileData);
        Attachment attachment = new Attachment();
        attachment.Body = EncodingUtil.base64Decode(fileData);
        attachment.Name = fileName;
        attachment.ParentId = parentId;
        attachment.ContentType = contentType;
          system.debug(' @@  attachment.Body : ' +  attachment.Body);
        insert attachment;

         system.debug(' @@ attachment.Id : ' + attachment.Id);

       String jobid =upload(attachment,parentId);
    
       return parentId+'---'+jobid;

       //    return parentId+'---'+attachment.Id;


    }


	 @AuraEnabled
    public static String getSampleFiles()
    {
    
 CSV_Load_Configuration__c config = CSV_Load_Configuration__c.getOrgDefaults();
  String introFileID = config.Instructions_File_Id__c;
    String sampleFileID = config.Sample_File_Id__c;

	return introFileID+'---'+sampleFileID;


    }
    
    @AuraEnabled
    public static String checkBatch(String jobid)
    {
    
    AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
        TotalJobItems, CreatedBy.Email, ExtendedStatus
        from AsyncApexJob where Id = :jobid]; 

         system.debug(' @@ a.Status : ' + a.Status);
         return a.Status;
    }


    public static String upload(Attachment attachment ,String parentId){
    String jobid;
    String m_csvFile;
        CSVIteratorPLM it = null;
         blob file; 
       

    system.debug(' @@ attachment.name : ' + attachment.name);
    system.debug(' @@ parentId : ' + parentId);

        try 
        {
         if(Schema.getGlobalDescribe().get('Attachment').getDescribe().fields.getMap().get('name').getDescribe().isAccessible()){
      if(attachment.name == null && attachment.body == null ) {
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Select File for CSV Upload'));
          return null;
      } 
     /* if (!attachment.name.endsWithIgnoreCase('.CSV')) {
        attachment = new Attachment();
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Select Valid CSV File'));
        return null;
      }*/
        }
        

         if(Schema.getGlobalDescribe().get('Attachment').getDescribe().fields.getMap().get('body').getDescribe().isAccessible()){
       
         //for check file is empty or not
          file = attachment.body;
         m_csvFile = file.toString();

       }
       system.debug(' @@ m_csvFile : ' + m_csvFile);

			if (m_csvFile==null || m_csvFile=='')
			{
			return null;
			}

       try {
                it = new CSVIteratorPLM(m_csvFile, ParserPLM.LF);
                 system.debug(' ParserPLM.LF : ' );
           } catch ( Exception e ){
                it = new CSVIteratorPLM(m_csvFile, ParserPLM.CRLF);
                 system.debug(' ParserPLM.CRLF : ' );
           }
           
           Integer lineCount = 0;
           while(it.hasNext() && lineCount <= 1) {
               lineCount++;
                system.debug('it.next  : ' +it.next());
           }

           system.debug('lineCount  : ' +lineCount);
           
           if(!(lineCount < 2)) {

           String batchNo = '0000000001';

            String prefix = getNamespace('FileController');

            List<CSV_Load_History__c> historyList = new List<CSV_Load_History__c>();
       historyList=[SELECT Id,Name from CSV_Load_History__c where Id=:parentId];
    
        system.debug(' @@ prefix : ' + prefix);
             
        if(Schema.getGlobalDescribe().get(prefix+'CSV_Load_History__c').getDescribe().fields.getMap().get(prefix+'Name').getDescribe().isAccessible()){
                  //  batchNo = String.valueof(Long.valueOf(historyList[0].Name)+1);
                  batchNo = historyList[0].Name;
        }
        while(batchNo.length()<10){
                  batchNo = '0'+batchNo;
              }
                  system.debug(' @@ batchNo : ' + batchNo);
                CsvLoadBatch b = new CsvLoadBatch (batchNo,attachment.Name,file,parentId);
              if(!b.hasError){
                
             
               jobid = DataBase.executeBatch(b,1);
              system.debug(' @@ jobid : ' + jobid);
              }
                
            }
			else
			{
			 return 'Records not availble.';
			}
              }
              catch (DMLException e) {
          
          
              system.debug('Exception : ' + e);
          
        }
        return jobid;
    }





     public static String getNamespace(String clsName) 
    {
    Map<String, String> persistNameSpace = new Map<String, String> ();
        if (persistNameSpace != null && persistNameSpace.get(clsName) != null)
        {
            return persistNameSpace.get(clsName);
        }
        else
        {
            return '';
        }
    }


       public static List<Id> getRecordIdsAgainstView(Id viewId ) {
        /*if (query == null)
        {
            query = 'SELECT Id FROM '+objName+' limit 1';
        }*/

        String query = 'select Id,name from CSV_Load_History__c limit 1';

        System.debug('getRecordIdsAgainstView:'+query); 
        //System.debug('recordsMap.size : 8'+query);
        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(Database.getquerylocator(query));     
        System.debug('recordsMap.size : 9'+viewId);   
        ssc.setFilterId(viewId);    
    
        ssc.setPageSize(20);
    
        ssc.setPageNumber(1);
        List<id> recordsMap = new List<id>();
        //ssc.getCompleteResult();
        for(sObject testRecord : (List<sObject>) ssc.getRecords())
        {
            recordsMap.add(testRecord.id);
        }
        while (ssc.getHasNext())
        {           
            
            ssc.next();
            System.debug('getPageNumber'+  ssc.getPageNumber());  
            //recordsMap = new Map<id,SObject>(ssc.getRecords());
            for(sObject testRecord : (List<sObject>) ssc.getRecords())
            {
                recordsMap.add(testRecord.id);
            }
        }
        //Map<id,SObject> recordsMap = new Map<id,SObject>(ssc.getRecords());
        
        //System.debug('recordsMap.size : '+ssc.getRecords()[0]); */
        System.debug('recordsMap.size : '+recordsMap.size());
        //sw.cases =  ssc.getRecords();     
        //sw.total =  recordsMap.size();
        return recordsMap;
    }

     public class SearchWrapper {
        @AuraEnabled public Integer pageSize {get;set;}
        @AuraEnabled public Integer page {get;set;}
        @AuraEnabled public Integer total {get;set;}
        @AuraEnabled public List<CSV_Load_History__c> records {get;set;}
    
    }


}