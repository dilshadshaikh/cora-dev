public class VendorInquiryFieldSetController {
    
	@AuraEnabled
    public static EsmHelper.dataHelperRecord getFields() {
		String objName = 'CaseManager__c';
		String fsname='VendorInquiry';
		List<EsmHelper.Columns> cols =new List<EsmHelper.Columns>();
		String selectedRows = UtilityController.getAllSelectedFields(cols);
		CaseManager__c CaseObj =new CaseManager__c();

		Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objName);
        Schema.DescribeSObjectResult describe = targetType.getDescribe();
        Map<String, Schema.FieldSet> fsMap = describe.fieldSets.getMap();
        Schema.FieldSet fs = fsMap.get(fsName);
		List<Schema.FieldSetMember> fieldSet = fs.getFields();
        List<FieldSetMember> fset = new List<FieldSetMember>();
		for (Schema.FieldSetMember f: fieldSet) {
			EsmHelper.Columns col =new EsmHelper.Columns();
			col.name = f.getFieldPath();
			if (String.valueOf(f.getType())=='REFERENCE')
			{
				col.reference='NAME';
				col.type = 'reference';
			}
			cols.add(col);
        }
		EsmHelper.dataHelperRecord dh = UtilityController.getWrapperFromJson(CaseObj, objName, cols);
		//System.debug('Dh---'+ dh);
		
		return dh;
    }

	@AuraEnabled
    public static VendorInquiryFieldSetMember.UnameMail getUsernameEmail() {
		VendorInquiryFieldSetMember.UnameMail ue = new VendorInquiryFieldSetMember.UnameMail();

		//System.debug('Username -- '+ UserInfo.getName());
		//System.debug('User Email -- '+ UserInfo.getUserEmail());
		ue.UserName=UserInfo.getName();
		ue.Email=UserInfo.getUserEmail();
		return ue;
	}
	
	@AuraEnabled
	public static String getInvoicenumber(String invoiceid) {
		//Select id,Name from invoice__c where id='a173700000019eVAAQ'
		Invoice__c cm = [Select id,Name from invoice__c where id=:invoiceid];
		String invoicenm=cm.Name;
		System.debug('invoicenm -- '+ invoicenm);
		return invoicenm;
	}

	@AuraEnabled
	public static String saveCaseData(CaseManager__c CaseObj) {
		try 
		{
			//[Start] Case manager insert data
			//System.debug('CaseObj -- '+CaseObj);
			String startToken = ESMConstants.SUBJECT_START_IDENTIFIER;
			String endToken = ESMConstants.SUBJECT_END_IDENTIFIER;
			String emailSubject = (String)CaseObj.get(Schema.CaseManager__c.Subject__c);
			String emailBody = (String)CaseObj.get(Schema.CaseManager__c.Comments__c);
		
			upsert CaseObj;
			//[End] Case manager insert data

			//System.debug('New ID -- '+CaseObj.Id);
			//String emailSubject =Subject;
			//String emailBody = Comments;

			//[Start] Email Message insert data
			String ctid =	CaseObj.Id;
			EmailMessage[] newEmail = new EmailMessage[0];
			newEmail.add(new EmailMessage(Subject = emailSubject,
											TextBody = emailBody,
											HtmlBody = emailBody,
											FromName = UserInfo.getName(), 
											FromAddress = UserInfo.getUserEmail(),
											Incoming = true,
											RelatedToId = ctid,
											status = '0'));
			insert newEmail;
			//[End] Email Message insert data
			
			//[Start] Send Email
			string query = 'SELECT Id,Name,Subject__c, SuppliedEmail__c FROM CaseManager__c WHERE Id =:ctid Limit 1';
			CaseManager__c ct = Database.query(query);
			List<String> toList = new List<String> ();

			//System.debug('ct -- '+ct);
			//String mailBody = System.Label.Case_Created_Manually_Email + ct.Name;
			String mailBody = emailBody;
			if (String.isNotBlank(ct.SuppliedEmail__c))
			{
				toList.add(ct.SuppliedEmail__c);
				//sendSingleMail(null, toList, null, ct.Subject__c + ':' + startToken + ct.Name + endToken, mailBody, ct.Id);
				String subject=ct.Subject__c + ':' + startToken + ct.Name + endToken;
				
				List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage> ();
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
				mail.setToAddresses(toList);
				mail.setSubject(subject);
				mail.setHtmlBody(mailBody);
				mail.setWhatId(ctid);
				mails.add(mail);
				Messaging.sendEmail(mails);
			}
			//[End] Send Email
		
	}
	catch(Exception ex) {
				ExceptionHandlerUtility.writeException('VendorInquiryFieldSetController', ex.getMessage(), ex.getStackTraceString());
	}
		return 'Success';	
	}
}