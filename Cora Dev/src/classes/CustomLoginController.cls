public with sharing class CustomLoginController {
    public String username{get;set;}
    public String password{get;set;}
    public String customerLogoImageURL {get;set;}
    public System_Configuration__c sysConf;
    public CustomLoginController () {
        customerLogoImageURL = '';
        List<System_Configuration__c> sysConfList = [SELECT Id, Name, Value__c FROM System_Configuration__c WHERE Module__c = 'Vendor_Master' AND Sub_Module__c = 'Vendor_Portal_Logo'];
        if(sysConfList != null && sysConfList.size() >0){
            for(System_Configuration__c sysConf: sysConfList){
                customerLogoImageURL = '/servlet/servlet.FileDownload?file='+sysConf.Value__c;
            }
        }
    }
    public PageReference forwardToCustomAuthPage() {
        return new PageReference( '/CustomLogin');
    }
    public PageReference login() {
        return Site.login(username, password, null);
    }    
}