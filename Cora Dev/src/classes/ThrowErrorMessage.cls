public class ThrowErrorMessage { 

    @InvocableMethod(label = 'ThrowErrorMessage' description = 'ThrowErrorMessage')
    public static List<Invoice__c> errorError(List<Invoice__c> invList) {
        Error_Message__c newError = new Error_Message__c();
		newError.put('Invoice__c', invList.get(0).Id);
		newError.put('Error__c', 'error has occured');
		System.debug('newError:' + newError);
		Database.insert(newError);
		System.debug('newError:' + newError);
        return null;
    }

}