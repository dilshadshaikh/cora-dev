public class VendorDetail {
    
    @AuraEnabled
    public static  List<EsmHelper.dataHelperRecord> fetchVendorDetail(String recId,String objName)
    {
    
        List<EsmHelper.dataHelperRecord> dh = new List<EsmHelper.dataHelperRecord> ();
        List<EsmHelper.Columns> colsVenInfoSec = new List<EsmHelper.Columns> ();
      
        String errmsg='';
        Sobject query;
        try 
        {           
            System.debug('recId' + recId);
            System.debug('objName' + objName);
            
            string esmNamespace = UtilityController.esmNameSpace;
            objName = esmNamespace + objName ;

            if (objName != null)
            {
                query = Database.query(CommonFunctionController.getSOQLQuery(objName, 'Id=:recId'));
                System.debug('query:' + query);
            }
            
                //query = 'SELECT ';
                List<Schema.fieldsetmember> fdsvendorinfo = CommonFunctionController.getFieldsFromFieldSet(objName, esmNamespace +'Vendor_Detail');
                system.debug('fdsvendorinfo :-----------'+fdsvendorinfo);
                 for (Schema.fieldsetmember fieldSetMemberObj : fdsvendorinfo ) {
                    //system.debug('API Name ====>' + fieldSetMemberObj.getFieldPath()); //api name
                    //system.debug('Label ====>' + fieldSetMemberObj.getLabel());
                    //system.debug('Required ====>' + fieldSetMemberObj.getRequired());
                    //system.debug('DbRequired ====>' + fieldSetMemberObj.getDbRequired());
                    //system.debug('Type ====>' + fieldSetMemberObj.getType());   //type - STRING,PICKLIST                
                     EsmHelper.Columns col = new EsmHelper.Columns();
                     col.name = fieldSetMemberObj.getFieldPath();
                     col.label = fieldSetMemberObj.getLabel();
                     col.type= String.valueOf(fieldSetMemberObj.getType()) ;
                     colsVenInfoSec.add(col);
                     //query += fieldSetMemberObj.getFieldPath()+ ', ';    
                }
				System.debug('fieldSet colsVenInfoSec -------'+colsVenInfoSec);
                //query += 'Id FROM '+ objName  +' WHERE Id = : recId ' ;
                 //system.debug('query  ----------------'+query );
                          
            
        }
        catch (Exception ex)
        {
            errmsg=ex.getMessage();
            System.debug(errmsg);
        }
          dh.add(UtilityController.getWrapperFromJson(query , objName, colsVenInfoSec,errmsg));
		  System.debug('dh -----'+dh);
          return dh;  
    }
    
}