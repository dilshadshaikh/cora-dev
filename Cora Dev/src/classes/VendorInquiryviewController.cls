public class VendorInquiryviewController{
    
    @AuraEnabled
    public static List<EmailMessage> getinitialData() {
        String ctid='a09370000087PNaAAM';
        List<EmailMessage> interactionList = [SELECT Id,Subject, TextBody, HtmlBody, FromName, FromAddress,CreatedById,CreatedDate FROM EmailMessage WHERE RelatedToId=:ctid ORDER BY CreatedDate desc];
        return interactionList;
    }

    @AuraEnabled
    public static List<ContentDocumentLink> getattachementData() {
        String ctid='a09370000087PNaAAM';
        List<ContentDocumentLink> attachmentList = [SELECT ContentDocument.Owner.Name,ContentDocument.LatestPublishedVersion.Is_Primary_Doc__c,ContentDocument.Id,ContentDocument.Title,ContentDocument.FileType,ContentDocument.LatestPublishedVersionId, LinkedEntityId, Id,ContentDocument.LastModifiedDate,ContentDocument.FileExtension,ContentDocument.ContentSize FROM ContentDocumentLink where LinkedEntityId=:ctid order by SystemModstamp desc];
        return attachmentList;
    }

    @AuraEnabled
    public static String getUserid() {
        return UserInfo.getUserId();
    }

    @AuraEnabled
    public static CaseManager__c getCase() {
        String ctid='a09370000087PNaAAM';
        CaseManager__c cm = [SELECT Id,Priority__c,Subject__c,Status__c,Comments__c FROM CaseManager__c WHERE Id=:ctid];
        return cm;
    }

    @AuraEnabled
    public static List<String> getPicklistvalues(){
        CaseManager__c objName= new CaseManager__c();
        String field_apiname='Priority__c';
        List<String> optionlist = new List<String>();
 
        Schema.sObjectType sobject_type = objName.getSObjectType();
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe();
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
 
        List<Schema.PicklistEntry> pick_list_values = field_map.get(field_apiname).getDescribe().getPickListValues();
        for (Schema.PicklistEntry a : pick_list_values) {
            optionlist.add(a.getValue());
        }
        return optionlist;
    }
    @AuraEnabled
    public static EmailMessage[] saveCaseData(CaseManager__c CaseObj,String mailbody) {
        EmailMessage[] newEmail = new EmailMessage[0];
        try 
        {
            //[Start] Case manager insert data
            //System.debug('CaseObj -- '+CaseObj);
            String startToken = ESMConstants.SUBJECT_START_IDENTIFIER;
            String endToken = ESMConstants.SUBJECT_END_IDENTIFIER;
            String emailSubject = 'Reply : ' + (String)CaseObj.get(Schema.CaseManager__c.Subject__c);
            //String emailBody = mailbody;
        
            upsert CaseObj;
            //[End] Case manager insert data

            //[Start] Email Message insert data
            String ctid =   CaseObj.Id;
            
            newEmail.add(new EmailMessage(Subject = emailSubject,
                                            TextBody = mailbody,
                                            HtmlBody = mailbody,
                                            FromName = UserInfo.getName(), 
                                            FromAddress = UserInfo.getUserEmail(),
                                            Incoming = true,
                                            RelatedToId = ctid,
                                            status = '0'));
            insert newEmail;
            //[End] Email Message insert data

            //[Start] Send Email
            /*string query = 'SELECT Id,Name,Subject__c, SuppliedEmail__c FROM CaseManager__c WHERE Id =:ctid Limit 1';
            CaseManager__c ct = Database.query(query);
            List<String> toList = new List<String> ();

            
            String mailBody = emailBody;
            if (String.isNotBlank(ct.SuppliedEmail__c))
            {
                toList.add(ct.SuppliedEmail__c);
                String subject=ct.Subject__c + ':' + startToken + ct.Name + endToken;
                
                List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage> ();
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(toList);
                mail.setSubject(subject);
                mail.setHtmlBody(mailBody);
                mail.setWhatId(ctid);
                mails.add(mail);
                Messaging.sendEmail(mails);
            }*/
            //[End] Send Email
            
        }
        catch(Exception ex) {
                ExceptionHandlerUtility.writeException('VendorInquiryviewController', ex.getMessage(), ex.getStackTraceString());
        }
        return newEmail;
    }
}