@isTest
public class ComponentConfigControllerTest{
    public static testMethod void testFirst(){
	ComponentConfigController.LayoutInfo layoutWrap = new ComponentConfigController.LayoutInfo();
      layoutWrap.ObjectName = 'invoice__c';
      layoutWrap.LayoutType = 'create';
      layoutWrap.isActive = false;
      layoutWrap.id = '1';
      layoutWrap.PageLayout = null;
        
    ComponentConfigController.PageLayout PageLayoutWrap = new ComponentConfigController.PageLayout();    
      PageLayoutWrap.objectName = 'invoice__c'; 
      PageLayoutWrap.objectLabel = 'abc';
      PageLayoutWrap.objectNameSpace = 'invoice';
        
     ComponentConfigController.Option optionWrap = new  ComponentConfigController.Option();
        optionWrap.value = 'abc';
        optionWrap.text = 'xyz';
       ComponentConfigController.Option optionWrap1 = new  ComponentConfigController.Option('abc','xyz');
        
        ComponentConfigController.FieldInfo FieldInfoWrap = new  ComponentConfigController.FieldInfo();
        FieldInfoWrap.label = 'abc';
        FieldInfoWrap.apiName = 'invoice__c';
        FieldInfoWrap.type = 'create';
        
		ComponentConfigController.ComponentInfo ComponentInfoWrap = new ComponentConfigController.ComponentInfo();
        
        ComponentInfoWrap.ChildObject = 'invoice__c';
        ComponentInfoWrap.ComponentTitle = 'abc';
        ComponentInfoWrap.ComponentType = 'create';
        ComponentInfoWrap.id = '1';
        ComponentInfoWrap.isActive = false;
        ComponentInfoWrap.ObjectName = 'invoice__c';
        ComponentInfoWrap.PageComponent = null;
        ComponentInfoWrap.RelatedList = null;
        
       ComponentConfigController.column columnWrap = new ComponentConfigController.column();
        columnWrap.label = 'a';
        columnWrap.name = 'b';
        columnWrap.reference  = 'a'; 
        
        
       ComponentConfigController.PageComponent  PageComponentWrap = new ComponentConfigController.PageComponent();
        PageComponentWrap.configuration = null;
        
        ComponentConfigController.JsonConfiguration  JsonConfigurationWrap = new ComponentConfigController.JsonConfiguration();
        JsonConfigurationWrap.allowAttachPrime = false;
        JsonConfigurationWrap.allowedExt = '.pdf';
        JsonConfigurationWrap.allowedExtForPrime ='.jpg';
        JsonConfigurationWrap.allowedSize = 2;
        
        ComponentConfigController.RelatedList RelatedListWrap = new ComponentConfigController.RelatedList();
       
//       List<column> listColumn = new List<column>();
       // listColumn.add(columnWrap);
       	ComponentConfigController.ChildObjectDetail ChildObjectDetailWrap = new ComponentConfigController.ChildObjectDetail();
       // ChildObjectDetailWrap = 'aa';
        ChildObjectDetailWrap.label = 'invoice';
        ChildObjectDetailWrap.objectName = 'invoice__c';
        
        ComponentConfigController.CmptFieldInfo CmptFieldInfoWrap = new ComponentConfigController.CmptFieldInfo();
        CmptFieldInfoWrap.apiName = 'invoice';
        CmptFieldInfoWrap.label ='invoice';
        CmptFieldInfoWrap.type = 'create';
       
        
        ComponentConfigController.saveComponent('Attachment');
        ComponentConfigController.saveComponent('InteractionHistory');
        ComponentConfigController.saveComponent('Route');
        ComponentConfigController.saveComponent('RecordHistory');
        ComponentConfigController.saveComponent('QualityCheck');
        
        ComponentConfigController.getObjectFields('invoice__c');
        ComponentConfigController.getAllComponents();
        ComponentConfigController.removeComponent('1');
        ComponentConfigController.changecmpState('1');
        ComponentConfigController.getAllObjects();
        ComponentConfigController.getAllChildObjects();
        ComponentConfigController.ComponentInfo ComponentInfoWrap1 = new ComponentConfigController.ComponentInfo();
        ComponentConfigController.editComponentChange(ComponentInfoWrap1,'Invoice__c');
        ComponentConfigController.editComponentRelatedList(ComponentInfoWrap1,'Invoice__c',RelatedListWrap);
        
       
      
    }
}