@isTest
public class UISetupControllerTest{

    public static testMethod void testFirst(){
        UISetupController.getObjects();
        UISetupController.getLayoutTypeValue();
        UISetupController.getAllLayouts();
      	UISetupController.getsystem();
        UISetupController.checkLayoutTypeObj('create', 'invoice__c', 'hh','jdj');
      	UISetupController.removeLayout('1');
        UISetupController.getAllPickListValue('Invoice__c','Invoice__c');
        UISetupController.changeState('2', false); 
        UISetupController.getObjectFieldsData('Invoice__c');
        UISetupController.getChildObjectDetails('Invoice__c');
        UISetupController.getCurrentObjectChild('Invoice__c','Invoice__c');
        UISetupController.getActivewidget('Invoice__c');
        UISetupController.getObjectFields('Invoice__c');
        UISetupController.LayoutInfo layoutWrap = new UISetupController.LayoutInfo();
        layoutWrap.id = '1';
        layoutWrap.isActive = false;
        layoutWrap.LayoutSystem = 'a';
        layoutWrap.ObjectName = 'Invoice__c';
        layoutWrap.LayoutType = 'Create';
        layoutWrap.PageLayout = null;
       system.debug('layoutWrap---' +layoutWrap);
       UISetupController.saveRule(layoutWrap);
        
        UISetupController.PageLayout1 PageLayout1Wrap= new  UISetupController.PageLayout1();
        PageLayout1Wrap.PageLayout = null;
        
        UISetupController.PageLayout1 pageWrap1 = new UISetupController.PageLayout1();
        //pageWrap1.PageLayout = 1;
        UISetupController.PageLayout pageWrap = New UISetupController.PageLayout();
        pageWrap.objectName = 'Invoice__c';
        pageWrap.objectLabel = 'invoice';
        pageWrap.objectNameSpace = 'abc';
        pageWrap.POReference = null;
        pageWrap.additionalCharge = 'as';
        pageWrap.ShowPrimaryDocument = false;
        //pageWrap.layoutDetailSections = ;
        //LayoutDetailSection lds = new LayoutDetailSection();
        //lds.
        //insert lds;
        list<UISetupController.PageLayout> listLds1 = new list<UISetupController.PageLayout>();
        listLds1.add(pageWrap);
        
        UISetupController.LayoutDetailSection LdWrap = new UISetupController.LayoutDetailSection();
        LdWrap.active = false;
        LdWrap.title = 'CreateLayout';
        LdWrap.readonly =false;
        LdWrap.criteria = null;
         list<UISetupController.LayoutDetailSection> listLds = new list<UISetupController.LayoutDetailSection>();
        	listLds.add(LdWrap);
        
        UISetupController.LayoutSection ls = new UISetupController.LayoutSection();
        ls.headerSection = null;
        ls.additionalHeaderSection = null;
        
        UISetupController.LayoutHeaderSection lhs = new UISetupController.LayoutHeaderSection();
        lhs.active = false;
        lhs.readonly =false;
		lhs.title = 'create';
        lhs.criteria = null;
        //Criteria cr = new Criteria();
        //cr
        UISetupController.LayoutAdditionalHeaderSection lahWarp = new UISetupController.LayoutAdditionalHeaderSection();
        lahWarp.active = false;
        lahWarp.readonly = false;
        lahWarp.title = 'cretae';
		lahWarp.criteria = null;
        
        UISetupController.LayoutPORefSection lps = new UISetupController.LayoutPORefSection();
        lps.POReferenceRequired = false;
        lps.searchBy = 'abc';
        lps.searchFilter = 'b';
        lps.title = 'create';
        lps.criteria = null;
        UISetupController.Groups groupwrap = new UISetupController.Groups ();
        groupwrap.operator = '=';
       // Rules r = new Rules();
        //insert r;
        
       // list<Rules> listRules = new list<Rules>();
        //groupwrap.rule
        
        UISetupController.Rules ruleWrap = new UISetupController.Rules();
        ruleWrap.condition = 'hh';
        ruleWrap.field = null;
        //UISetupController.
        //
        UISetupController.Field fieldWrap = new UISetupController.Field();
        fieldWrap.apiName = 'invoice';
        fieldWrap.label = 'abc';
        fieldWrap.ReferenceTo = 'Invoice__c';
        fieldWrap.type = 'Edit';
       // ReferenceTo.picklistVal
       // 
       UISetupController.column columnWrap= new  UISetupController.column();
       columnWrap.name = 'd';
       columnWrap.label ='df'; 
       columnWrap.defaultValue = 'sll';
       columnWrap.readonly = false;
       columnWrap.required = false; 
       columnWrap.currentUserSelected = false; 
       columnWrap.excludeCurrentUser = false;
       columnWrap.requiredCriteria = null;
	   columnWrap.readonlyCriteria = null;
       columnWrap.criteria = null;
        
       UISetupController.RelatedList rlWrap = new UISetupController.RelatedList();
       rlWrap.objectName = 'Invoice__c';
       rlWrap.title = 'create';
       rlWrap.active = false; 
       rlWrap.readonly = false;
       rlWrap.allowButton = false;
       rlWrap.RelationField = 'as';
       rlWrap.whereClause = 'hh';
       rlWrap.groupBy = 'd';
       rlWrap.orderBy = 'd';
       rlWrap.label = 'd';
       rlWrap.type = 'd'; 
       rlWrap.additionalCharge = false;
       rlWrap.Component = null;
       rlWrap.criteria = null;
       //rlWarp.index = 1;
       UISetupController.FieldInfo FieldInfoWrap  = new UISetupController.FieldInfo ();
        FieldInfoWrap.label = 'ggs';
        FieldInfoWrap.apiName = 'h';
        FieldInfoWrap.type = 'd';
        
       //rlWrap.orderBy = 'd';
       //list<UISetupController.FieldInfo> fieldList = new list<UISetupController.FieldInfo>();
        //;fieldList.add(FieldInfoWrap);
        //fieldList.add(FieldInfoWrap);
        //UISetupController.getObjectFields('info');
        
       
       UISetupController.Component1 Component1Wrap = new UISetupController.Component1();
      Component1Wrap.allowedSize = 43;
      Component1Wrap.allowedExt = 'abc.txt';
      Component1Wrap.allowAttachPrime = false; 
      Component1Wrap.allowedExtForPrime= 'hhd';
        
      UISetupController.Configuration ConfigurationWrap = new UISetupController.Configuration ();
      ConfigurationWrap.allowedSize = 23;
      ConfigurationWrap.allowedExt = 'abc.txt';
      ConfigurationWrap.allowAttachPrime = false; 
      ConfigurationWrap.allowedExtForPrime = 'js';
       
      UISetupController.JSONValue jonWrap = new  UISetupController.JSONValue();
       jonWrap.configuration = null;
        
       UISetupController.Component cmpWarp = new UISetupController.Component ();
        cmpWarp.configuration = null;
        
        UISetupController.ComponentConfig ccWrap = new UISetupController.ComponentConfig ();
        ccWrap.JSONValue = null;
        ccWrap.ChildObjectName = 'gs';
        ccWrap.ConfigJson = 'jj';
        ccWrap.Title = 'j';
        ccWrap.isActive = false;
        ccWrap.IsReadonly = false;
        ccWrap.ObjectName = 'kl';
        ccWrap.ComponentType = 'll';
        ccWrap.objName = 'invoice__c';
     	ccWrap.label = 'invoice';
        
        
        
       UISetupController.ChildObjectDetail childobjectWrap = new UISetupController.ChildObjectDetail();
        childobjectWrap.objectName = 'Invoice__c';
        childobjectWrap.label = 'invoice';
       // childobjectWrap.
    }
}