/**
* Authors    : Atul Hinge
* Date created : 16/11/2017 
* Purpose      : Test Class for AttachmentController
* Dependencies : -
* -------------------------------------------------
*Modifications:
Date:   
Purpose of modification:  
Method/Code segment modified: 

*/
@isTest
public class AttachmentControllerTest {
    //Test 1
    public static testMethod void unitTest01(){
        List<CaseManager__c> ctList= ESMDataGenerator.createCases(1);
        String attachId= ESMDataGenerator.createContent(ctList[0]);
        AttachmentController.getAttachments(ctList[0].Id);
        AttachmentController.deleteAttachments(new List<Id>{attachId});
    }
    
    //Test 2
    public static testMethod void unitTest02(){
        try{
        	AttachmentController.getAttachments(null);
        }catch (Exception e) {}
        
        try{
        	AttachmentController.deleteAttachments(null);
        }catch (Exception e) {}
    }
}