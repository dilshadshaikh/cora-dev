public class HeaderSectionController {
	@AuraEnabled
	public static EsmHelper.dataHelperRecord getJsonBinding(String objName, List<String> jsonObj) {
		List<EsmHelper.Columns> cols = new List<EsmHelper.Columns>();
		for (String s : jsonObj) {
			List<EsmHelper.Columns> tmp = UtilityController.getHelperColumnsForWrapper(s);
			if (tmp != null)
			{
				for (EsmHelper.Columns t : tmp) {
					cols.add(t);
				}
			}
		}
		sObject currentObject = null;
		EsmHelper.dataHelperRecord wrap = UtilityController.getWrapperFromJson(currentObject, objName, cols);
		return wrap;
	}

}