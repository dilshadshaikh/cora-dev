/*
  Authors: Chandresh Koyani
  Date created: 14/11/2017
  Purpose: Blank class to handle Exception for EmailToCase Module.
  Dependencies: EmailToCaseManager.cls,EmailToCaseService.cls
  -------------------------------------------------
  Modifications:
  Date: 
  Purpose of modification:
  Method/Code segment modified:
 
*/
public with sharing class EmailToCaseException extends Exception {
	public CaseManager__c caseManager{
		get;set;
	}
	public EmailToCaseException(Exception ex,CaseManager__c caseManager){
		this.caseManager=caseManager;

	}
}