/*
 * Authors     :  Rahul Pastagiya
 * Date created :  09/08/2017
 * Purpose      :  To display all Cases tab wise with the help of CaseListItem Component.
 * Dependencies :  CaseList.cmp, CaseTracker.cmp
 * JIRA ID      :  PUX-23
 * -----------------------------------------------------------
 * Modifications: 1
 *        Authors:  Tushar Moradiya
 *        Date:  18-Sep-2017
 *        Purpose of modification:  Implement filters on list page.
 *        Method/Code segment modified: PUX-305   
 * 
 *  ____________________________________________________________
*   Modifications: 2
*           Authors:  Tushar Moradiya
*           Date:  11-Dec-2017
*           Purpose of modification: Filter removed while we go bake on case list view
*           Method/Code segment modified: PUX-421
*   Modifications: 3
*           Authors:  Rahul Pastagiya   
*           Date:  02-Feb-2018
*           Purpose of modification: Configuration ability of Case view
*           Method/Code segment modified: PUX-678
  *   Modifications: 4
  *           Authors:  Rahul Pastagiya   
  *           Date:  07-Feb-2018
  *           Purpose of modification: Optimize the list view to ensure key information is readily available
  *           Method/Code segment modified: PUX-12
 */

public with sharing class CaseListViewController {

    /*
        Authors: Rahul Pastagiya
        Purpose: To get The List of Cases based on condition
        Dependencies : CaseListHelper.js
    */
    @AuraEnabled
    public static CasePagerWrapper getCases(String whereClause, Integer pageNumber, Integer recordsToDisplay, 
    String sortField, String sortDirection, List < String > selectedQueue, Map < String, String > filterMap, String overDueIn,String viewId) {
        CasePagerWrapper cpw = new CasePagerWrapper();    
        try { 
            //PUX-678
            String objectName = ObjectUtil.getWithNameSpace('CaseManager__c');
            String fieldSet = ESMConstants.CASE_LIST_FIELD_SET;
            fieldSet= ObjectUtil.getValidFieldSetName(objectName,fieldSet);             

			Map<String,Schema.FieldSet> fieldSetMap=Schema.getGlobalDescribe().get(objectName).getDescribe().FieldSets.getMap();

			Schema.FieldSet fieldSetObj = fieldSetMap.get(fieldSet);
            Map<String, String> keyFieldLabelMap = new Map<String, String>();
            List < String > keyList = new List < String > ();
            keyList.add(ObjectUtil.getWithNameSpace('Escalation__c'));
            //Integer length = (fieldSetObj.getFields()).size() <= 6 ? (fieldSetObj.getFields()).size() : 6;
            Integer length = (fieldSetObj.getFields()).size();
			for (Schema.FieldSetMember fsm : fieldSetObj.getFields()) {
                keyList.add(fsm.getFieldPath());
                keyFieldLabelMap.put(fsm.getFieldPath(), fsm.getLabel());      
            }
            
            set < String > fieldList = new set < String > (keyList);
            String emailfieldSet= ObjectUtil.getValidFieldSetName(objectName,'EmailProcessingFields');          
			Schema.FieldSet fieldSetObj1 = fieldSetMap.get(emailfieldSet);
			for (Schema.FieldSetMember fsm : fieldSetObj1.getFields()) {
                fieldList.add(fsm.getFieldPath());
            }
            fieldList.add(ObjectUtil.getWithNameSpace('User_Action__c'));
	  fieldList.add(ObjectUtil.getWithNameSpace('SkipIndexing__c'));
		//	fieldList.add(ObjectUtil.getWithNameSpace('OverdueIn__c'));
			//fieldList.add(ObjectUtil.getWithNameSpace('Indicator__c'));
			//fieldList.add(ObjectUtil.getWithNameSpace('Email_Subject__c'));
			//fieldList.add(ObjectUtil.getWithNameSpace('Target_TAT_Time__c'));
			//fieldList.add(ObjectUtil.getWithNameSpace('PredictedClosureDate__c'));
			//fieldList.add(ObjectUtil.getWithNameSpace('Unread_Email_Count__c'));
			//fieldList.add('Id');
			//fieldList.add('Name');
            fieldList.add('OwnerId');
			//fieldList.add('RecordType.Name');

            //PUX-678
            String uid = UserInfo.getUserId();
            Integer offset = (Integer.valueOf(pageNumber) - 1) * Integer.valueOf(recordsToDisplay);
            String queryWhereClause = '';
            String totalCaseCountQuery = 'select count() from CaseManager__c where';
            //PUX-678
            String query = 'select id,'+string.join(new List<String>(fieldList),',')+',OverdueIn__c,Indicator__c,Subject__c,Target_TAT_Time__c,(SELECT CreatedDate, FromAddress, FromName, TextBody, HtmlBody , Id, Subject FROM Emails WHERE Incoming = true order by CreatedDate desc limit 1), Unread_Email_Count__c, RecordType.Name from CaseManager__c where ';
            //PUX-678
            List<string> excludeCaseStatus=new List<string>();
            excludeCaseStatus.add('Junk Case');

            List<string> completedCaseStatus=new List<string>();
            completedCaseStatus.add('Completed');
            completedCaseStatus.add('Rejected');
            completedCaseStatus.add('Pending For Archival');

            if (whereClause == 'My Cases') {
                queryWhereClause += ' OwnerId = :uid and Current_State__c not in : excludeCaseStatus and Current_State__c Not in : completedCaseStatus';
            } else if (whereClause == 'Unassigned') {
                queryWhereClause += ' Owner.type = \'Queue\' and ( Previous_Queue_Name__c = null or Previous_Queue_Name__c = \'\' ) and Current_State__c not in : excludeCaseStatus';
            } else if (whereClause == 'Closed') {
                queryWhereClause += ' OwnerId = :uid and Current_State__c in :completedCaseStatus';
            } else if (whereClause == 'Shared Cases') {
                queryWhereClause += ' OwnerId != :uid and Previous_Queue_Name__c != null and Previous_Queue_Name__c != \'\' and Current_State__c not in : excludeCaseStatus';
            } else if (whereClause == 'All Cases') {
				//system.debug('tushaar '+viewId);
				if (viewId != null && viewId != '') {
					String query1 = 'select id,name from ' + objectName + ' limit 1';
					List<Id> ids = getRecordIdsAgainstView(objectName, viewId, query1, pageNumber, recordsToDisplay);
					queryWhereClause += ' id=:ids ';
				} else {
					queryWhereClause += '  id!=null ';
				}

				}
            /* Code start for PUX-485 */
            if(overDueIn != ''){
                if(overDueIn == 'Overdue')
                    queryWhereClause += ' And OverdueIn__c != \'Overdue\' ';
                else
                    queryWhereClause += ' And Current_State__c in (\'Awaiting Email Response\',\'Awaiting Supervisory Resolution\') ';
            }
            /* Code End for PUX-485 */
            //For Security scan
            List<String> fieldsName = new List<String>{'id','Process__c','WorkType__c','Document_Type__c','Location__c','ERP_System__c','Sender_Domain__c','Amount__c','SuppliedEmail__c','User_Action__c','OwnerName__c','Favourited__c','Target_TAT_Time__c','Escalation__c','OverdueIn__c','name','Current_State__c','Priority__c','Indicator__c','Subject__c',' Ownerid','Previous_Queue_Name__c','Owner_Queue__c'};
            
            //For Security scan
            /*
              Authors: Tushar Moradiya
              Purpose: apply selected filter to cases
              Dependencies: none.   
              Start - PUX-305 
             */

            //To apply Queue filter when applied on screen 
            if (selectedQueue != null && selectedQueue.size() > 0) {
                queryWhereClause += ' AND Owner_Queue__c IN: selectedQueue ';
            }
            // Start PUX 421
            if(filterMap!=null){
                for (String key: filterMap.keySet()) {
                
                    String fieldName=key.split(':')[0];
                    fieldsName.add((String)filterMap.get(fieldName));
                    System.debug('filterMap*********'+filterMap);
                    Map < String, Schema.SObjectField > fieldsMap = Schema.SObjectType.CaseManager__c.fields.getMap();                    
                    String fieldResult = String.valueOf(fieldsMap.get(fieldName).getDescribe().getType());   
                    System.debug('fieldResult: '+fieldResult);             
                    if (fieldResult == 'DATETIME') {
                        Date myDate = date.valueOf(String.valueOf(filterMap.get(fieldName)));
                        Date myDate1 = date.valueOf(String.valueOf(filterMap.get(fieldName))) + 1;
                        queryWhereClause += ' AND ' + fieldName + ' >= :myDate AND ' + fieldName + ' < :myDate1';
                    }
                    else if (fieldResult == 'CURRENCY' || fieldResult == 'DOUBLE' || fieldResult == 'BOOLEAN') {
                        queryWhereClause += ' AND ' + fieldName + ' in ('+filterMap.get(fieldName)+')';
                    } else {
						Schema.DescribeFieldResult fieldResultDesc = fieldsMap.get(fieldName).getDescribe();
						if (fieldResultDesc.isCalculated())
						{
							queryWhereClause += ' AND ' + fieldName + ' Like \'%' + filterMap.get(fieldName).replaceAll(',', '\',\'') + '%\'';
						} else {
                        //queryWhereClause += ' AND '+fieldName+' = \''+filterMap.get(fieldName)+'\'';
                        queryWhereClause += ' AND ' + fieldName + ' in (\''+filterMap.get(fieldName).replaceAll(',','\',\'')+'\')';                   
                    }
                }
            }
}
            // End PUX 421
            /*End - PUX-305 */
            //PUX-207   
            totalCaseCountQuery += queryWhereClause + ' limit 2010';
            System.debug('selectedQueue*********'+selectedQueue);   
            System.debug('totalCaseCountQuery*********'+totalCaseCountQuery);   
            cpw.total = (database.countQuery(totalCaseCountQuery));

            query += queryWhereClause;
            queryWhereClause = '';
            query += ' order by ' + sortField + ' ' + sortDirection + ' NULLS LAST';
            query += ' Limit ' + recordsToDisplay + ' OFFSET ' + offset;
            //PUX-207
            system.debug('########'+query);
            //cpw.cases =  CaseListViewController.prepareResponse(database.query(query)); 
            keyList.remove(0);                      
            cpw.cases = database.query(query);
            //PUX-678
            cpw.fieldSetKeyList = keyList;
            cpw.keyFieldLabelMap = keyFieldLabelMap;
            //PUX-678
            cpw.pageSize = recordsToDisplay;
            cpw.page = pageNumber;
            cpw.total = (database.countQuery(totalCaseCountQuery));
            
        } catch (Exception ex) {
            System.debug(ex.getStackTraceString());
        }
        return cpw;
    }
    
   

    /*
        Authors: Niraj P.
        Purpose: To get current login user id and Admin Session ID
        Dependencies : CaseTrackerHelper.js
    */
    @AuraEnabled
    public static Map < String, Object > getUserInfo() {
        Map < String, Object > userDetails = new Map < String, Object > ();
        try {           
            userDetails.put('UserId', userinfo.getuserid());
			//userDetails.put('UserSessionId', getSessionID());
			userDetails.put('UserSessionId', userinfo.getSessionId());
            userDetails.put('InstanceName',  [SELECT Id, InstanceName FROM Organization].InstanceName);
            userDetails.put('UserInfo',[ SELECT Id, Split_Permission__c, Merge_Permission__c, New_Case_Permission__c, Change_Owner_Permission__c, Profile.PermissionsTransferAnyEntity  FROM User where id=:userinfo.getuserid()][0]);
           
        } catch (Exception ex) {
            System.debug(ex.getStackTraceString());
        }
        return userDetails;
    }

   
   
    /*
      Authors: Tushar Moradiya
      Purpose: To get all related Queue name based on logic user cases
      Dependencies: none.   
      Start - PUX-305 
     */
    @AuraEnabled
	public static List<QueueSobject> getQueueDropdown() {
		List<QueueSobject> queueList = new List<QueueSobject> ();
        try {

            //Declaring a Set as we don't want Duplicate Group Ids
            Set<Id> results = new Set<Id>();

            ///Declaring a Map for Group with Role
            Map<Id,Id> grRoleMap = new Map<Id,Id>();

            //Populating the Map with RelatedID(i.e.UserRoledId) as Key
            for(Group gr : [select id,relatedid,name from Group])
            {
                grRoleMap.put(gr.relatedId,gr.id);
            }

            //Groups directly associated to user
            Set<Id> groupwithUser = new Set<Id>();

            //Populating the Group with User with GroupId we are filtering only  for Group of Type Regular,Role and RoleAndSubordinates
            for(GroupMember  u :[select groupId from GroupMember where UserOrGroupId=: UserInfo.getUserID() and (Group.Type = 'Regular' OR Group.Type='Role' OR Group.Type='RoleAndSubordinates')])
            {
                groupwithUser.add(u.groupId);
            }

            //Groups with Role
            for(User  u :[select UserRoleId from User where id=:UserInfo.getUserID() ])
            {
                //Checking if the current User Role is part of Map or not
                if(grRoleMap.containsKey(u.UserRoleId))
                {
                    results.add(grRoleMap.get(u.UserRoleId));
                }
            }
            //Combining both the Set
            results.addAll(groupwithUser);

            //Traversing the whole list of Groups to check any other nested Group
            Map<Id,Id> grMap = new Map<Id,Id>();
            for(GroupMember gr : [select id,UserOrGroupId,Groupid from GroupMember where
                    (Group.Type = 'Regular' OR Group.Type='Role' OR Group.Type='RoleAndSubordinates')])
            {
                grMap.put(gr.UserOrGroupId,gr.Groupid);
            }
            for(Id i :results)
            {
                if(grMap.containsKey(i))
                {
                    results.add(grMap.get(i));
                }
            }

            
            /*List < String > roleRelatedGroupIds = new List < String > ();
            List < Group > groupList = [SELECT id, RelatedId, Type FROM Group where RelatedId =: UserInfo.getUserRoleId() OR DeveloperName = 'AllInternalUsers'];
            for (Group g: groupList) {
                roleRelatedGroupIds.add(g.id);
            }
            */
            set < string > groupIds = new set < string > ();
			/* queueList = [SELECT Group.Name, GroupId FROM GroupMember WHERE(UserOrGroupId =: UserInfo.getUserID() AND(Group.Type = 'Queue'
                    OR Group.Type = 'All Internal Users'))
                OR(UserOrGroupId IN: results AND(Group.Type = 'Queue'
                    OR Group.Type = 'All Internal Users'))
			  Order by Group.Name
            ];
			 */
			Set<Id> groupwithcustomObj = new Set<Id> ();
			for (GroupMember gm :[SELECT Group.Name, GroupId FROM GroupMember WHERE(UserOrGroupId = :UserInfo.getUserID() AND(Group.Type = 'Queue'
			                                                                                                                  OR Group.Type = 'All Internal Users'))
			     OR(UserOrGroupId IN :results AND(Group.Type = 'Queue'
			                                      OR Group.Type = 'All Internal Users'))
			     Order by Group.Name
			     ])
			{
				groupwithcustomObj.add(gm.GroupId);
			}
			String objectType = ObjectUtil.getWithNameSpace('CaseManager__c');
			queueList = [select Id, Queue.Name, SobjectType from QueueSobject where SobjectType = :objectType and QueueId in :groupwithcustomObj Order by Queue.Name];


        } catch (Exception ex) {
            System.debug(ex.getStackTraceString());
        }
        return queueList;
    }
    /*End- PUX-305 */

    /*
        Authors: Rahul Pastagiya
        Purpose: To get all sort fields PUX-290
        Dependencies : CaseListHelper.js
    */
    @AuraEnabled
    public static List < map < String, String >> getSortFieldList() {
        List < map < String, String >> sortDropdownList = new List < map < String, String >> ();
        try {
            String objectType = ObjectUtil.getWithNameSpace('CaseManager__c');
            String fieldSet = ESMConstants.CASE_LIST_SORT_FIELD_SET;
            fieldSet=ObjectUtil.getValidFieldSetName(objectType,fieldSet); 
            Schema.FieldSet fieldSetObj = Schema.getGlobalDescribe().get(objectType).getDescribe().FieldSets.getMap().get(fieldSet);
            for (Schema.FieldSetMember fsm: fieldSetObj.getFields()) {
                Map < String, String > propMap = new Map < String, String > ();
                propMap.put('label', (String) fsm.getLabel());
                propMap.put('value', (String) fsm.getFieldPath());
                sortDropdownList.add(propMap);
            }
        } catch (Exception ex) {
            System.debug(ex.getStackTraceString());
        }
        return sortDropdownList;
    }
    //To get all sort fields PUX-290 

    /*
      Authors: Tushar Moradiya
      Purpose: To get fieldset for filter dropdown
      Dependencies: none.   
      Start - PUX-305 
     */
    @AuraEnabled
    public static List < MapWrapper > getFieldDropdown() {
        List < MapWrapper > lstfieldname = new List < MapWrapper > ();
        try {
            String objectType = ObjectUtil.getWithNameSpace('CaseManager__c');
            String fieldSet =ESMConstants.CASE_LIST_FILTER_FIELD_SET;
            fieldSet=ObjectUtil.getValidFieldSetName(objectType,fieldSet); 
            MapWrapper listKeyVal;
            Schema.FieldSet fieldSetObj = Schema.getGlobalDescribe().get(objectType).getDescribe().FieldSets.getMap().get(fieldSet);
            for (Schema.FieldSetMember fsm: fieldSetObj.getFields()) {
                listKeyVal = new MapWrapper();
                listKeyVal.key = fsm.getFieldPath();
                listKeyVal.value = fsm.getLabel();
                listKeyVal.type = String.valueOf(fsm.getType());
                lstfieldname.add(listKeyVal);
            }
        } catch (Exception ex) {
            System.debug(ex.getStackTraceString());
        }
        //lstfieldname.put
        return lstfieldname;
    }

    /*
      Authors: Tushar M.
      Purpose: Wrapper class for Filter field dropdown.
      Dependencies: used in method 'getFieldDropdown'. PUX-305
     */
    class MapWrapper {
        @AuraEnabled
        public String key;
        @AuraEnabled
        public String value;
        @AuraEnabled
        public String type;
    }
    /*End- PUX-305 */

    /*
        Authors: Rahul Pastagiya
        Purpose: To wrap pagination related values
        Dependencies : CaseListHelper.js
    */
    public class CasePagerWrapper {
        @AuraEnabled public Integer pageSize {get;set;}
        @AuraEnabled public Integer page {get;set;}
        @AuraEnabled public List<String> fieldSetKeyList {get;set;}
        @AuraEnabled public Integer total {get;set;}
        @AuraEnabled public List < CaseManager__c > cases {get;set;}
        @AuraEnabled public Map<String, String> keyFieldLabelMap;
	}

	@AuraEnabled
	public static List<Map<String, String>> getViews() {
		List<System.SelectOption> listViews = new List<System.SelectOption> ();
		List<Map<String, String>> listOfMap = new List<Map<String, String>> ();
		Map<String, String> viewValueLabel = new Map<String, String> ();
		Map<String, String> LabelValueMap = new Map<String, String> ();
		List<String> listViewsLabels = new List<String> ();
		String objectType = ObjectUtil.getWithNameSpace('CaseManager__c');
		String SOQL = 'SELECT Id FROM ' + objectType + ' LIMIT 1';
		ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(Database.getquerylocator(SOQL));
		listViews = ssc.getListViewOptions();
		for (System.SelectOption sel : listViews)
		{
			listViewsLabels.add(sel.getLabel());
			LabelValueMap.put(sel.getLabel(), sel.getValue());
		}
		listViewsLabels.sort();
		for (String label : listViewsLabels)
		{
			viewValueLabel = new Map<String, String> ();
			viewValueLabel.put('label', label);
			viewValueLabel.put('value', LabelValueMap.get(label));
			listOfMap.add(viewValueLabel);
		}
		return listOfMap;
	}

	public static List<Id> getRecordIdsAgainstView(String objName, Id viewId, String query, Integer pageNumber, Integer recordsToDisplay) {
		/*if (query == null)
		  {
		  query = 'SELECT Id FROM '+objName+' limit 1';
		  }*/
		System.debug('getRecordIdsAgainstView:' + query);
		//System.debug('recordsMap.size : 8'+query);
		ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(Database.getquerylocator(query));
		System.debug('recordsMap.size : 9' + viewId);
		ssc.setFilterId(viewId);
		System.debug('recordsMap.size : 10' + recordsToDisplay);
		ssc.setPageSize(2000);
		System.debug('recordsMap.size : 11' + pageNumber);
		ssc.setPageNumber(1);
		List<id> recordsMap = new List<id> ();
		//ssc.getCompleteResult();
		for (sObject testRecord : (List<sObject>) ssc.getRecords())
		{
			recordsMap.add(testRecord.id);
		}
		while (ssc.getHasNext())
		{
			ssc.next();
			//recordsMap = new Map<id,SObject>(ssc.getRecords());
			for (sObject testRecord : (List<sObject>) ssc.getRecords())
			{
				recordsMap.add(testRecord.id);
			}
		}
		//Map<id,SObject> recordsMap = new Map<id,SObject>(ssc.getRecords());

		//System.debug('recordsMap.size : '+ssc.getRecords()[0]); */
		System.debug('recordsMap.size : ' + recordsMap.size());
		return recordsMap;
    }
}