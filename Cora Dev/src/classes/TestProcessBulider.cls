global class TestProcessBulider
{
    public TestProcessBulider()
    {
        
    }
    
    public pageReference Clickbutton()
    {
        try
        {
            system.debug('-------------Call------------');
            GL_Code__c gl=new GL_Code__c();
            gl.Description__c='Test';
            gl.Name='testPB';
            insert gl;
            
            PB_Error__c pb=[select id,Error_Message__c from PB_Error__c limit 1];
            if(pb!=null)
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,pb.Error_Message__c));
                delete pb;
            }
        }
        catch(Exception e)
        {
            system.debug('-------------e------------'+e.getcause());
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,string.valueof(e.getcause())));
        }
        return null;
    }
    
      @InvocableMethod(label='Check PB Error' description='Check PB ERrror')
      public static List<String> getAccountNames() {
            try
            {
                system.debug('-------------Call------------');
                GL_Code__c gl=new GL_Code__c();
                gl.Description__c='Test';
                gl.Name='testPB';
                update gl;
                return null;
            }
            catch(Exception e)
            {
                system.debug('-------------e------------'+e.getmessage());
                List<String> temp=new List<String>();
                temp.add(string.valueof(e.getcause()));
                PB_Error__c pb=new PB_Error__c();
                pb.Error_Message__c=string.valueof(e.getmessage());
                insert pb;
                return temp;
            }
      }
}