public class InvoiceValidationExceptionManagerNew {
	/*
	 implements InvoiceValidationExceptionInterface
	global Map<String, PO_Line_Item__c> poLineMap = new Map<String, PO_Line_Item__c> ();
	Map<String, Map<String, String>> ValidationExceptionMsgMap = new Map<String, Map<String, String>> ();
	Map<String, String> ValExecMsgMap = new Map<String, String> ();
	CommonFunctionController cfObj = new CommonFunctionController();

	//this is deprecated method of InvoiceValidationExceptionInterface interface
	global Map<Object, Object> execute(sobject childSobject, String objectName, String childNameSpace, String prefix, List<PO_Line_Item__c> poLineItemList,
	                                   List<Invoice_Line_Item__c> invLineItemList, List<GRN_Line_Item__c> grnLineItemList, Map<Object, Object> validationMap) {
		return null;
	}

	/*
	  extraParam - {'POLst','Purchase Order List'}
	 */
	 /*
	global Map<Object, Object> execute(sobject childSobject, String objectName, String childNameSpace, String prefix, List<PO_Line_Item__c> poLineItemList,
	                                   List<Invoice_Line_Item__c> invLineItemList, List<GRN_Line_Item__c> grnLineItemList, Map<Object, Object> validationMap, Map<Object, Object> extraParam) {


		Set<String> poLineIdSet = new Set<String> ();
		if (poLineItemList != null && poLineItemList.size() > 0) {
			for (PO_Line_Item__c poLine : poLineItemList) {
				poLineIdSet.add(poLine.Id);
			}
			for (PO_Line_Item__c poLine : database.query(cfObj.getCreatableFieldsSOQL(childNameSpace + 'PO_Line_Item__c', 'Id IN: poLineIdSet'))) {
				poLineMap.put(poLine.Id, poLine);
			}
		}

		validationMap.put('stopExecution', false);
		//--- Changed by Dilshad | JIRA:ESMPROD-75 | 14-09-2015 | Starts ---//
		//--- Changed by Dilshad | JIRA:ESMPROD-10 | 31-08-2015 | Starts ---//
		//--- PO Invoice ---//
		if (childSobject.get(childNameSpace + 'Document_Type__c') == 'PO Invoice') {
			//--- PO Invoice Validation ---//
			POMatchValidation(childSobject, objectName, childNameSpace, prefix, poLineItemList, invLineItemList, validationMap);
			//--- PO Invoice Exception ---//
			if (validationMap.get('stopExecution') != null && validationMap.get('stopExecution') != '' && Boolean.ValueOf(validationMap.get('stopExecution')) == false) {
				POMatchException(childSobject, objectName, childNameSpace, prefix, poLineItemList, invLineItemList, validationMap);
			}
			//--- PO Invoice GRN Valiation & Exception ---//
			//--- Changed by Dilshad | JIRA:ESMPROD-39 | 03-09-2015 | Starts ---//
			//--- Addition condition of Do_GRN_Match has been added ---//     
			if (childSobject.get(childNameSpace + 'Do_GRN_Match__c') != null && childSobject.get(childNameSpace + 'Do_GRN_Match__c') == true
			    && validationMap.get('stopExecution') != null && validationMap.get('stopExecution') != '' && Boolean.ValueOf(validationMap.get('stopExecution')) == false) {
				//--- Changed by Dilshad | JIRA:ESMPROD-39 | 03-09-2015 | Ends ---//
				GRNMatchValidation(childSobject, objectName, childNameSpace, prefix, poLineItemList, invLineItemList, grnLineItemList, validationMap);

			}
		}

		//--- Non PO Invoice ---//
		if (childSobject.get(childNameSpace + 'Document_Type__c') == 'Non PO Invoice') {
			//--- Non PO Invoice Validation ---//
			NonPOMatchValidation(childSobject, objectName, childNameSpace, prefix, poLineItemList, invLineItemList, validationMap);
			system.debug('@@ NonPOMatchValidation:' + validationMap);
			//--- Non PO Invoice Exception ---//
			if (validationMap.get('stopExecution') != null && validationMap.get('stopExecution') != '' && Boolean.ValueOf(validationMap.get('stopExecution')) == false) {
				NonPOMatchException(childSobject, objectName, childNameSpace, prefix, poLineItemList, invLineItemList, validationMap);
				System.debug('@@ NonPOMatchException:' + validationMap);
			}
		}

		//--- Changed by Dilshad | JIRA:ESMPROD-75 | 14-09-2015 | Ends ---//    


		//--- Insert Exception History---//
		cfObj.InsertExceptionHistory(childSobject, childNameSpace);
		//-------------------------------//
		return validationMap;
		//--- Changed by Dilshad | JIRA:ESMPROD-10 | 31-08-2015 | Ends ---//
	}

	//--- Changed by Dilshad | JIRA:ESMPROD-10 | 31-08-2015 | Starts ---//
	global Map<Object, object> POMatchValidation(sobject childSobject, String objectName, String childNameSpace, String prefix, List<PO_Line_Item__c> poLineItemList, List<Invoice_Line_Item__c> invLineItemList, Map<Object, Object> validationMap) {

		//string isHardStopValidation = String.ValueOf(validationMap.get('isHardStopValidation'));

		//-- Duplicate invoice --//
		checkDuplicateInvoice(childSobject, objectName, childNameSpace, validationMap);
		if (Boolean.ValueOf(validationMap.get('stopExecution'))) {
			return validationMap;
		}

		//--- Added by Dilshad | JIRA:ESMPROD-43 | 07-09-2015 | Starts ---//
		//--- Check for duplicate Invoice Line No ---//
		checkDuplicateInvoiceLineNo(childSobject, childNameSpace, invLineItemList, validationMap);
		//--- Added by Dilshad | JIRA:ESMPROD-43 | 07-09-2015 | Ends ---//

		//-- Invoice line item total should match with invoice amount --//
		checkInvAmtInvLineAmt(childSobject, childNameSpace, invLineItemList, validationMap);
		if (Boolean.ValueOf(validationMap.get('stopExecution'))) {
			return validationMap;
		}

		//-- Invoice Total amount shiuld match with Amount + Tax + Freight amount
		checkTotalAmountMismatch(childSobject, childNameSpace, invLineItemList, validationMap);
		if (Boolean.ValueOf(validationMap.get('stopExecution'))) {
			return validationMap;
		}

		return validationMap;
	}

	//--- Changed by Dilshad | JIRA:ESMPROD-75 | 14-09-2015 | Starts ---//
	public Map<Object, object> NonPOMatchValidation(sobject childSobject, String objectName, String childNameSpace, String prefix, List<PO_Line_Item__c> poLineItemList, List<Invoice_Line_Item__c> invLineItemList, Map<Object, Object> validationMap) {

		//-- Duplicate invoice --//
		checkDuplicateInvoice(childSobject, objectName, childNameSpace, validationMap);
		if (Boolean.ValueOf(validationMap.get('stopExecution'))) {
			return validationMap;
		}
		//-- Invoice Total amount shiuld match with Amount + Tax + Freight amount
		checkTotalAmountMismatch(childSobject, childNameSpace, invLineItemList, validationMap);
		if (Boolean.ValueOf(validationMap.get('stopExecution'))) {
			return validationMap;
		}
		return validationMap;
	}
	//--- Changed by Dilshad | JIRA:ESMPROD-75 | 14-09-2015 | Ends ---//

	global Map<Object, Object> POMatchException(sobject childSobject, String objectName, String childNameSpace, String prefix, List<PO_Line_Item__c> poLineItemList, List<Invoice_Line_Item__c> invLineItemList, Map<Object, Object> validationMap) {
		//-- Requester missing --//
		RequestorMissingException(childSobject, childNameSpace, validationMap);
		//-- Invoice line not found --//
		if (childSobject.get(childNameSpace + 'Document_Type__c') == 'PO Invoice') {
			InvoiceLineNotFoundException(childSobject, childNameSpace, invLineItemList, validationMap);
		}
		if (childSobject.get(childNameSpace + 'Document_Type__c') == 'PO Invoice') {
			Map<String, Double> InvLineQtyMap = new Map<String, Double> ();
			if (invLineItemList != null && invLineItemList.size() > 0) {
				string QuantityMismatchMsg = '';
				string UOMMismatchMsg = '';
				string POLineClosedMsg = '';
				//--- Added by Dilshad | JIRA:ESMPROD-233 | 07-03-2016 | Starts ---//
				Map<String, Double> InvLineRemQtyMap = new Map<String, Double> ();
				for (Invoice_Line_Item__c invLine : invLineItemList) {
					if (InvLineRemQtyMap.get(invLine.PO_Line_Item__c) != null) {
						InvLineRemQtyMap.put(invLine.PO_Line_Item__c, InvLineRemQtyMap.get(invLine.PO_Line_Item__c) + invLine.Old_Quantity__c);
					} else {
						InvLineRemQtyMap.put(invLine.PO_Line_Item__c, invLine.Old_Quantity__c);
					}
				}
				//--- Added by Dilshad | JIRA:ESMPROD-233 | 07-03-2016 | Ends ---//
				for (Invoice_Line_Item__c invLine : invLineItemList) {

					//-- fillup inv line qty map --//
					if (InvLineQtyMap.get(invLine.PO_Line_Item__c) != null) {
						InvLineQtyMap.put(invLine.PO_Line_Item__c, InvLineQtyMap.get(invLine.PO_Line_Item__c) + invLine.Quantity__c);
					} else {
						InvLineQtyMap.put(invLine.PO_Line_Item__c, invLine.Quantity__c);
					}

					//--- Added by Dilshad | JIRA:ESMPROD-84 | 22-09-2015 | Starts ---//
					invLine.Is_Mismatch__c = false;
					//--- Added by Dilshad | JIRA:ESMPROD-84 | 22-09-2015 | Ends ---//

					PO_Line_Item__c poLineObj = poLineMap.get(invLine.PO_Line_Item__c);

					if (poLineObj != null) {

						//-- Quntity mismatch --//
						//--- Added by Dilshad | JIRA:ESMPROD-233 | 07-03-2016 | Starts ---//
						Double poLineRemQuantity = InvLineRemQtyMap.get(invLine.PO_Line_Item__c) != null ? (poLineObj.ESM_Remaining_Quantity__c + InvLineRemQtyMap.get(invLine.PO_Line_Item__c)) : poLineObj.ESM_Remaining_Quantity__c;
						if (InvLineQtyMap.get(invLine.PO_Line_Item__c) > poLineRemQuantity) {
							//--- Added by Dilshad | JIRA:ESMPROD-233 | 07-03-2016 | Ends ---// 
							invLine.Is_Mismatch__c = true;
							QuantityMismatchMsg += invLine.Invoice_Line_No__c + ',';
							validationMap.put('stopExecution', true);
							//--- Changed by Dilshad | JIRA:ESMPROD-60 | 09-09-2015 | Starts ---//
							if (childSobject.get(childNameSpace + 'Exception_Reason__c') != null && childSobject.get(childNameSpace + 'Exception_Reason__c') != '') {
								String exceptionReasons = String.valueOf(childSobject.get(childNameSpace + 'Exception_Reason__c'));
								if (!exceptionReasons.containsIgnoreCase('Quantity MisMatch')) {
									childSobject.put(childNameSpace + 'Exception_Reason__c', exceptionReasons + ';Quantity MisMatch');
								}
							}
							else {
								childSobject.put(childNameSpace + 'Exception_Reason__c', 'Quantity Mismatch');
							}
							//--- Changed by Dilshad | JIRA:ESMPROD-60 | 09-09-2015 | Ends ---//
						}

						//-- UOM mismatch --//
						if (invLine.UOM__c != poLineObj.UOM__c) {
							invLine.Is_Mismatch__c = true;
							UOMMismatchMsg += invLine.Invoice_Line_No__c + ',';
							validationMap.put('stopExecution', true);
							//--- Changed by Dilshad | JIRA:ESMPROD-60 | 09-09-2015 | Starts ---//
							if (childSobject.get(childNameSpace + 'Exception_Reason__c') != null && childSobject.get(childNameSpace + 'Exception_Reason__c') != '') {
								String exceptionReasons = String.valueOf(childSobject.get(childNameSpace + 'Exception_Reason__c'));
								if (!exceptionReasons.containsIgnoreCase('UOM Mismatch')) {
									childSobject.put(childNameSpace + 'Exception_Reason__c', exceptionReasons + ';UOM Mismatch');
								}
							}
							else {
								childSobject.put(childNameSpace + 'Exception_Reason__c', 'UOM Mismatch');
							}
							//--- Changed by Dilshad | JIRA:ESMPROD-60 | 09-09-2015 | Ends ---//
						}

						//-- PO Line closed--//
						if (poLineObj.Status__c != null && String.valueOf(poLineObj.Status__c).toLowercase() == 'closed') {
							invLine.Is_Mismatch__c = true;
							if (invLine.Invoice_Line_No__c == null) {
								invLine.Invoice_Line_No__c = '';
							}
							POLineClosedMsg += invLine.Invoice_Line_No__c + ',';
							validationMap.put('stopExecution', true);

							//--- Changed by Dilshad | JIRA:ESMPROD-60 | 09-09-2015 | Starts ---//
							if (childSobject.get(childNameSpace + 'Exception_Reason__c') != null && childSobject.get(childNameSpace + 'Exception_Reason__c') != '') {
								String exceptionReasons = String.valueOf(childSobject.get(childNameSpace + 'Exception_Reason__c'));
								if (!exceptionReasons.containsIgnoreCase('PO Line Closed')) {
									childSobject.put(childNameSpace + 'Exception_Reason__c', exceptionReasons + ';PO Line Closed');
								}
							}
							else {
								childSobject.put(childNameSpace + 'Exception_Reason__c', 'PO Line Closed');
							}
							//--- Changed by Dilshad | JIRA:ESMPROD-60 | 09-09-2015 | Ends ---//
						}

					}

				}
				///----///
				if (POLineClosedMsg != '') {
					POLineClosedMsg = POLineClosedMsg.removeEnd(',');
					if (ValExecMsgMap.get('Invoice_Exception') != null) {
						ValExecMsgMap.put('Invoice_Exception', ValExecMsgMap.get('Invoice_Exception') + '\n' + 'PO Line Closed has been identified for Invoice Line Item : ' + POLineClosedMsg);
					} else {
						ValExecMsgMap.put('Invoice_Exception', 'PO Line Closed has been identified for Invoice Line Item : ' + POLineClosedMsg);
					}
				}
				if (QuantityMismatchMsg != '') {
					QuantityMismatchMsg = QuantityMismatchMsg.removeEnd(',');
					if (ValExecMsgMap.get('Invoice_Exception') != null) {
						ValExecMsgMap.put('Invoice_Exception', ValExecMsgMap.get('Invoice_Exception') + '\n' + 'Quantity Mismatch has been identified for Invoice Line Item : ' + QuantityMismatchMsg);
					} else {
						ValExecMsgMap.put('Invoice_Exception', 'Quantity Mismatch has been identified for Invoice Line Item : ' + QuantityMismatchMsg);
					}
				}

				if (UOMMismatchMsg != '') {
					UOMMismatchMsg = UOMMismatchMsg.removeEnd(',');
					if (ValExecMsgMap.get('Invoice_Exception') != null) {
						ValExecMsgMap.put('Invoice_Exception', ValExecMsgMap.get('Invoice_Exception') + '\n' + 'UOM Mismatch has been identified for Invoice Line Item : ' + UOMMismatchMsg);
					} else {
						ValExecMsgMap.put('Invoice_Exception', 'UOM Mismatch has been identified for Invoice Line Item : ' + UOMMismatchMsg);
					}
				}
			}
		}
		validationMap.put('jsonValidationExceptionMsg', JSON.serialize(ValExecMsgMap));
		return validationMap;
	}

	//--- Added by Dilshad | JIRA:ESMPROD-75 | 15-09-2015 | Starts ---//
	public Map<Object, Object> NonPOMatchException(sobject childSobject, String objectName, String childNameSpace, String prefix, List<PO_Line_Item__c> poLineItemList, List<Invoice_Line_Item__c> invLineItemList, Map<Object, Object> validationMap) {
		//-- Requester missing --//
		RequestorMissingException(childSobject, childNameSpace, validationMap);
		validationMap.put('jsonValidationExceptionMsg', JSON.serialize(ValExecMsgMap));
		return validationMap;
	}
	//--- Added by Dilshad | JIRA:ESMPROD-75 | 15-09-2015 | Ends ---//

	global Map<object, object> GRNMatchValidation(sobject childSobject, String objectName, String childNameSpace, String prefix, List<PO_Line_Item__c> poLineItemList, List<Invoice_Line_Item__c> invLineItemList, List<GRN_Line_Item__c> grnLineItemList, Map<Object, Object> validationMap) {

		string jsonValidationExceptionMsg = '';
		childSobject.put(childNameSpace + 'Pending_GRN__c', false);
		Map<Id, GRN_Line_Item__c> grnLineItemMap = new Map<Id, GRN_Line_Item__c> ();
		for (GRN_Line_Item__c grnl : grnLineItemList) {
			GRNLineItemMap.put(grnl.id, grnl);
		}

		//--- Condition-1: PO Mismatch between selected GRN and Invoice ---//
		//--- Changed by Dilshad | JIRA:ESMPROD-10 | 31-08-2015 | Starts ---//
		Set<Id> GRNId = new set<Id> ();
		Set<Id> POId = new set<Id> ();
		Map<Id, Id> GRNMap = new Map<Id, Id> ();
		for (PO_Line_Item__c pol : poLineItemList) {
			POId.add(pol.Purchase_Order__c);
		}
		for (GRN_Line_Item__c grnl : grnLineItemList) {
			GRNMap.put(grnl.GRN__c, grnl.Purchase_Order__c);
		}

		for (Invoice_Line_Item__c invl : invLineItemList) {
			if (invl.GRN__c != null) {

				if (!POId.contains(GRNMap.get(invl.GRN__c))) {
					validationMap.put('stopExecution', 'true');
					ValExecMsgMap.put('Invoice_Validation', 'PO Mismatch between selected GRN and Invoice.');
					system.debug('PO Mismatch between selected GRN and Invoice.');
					validationMap.put('jsonValidationExceptionMsg', JSON.serialize(ValExecMsgMap));
					return validationMap;
				}
			}
		}
		//---------------------------------------------------------------//
		//--- Changed by Dilshad | JIRA:ESMPROD-10 | 31-08-2015 | Ends ---//

		Map<String, Double> InvLineQtyMap = new Map<String, Double> ();

		if (invLineItemList != null && invLineItemList.size() > 0) {
			for (Invoice_Line_Item__c invl : invLineItemList) {
				invl.Is_Mismatch__c = false;
				//--- Changed by Dilshad | JIRA:ESMPROD-88 | 24-09-2015 | Starts ---//
				//if(invl.GRN__c != null ){
				//--- Changed by Dilshad | JIRA:ESMPROD-88 | 24-09-2015 | Ends ---//
				system.debug('invl.GRN_Line_Item__c:' + invl.GRN_Line_Item__c);
				if (invl.GRN_Line_Item__c != null) {
					if (GRNLineItemMap.get(invl.GRN_Line_Item__c) == null) {
						validationMap.put('stopExecution', 'true');
						ValExecMsgMap.put('Invoice_Validation', 'GRN and GRN Line item\'s GRN should be same.');
						validationMap.put('jsonValidationExceptionMsg', JSON.serialize(ValExecMsgMap));
						return validationMap;
					}

					if (GRNLineItemMap.get(invl.GRN_Line_Item__c) != null) {
						//--- Added by Dilshad | JIRA:ESMPROD-54 | 07-09-2015 | Starts ---//
						//--- Changed by Dilshad | JIRA:ESMPROD-88 | 24-09-2015 | Starts ---//
						if (invl.GRN__c != null && invl.GRN__c != GRNLineItemMap.get(invl.GRN_Line_Item__c).GRN__c) {
							//--- Changed by Dilshad | JIRA:ESMPROD-88 | 24-09-2015 | Ends ---//    
							validationMap.put('stopExecution', 'true');
							ValExecMsgMap.put('Invoice_Validation', 'GRN and GRN Line item\'s GRN should be same.');
							validationMap.put('jsonValidationExceptionMsg', JSON.serialize(ValExecMsgMap));

							return validationMap;
						}
						//--- Added by Dilshad | JIRA:ESMPROD-54 | 07-09-2015 | Ends ---//
						//--- Changed by Dilshad | JIRA:ESMPROD-45 | 03-09-2015 | Starts ---//
						//--- the below validation has been moved above quantity match check ---//
						if (GRNLineItemMap.get(invl.GRN_Line_Item__c).PO_Line_Item__c != null && GRNLineItemMap.get(invl.GRN_Line_Item__c).PO_Line_Item__c != invl.PO_Line_Item__c) {
							validationMap.put('stopExecution', 'true');
							ValExecMsgMap.put('Invoice_Validation', 'GRN Line No. mismatch identified for Invoice Line No. : ' + invl.Invoice_Line_No__c);
							validationMap.put('jsonValidationExceptionMsg', JSON.serialize(ValExecMsgMap));
							return validationMap;
						}
						//--- Changed by Dilshad | JIRA:ESMPROD-45 | 03-09-2015 | Ends ---//     
						// ==================== For Quantity Match Check ================== 
						Decimal GRNLineQuantiy = GRNLineItemMap.get(invl.GRN_Line_Item__c) != null ? GRNLineItemMap.get(invl.GRN_Line_Item__c).Quantity__c : 0;

						if (InvLineQtyMap.get(invl.GRN_Line_Item__c) != null) {
							InvLineQtyMap.put(invl.GRN_Line_Item__c, InvLineQtyMap.get(invl.GRN_Line_Item__c) + invl.Quantity__c);
						} else {
							InvLineQtyMap.put(invl.GRN_Line_Item__c, invl.Quantity__c);
						}
						system.debug('Invoice Line Item Quantity:' + InvLineQtyMap.get(invl.GRN_Line_Item__c));
						system.debug('GRN Line Item Quantity:' + GRNLineQuantiy);
						if (InvLineQtyMap.get(invl.GRN_Line_Item__c) > GRNLineQuantiy) {

							childSobject.put(childNameSpace + 'Pending_GRN__c', true);
							invl.Is_Mismatch__c = true;
						} else {

							if (GRNLineItemMap.get(invl.GRN_Line_Item__c).Quantity__c <= 0) {
								childSobject.put(childNameSpace + 'Pending_GRN__c', true);
								invl.Is_Mismatch__c = true;
							}
						}
						// ==================== For Quantity MAtch Check ================== 

					}
					else {
						childSobject.put(childNameSpace + 'Pending_GRN__c', true);
						invl.Is_Mismatch__c = true;
					}
				} else {
					childSobject.put(childNameSpace + 'Pending_GRN__c', true);
					invl.Is_Mismatch__c = true;
				}
				//--- Changed by Dilshad | JIRA:ESMPROD-88 | 24-09-2015 | Starts ---//
				//}else{
				//    childSobject.put(childNameSpace+'Pending_GRN__c', true);
				//    invl.Is_Mismatch__c = true;

				//}
				if (invl.GRN__c == null && !invl.Is_Mismatch__c) {
					invl.GRN__c = GRNLineItemMap.get(invl.GRN_Line_Item__c).GRN__c;
				}
				//--- Changed by Dilshad | JIRA:ESMPROD-88 | 24-09-2015 | Ends ---//
			}
		}
		if (Boolean.ValueOf(childSobject.get(childNameSpace + 'Pending_GRN__c'))) {
			childSobject.put(childNameSpace + 'Exception_Reason__c', 'Pending GRN');
			ValExecMsgMap.put('Invoice_Exception', 'Pending GRN exception has been identified for this document.');
			validationMap.put('stopExecution', 'true');
		}

		validationMap.put('jsonValidationExceptionMsg', JSON.serialize(ValExecMsgMap));

		return validationMap;
	}

	//--- Duplicate Invoice is found for Invoice No ---//
	private Map<Object, Object> checkDuplicateInvoice(sobject childSobject, String objectName, String childNameSpace, Map<Object, Object> validationMap) {
		String query = 'SELECT count() FROM ' + objectName + ' WHERE ' + childNameSpace + 'Invoice_No__c = \'' + childSobject.get(childNameSpace + 'Invoice_No__c') + '\'';
		if (childSobject.get('Id') != null && childSobject.get('Id') != '') {
			query += ' AND Id != \'' + childSobject.get('Id') + '\'';
		}
		query += ' LIMIT 1';
		System.Debug('@@@ duplicate invoice query :' + query);
		if (Database.countQuery(query) > 0) {
			ValExecMsgMap.put('Invoice_Validation', 'Duplicate Invoice is found for Invoice No.: ' + childSobject.get(childNameSpace + 'Invoice_No__c'));
			validationMap.put('stopExecution', true);
			validationMap.put('jsonValidationExceptionMsg', JSON.serialize(ValExecMsgMap));
		}
		return validationMap;
	}

	//--- Invoice Amount must be sum of invoice line item Amount ---//
	private Map<Object, Object> checkInvAmtInvLineAmt(sobject childSobject, String childNameSpace, List<Invoice_Line_Item__c> invLineItemList, Map<Object, Object> validationMap) {
		Decimal invLineAmount = 0;
		if (invLineItemList != null) {
			for (Invoice_Line_Item__c invLine : invLineItemList) {
				if (invLine.Amount__c == null) {
					invLine.Amount__c = 0;
				}
				invLineAmount += invLine.Amount__c;
			}
		}
		//--- Changed by Mekyush | JIRA:ESMPROD-669 | 13-07-2017 | Start ---//
		if (invLineItemList.size() > 0 &&
		((!CommonFunctionController.isMultiCurrEnabled && childSobject.get(childNameSpace + 'Amount__c') != null && childSobject.get(childNameSpace + 'Amount__c') != invLineAmount) ||
		((CommonFunctionController.isMultiCurrEnabled && childSobject.get(childNameSpace + 'Amount_Currency__c') != null && childSobject.get(childNameSpace + 'Amount_Currency__c') != invLineAmount)))
		) {
			//--- Changed by Mekyush | JIRA:ESMPROD-669 | 13-07-2017 | End ---//
			validationMap.put('stopExecution', 'true');
			ValExecMsgMap.put('Invoice_Validation', 'Invoice Amount must be sum of invoice line item Amount.');
			validationMap.put('jsonValidationExceptionMsg', JSON.serialize(ValExecMsgMap));
		}
		return validationMap;
	}

	//--- Invoice Total Amount mismatch (Total Amount = Amount + Tax + Other Charges/Surcharges) ---//
	private Map<Object, Object> checkTotalAmountMismatch(sobject childSobject, String childNameSpace, List<Invoice_Line_Item__c> invLineItemList, Map<Object, Object> validationMap) {
		Decimal invTotalAmount = childSobject.get(childNameSpace + 'Total_Amount__c') != null ? (Decimal) childSobject.get(childNameSpace + 'Total_Amount__c') : 0;
		//--- Changed by Mekyush | JIRA:ESMPROD-669 | 13-07-2017 | Start ---//
		Decimal invAmount = !CommonFunctionController.isMultiCurrEnabled && childSobject.get(childNameSpace + 'Amount__c') != null ? (Decimal) childSobject.get(childNameSpace + 'Amount__c') : CommonFunctionController.isMultiCurrEnabled && childSobject.get(childNameSpace + 'Amount_Currency__c') != null ? (Decimal) childSobject.get(childNameSpace + 'Amount_Currency__c') : 0;
		Decimal invOthsurChargeAmount = !CommonFunctionController.isMultiCurrEnabled && childSobject.get(childNameSpace + 'Other_Charges_Surcharges__c') != null ? (Decimal) childSobject.get(childNameSpace + 'Other_Charges_Surcharges__c') : CommonFunctionController.isMultiCurrEnabled && childSobject.get(childNameSpace + 'Other_Charges_Surcharges_Currency__c') != null ? (Decimal) childSobject.get(childNameSpace + 'Other_Charges_Surcharges_Currency__c') : 0;
		Decimal invTaxAmount = !CommonFunctionController.isMultiCurrEnabled && childSobject.get(childNameSpace + 'Tax__c') != null ? (Decimal) childSobject.get(childNameSpace + 'Tax__c') : CommonFunctionController.isMultiCurrEnabled && childSobject.get(childNameSpace + 'Tax_Currency__c') != null ? (Decimal) childSobject.get(childNameSpace + 'Tax_Currency__c') : 0;
		//--- Changed by Mekyush | JIRA:ESMPROD-669 | 13-07-2017 | End ---//

		if (invTotalAmount != (invAmount + invOthsurChargeAmount + invTaxAmount)) {
			validationMap.put('stopExecution', 'true');
			ValExecMsgMap.put('Invoice_Validation', 'Invoice Total Amount mismatch (Total Amount = Amount + Tax + Other Charges/Surcharges)');
			validationMap.put('jsonValidationExceptionMsg', JSON.serialize(ValExecMsgMap));
		}
		return validationMap;
	}

	//--- Exception Requestor/Buyer Missing has been identified for this document ---//
	private Map<Object, Object> RequestorMissingException(sobject childSobject, String childNameSpace, Map<Object, Object> validationMap) {
		if (childSobject.get(childNameSpace + 'Requestor_Buyer__c') == null) {
			validationMap.put('stopExecution', true);
			if (ValExecMsgMap.get('Invoice_Exception') != null) {
				ValExecMsgMap.put('Invoice_Exception', ValExecMsgMap.get('Invoice_Exception') + '\n' + 'Requestor/Buyer Missing has been identified for this document.');
			} else {
				ValExecMsgMap.put('Invoice_Exception', 'Requestor/Buyer Missing has been identified for this document.');
			}
			//--- Changed by Dilshad | JIRA:ESMPROD-60 | 09-09-2015 | Starts ---//
			if (childSobject.get(childNameSpace + 'Exception_Reason__c') != null && childSobject.get(childNameSpace + 'Exception_Reason__c') != '') {
				String exceptionReasons = String.valueOf(childSobject.get(childNameSpace + 'Exception_Reason__c'));
				if (!exceptionReasons.containsIgnoreCase('Requestor/Buyer Missing')) {
					childSobject.put(childNameSpace + 'Exception_Reason__c', exceptionReasons + ';Requestor/Buyer Missing');
				}
			}
			else {
				childSobject.put(childNameSpace + 'Exception_Reason__c', 'Requestor/Buyer Missing');
			}
			//--- Changed by Dilshad | JIRA:ESMPROD-60 | 09-09-2015 | Ends ---//
		}
		return validationMap;
	}

	//--- Invoice Line Not Found has been identified for this document ---//
	private Map<Object, Object> InvoiceLineNotFoundException(sobject childSobject, String childNameSpace, List<Invoice_Line_Item__c> invLineItemList, Map<Object, Object> validationMap) {
		if (invLineItemList == null || invLineItemList.size() == 0) {
			validationMap.put('stopExecution', true);
			if (ValExecMsgMap.get('Invoice_Exception') != null) {
				ValExecMsgMap.put('Invoice_Exception', ValExecMsgMap.get('Invoice_Exception') + '\n' + 'Invoice Line Not Found has been identified for this document.');
			} else {
				ValExecMsgMap.put('Invoice_Exception', 'Invoice Line Not Found has been identified for this document.');
			}
			//--- Changed by Dilshad | JIRA:ESMPROD-60 | 09-09-2015 | Starts ---//
			if (childSobject.get(childNameSpace + 'Exception_Reason__c') != null && childSobject.get(childNameSpace + 'Exception_Reason__c') != '') {
				String exceptionReasons = String.valueOf(childSobject.get(childNameSpace + 'Exception_Reason__c'));
				if (!exceptionReasons.containsIgnoreCase('Invoice Line Not Found')) {
					childSobject.put(childNameSpace + 'Exception_Reason__c', exceptionReasons + ';Invoice Line Not Found');
				}
			}
			else {
				childSobject.put(childNameSpace + 'Exception_Reason__c', 'Invoice Line Not Found');
			}
			//--- Changed by Dilshad | JIRA:ESMPROD-60 | 09-09-2015 | Ends ---//
		}
		return validationMap;
	}

	global void GRNMatchException() {
	}
	//--- Changed by Dilshad | JIRA:ESMPROD-10 | 31-08-2015 | Ends ---//
	//--- Changed by Dilshad | JIRA:ESMPROD-32 | 01-09-2015 | Ends ---//
	//--- Added by Dilshad | JIRA:ESMPROD-43 | 07-09-2015 | Starts ---//
	//--- Check for duplicate Invoice Line No ---//
	private Map<Object, Object> checkDuplicateInvoiceLineNo(sobject childSobject, String childNameSpace, List<Invoice_Line_Item__c> invLineItemList, Map<Object, Object> validationMap) {
		if (invLineItemList != null && invLineItemList.size() > 0) {
			Set<String> InvLineNoSet = new Set<String> ();
			for (Invoice_Line_Item__c invl : invLineItemList) {

				if (InvLineNoSet.contains(invl.Invoice_Line_No__c)) {
					ValExecMsgMap.put('Invoice_Validation', 'Duplicate Invoice Line No is found for Invoice No.: ' + childSobject.get(childNameSpace + 'Invoice_No__c'));
					validationMap.put('stopExecution', true);
					validationMap.put('jsonValidationExceptionMsg', JSON.serialize(ValExecMsgMap));
					//return validationMap;
				}
				InvLineNoSet.add(invl.Invoice_Line_No__c);
			}
		}

		return validationMap;
	}
	//--- Added by Dilshad | JIRA:ESMPROD-43 | 07-09-2015 | Ends ---//
	*/
}