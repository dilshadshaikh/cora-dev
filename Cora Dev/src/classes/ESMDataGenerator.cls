public class ESMDataGenerator {
	public static String esmNameSpace = UtilityController.esmNameSpace;
	public static Invoice__c invoicePOInsert() {
		Invoice__c obj = new Invoice__c();
		obj.Document_Type__c = 'PO Invoice';
		obj.Invoice_No__c = 'INV-0001';
		obj.Invoice_Date__c = Date.Today();
		obj.Amount__c = 100;
		obj.Comments__c = 'test comment';
		obj.User_Action__c = 'Validate';
		obj.Current_State__c = 'Ready For Processing';
		obj.Amount__c = 100;
		//obj.Amount_Currency__c = 100;
		//obj.Do_GRN_Match__c = true;
		System.debug('esmnamesesmNameSpace = ' + esmNameSpace);
		if (UtilityController.isFieldCreateable('Invoice__c', 'Document_Type__c,Invoice_No__c,Invoice_Date__c,Amount__c,Comments__c,User_Actions__c,Tracker_Id__c,Current_State__c')) {
			insert obj;
		}
		return obj;
	}
	public static Invoice__c invoiceNPOInsert() {
		Invoice__c obj = new Invoice__c();
		obj.Document_Type__c = 'Non PO Invoice';
		obj.Invoice_No__c = 'INVN-0002';
		obj.Invoice_Date__c = Date.Today();
		obj.Amount__c = 1000;
		obj.Total_Amount__c = 1000;
		obj.Comments__c = 'test comment';
		obj.User_Action__c = 'Validate';
		obj.Current_State__c = 'Ready For Processing';
		obj.Amount__c = 100;
		//obj.Amount_Currency__c = 100;
		//obj.Do_GRN_Match__c = true;
		System.debug('esmnamesesmNameSpace = ' + esmNameSpace);
		//if (UtilityController.isFieldCreateable('Invoice__c', 'Document_Type__c,Invoice_No__c,Invoice_Date__c,Amount__c,Comments__c,User_Actions__c,Tracker_Id__c,Current_State__c')) {
		//insert obj;
		//}
		return obj;
	}
	public Static List<User_Master__c> insertBuyerUser(List<String> emailAdd) {
		List<User_Master__c> bul = new List<User_Master__c> ();
		for (String email : emailAdd) {
			User_Master__c bu = new User_Master__c();
			bu.Email__c = email;
			bu.user_type__c = 'Requestor/Buyer';
			bu.user_name__c = email;
			bul.add(bu);
		}

		if (UtilityController.isFieldCreateable(esmNameSpace + 'User_Master__c', esmNameSpace + 'Email__c,' + esmNameSpace + 'User_Type__c,' + esmNameSpace + 'User_Name__c'))
		{
			Insert bul;
		}

		return bul;
	}
	public static Vendor_Maintenance__c invoiceVendorMaintenance(String name) {
		Vendor_Maintenance__c vm = new Vendor_Maintenance__c();
		vm.Request_Info__c = 'New';
		if (UtilityController.isFieldCreateable('Vendor_Maintenance__c', null)) {
			insert vm;
		}
		return vm;
	}
	public static void CreateRuleAlwaysMoveToQCAmount() {
		QCHelper.QCRule qcRuleObj = new QCHelper.QCRule();
		qcRuleObj.JSONValue = new QCHelper.JSONValue();

		qcRuleObj.JSONValue.Criterias = new List<QCHelper.Criteria> ();
		qcRuleObj.JSONValue.AlwaysMoveToQC = true;
		qcRuleObj.JSONValue.QCPerecentage = 0;

		QCHelper.Criteria cre = null;
		cre = new QCHelper.Criteria();
		cre.Field = ESMDataGenerator.getPackagedFieldName('Amount__c');
		cre.Operator = 'greater or equal';
		cre.Value = '10000';
		cre.Type = 'NUMBER';
		cre.RefField = '';
		qcRuleObj.JSONValue.Criterias.add(cre);

		QC_Rule_Config__c qcConfig = new QC_Rule_Config__c();
		qcConfig.Rule_Name__c = 'Test';
		qcConfig.JSON__c = JSON.serialize(qcRuleObj.JSONValue);
		qcConfig.IsActive__c = true;
		qcConfig.Order__c = 1;

		insert qcConfig;

	}
	public static void CreateRuleAlwaysMoveToQC() {
		QCHelper.QCRule qcRuleObj = new QCHelper.QCRule();
		qcRuleObj.JSONValue = new QCHelper.JSONValue();

		qcRuleObj.JSONValue.Criterias = new List<QCHelper.Criteria> ();
		qcRuleObj.JSONValue.AlwaysMoveToQC = true;
		qcRuleObj.JSONValue.QCPerecentage = 0;

		QCHelper.Criteria cre = null;
		cre = new QCHelper.Criteria();
		cre.Field = ESMDataGenerator.getPackagedFieldName('Process__c');
		cre.Operator = 'contains';
		cre.Value = 'AP';
		cre.Type = 'PICKLIST';
		cre.RefField = '';
		qcRuleObj.JSONValue.Criterias.add(cre);


		cre = new QCHelper.Criteria();
		cre.Field = ESMDataGenerator.getPackagedFieldName('Worktype__c');
		cre.Operator = 'contains';
		cre.Value = 'PO Invoices';
		cre.Type = 'PICKLIST';
		cre.RefField = '';
		qcRuleObj.JSONValue.Criterias.add(cre);

		cre = new QCHelper.Criteria();
		cre.Field = ESMDataGenerator.getPackagedFieldName('Amount__c');
		cre.Operator = 'greater or equal';
		cre.Value = '10000';
		cre.Type = 'NUMBER';
		cre.RefField = '';
		qcRuleObj.JSONValue.Criterias.add(cre);

		QC_Rule_Config__c qcConfig = new QC_Rule_Config__c();
		qcConfig.Rule_Name__c = 'Test';
		qcConfig.JSON__c = JSON.serialize(qcRuleObj.JSONValue);
		qcConfig.IsActive__c = true;
		qcConfig.Order__c = 1;

		insert qcConfig;
	}

	public static List<CaseManager__c> createCases(integer count) {
		List<CaseManager__c> ctList = new List<CaseManager__c> ();
		for (integer i = 0; i < count; i++) {
			ctList.add(getCase());
		}
		insert ctList;
		return ctList;
	}
	public static CaseManager__c getCase() {
		CaseManager__c ct = new CaseManager__c();
		ct.Subject__c = 'Invalid Invoice';
		ct.Status__c = 'Ready For Processing';
		return ct;
	}
	public static CaseManager__c createCaseTracker() {
		CaseManager__c ct = new CaseManager__c();
		ct.Subject__c = 'Invalid Invoice';
		ct.Status__c = 'Ready For Processing';
		insert ct;
		return ct;
	}
	public static EmailMessage createEmailMessage(CaseManager__c ct) {
		EmailMessage emailMsg = new EmailMessage();
		emailMsg.FromName = 'Mr. John Green';
		emailMsg.ToAddress = 'ToAbc@Domain.com';
		emailMsg.CcAddress = 'ToAbc@Domain.com';
		emailMsg.BccAddress = 'ToAbc@Domain.com';
		emailMsg.FromAddress = 'FromAbc@Domain.com';
		// emailMsg.RelatedToId=ct.Id;
		insert emailMsg;
		return emailMsg;
	}

	public static ID createContent(CaseManager__c ct) {
		ContentVersion cont = new ContentVersion();
		cont.Title = 'file.png';
		cont.PathOnClient = 'file.png';
		cont.VersionData = EncodingUtil.base64Decode('Test Blob');
		//cont.AttachmentType__c='Related'; 
		cont.Origin = 'H';
		cont.IsMajorVersion = true;
		cont.Title = 'Google.com';
		insert cont;

		ContentVersion testContent = [SELECT ContentDocumentId FROM ContentVersion where Id = :cont.Id];
		ContentDocumentLink cntent = new ContentDocumentLink();
		cntent.ContentDocumentId = testContent.ContentDocumentId;
		cntent.LinkedEntityId = ct.Id;
		cntent.ShareType = 'V';
		cntent.Visibility = 'AllUsers';

		insert cntent;

		return testContent.ContentDocumentId;
	}
	public static User createUser() {
		List<CaseManager__c> ctList = ESMDataGenerator.createCases(1);
		String attachId = ESMDataGenerator.createContent(ctList[0]);

		// Setup test data
		// Create a unique UserName
		String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
		// This code runs as the system user
		Profile p = [SELECT Id FROM Profile WHERE Name = 'Chatter Only User'];
		User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
		                  EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
		                  LocaleSidKey = 'en_US', ProfileId = p.Id,
		                  TimeZoneSidKey = 'America/Los_Angeles',
		                  UserName = uniqueUserName);

		return u;
	}

	public static EmailTemplate createEmailTemplate() {

		List<Folder> lstFolder = [Select Id From Folder LIMIT 3];
		System.debug('lstFolder' + lstFolder);
		User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
		EmailTemplate validEmailTemplate = new EmailTemplate();
		System.runAs(thisUser) {
			validEmailTemplate.isActive = true;
			validEmailTemplate.Name = 'TemplteTemplate1';
			validEmailTemplate.DeveloperName = 'unique_name11';
			validEmailTemplate.TemplateType = 'text';
			validEmailTemplate.FolderId = lstFolder[2].Id;
			validEmailTemplate.Subject = 'Email template Subject }{';
			validEmailTemplate.Body = 'Email template body][';
			insert validEmailTemplate;
		}

		return validEmailTemplate;
	}
	public static String getPackagedFieldName(String fieldName) {
		String nameSpace = ESMDataGenerator.getNamespacePrefix(); //ESMConstants.NAMESPACE;
		if (!String.isEmpty(nameSpace)) {
			return nameSpace + '__' + fieldName;
		}
		return fieldName;
	}
	public static String getWithNameSpace(String fieldName) {
		String nameSpace = ESMDataGenerator.getNamespacePrefix(); //ESMConstants.NAMESPACE;
		if (!String.isEmpty(nameSpace) && !fieldName.toLowerCase().startsWith(nameSpace.toLowerCase())) {
			return nameSpace + '__' + fieldName;
		}
		return fieldName;
	}

	public static String getValidFieldSetName(String objetName, String fsname) {
		Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objetName);
		Schema.DescribeSObjectResult describe = targetType.getDescribe();
		Map<String, Schema.FieldSet> fsMap = describe.fieldSets.getMap();
		if (!fsMap.containsKey(fsname)) {
			return getWithNameSpace(ESMConstants.DEFAULT_FIELD_SET);
		}
		return fsname;
	}

	//Method to return namespace
	public static String getNamespacePrefix() {
		string[] parts = string.valueOf(ESMConstants.class).split('\\.', 2);
		string namespacePrefix = (parts.size() == 2 ? parts[0] : '');
		return namespacePrefix;
	}
    public static void createIgnoreRule(string templateId){
        EmailToCaseSettingHelper.JSONValue jsonObj=new EmailToCaseSettingHelper.JSONValue();
        jsonObj.Criterias=new List<EmailToCaseSettingHelper.Criteria>();
        jsonObj.NotifiationCriterias=new List<EmailToCaseSettingHelper.Criteria>();
        jsonObj.SendACKMail=true;
        jsonObj.SendACKMailToSender=true;
        jsonObj.IsNotifiationCriteria=true;
        jsonObj.TemplateId=templateId;

        EmailToCaseSettingHelper.Criteria cre=null;
        cre=new EmailToCaseSettingHelper.Criteria();
        cre.Field='Sender Email';
        cre.Operator='contains';
        cre.Value='@sftpl.com';
        cre.Type='TEXT';
        cre.Source='EMAIL';
        jsonObj.Criterias.add(cre);
        jsonObj.NotifiationCriterias.add(cre);

        cre=new EmailToCaseSettingHelper.Criteria();
        cre.Field='Attachment Count';
        cre.Operator='greater or equal';
        cre.Value='2';
        cre.Type='NUMBER';
        cre.Source='EMAIL';
        jsonObj.Criterias.add(cre);

        Email_To_Case_Rule__c emailToCaseRule=new Email_To_Case_Rule__c();
        emailToCaseRule.IsActive__c=true;
        emailToCaseRule.JSON__c=JSON.serialize(jsonObj);
        emailToCaseRule.Rule_Name__c='BlacklistedDomain';
        emailToCaseRule.Order__c=1;
        emailToCaseRule.Object_Name__c ='CaseManager__c';
        emailToCaseRule.Type__c='Ignore';

        insert emailToCaseRule;
    }
    public static void createAcceptRule(string templateId){
        EmailToCaseSettingHelper.JSONValue jsonObj=new EmailToCaseSettingHelper.JSONValue();
        jsonObj.Criterias=new List<EmailToCaseSettingHelper.Criteria>();
        jsonObj.ActionFields=new List<EmailToCaseSettingHelper.ActionField>();
        jsonObj.SendACKMail=true;
        jsonObj.SendACKMailToSender=true;
        jsonObj.IsNotifiationCriteria=false;
        jsonObj.TemplateId=templateId;

        EmailToCaseSettingHelper.Criteria cre=null;
        cre=new EmailToCaseSettingHelper.Criteria();
        cre.Field='Subject';
        cre.Operator='contains';
        cre.Value='AP_PO';
        cre.Type='TEXT';
        cre.Source='EMAIL';
        jsonObj.Criterias.add(cre);

        cre=new EmailToCaseSettingHelper.Criteria();
        cre.Field='Attachment Count';
        cre.Operator='greater or equal';
        cre.Value='2';
        cre.Type='NUMBER';
        cre.Source='EMAIL';
        jsonObj.Criterias.add(cre);

        EmailToCaseSettingHelper.ActionField actionField=null;

        actionField=new EmailToCaseSettingHelper.ActionField();
        actionField.Field=ESMDataGenerator.getPackagedFieldName('Process__c');
        actionField.value='AP';
        actionField.UseEmailField=false;
        actionField.Type='PICKLIST';
        jsonObj.ActionFields.add(actionField);

        actionField=new EmailToCaseSettingHelper.ActionField();
        actionField.Field=ESMDataGenerator.getPackagedFieldName('WorkType__c');
        actionField.value='Non-PO Invoices';
        actionField.UseEmailField=false;
        actionField.Type='PICKLIST';
        jsonObj.ActionFields.add(actionField);

        actionField=new EmailToCaseSettingHelper.ActionField();
        actionField.Field=ESMDataGenerator.getPackagedFieldName('Document_Type__c');
        actionField.value='Electricity';
        actionField.UseEmailField=false;
        actionField.Type='PICKLIST';
        jsonObj.ActionFields.add(actionField);

        actionField=new EmailToCaseSettingHelper.ActionField();
        actionField.Field=ESMDataGenerator.getPackagedFieldName('Status__c');
        actionField.value='Start';
        actionField.UseEmailField=false;
        actionField.Type='PICKLIST';
        jsonObj.ActionFields.add(actionField);

        actionField=new EmailToCaseSettingHelper.ActionField();
        actionField.Field=ESMDataGenerator.getPackagedFieldName('Requestor_Email__c');
        actionField.value='Sender Email';
        actionField.UseEmailField=true;
        actionField.Type='TEXT';
        jsonObj.ActionFields.add(actionField);

        actionField=new EmailToCaseSettingHelper.ActionField();
        actionField.Field=ESMDataGenerator.getPackagedFieldName('Subject__c');
        actionField.value='Subject';
        actionField.UseEmailField=true;
        actionField.Type='TEXT';
        jsonObj.ActionFields.add(actionField);
        

        Email_To_Case_Rule__c emailToCaseRule=new Email_To_Case_Rule__c();
        emailToCaseRule.IsActive__c=true;
        emailToCaseRule.JSON__c=JSON.serialize(jsonObj);
        emailToCaseRule.Rule_Name__c='AP_PO';
        emailToCaseRule.Order__c=1;
        emailToCaseRule.Object_Name__c ='CaseManager__c';
        emailToCaseRule.Type__c='Accept';

        insert emailToCaseRule;
    }
    public static void createExitingCaseRule(string templateId){
        EmailToCaseSettingHelper.JSONValue jsonObj=new EmailToCaseSettingHelper.JSONValue();
        jsonObj.Criterias=new List<EmailToCaseSettingHelper.Criteria>();
        jsonObj.ActionFields=new List<EmailToCaseSettingHelper.ActionField>();
        jsonObj.SendACKMail=true;
        jsonObj.SendACKMailToSender=true;
        jsonObj.IsNotifiationCriteria=false;
        jsonObj.TemplateId=templateId;
        jsonObj.Action='Update Existing Case';

        EmailToCaseSettingHelper.Criteria cre=null;

        cre=new EmailToCaseSettingHelper.Criteria();
        cre.Field=ESMDataGenerator.getPackagedFieldName('Status__c');
        cre.Operator='equals';
        cre.Value='Closed';
        cre.Type='PICKLIST';
        cre.Source='CASE';
        jsonObj.Criterias.add(cre);

        cre=new EmailToCaseSettingHelper.Criteria();
        cre.Field='Attachment Count';
        cre.Operator='greater or equal';
        cre.Value='2';
        cre.Type='NUMBER';
        cre.Source='EMAIL';
        jsonObj.Criterias.add(cre);

        cre=new EmailToCaseSettingHelper.Criteria();
        cre.Field='Sender Email';
        cre.Operator='contains';
        cre.Value='@gmail.com';
        cre.Type='TEXT';
        cre.Source='EMAIL';
        jsonObj.Criterias.add(cre);

        EmailToCaseSettingHelper.ActionField actionField=null;

        actionField=new EmailToCaseSettingHelper.ActionField();
        actionField.Field=ESMDataGenerator.getPackagedFieldName('UserAction__c');
        actionField.value='Reopen';
        actionField.UseEmailField=false;
        actionField.Type='PICKLIST';
        jsonObj.ActionFields.add(actionField);

        
        
        Email_To_Case_Rule__c emailToCaseRule=new Email_To_Case_Rule__c();
        emailToCaseRule.IsActive__c=true;
        emailToCaseRule.JSON__c=JSON.serialize(jsonObj);
        emailToCaseRule.Rule_Name__c='AP_PO';
        emailToCaseRule.Order__c=1;
        emailToCaseRule.Type__c='Existing Case';
        emailToCaseRule.Object_Name__c ='CaseManager__c';
        insert emailToCaseRule;
    }
    public static void createIdentifyExitingCaseRule(string templateId){
        EmailToCaseSettingHelper.JSONValue jsonObj=new EmailToCaseSettingHelper.JSONValue();
        jsonObj.Criterias=new List<EmailToCaseSettingHelper.Criteria>();
        jsonObj.ActionFields=new List<EmailToCaseSettingHelper.ActionField>();
        jsonObj.SendACKMail=true;
        jsonObj.SendACKMailToSender=true;
        jsonObj.IsNotifiationCriteria=false;
        jsonObj.TemplateId=templateId;
        jsonObj.Action='Update Existing Case';
		jsonObj.findCaseIdInSubject=true;
		jsonObj.findCaseIdInBody=false;
		jsonObj.findCaseIdFrom='Subject';
		jsonObj.excludeKeywords='';

        EmailToCaseSettingHelper.Criteria cre=null;

        /*cre=new EmailToCaseSettingHelper.Criteria();
        cre.Field='Status__c';
        cre.Operator='equals';
        cre.Value='Closed';
        cre.Type='PICKLIST';
        cre.Source='CASE';
        jsonObj.Criterias.add(cre);*/

        /*cre=new EmailToCaseSettingHelper.Criteria();
        cre.Field='Attachment Count';
        cre.Operator='greater or equal';
        cre.Value='2';
        cre.Type='NUMBER';
        cre.Source='EMAIL';
        jsonObj.Criterias.add(cre);*/

        cre=new EmailToCaseSettingHelper.Criteria();
        cre.Field='Sender Email';
        cre.Operator='contains';
        cre.Value='@gmail.com';
        cre.Type='TEXT';
        cre.Source='EMAIL';
        jsonObj.Criterias.add(cre);

        /*EmailToCaseSettingHelper.ActionField actionField=null;

        actionField=new EmailToCaseSettingHelper.ActionField();
        actionField.Field='UserAction__c';
        actionField.value='Reopen';
        actionField.UseEmailField=false;
        actionField.Type='PICKLIST';
        jsonObj.ActionFields.add(actionField);*/

        
        
        Email_To_Case_Rule__c emailToCaseRule=new Email_To_Case_Rule__c();
        emailToCaseRule.IsActive__c=true;
        emailToCaseRule.JSON__c=JSON.serialize(jsonObj);
        emailToCaseRule.Rule_Name__c='AP_PO';
        emailToCaseRule.Order__c=1;
        emailToCaseRule.Type__c='Identify Existing';
        emailToCaseRule.Object_Name__c ='CaseManager__c';
        insert emailToCaseRule;
    }
}