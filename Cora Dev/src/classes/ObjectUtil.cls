public class ObjectUtil {
    public static String getPackagedFieldName(String fieldName){
        String nameSpace = ObjectUtil.getNamespacePrefix();//ESMConstants.NAMESPACE;
        if (!String.isEmpty(nameSpace)) {
            return nameSpace+'__' +fieldName;
        }
        return fieldName;
    }
    
    public static String getWithNameSpace(String fieldName){
        String nameSpace = ObjectUtil.getNamespacePrefix();//ESMConstants.NAMESPACE;
        if (!String.isEmpty(nameSpace) && !fieldName.toLowerCase().startsWith(nameSpace.toLowerCase())) {
            return nameSpace+'__' +fieldName;
        }
        return fieldName;
    }
    
    public static String getValidFieldSetName(String objetName,String fsname){
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objetName);
        Schema.DescribeSObjectResult describe = targetType.getDescribe();
        Map<String, Schema.FieldSet> fsMap = describe.fieldSets.getMap();
        if(!fsMap.containsKey(fsname)){
            return getWithNameSpace(ESMConstants.DEFAULT_FIELD_SET);
        }
        return fsname;
    }
    
    //Method to return namespace
    public static String getNamespacePrefix(){
    	string[] parts = string.valueOf(ESMConstants.class).split('\\.', 2);
		string namespacePrefix = (parts.size() == 2 ? parts[0] : '');
		return namespacePrefix;
    }
}