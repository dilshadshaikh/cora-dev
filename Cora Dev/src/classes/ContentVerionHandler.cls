/*
  Authors	   :   Niraj Prajapati
  Date created :   29/01/2018
  Purpose      :   Helper class for Content Version object
  Dependencies :   NA
  __________________________________________________________________________
  Modifications:
		    Date:  
		    Purpose of modification:  
		    Method/Code segment modified: 
 */
public class ContentVerionHandler {
     /*
    	Authors: Niraj Prajapati
    	Purpose: To update Flag of Attachment
		Dependencies : NA
	*/
	
    public static void updateFlag(List<ContentVersion> newContentVersion, List<ContentVersion> oldContentVersion){
		//Check supplier profile user
		List<System_Configuration__c> sysConf = [SELECT Sub_Module__c, Value__c FROM System_Configuration__c where Module__c = 'Invoice__c' and (Sub_Module__c = 'Allow_Image_Size' or Sub_Module__c = 'Allowed_Primary_Format')];
		String allowImageSize = null;
		String allowedPrimaryFormat = null;
		if (sysConf != null && sysConf.size() > 0)
		{
			System.debug('sysConf:::'+sysConf);
			for (System_Configuration__c config : sysConf) {
				if (config.Sub_Module__c == 'Allow_Image_Size')
				{
					allowImageSize = config.Value__c;
				}
				if (config.Sub_Module__c == 'Allowed_Primary_Format')
				{
					allowedPrimaryFormat = config.Value__c;
				}
			}
		}
		System.debug('allowImageSize:::'+allowImageSize);
		System.debug('allowedPrimaryFormat:::'+allowedPrimaryFormat);
        if(allowImageSize!=null && allowImageSize!='' && allowedPrimaryFormat!=null && allowedPrimaryFormat!=''){
        Decimal allowedSize= Decimal.valueof(allowImageSize)*1000;
         
        Set<String> ext = new Set<String>(allowedPrimaryFormat.split(','));
        String extension = '';
        for (Integer i = 0; i < newContentVersion.size(); i++) {
           if(newContentVersion[i].Title.split('\\.').size() > 1 )
               extension= newContentVersion[i].Title.split('\\.')[1].toLowerCase();
           
           if(ext.contains(extension)) {    
                newContentVersion[i].flag__c='Flag';
           }else if((extension == 'jpg' || extension == 'png') && newContentVersion[i].VersionData.size()>allowedSize){
                   newContentVersion[i].flag__c='Flag';
           }else{
               newContentVersion[i].flag__c='Non Flag';
           		}	
        	}
       	}
    }

}