@isTest
public class PayMeEarlyControllerTest {

	public static testmethod void TestMethod1() {

		Payment_Term__c paymentobj = new Payment_Term__c();

		paymentobj.put('Discount_Percentage__c', 0.10);
		paymentobj.put('Name', 'Net 30');
		insert paymentobj;
		System.debug('paymentobj' + paymentobj);

		System_Configuration__c sysconfig=new System_Configuration__c ();
		sysconfig.Module__c='Custom_Record_History';
		sysconfig.Value__c ='Invoice__c';
		insert sysconfig;

		Invoice__c invobj = new Invoice__c();		
		invobj.Invoice_Date__c = Date.Today();
		invobj.Net_Due_Date__c = Date.Today().addDays(60);		
		invobj.Total_Amount__c = 500.00;
		
		System.debug('invobj1' + invobj);
		insert invobj;
		System.debug('invobj2' + invobj);
				
		List<EsmHelper.dataHelperRecord> dhr = new List<EsmHelper.dataHelperRecord> ();
		dhr = PayMeEarlyController.getSObjectDetails(invobj.id, invobj);
				
		Double discper=PayMeEarlyController.getDiscountVal(paymentobj.id);

		PayMeEarlyController.updateInvoice(invobj);
	}

}