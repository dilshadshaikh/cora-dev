public class TriggerHelperClass {
    public static Map<String,sObject> firstRecHist = new Map<String,sObject>();
    //public static List<InvoicePOMatrix__c> firstPOMatrixRecord = new List<InvoicePOMatrix__c>();
    //public static List<akritivtlm__TAT__c> firstTATRecord = new List<akritivtlm__TAT__c>();
    //--- Added By Dilshad | JIRA:ESMPROD-500 | 07-02-2017 | Starts ---//
    public static boolean singlePurchaseOrder = true;
    //--- Added By Dilshad | JIRA:ESMPROD-500 | 07-02-2017 | Ends ---//
	/*
	public void invoiceAutoMatchProcess(List<Invoice_Line_Item__c> invlList, Invoice_Configuration__c invConf, String esmNameSpace){
		if(invConf.Auto_Match_Flow_Method__c != null && invConf.Auto_Match_Flow_Method__c != ''){
			boolean skipAutoMatch = false;
			Boolean amwg = invConf.Enable_Auto_Flipping_With_GRN__c; // Auto Match with GRN Line Item
            Set<string> poSet = new set<string>();
            for(Invoice_Line_Item__c invl : invlList){
                system.debug('invl:'+invl);
                if(invl.PO_Line_Item__c != null || invl.Purchase_Order__c == null){
                    // When PO Line Item is already mapped (i.e. when it Invoice Line Item is create in ESM) then there is no need of auto match
                    // When Purchase Order is not mapped on Invoice Line Item (i.e. in case of NON PO Invoice) then there is no need of auto match
                    skipAutoMatch = true;
                    break;
                }
                poSet.add(invl.Purchase_Order__c);
            }
            if(!skipAutoMatch){ 
                Map<String,string> polinvlMap = new Map<String,string>();    
                Map<string,string> InvMap = new Map<string,string>();
                Map<string,set<string>> InvPOMap = new Map<string,set<string>>();
                List<sObject> polgrnlList;
				string queryStr = 'SELECT Id,Name';
                for(PO_Line_Item_Invoice_Line_Item_Mapping__c polinvlMapping:PO_Line_Item_Invoice_Line_Item_Mapping__c.getAll().values()){
                    if(polinvlMapping.Used_In_Auto_Match__c){
                        polinvlMap.put(polinvlMapping.PO_Line_Item_Field_Name__c,polinvlMapping.Invoice_Line_Item_Field_Name__c);
                        queryStr += ',' + polinvlMapping.PO_Line_Item_Field_Name__c;
                    }
                }
                if(!queryStr.containsIgnoreCase('Purchase_Order__c')){
                    queryStr += ','+esmNameSpace+'Purchase_Order__c';
                }
				if(invConf.Enable_Auto_Flipping_With_GRN__c){
					polgrnlList = new List<GRN_Line_Item__c>();
					queryStr += ' FROM GRN_Line_Item__c WHERE Purchase_Order__c IN:poSet'; 
					if(UtilityController.isFieldAccessible(esmNameSpace+'GRN_Line_Item__c',null)){
						polgrnlList = database.query(queryStr);
					}
				} else {
					polgrnlList = new List<PO_Line_Item__c>();
					queryStr += ',GRN_Match_Required_Formula__c,PO_Line_No__c';
					queryStr += ' FROM PO_Line_Item__c WHERE Purchase_Order__c IN:poSet'; 
					if(UtilityController.isFieldAccessible(esmNameSpace+'PO_Line_Item__c',null)){
						polgrnlList = database.query(queryStr);
					}
					List<GRN_Line_Item__c> grnLineItemList = new List<GRN_Line_Item__c>();
					if(UtilityController.isFieldAccessible(esmNameSpace+'GRN_Line_Item__c',null)){
						grnLineItemList = [SELECT Id,Name,PO_Line_Item__c,Purchase_Order__c,GRN__c,PO_Line_Item__r.GRN_Match_Required_Formula__c FROM GRN_Line_Item__c WHERE Purchase_Order__c IN:poSet];
					}
					Map<String,String> GRNLineMap = new Map<String,String>();
					Map<String,String> GRNMap = new Map<String,String>();
					if(grnLineItemList != null && grnLineItemList.size() > 0){
						for(GRN_Line_Item__c grnl : grnLineItemList){
							if(grnl.PO_Line_Item__r.GRN_Match_Required_Formula__c != null && grnl.PO_Line_Item__r.GRN_Match_Required_Formula__c){
								if(GRNLineMap.get(grnl.PO_line_Item__c)!= null){
									GRNLineMap.put(grnl.PO_line_Item__c,GRNLineMap.get(grnl.PO_line_Item__c)+'~'+grnl.id);
									GRNMap.put(grnl.PO_line_Item__c,GRNMap.get(grnl.PO_line_Item__c)+'~'+grnl.GRN__c);
								}
								else{
									GRNLineMap.put(grnl.PO_line_Item__c,grnl.id);
									GRNMap.put(grnl.PO_line_Item__c,grnl.GRN__c);
								}
							}
						}
					}
                }    
                
                
                set<string> cPOSet = new set<string>();
                for(Invoice_Line_Item__c invl : invlList){
                    if(InvPOMap.get(invl.Invoice__c) != null){
                        cPOSet = InvPOMap.get(invl.Invoice__c);
                        cPOSet.add(invl.Purchase_Order__c);
                        InvPOMap.put(invl.Invoice__c,cPOSet);
                    } else {
                        cPOSet = new set<string>();
                        cPOSet.add(invl.Purchase_Order__c);
                        InvPOMap.put(invl.Invoice__c,cPOSet);
                    }
                    if(invl.PO_Line_Item__c == null){
                        invl.Is_Mismatch__c = true;
                        boolean Matched = true;
                            
                        if(invConf.Auto_Match_Flow_Method__c.equalsIgnoreCase('Sequence')){
                            for(String polFieldName:polinvlMap.keySet()){
                                String invlFieldName = polinvlMap.get(polFieldName);
                                for(sObject polgrnl:polgrnlList){
                                    Matched = false;
                                    if(polgrnl.get('Purchase_Order__c') == invl.Purchase_Order__c){
                                        if(polgrnl.get(polFieldName) != null && polgrnl.get(polFieldName).equals(invl.get(invlFieldName))){
                                            //invl.GRN_Line_Item__c = amwg ? polgrnl.Id : (GRNLineMap != null && GRNLineMap.size() > 0 && GRNLineMap.get(polgrnl.Id) != null && !GRNLineMap.get(polgrnl.Id).contains('~')) ? GRNLineMap.get(polgrnl.Id) : null;
											//invl.GRN__c = amwg ? polgrnl.GRN__c : (GRNMap != null && GRNMap.size() > 0 && GRNMap.get(polgrnl.Id) != null && !GRNMap.get(polgrnl.Id).contains('~')) ? GRNMap.get(polgrnl.Id) : null;
                                            //invl.PO_Line_Item__c = amwg ? polgrnl.PO_Line_Item__c : polgrnl.Id;
											if(amwg){
												invl.GRN_Line_Item__c = String.valueOf(polgrnl.get('Id'));
												invl.GRN__c = polgrnl.get('GRN__c');
												invl.PO_Line_Item__c = polgrnl.get('PO_Line_Item__c');
											} else {
												invl.GRN_Line_Item__c = (GRNLineMap != null && GRNLineMap.size() > 0 && GRNLineMap.get(polgrnl.get('Id')) != null && !GRNLineMap.get(polgrnl.get('Id')).contains('~')) ? GRNLineMap.get(polgrnl.get('Id')) : null;
												invl.GRN__c = (GRNMap != null && GRNMap.size() > 0 && GRNMap.get(polgrnl.get('Id')) != null && !GRNMap.get(polgrnl.get('Id')).contains('~')) ? GRNMap.get(polgrnl.get('Id')) : null;
												invl.PO_Line_Item__c = polgrnl.get('Id');
											}
											break;                                         
                                        }
                                    }
                                }
                                if(invl.PO_Line_Item__c != null){
                                    break;                                      
                                }
                            }
                        }
                        else if(invConf.Auto_Match_Flow_Method__c.equalsIgnoreCase('Parallel')){
                            for(sObject polgrnl:polgrnlList){
                                Matched = true;
                                if(polgrnl.get('Purchase_Order__c') == invl.Purchase_Order__c){
                                    for(String polFieldName : polinvlMap.keySet()){
                                        String invlFieldName = polinvlMap.get(polFieldName);
                                        if((polgrnl.get(polFieldName) != null && invl.get(invlFieldName) == null) || (polgrnl.get(polFieldName) == null && invl.get(invlFieldName) != null)){
                                            Matched = false;
                                        }
                                        else if(polgrnl.get(polFieldName) != null && !polgrnl.get(polFieldName).equals(invl.get(invlFieldName))){
                                            Matched = false;
                                        }
                                    }
                                    if(Matched){
                                        //invl.PO_Line_Item__c = pol.Id;
										if(amwg){
											invl.GRN_Line_Item__c = polgrnl.get('Id');
											invl.GRN__c = polgrnl.get('GRN__c');
											invl.PO_Line_Item__c = polgrnl.get('PO_Line_Item__c');
										} else {
											invl.GRN_Line_Item__c = (GRNLineMap != null && GRNLineMap.size() > 0 && GRNLineMap.get(polgrnl.get('Id')) != null && !GRNLineMap.get(polgrnl.get('Id')).contains('~')) ? GRNLineMap.get(polgrnl.get('Id')) : null;
											invl.GRN__c = (GRNMap != null && GRNMap.size() > 0 && GRNMap.get(polgrnl.get('Id')) != null && !GRNMap.get(polgrnl.get('Id')).contains('~')) ? GRNMap.get(polgrnl.get('Id')) : null;
											invl.PO_Line_Item__c = polgrnl.get('Id');
										}
                                        if(invl.Invoice_Line_Item_No__c == null){
                                            invl.Invoice_Line_Item_No__c = polgrnl.get('Purchase_Order__c') + '~' + polgrnl.get('PO_Line_No__c') + '~' + CommonFunctionController.getRandomnumber(10);
                                        }
                                        break;                                      
                                    }
                                }
                            }
                        }
                        if(invl.PO_Line_Item__c != null){
                            invl.Is_Mismatch__c = false;
                            if(InvMap.get(invl.Invoice__c) == null || (InvMap.get(invl.Invoice__c) != null && InvMap.get(invl.Invoice__c) != 'Fail')){
                                InvMap.put(invl.Invoice__c, 'Pass');
                            }
							
                            try{
                                if(invl.Quantity__c != null){
                                    if(UserInfo.isMultiCurrencyOrganization() && invl.Quantity__c != null && invl.Rate_Currency__c != null){
                                        invl.Amount_Currency__c = invl.Quantity__c * invl.Rate_Currency__c.setscale(3,System.RoundingMode.HALF_UP);
                                    } else if(invl.Quantity__c != null && invl.Rate__c != null){
                                        invl.Amount__c = invl.Quantity__c * invl.Rate__c.setscale(3,System.RoundingMode.HALF_UP);
                                    }
                                }
                            } catch(exception ex){}
                        }
                        else {
                            invl.Exception_Reason__c = 'PO Line Not Found';
                            invl.Is_Mismatch__c = true;
                            InvMap.put(invl.Invoice__c, 'Fail');
                            if(invl.Invoice_Line_Item_No__c == null){
                                invl.Invoice_Line_Item_No__c = invl.Purchase_Order__c + '~null~' + CommonFunctionController.getRandomnumber(10);
                            }
                        }
                    }
                        
                }
                    
                if(InvMap != null && InvMap.size() > 0){
                    List<Invoice__c> invList = new List<Invoice__c>();
                    if(UtilityController.isFieldAccessible(esmNameSpace+'Invoice__c','Id,Name,'+esmNameSpace+'Exception_Reason__c,'+esmNameSpace+'User_Actions__c,'+esmNameSpace+'Current_State__c')){
                        invList = [SELECT Id,Name,Exception_Reason__c,User_Actions__c,Current_State__c FROM Invoice__c WHERE Id IN:InvMap.keySet()];
                    }
                    if(invList != null && invList.size() > 0){
                        for(Invoice__c inv:InvList){
                            if(InvMap.get(inv.Id).equalsIgnoreCase('Fail') || (inv.Exception_Reason__c != null && inv.Exception_Reason__c.contains('PO Line Not Found'))){
                                if(inv.Exception_Reason__c != null && !inv.Exception_Reason__c.contains('PO Line Not Found')){
                                    inv.Exception_Reason__c = inv.Exception_Reason__c+';PO Line Not Found';
                                } else {
                                    inv.Exception_Reason__c = 'PO Line Not Found';
                                }
                                inv.User_Actions__c = 'Auto Match Fail';
                            } else {
                                inv.User_Actions__c = 'Auto Match Pass';
                            }
                        }
                        system.debug('InvMap:'+InvMap);
                        if(Invoice__c.getSObjectType().getDescribe().isUpdateable()
                        && Schema.sObjectType.Invoice__c.fields.Exception_Reason__c.isAccessible()
                        && Schema.sObjectType.Invoice__c.fields.User_Actions__c.isAccessible()){
                            update invList;
                        } else {
                            throw new CustomCRUDFLSException(UtilityController.getUpdateExceptionMessage(esmNameSpace + 'Invoice__c'));
                        }
                    }
                }
                //--- Update InvoicePOMatrix ---//
                if(InvPOMap != null && InvPOMap.size() > 0){
                    List<InvoicePOMatrix__c> invPoList = new List<InvoicePOMatrix__c>();
                    for(String inv: InvPOMap.keySet()){
                        for(String po: InvPOMap.get(inv)){
                            InvoicePOMatrix__c invpoObj = new InvoicePOMatrix__c();
                            invpoObj.Invoice__c = inv;
                            invpoObj.Purchase_Order__c = po;
                            invPoList.add(invpoObj);
                        }
                    }
                    if(invPoList != null && invPoList.size() > 0){
                        if(InvoicePOMatrix__c.getSObjectType().getDescribe().isCreateable()
                        && Schema.sObjectType.InvoicePOMatrix__c.fields.Invoice__c.isCreateable()
                        && Schema.sObjectType.InvoicePOMatrix__c.fields.Purchase_Order__c.isCreateable()){
                            insert invPoList;
                        } else {
                            throw new CustomCRUDFLSException(UtilityController.getAccessExceptionMessage(esmNameSpace + 'InvoicePOMatrix__c'));
                        }
                    }
                }
            }
        }
	}*/
}