@isTest
public class InvoiceControllerTest {
      
    public static testmethod void testmethod1()
    {
            System_Configuration__c sysconfig=new System_Configuration__c ();
            sysconfig.Module__c='Custom_Record_History';
            sysconfig.Value__c ='Invoice__c';
            insert sysconfig;
        
            Invoice_Configuration__c invConf = new Invoice_Configuration__c();
            invConf.Enable_Quantity_Calculation__c = true;
            invConf.GRN_Flag_In_PO_Header__c = true;
            invConf.Excluded_GRN_Status_For_Processing__c = 'Closed,Completed';
            invConf.Quantity_Calculation_Flag_In_PO_Header__c = false;
            invConf.Enable_Auto_Flipping_With_GRN__c = true;
            invConf.PO_Number_Field_for_Search__c = 'Name';
            invConf.Invoice_Reject_Current_State__c ='';
            invConf.Auto_Match_Flow_Method__c = 'Sequence';
            invConf.Auto_Match_Current_State__c = 'Ready For Indexing,Approved With Changes';
            invConf.Auto_Validation_Current_States__c = '';
            insert invConf;
        
            Invoice__c inv = new Invoice__c();   
            inv.Invoice_No__c = 'INV-0001';
            inv.Invoice_Date__c = Date.Today();
            inv.Net_Due_Date__c = Date.Today().addDays(60);    
            inv.Total_Amount__c = 500.00;
            inv.Comments__c = 'test comment';
            //inv.User_Actions__c = 'Validate';
            inv.Current_State__c = 'Ready For Processing';        
            insert inv;
        	system.debug('-----inv'+inv);
       
        
            Invoice_Line_Item__c InvLine = new  Invoice_Line_Item__c();
            InvLine.Invoice_Line_No__c='INVL-1001';
            InvLine.Invoice__c=inv.id;
            insert InvLine;
        	system.debug('-----InvLine'+InvLine);
        
            Purchase_Order__c  po = new Purchase_Order__c();
            po.PO_No__c = 'PO-1001';
            po.Amount__c = 123;
            insert po;
        	system.debug('-----po'+po);
            PO_Line_Item__c poL = new PO_Line_Item__c();
            poL.Purchase_Order__c = po.id;
            poL.Rate__c = 123;
            insert poL; 
        	system.debug('-----poL'+poL);
                        
            List<Invoice_Line_Item__c> InvLineItemList = new List<Invoice_Line_Item__c>();
            InvLineItemList.add(InvLine);
        	system.debug('-----InvLineItemList'+InvLineItemList);
            List<PO_Line_Item__c> POLineItemList = new List<PO_Line_Item__c>();
            POLineItemList.add(poL);
        
        	system.debug('-----POLineItemList'+POLineItemList);
            List<Invoice_Line_Item__c> otherChargesList = new List<Invoice_Line_Item__c>();
            otherChargesList.add(InvLine);
            system.debug('-----otherChargesList'+otherChargesList);
        
            Attachment attach = new Attachment();
            attach.name = 'test.pdf';
            attach.body = Blob.valueOf(('test data testing'));
            attach.parentId = inv.id;
            insert attach;                  
                      
            List<ContentDocument> documents =new List<ContentDocument> ();
            documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument limit 1];
            system.debug('----documents--'+documents);
        
             ContentVersion cv = new ContentVersion();
             cv.Title = 'Demopdf';
             cv.PathOnClient = 'Demopdf.pdf';
             cv.VersionData = Blob.valueOf('Test Content');
             cv.IsMajorVersion = true;      
        	system.debug('----cv--'+cv);
            List<ContentVersion> cvList = new List<ContentVersion>();          
            cvList.add(cv);
        	system.debug('-----cvList'+cvList);
        
            
            InvoiceController.getInvoice(inv.id);  
            InvoiceController.saveInvoiceData(inv);
          
            //InvoiceController.insertAttachentData('',cvList,inv.id);
        
            //InvoiceController.saveInvoice(inv,InvLineItemList,POLineItemList,otherChargesList,'EDIT','',cvList,null);
            InvoiceController inc = new InvoiceController();
			inc.validateInvoice();
    }
}