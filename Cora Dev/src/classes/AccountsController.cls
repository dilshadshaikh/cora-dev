public class AccountsController {
  @AuraEnabled
  public static List<Invoice__c> getAccounts() {
    return [SELECT Id, name FROM Invoice__c Limit 1];
  }
}