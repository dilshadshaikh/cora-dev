public class GRNLineItemController {
	@AuraEnabled
	public static EsmHelper.dataHelper getGRNLineDataServer(String strObjectName, String jsonObj, String objId, String poFlip) {
		EsmHelper.dataHelper dh = new EsmHelper.dataHelper();
		try
		{
			String namespace = UtilityController.esmNameSpace;
			Set<String> poList = new Set<String> ();
			List<EsmHelper.Columns> cols = UtilityController.getHelperColumnsForWrapper(jsonObj);
			String selectedRows = UtilityController.getAllSelectedFields(cols);

			if (poFlip != null) {
				for (String pol : poFlip.split(',')) {
					poList.add(pol);
				}
			} else {
				poList = CommonFunctionController.getSetOfPOidFrminvoice(objId);
			}
			List<sObject> objList = new List<sObject> ();
			if (UtilityController.isFieldAccessible(strObjectName, selectedRows))
			{
				objList = CommonFunctionController.getGRNline(poList);
			}
			dh = UtilityController.getWrapperFromJson(objList, strObjectName, cols);
		}
		catch(Exception e)
		{
			ExceptionHandlerUtility.customDebug(e, 'UtilityController');
			throw new CustomCRUDFLSException(e);
		}
		return dh;
	}
	@AuraEnabled
	public static EsmHelper.dataHelper getPOLineFromPODataServer(String strObjectName, String objId) {
		String namespace = UtilityController.esmNameSpace;
		Set<String> poset = new Set<String> ();
		for (String pol : objId.split(',')) {
			poset.add(pol);
		}
		String whereClause = ' ' + namespace + 'Purchase_Order__c = :poset';
		String query = CommonFunctionController.getSOQLQuery(strObjectName) + ' ' + whereClause;
		query = CommonFunctionController.getCreatableFieldsSOQL(strObjectName, whereClause, true);
		//System.debug(query);
		List<sObject> objList = new List<sObject> ();
		EsmHelper.dataHelper dh = new EsmHelper.dataHelper();
		dh.DataList = Database.query(query);
		return dh;
	}
}