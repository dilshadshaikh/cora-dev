@isTest
public class LayoutControllerTest {
    public static testMethod void testCaseServiceMethods(){
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        test.startTest();
            Map<String, Object> requestParms =new Map<String, Object>();
            requestParms.put('objectName', ObjectUtil.getWithNameSpace('CaseManager__c'));
            requestParms.put('recordTypeName', 'Default');
            requestParms.put('sessionId', UserInfo.getSessionId());
            String jsonString = JSON.serialize(requestParms);
            LayoutController.getFields(jsonString);
            LayoutController.testLayout();
            CaseManager__c casemanager = new CaseManager__c();
            insert casemanager ;
            ID casemanagerId = casemanager.ID;
            ID sessionId = casemanager.ID;
            LayoutController.getCaseById(casemanagerId);
            //LayoutController.GetPicklistValuesBasedOnRecordType('objectName','recordTypeName',sessionId);
            
            ESMUISetupController esmuisetupcontroller = new ESMUISetupController ();
            ESMUISetupController.Rule  esmuisetupcontrollerRuleObj = new ESMUISetupController.Rule();
            ESMUISetupController.Criteria esmuisetupControllerCriteriaObj = new ESMUISetupController.Criteria();
            list<ESMUISetupController.Criteria> newCriteria=new list<ESMUISetupController.Criteria>();
            esmuisetupControllerCriteriaObj.Type ='TEXT';  
            //esmuisetupControllerCriteriaObj.Type ='NUMBER';
            //esmuisetupControllerCriteriaObj.Type ='BOOLEAN';
            newCriteria.add(esmuisetupControllerCriteriaObj);
            esmuisetupcontrollerRuleObj.criterias = newCriteria;
            
            //newCriteria.Criteria.Type='TEXT';
        
            LayoutController.isRuleMatch(esmuisetupcontrollerRuleObj,casemanager);
            
            LayoutController.isMatch('equals', 'true', true);
            LayoutController.isMatch('not equal to', 'true', true);
            LayoutController.isMatch('not equal to', '10', 10);
            LayoutController.isMatch('equals', '10', 10);
            LayoutController.isMatch('less than', '10', 10);
            LayoutController.isMatch('greater than', '10', 10);
            LayoutController.isMatch('less or equal', '10', 10);
            LayoutController.isMatch('greater or equal', '10', 10);
        
            LayoutController.isMatch('equals', '10', '10','PICKLIST');
            LayoutController.isMatch('not equal to', '10', '10','PICKLIST');
            LayoutController.isMatch('starts with', '10', '10','PICKLIST');
            LayoutController.isMatch('end with', '10', '10','PICKLIST');
            LayoutController.isMatch('does not contain', '10', '10','PICKLIST');
            LayoutController.isMatch('contains in list', '10', '10','PICKLIST');
            LayoutController.isMatch('does not contains in list', '10', '10','PICKLIST');
            LayoutController.isMatch('contains', '10', '10','PICKLIST');
            //LayoutController.isMatch('contains in list', '10', '10','abc');
            //ESMUISetupController.getSobjectId();
              EmailController emailcontroller = new EmailController();
        test.stopTest();
    }
    private class WebServiceMockImpl implements WebServiceMock {    
        public void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) {
            if(request instanceof ESMMetadataService.describeMetadata_element){
                response.put('response_x', new ESMMetadataService.describeMetadataResponse_element());
            }else if(request instanceof ESMMetadataService.readMetadata_element){
                ESMMetadataService.readRecordTypeResponse_element rrtr=new ESMMetadataService.readRecordTypeResponse_element();
                rrtr.result=new ESMMetadataService.ReadRecordTypeResult();
                ESMMetadataService.RecordType rt =new ESMMetadataService.RecordType();
                ESMMetadataService.RecordTypePicklistValue rtpv = new ESMMetadataService.RecordTypePicklistValue();
                ESMMetadataService.PicklistValue plv=new ESMMetadataService.PicklistValue();
                
                rtpv.values = new List<ESMMetadataService.PicklistValue>{plv};
                    rt.picklistValues = new List<ESMMetadataService.RecordTypePicklistValue>{rtpv};
                        rrtr.result.records=new List<ESMMetadataService.RecordType>{rt};
                            response.put('response_x', rrtr);
                //  response.put('response_x', new ESMMetadataService.readRecordTypeResponse_element());
            }
            return;
        } 
    }
}