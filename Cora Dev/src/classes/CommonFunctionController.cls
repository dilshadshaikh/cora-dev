public with sharing class CommonFunctionController {
    static String namespace = UtilityController.esmNameSpace;
    public static String getSOQLQuery(String objectName) {
        try {
            return 'SELECT ' + getAllFieldsOfObject(objectName) + ' FROM ' + objectName;
        } catch(Exception ex) {
            UtilityController.customDebug(ex, 'CommonFunctionController');
        }
        return null;
    }

    public static String getSOQLQuery(String objectName, string whereClause) {
        try {
            return getSOQLQuery(objectName) + ' WHERE ' + whereClause;
        } catch(Exception ex) {
            UtilityController.customDebug(ex, 'CommonFunctionController');
        }
        return null;
    }

    public static String getAllFieldsOfObject(String objectName) {
        try {
            String returnVal = '';
            List<String> lst;
            if (objectName == null || objectName == '') {
                return null;
            }
            Set<String> sets = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap().keySet();
            lst = new List<String> ();
            lst.addAll(sets);
            return String.join(lst, ',');
        } catch(Exception ex) {
            UtilityController.customDebug(ex, 'CommonFunctionController');
        }
        return null;
    }

    public static List<schema.fieldsetmember> getFieldsFromFieldSet(String objectName, String fieldSetName) {
        try {
            if (objectName != null && fieldSetName != null && fieldSetName != '') {
                Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe();
                Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectName);
                Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();

                Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);

                if (fieldSetObj != null) {
                    System.debug('list of Database' + fieldSetObj.getFields());
                    return fieldSetObj.getFields();
                } else {
                    return null;
                }
            }
            else {
                return null;
            }
        } catch(Exception ex) {
            UtilityController.customDebug(ex, 'CommonFunctionController');
        }
        return null;
    }

    public String getCreatableFieldsSOQL(String objectName, String whereClause) {
        return getCreatableFieldsSOQL(objectName, whereClause, '', false);
    }

    public static String getCreatableFieldsSOQL(String objectName, String whereClause, Boolean addLookupFieldName) {
        return getCreatableFieldsSOQL(objectName, whereClause, '', addLookupFieldName);
    }

    public static String getCreatableFieldsSOQL(String objectName, String whereClause, String additionalFields, Boolean addLookupFieldName) {

        String selectfields = '';
        try {
            Set<string> cFieldSet = new Set<string> ();

            if (additionalFields != null && additionalFields != '') {
                additionalFields = additionalFields.toLowerCase();
                for (String cFields : additionalFields.split(',')) {
                    if (cFields != '') {
                        cFieldSet.add(cFields);
                    }
                }
            }

            if (whereClause == null || whereClause == '') { return null; }
            Map<string, Schema.sObjectType> describeInfo = Schema.getGlobalDescribe();
            Map<String, Schema.SObjectField> fMap = describeInfo.get(objectName.toLowerCase()).getDescribe().Fields.getMap();
            if (fMap != null) {
                for (Schema.SObjectField f : fMap.values()) {
                    string field = f.getDescribe().getName().toLowerCase();
                    System.debug('field:' + field);
                    if (!cFieldSet.contains(field)) {
                        selectfields += field + ',';
                    }
                    Schema.DescribeFieldResult tempObj = f.getDescribe();
                    if (addLookupFieldName && tempObj.isCustom() && String.ValueOf(tempObj.getType()).equalsignoreCase('reference')) {
                        if (describeInfo.get(String.valueOf(tempObj.getReferenceTo().get(0))).getDescribe().isAccessible()) {
                            selectfields += field.removeEnd('__c') + '__r.Name' + ',';
                        } else {
                            throw new CustomCRUDFLSException(UtilityController.getAccessExceptionMessage(String.valueOf(tempObj.getReferenceTo().get(0))));
                        }
                    }
                }
                if (additionalFields != null && additionalFields != '') {
                    selectfields += additionalFields;
                } else {
                    selectfields = selectfields.removeEnd(',');
                }
            }
        } catch(Exception ex) { UtilityController.customDebug(ex, 'CommonFunctionController'); }
        return 'SELECT ' + selectfields + ' FROM ' + objectName + ' WHERE ' + whereClause;
    }
    public static List<Schema.DescribeFieldResult> getFieldsFromFieldSetNew(String objectName, String fieldSetName) {
        List<Schema.DescribeFieldResult> desFieldResList = new List<Schema.DescribeFieldResult> ();
        try
        {
            if (objectName != null && fieldSetName != null && fieldSetName != '') {
                Set<String> uniqueFieldName = new Set<String> ();
                //changed by Ronak | JIRA:ESMPROD-372 | 23-08-2016 | Starts 
                Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe();
                Schema.FieldSet fieldSetObj = GlobalDescribeMap.get(objectName).getDescribe().FieldSets.getMap().get(fieldSetName);
                system.debug('fieldSetObj :-----'+fieldSetObj);
                Map<String, Schema.sObjectField> fieldsMap = GlobalDescribeMap.get(objectName).getDescribe().fields.getMap();
                if (fieldSetObj != null) {
                    for (schema.fieldsetmember invlCol : fieldSetObj.getFields()) {
                        desFieldResList.add(fieldsMap.get(invlCol.getFieldPath()).getDescribe());
                        //changed by Ronak | JIRA:ESMPROD-372 | 23-08-2016 | Ends  
                    }
                }
            }
        }

        //---Added by Bhupinder Mankotia|| 28-09-2016 || Starts.---//
        catch(Exception ex) {
            UtilityController.customDebug(ex, 'CommonFunctionController');
            //return null;          
        }
        //---Added by Bhupinder Mankotia|| 28-09-2016 || Ends.---//
        return desFieldResList;
    }
    //changed by Ronak | JIRA:ESMPROD-372 | 23-08-2016 | Starts
    public static Map<schema.fieldsetmember, Schema.DescribeFieldResult> getFieldsMapFromFieldSetNew(String objectName, String fieldSetName) {
        Map<schema.fieldsetmember, Schema.DescribeFieldResult> desFieldResList = new Map<schema.fieldsetmember, Schema.DescribeFieldResult> ();
        try
        {
            if (objectName != null && fieldSetName != null && fieldSetName != '') {
                Set<String> uniqueFieldName = new Set<String> ();

                Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe();
                List<schema.fieldsetmember> fieldSetObj = GlobalDescribeMap.get(objectName).getDescribe().FieldSets.getMap().get(fieldSetName).getFields();
                Map<String, Schema.sObjectField> fieldsMap = GlobalDescribeMap.get(objectName).getDescribe().fields.getMap();

                Integer size = fieldSetObj.size();
                schema.fieldsetmember invlCol;
                if (fieldSetObj != null) {
                    for (Integer i = size - 1; i >= 0; i--) {
                        invlCol = null;
                        invlCol = fieldSetObj.get(i);
                        String[] temp = invlCol.getFieldPath().split('\\.');
                        if (temp.size() < 2) {
                            desFieldResList.put(invlCol, fieldsMap.get(invlCol.getFieldPath()).getDescribe());
                        } else {

                            if (!temp[0].equalsIgnoreCase('CreatedBy') && !temp[0].equalsIgnoreCase('LastModifiedBy') && !temp[0].equalsIgnoreCase('Owner')) {
                                Schema.DescribeFieldResult desField = fieldsMap.get(temp[0].removeEnd('__r') + '__c').getDescribe();
                                desFieldResList.put(invlCol, desField.getReferenceTo().get(0).getDescribe().fields.getMap().get(temp[1]).getDescribe());
                            } else {
                                Schema.DescribeFieldResult desField = fieldsMap.get(temp[0] + 'id').getDescribe();
                                desFieldResList.put(invlCol, desField.getReferenceTo().get(0).getDescribe().fields.getMap().get(temp[1]).getDescribe());
                            }

                        }

                    }
                }
            }
        }

        //---Added by Bhupinder Mankotia|| 28-09-2016 || Starts.---//
        catch(Exception ex) {
            UtilityController.customDebug(ex, 'CommonFunctionController');
            // return null; 
        }
        //---Added by Bhupinder Mankotia|| 28-09-2016 || Ends.---//
        return desFieldResList;
    }
    public static Map<Boolean,string> insertCommentHistory(List<sObject> childObjectList, String childObjectName, String esmNameSpace,Boolean isTrigger) {
        Map<Boolean,string> errorMap = new Map<Boolean,string>();
        insertCommentHistory(childObjectList,childObjectName,esmNameSpace);
        errorMap.put(false,'');
        return errorMap;
    }
    public static string insertCommentHistory(List<sObject> childObjectList, String childObjectName, String esmNameSpace) {
        try {
            System.debug('===childObjectList===' + childObjectList);
            List<Comments__c> commentObjList = new List<Comments__c> ();
            
            // -------- Added by Varsha | 11-04-2018 | JIRA ESMPROD-1031 | Start -------- //

            for (sObject obj : childObjectList) {

                if (UtilityController.InvFieldMap  != null ) {
               
                    System.debug('InvFieldMap :'+UtilityController.InvFieldMap);
                    System.debug('===obj===' + obj);
                    Comments__c commentObj = new Comments__c();
                    commentObj.put(childObjectName, String.valueOf(obj.get('Id')));
                    System.debug('Map :'+UtilityController.InvFieldMap.get(String.valueOf(obj.get('Invoice_No__c'))));
                    commentObj.Comment__c  = UtilityController.InvFieldMap.get(String.valueOf(obj.get('Invoice_No__c')));     // String.valueOf(obj.get(esmNameSpace + 'Comments__c'));
                    commentObj.User__c = String.valueOf(obj.get(esmNameSpace + 'Last_Modified_By__c'));
                    System.debug('Invoice Id :'+ String.valueOf(obj.get('Id')));                                                                                                           
                    commentObjList.add(commentObj);                                                  
                                             
                }
            }  
            
            // -------- Added by Varsha | 11-04-2018 | JIRA ESMPROD-1031 | End --------  //

        
            if (commentObjList.size() > 0) {
                insert commentObjList;
            }

        } catch(Exception ex) {
            return UtilityController.customDebug(ex, 'CommonFunctionController');
        }
        return null;
    }
    public static List<sobject> getGRNline(String invoiceId) {
        return getGRNline(getSetOfPOidFrminvoice(invoiceId));
    }
    public static List<sobject> getGRNline(set<String> poIdSet) {
        try
        {
            Set<String> excludeGrn = new Set<String> ();
            String whereClause = ' ' + namespace + 'Purchase_Order__c = :poIdSet';
            Invoice_Configuration__c invConf = getInvoiceConfiguration();
            if (invConf.get(namespace + 'Excluded_GRN_Status_For_Processing__c') != null && !invConf.get(namespace + 'Excluded_GRN_Status_For_Processing__c').equals(''))
            {
                for (String exgrn : String.valueOf(invConf.get(namespace + 'Excluded_GRN_Status_For_Processing__c')).split(','))
                {
                    excludeGrn.add(exgrn);
                }
                whereClause = whereClause + ' and ' + namespace + 'GRN__r.Status__c not in :excludeGrn';
            }
            String additionalFields = 'GRN__r.Purchase_Order__c,PO_Line_Item__r.GRN_Match_Required_Formula__c';
            String query = getCreatableFieldsSOQL(namespace + 'GRN_Line_Item__c', whereClause, additionalFields, true);
            system.debug('--------------------excludeGrn------------------'+excludeGrn);
            system.debug('--------------------poIdSet------------------'+poIdSet);
            system.debug('--------------------Query------------------'+query);
            return Database.query(query);
        }
        catch(Exception e)
        {
            throw e;
        }
    }
    public static Set<String> getSetOfPOidFrminvoice(String invoiceId) {
        try
        {
            Set<String> poList = new Set<String> ();
            List<Invoice_PO_Detail__c> invPO = new List<Invoice_PO_Detail__c> ();
            String query = 'SELECT ' + namespace + 'Purchase_Order__c FROM ' + namespace + 'Invoice_PO_Detail__c  where ' + namespace + 'Invoice__c = :invoiceId and Invoice__c!=null'; 
            invPO = Database.query(query);
            for (Invoice_PO_Detail__c pol : invPO) {
                poList.add(pol.Purchase_Order__c);
            }
            return poList;
        }
        catch(Exception e)
        {
            throw e;
        }
    }
    public static Invoice_Configuration__c getInvoiceConfiguration() {
        if (Invoice_Configuration__c.getInstance(Userinfo.getUserid()) != null) {
            return Invoice_Configuration__c.getInstance(Userinfo.getUserid());
        } else {
            return Invoice_Configuration__c.getOrgDefaults();
        }
    }
    
    public static String getRandomnumber(Integer len) {
        String randomStr;
        try {
            Blob blobKey = crypto.generateAesKey(128);
            String key = EncodingUtil.convertToHex(blobKey);
            randomStr = key.substring(0, len);
        } catch(Exception ex) {
            UtilityController.customDebug(ex, 'CommonFunctionController');
        }
        return randomStr;
    }

    public static Map<Boolean, string> InsertExceptionHistory(List<Invoice__c> invList, String childNameSpace, Map<String, string> sysConfMap) {
        Map<Boolean, string> errorMap = new Map<Boolean, string> ();
        try {
            String timezonedetail = currentDateWithTimezone(sysConfMap);
            for (Invoice__c inv : invList) {
                if (inv.User_Action__c != null && inv.User_Action__c == 'Validate' && inv.Exception_Reason__c != null && inv.Exception_Reason__c != '') {
                    String ExceptionHistoryOld = inv.Exception_History__c != null ? inv.Exception_History__c : '';
                    String ExceptionHistoryNew = inv.Exception_Reason__c + ' by ' + userinfo.getname() + ' on ' + timezonedetail + '\n' + ExceptionHistoryOld;
                    inv.Exception_History__c = ExceptionHistoryNew;
                }
            }
            errorMap.put(false, '');
        } catch(Exception ex) {
            errorMap.put(true, UtilityController.customDebug(ex, 'CommonFunctionController'));
        }
        return errorMap;
    }

    public static String currentDateWithTimezone(Map<String, string> sysConfMap) {
        String commentTimezone = 'GMT';
        String commentDateformat = 'MM/dd/yyyy HH:mm';
        DateTime myDateTime;
        try {
            if (sysConfMap.get('Timezone|Timezone_For_History_Fields') != null && sysConfMap.get('Timezone|Timezone_For_History_Fields') != '') {
                commentTimezone = sysConfMap.get('Timezone|Timezone_For_History_Fields');
            }
            if (sysConfMap.get('Timezone|Date_Format_For_History_Fields') != null && sysConfMap.get('Timezone|Date_Format_For_History_Fields') != null) {
                commentDateformat = sysConfMap.get('Timezone|Date_Format_For_History_Fields');
            }
            commentTimezone = commentTimezone.removeStart('GMT');

            myDateTime = datetime.now();
            if (commentTimezone.contains(':')) {
                myDateTime = myDateTime.addHours(Integer.ValueOf(commentTimezone.split(':') [0]));
                if (commentTimezone.contains('-')) {
                    myDateTime = myDateTime.addMinutes(Integer.ValueOf('-' + commentTimezone.split(':') [1]));
                } else {
                    myDateTime = myDateTime.addMinutes(Integer.ValueOf(commentTimezone.split(':') [1]));
                }
            }
        } catch(Exception ex) {
            UtilityController.customDebug(ex, 'CommonFunctionController');
        }
        return myDateTime.formatGMT(commentDateformat) + ' [' + UtilityController.getimezone('GMT' + commentTimezone) + ']';
    }

    public static string getInvLineItemQuery(sObject childSobject, String childNameSpace, String prefix, String fieldSetName, Set<String> invIdSet) {
        try {
            if (fieldSetName != '') {
                return getSelectQueryFromFieldSet(prefix + 'Invoice_Line_Item__c', fieldSetName, prefix + 'Purchase_Order__c,' + prefix + 'Invoice_Line_Item_No__c,' + prefix + 'PO_Line_Item__r.' + prefix + 'GRN_Match_Required_Formula__c,' + prefix + 'Old_Quantity__c,' + prefix + 'Old_GRN_Line_Item__c,' + prefix + 'Is_Mismatch__c,' + prefix + 'Exception_Reason__c', prefix + 'Invoice__c IN :invIdSet AND ' + prefix + 'Is_Other_Charge__c = FALSE ', true);
            }
            else {
                return getCreatableFieldsSOQL(prefix + 'Invoice_Line_Item__c', prefix + 'Invoice__c IN :invIdSet AND ' + prefix + 'Is_Other_Charge__c = FALSE ', prefix + 'PO_Line_Item__r.' + prefix + 'GRN_Match_Required_Formula__c', true);
            }
        } catch(Exception ex) {
            UtilityController.customDebug(ex, 'CommonFunctionController');
            return null;
        }
    }

    public static String getSelectQueryFromFieldSet(String objectAPI, String fieldSetName, String additionalFields, String whereClause, Boolean addLookupFieldName) {
        String fields = 'Id,Name,';
        String query;
        try {
            Map<String, Schema.FieldSet> fieldsMapSet = Schema.getGlobalDescribe().get(objectAPI).getDescribe().FieldSets.getMap();
            set<string> cFieldSet = new set<string> ();
            cFieldSet.add('id');
            cFieldSet.add('name');

            if (additionalFields != null) {
                additionalFields = additionalFields.toLowerCase();
                for (String cFields : additionalFields.split(',')) {
                    if (cFields != '') {
                        cFieldSet.add(cFields);
                    }
                }
            } else {
                additionalFields = '';
            }
            if (fieldsMapSet.get(fieldSetName) != null) {
                for (Schema.FieldSetMember st : fieldsMapSet.get(fieldSetName).getFields()) {
                    string currentField = st.getFieldPath().toLowerCase();
                    if (addLookupFieldName && String.valueOf(st.getType()).equalsignoreCase('reference')) {
                        String LookupFieldName = '';
                        if (currentField.split('\\.').size() < 2) {

                            if (!currentField.equalsIgnoreCase('CreatedById')
                                && !currentField.equalsIgnoreCase('LastModifiedById')
                                && !currentField.equalsIgnoreCase('OwnerId')) {
                                LookupFieldName = ((currentField).subString(0, (currentField).length() - 1) + 'r.Name').toLowerCase();
                            } else {
                                LookupFieldName = (currentField).subString(0, (currentField).length() - 2) + '.Name';
                            }
                        } else {
                            LookupFieldName = currentField;
                        }
                        if (!cFieldSet.contains(LookupFieldName)) {
                            if (additionalFields != '') {
                                additionalFields += ',' + LookupFieldName;
                            } else {
                                additionalFields += LookupFieldName;
                            }
                            cFieldSet.add(LookupFieldName);
                        }
                    }
                    if (!cFieldSet.contains(currentField)) {
                        fields += currentField + ',';
                        cFieldSet.add(currentField);
                    }

                }
            }
            fields = fields.removeEnd(',');
            if (additionalFields != null && additionalFields != '') {
                fields += ',' + additionalFields;
            }
        }
        catch(Exception ex) {
            UtilityController.customDebug(ex, 'CommonFunctionController');
        }
        query = 'SELECT ' + fields + ' FROM ' + objectAPI + ' WHERE ' + whereClause;
        return query;
    }
    public static List<Invoice_Line_Item__c> fillInvLineItem(String prefix, String invId) {
        try {
            if (invId != null && invId != '') {
                if (UtilityController.isFieldAccessible(prefix + 'Invoice_Line_Item__c', null)) {
                    Set<String> invIdSet = new Set<String> ();
                    invIdSet.add(invId);
                    return database.query(getInvLineItemQuery(null, '', prefix, '', invIdSet));
                }
            }
        } catch(Exception ex) {
            UtilityController.customDebug(ex, 'CommonFunctionController');
        }
        return new List<Invoice_Line_Item__c> ();
    }
    public Static List<sObject> getSobject(String childId, String childObjName) {
        //to get child object
        //Get All Fields of the child object
        List<sObject> tempTracker1;
        try
        {
            Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(childObjName).getDescribe().fields.getMap();
            tempTracker1 = new List<sObject> ();

            String fieldQueryStr = '';
            List<String> strList = new List<String> (objectFields.keySet());
            fieldQueryStr = String.join(strList, ',');
            String query = 'Select ' + String.escapeSingleQuotes(fieldQueryStr) + ' from ' + String.escapeSingleQuotes(childObjName) + ' where Id= :childId';

            if (UtilityController.isFieldAccessible(childObjName, fieldQueryStr)) {
                tempTracker1 = Database.query(query);
            }

            if (tempTracker1.size() <= 0)
            {
                return null;
            }
        } catch(Exception ex) {
            UtilityController.customDebug(ex, 'CommonFunctionController');
        }
        return tempTracker1;
    }
    public static List<Invoice_Line_Item__c> fillOtherChargesList(String prefix, String invId) {
        try {
            if (invId != null && invId != '') {
                String fieldQueryStr = getAllFieldsOfObject(prefix + 'Invoice_Line_Item__c');
                if (UtilityController.isFieldAccessible(prefix + 'Invoice_Line_Item__c', fieldQueryStr)) {
                    String query = 'SELECT ' + String.escapeSingleQuotes(fieldQueryStr) + ' FROM ' + prefix + 'Invoice_Line_Item__c WHERE Invoice__c = :invId AND Is_Other_Charge__c = TRUE';
                    return database.query(query);
                }
            }
        } catch(Exception ex) {
            UtilityController.customDebug(ex, 'CommonFunctionController');
        }
        return new List<Invoice_Line_Item__c> ();
    }
    public static List<Purchase_Order__c> getPOList(String prefix, Set<String> setPoId) {
        try {
            if (setPoId != null && setPoId.size() > 0) {
                if (UtilityController.isFieldAccessible(prefix + 'Purchase_Order__c', null)) {
                    return database.query(getCreatableFieldsSOQL(prefix + 'Purchase_Order__c', 'Id IN :setPoId', false));
                }
            }
        } catch(Exception ex) {
            UtilityController.customDebug(ex, 'CommonFunctionController');
        }
        return new List<Purchase_Order__c> ();
    }
    public static List<Purchase_Order__c> getPOList(String prefix, String POFieldsetName, Set<String> setPoId, String POnumber) {
        try {
            if (UtilityController.isFieldAccessible(prefix + 'Purchase_Order__c', null)) {
                if (POFieldsetName != null && POFieldsetName != '') {
                    if (setPoId != null) {
                        return database.query(getSelectQueryFromFieldSet(prefix + 'Purchase_Order__c', POFieldsetName, prefix + 'GRN_Match_Required__c,' + prefix + 'PO_No__c', 'Id IN :setPoId', false));
                    } else if (POnumber != null && POnumber != '') {
                        return database.query(getSelectQueryFromFieldSet(prefix + 'Purchase_Order__c', POFieldsetName, prefix + 'GRN_Match_Required__c,' + prefix + 'PO_No__c', prefix + 'PO_No__c =:POnumber OR Name =:POnumber', false));
                    }

                } else {
                    if (setPoId != null) {
                        return database.query(getCreatableFieldsSOQL(prefix + 'Purchase_Order__c', 'Id IN :setPoId', false));
                    } else if (POnumber != null && POnumber != '') {
                        return database.query(getCreatableFieldsSOQL(prefix + 'Purchase_Order__c', prefix + 'PO_No__c =:POnumber OR Name =:POnumber', false));
                    }
                }
            }

        } catch(Exception ex) {
            UtilityController.customDebug(ex, 'CommonFunctionController');
        }
        return new List<Purchase_Order__c> ();
    }
    public static List<sobject> getPOline(set<String> poId) {
        try 
        {
            if (UtilityController.isFieldAccessible('Purchase_Order__c', null)) {
                String query = 'select ' + CommonFunctionController.getAllFieldsOfObject('PO_Line_Item__c') + ' from PO_Line_Item__c where Purchase_Order__c=:poId';
                return Database.query(query);
            }else{
                throw new CustomCRUDFLSException('Insufficient rights on PO_Line_Item__c');
            }
            
        }
        catch(Exception e)
        {
            throw e;
        }
    }
    public static List<sobject> getInvoicePODetail(String prefix, String invId) {
        try 
        {
            if (UtilityController.isFieldAccessible('Invoice_PO_Detail__c', null)) {
                String query = 'SELECT '+CommonFunctionController.getAllFieldsOfObject('Invoice_PO_Detail__c')+' FROM Invoice_PO_Detail__c where Invoice__c = :invId';
                return Database.query(query);
            }else{
                throw new CustomCRUDFLSException('Insufficient rights on PO_Line_Item__c');
            }
            
        }
        catch(Exception e)
        {
            throw e;
        }
    }
}