/*
  Authors: Chandresh Koyani
  Date created: 22/11/2017
  Purpose: Schedulable class to schedule Reminder and Escalation batch job.
  Dependencies: 
  -------------------------------------------------
  Modifications:
  Date: 
  Purpose of modification: 
  Method/Code segment modified: 
 
*/
global class ReminderAndEscalationSchedulable implements Schedulable {
    
    /*
      Authors: Chandresh Koyani
      Purpose: Execute method called when batch is executed on scheduled time. 
      Dependencies: 
    */
    global void execute(SchedulableContext context) {
        ReminderAndEscalationBatch batch = new ReminderAndEscalationBatch(context);
        Database.executeBatch(batch,10);
    }
}