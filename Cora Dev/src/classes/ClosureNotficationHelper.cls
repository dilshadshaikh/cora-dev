global class ClosureNotficationHelper {

    public static void sendCaseClosureNotification(List<CaseManager__c> caseTrackeListNew, Map<Id, CaseManager__c> caseTrackerOldMap) {
        List<CaseManager__c> notficationList = new List<CaseManager__c> ();

        for (CaseManager__c caseTrackerNew : caseTrackeListNew) {
            CaseManager__c caseTrackerOld = caseTrackerOldMap.get(caseTrackerNew.Id);
            if (caseTrackerNew.Status__c != caseTrackerOld.Status__c && caseTrackerNew.Status__c == 'Pending For Archival') {
                notficationList.add(caseTrackerNew);
            }
        }

        if (notficationList.size() > 0) {
            sendNotification(notficationList, 'Closure');
        }
    }
    @InvocableMethod(label = 'Send Closure Notification' description = 'Send Closure Notification based on Closure Rules.')
    global static void sendCaseClosureNotification(List<CaseManager__c> caseManagers) {
        if (caseManagers.size() > 0) {
            sendNotification(caseManagers, 'Closure');
        }
    }
    public static void sendNotification(List<CaseManager__c> caseManagers, string type) {
        List<Reminder_Rule_Config__c> remRuleConfiges = [select id, Name, JSON__c, Order__c, IsActive__c from Reminder_Rule_Config__c where IsActive__c = true and Type__C = :type order by Order__c];

        List<Messaging.SingleEmailMessage> mailMessageList = new List<Messaging.SingleEmailMessage> ();

        for (CaseManager__c caseManager : caseManagers) {
            ReminderAndEscalationHelper.ReminderRule rule = ReminderAndEscalationHelper.findRule(caseManager, remRuleConfiges);
            if (rule != null && rule.JSONValue.notifications != null && rule.JSONValue.notifications.size() > 0) {
                ReminderAndEscalationHelper.Notification notificationObj = rule.JSONValue.notifications[0];

                Messaging.SingleEmailMessage mailMsg = createMailMessage(caseManager, notificationObj);
                mailMessageList.add(mailMsg);

            }
        }
        if (mailMessageList.size() > 0) {
            try{
                Messaging.sendEmail(mailMessageList);
            }
            catch(Exception ex){

            }
        }
    }

    private static Messaging.SingleEmailMessage createMailMessage(CaseManager__c caseManager, ReminderAndEscalationHelper.Notification notificationObj) {

        Messaging.SingleEmailMessage mailMsg = new Messaging.SingleEmailMessage();

        TemplateHelper.EmailToCaseTemplateResponse emailMergeResponse = TemplateHelper.mergeTemplate(notificationObj.emailTemplate, caseManager.id);

        string mailBody = emailMergeResponse.body;

        if (notificationObj.mailTrailIn != null && notificationObj.mailTrailIn != 'None') {
            string body = '';
            List<EmailMessage> emailMsgList = [Select Id, Subject, fromAddress, createdDate, ToAddress, CcAddress, HtmlBody, TextBody from EmailMessage Where RelatedToId = :caseManager.id and Incoming = true Order By CreatedDate];
            if (emailMsgList.size() > 0) {
                body = getBodyToAddAsAttachment(emailMsgList[0]);

                if (notificationObj.mailTrailIn == 'Attachment') {
                    Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                    efa.setFileName('Mail.html');
                    efa.setBody(Blob.valueOf(body));
                    mailMsg.setFileAttachments(new List<Messaging.EmailFileAttachment> { efa });
                }
                else if (notificationObj.mailTrailIn == 'In Mail') {
                    mailBody = mailBody + '<br/><hr/><br/>' + body;
                }
            }
        }


        mailMsg.setHtmlBody(mailBody);
        mailMsg.setSubject(emailMergeResponse.Subject);
        mailMsg.setWhatId(caseManager.Id);

        List<string> toAddresses = new List<string> ();
        List<string> ccAddresses = new List<string> ();
        List<string> bccAddresses = new List<string> ();

        if (!String.isEmpty(notificationObj.toAddressField)) {
            string fieldValue = String.valueOf(caseManager.get(notificationObj.toAddressField));
            if(!String.isEmpty(fieldValue)){
                toAddresses.addAll(fieldValue.split(';'));
            }

        }
        if (!String.isEmpty(notificationObj.ccAddressField) && notificationObj.ccAddressField!='None') {
            string fieldValue = String.valueOf(caseManager.get(notificationObj.ccAddressField));
            if(!String.isEmpty(fieldValue)){
                ccAddresses.addAll(fieldValue.split(';'));
            }
        }
        if (!String.isEmpty(notificationObj.bccAddressField) && notificationObj.bccAddressField!='None') {
            string fieldValue = String.valueOf(caseManager.get(notificationObj.bccAddressField));
            if(!String.isEmpty(fieldValue)){
                bccAddresses.addAll(fieldValue.split(';'));
            }
        }
        if (!string.isEmpty(notificationObj.additionalRecipient)) {
            List<string> additionalList = notificationObj.additionalRecipient.split(';');
            if (!string.isEmpty(notificationObj.includeIn)) {
                if (notificationObj.includeIn == 'TO') {
                    toAddresses.addAll(additionalList);
                }
                else if (notificationObj.includeIn == 'CC') {
                    ccAddresses.addAll(additionalList);
                }
                else {
                    bccAddresses.addAll(additionalList);
                }
            }
            else {
                toAddresses.addAll(additionalList);
            }
        }
        if (!String.isEmpty(notificationObj.orgWideId)) {
            mailMsg.setOrgWideEmailAddressId(notificationObj.orgWideId);
        }



        mailMsg.setToAddresses(toAddresses);
        mailMsg.setCCAddresses(ccAddresses);
        mailMsg.setBCCAddresses(bccAddresses);

        return mailMsg;
    }

    private static string getBodyToAddAsAttachment(EmailMessage emailMsg) {
        String FORM_HTML_START = '<HTML><head><title></title><meta charset="UTF-8"></head><BODY> <div style="font-family: Arial Unicode MS">';
        String FORM_HTML_END = '</div></BODY></HTML>';
        String emailBody = '';
        emailBody = emailBody + '' + FORM_HTML_START;
        emailBody = emailBody + 'From:' + emailMsg.fromAddress + '<br>';
        emailBody = emailBody + 'Received Date:' + emailMsg.createdDate + '<br>';
        emailBody = emailBody + 'To:' + emailMsg.ToAddress + '<br>';
        if (emailMsg.CcAddress != null) {
            emailBody = emailBody + 'Cc:' + emailMsg.CcAddress + '<br>';
        }
        emailBody = emailBody + 'subject:' + emailMsg.subject + '<br><br><br><br>';

        if (emailMsg.htmlBody != null) {
            emailBody = emailBody + emailMsg.HtmlBody;
        } else {
            emailBody = emailBody + emailMsg.TextBody;
        }
        emailBody = emailBody + FORM_HTML_END;

        return emailBody;
    }
}