public with sharing class ChangeOwnerController {
    @AuraEnabled
    public static List<User> userDetail(String owner,List<String> caseId){
        List<User> response = new  List<User>();
        try{
            List<User> userList  = new List<User>();
                userList = [Select Id,Name,SmallPhotoUrl from User where isActive = true and profile.Name !=null AND UserType != 'Guest' order by Name];
            
            List<CaseManager__c> caseList = new List<CaseManager__c>();
            if(owner!='' || owner!= null){
                
                for(CaseManager__c ca:[Select Id , Owner.Name,Owner.Id from CaseManager__c where Id in:caseId ]){
                    //System.debug('case Manager list' +ca);
                    ca.OwnerId = owner;
                    caseList.add(ca);
                }
            }
            if(caseList.size()>0){
                update caseList;
                //System.debug('updated owner' +caseList);
                
            }
            response =  userList;            
        }
        
        catch(Exception e){
            //System.debug('error ' +e);
            //response.put('error', errorMessage + e.getMessage());
			ExceptionHandlerUtility.writeException('ChangeOwnerController', e.getMessage(), e.getStackTraceString());
            //throw new AuraHandledException( ' Yor Error Message ==>'+e);
            response=null;
        }
        return response;
    }
    
    @AuraEnabled
    public static List<Group> queueData(String owner,List<String> caseId){
        try{
            String currentUserId = UserInfo.getUserId();
            List<Group> queueList = new List<Group>();
            List<Group> listOfUserQueue  = new List<Group>();
            //queueList = [Select Id,Name from Group where Type='Queue'];
            //System.debug('queue namne is'+owner); 
            queueList = [Select Id,Name from Group where type = 'Queue' order by Name];
           /** for(Group gp:queueList){
                for(GroupMember gpMem :gp.GroupMembers){
                    if(gpMem.UserOrGroupId == currentUserId){
                        listOfUserQueue.add(gp);
                    }
                }
            }*/
            //System.debug('queuelist' +queueList);
            List<CaseManager__c> caseList = new List<CaseManager__c>();
            if(owner!='' || owner!= null){
                
                for(CaseManager__c ca:[Select Id ,Amount__c, Owner.Name,Owner.Id from CaseManager__c where Id in:caseId ]){
                    //System.debug('case queue list' +ca);
                    ca.OwnerId = owner;
                    caseList.add(ca);
                }
            }
            if(caseList.size()>0){
                update caseList;
                //System.debug('updated queue' +caseList);
            }
            ////System.debug('Queue list of user' +listOfUserQueue);
            return queueList;
        }        
        catch(Exception e){
            //System.debug('error ' +e);
            throw new AuraHandledException( ' Yor Error Message ==>'+e);
            
        }
        
        
    }
}