@isTest
public class RelatedListComponentControllerTest {

     public static testmethod void TestMethod1() {
         
        System_Configuration__c sysconfig=new System_Configuration__c ();
		sysconfig.Module__c='Custom_Record_History';
		sysconfig.Value__c ='Invoice__c';
		insert sysconfig;
         
         Invoice_Configuration__c invConf = new Invoice_Configuration__c();
         invConf.Enable_Quantity_Calculation__c = true;
         invConf.GRN_Flag_In_PO_Header__c = true;
         invConf.Excluded_GRN_Status_For_Processing__c = 'Closed,Completed';
         invConf.Quantity_Calculation_Flag_In_PO_Header__c = false;
         invConf.Enable_Auto_Flipping_With_GRN__c = true;
         invConf.PO_Number_Field_for_Search__c = 'Name';
         invConf.Invoice_Reject_Current_State__c ='';
         invConf.Auto_Match_Flow_Method__c = 'Sequence';
         invConf.Auto_Match_Current_State__c = 'Ready For Indexing,Approved With Changes';
         invConf.Auto_Validation_Current_States__c = '';
         insert invConf;
         
         Purchase_Order__c  po = new Purchase_Order__c();
         po.PO_No__c = 'PO-1001';
         po.Amount__c = 123;
         insert po;
         
        Account accobj = new Account();
        accobj.Name='TestAccount';
        insert accobj;

		Invoice__c invobj = new Invoice__c();		
		invobj.Invoice_Date__c = Date.Today();
		invobj.Net_Due_Date__c = Date.Today().addDays(60);		
		invobj.Total_Amount__c = 500.00;
		
		System.debug('invobj1' + invobj);
		insert invobj;
         
        Payment__c payobj = new Payment__c();
        payobj.Payment_Date__c =  Date.newInstance(2016, 12, 9);
        payobj.Vendor__c= accobj.id;
        payobj.Payment_Amount__c  = 2000.00 ;
        payobj.Currency__c = 'INR';
        insert payobj;

		System.debug('payobj :'+payobj);

		List<EsmHelper.dataHelper> dh = new List<EsmHelper.dataHelper> ();
		
         
        RelatedListComponentController.getRelatedListDataServer('Payment__c',payobj.id,'[{"name":"Payment_ID__c","reference":null},{"name":"Payment_Date__c","reference":null},{"name":"Vendor__c","reference":"Name"},{"name":"Payment_Amount__c","reference":null},{"name":"Currency__c","reference":null}]',1,15,'Invoice__c','','');

		RelatedListComponentController.getRelatedListDataServer('Payment__c',payobj.id,'[{"name":"Payment_ID__c","reference":null},{"name":"Payment_Date__c","reference":null},{"name":"Vendor__c","reference":"Name"},{"name":"Payment_Amount__c","reference":null},{"name":"Currency__c","reference":null}]',1,15,'Invoice__c',null,null);
         
     }
    
}