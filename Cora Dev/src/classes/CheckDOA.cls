global class CheckDOA {
	@InvocableMethod(label = 'CheckDOA' description = 'Check for DOA Limit and route to new approver')
	global static List<Invoice__c> checkUserDOA(List<Invoice__c> invList) {
		try {
			set<id> invidset = new set<id> ();
			for (invoice__c inv : invList) {
				InvIdSet.add(inv.Id);
			}
			DOA_Setting__c DOAConfig = new DOA_Setting__c();
			for (DOA_Setting__c doasetting :[SELECT Id, Name, Amount_Field__c, First_Approver_Selection_Field__c, DOA_Invoice_Field_Mapping__c FROM DOA_Setting__c]) {
				DOAConfig = doasetting;
			}
			Map<string, DOA_Matrix__c> doaMap = UtilityController.getDOAMatrixList(invList, UtilityController.esmNameSpace, DOAConfig, '');

			System.debug('doaMap-->' + doaMap);
			Map<string, Approval_Detail__c> appMap = new Map<string, Approval_Detail__c> ();

			List<Approval_Detail__c> appList = [SELECT Id, Name, Approver_User__c, Approval_Cycle__c, Invoice__c, Approver_Action__c FROM Approval_Detail__c WHERE Invoice__c IN :InvIdSet];
			for (Approval_Detail__c appObj : appList) {
				appMap.put(appObj.Approver_User__c + '~' + appObj.Approval_Cycle__c, appObj);
			}
			system.debug('appMap:' + appMap);
			List<Approval_Detail__c> appDetUpdate = new List<Approval_Detail__c> ();
			for (Invoice__c inv : invList) {
				System.debug('inv.User_Action__c : ' + inv.User_Action__c);
				System.debug('inv.Current_Approver__c : ' + inv.Current_Approver__c);
				System.debug('inv.Current_State__c : ' + inv.Current_State__c);
				System.debug('inv.Current_Approver__c : ' + inv.Current_Approver__c);
				System.debug('inv.Current_Cycle__c : ' + inv.Current_Cycle__c);
				System.debug('inv.Pending_Approval__c : ' + inv.Pending_Approval__c);
				System.debug('inv.Current_State_Assign_Date__c : ' + inv.Current_State_Assign_Date__c);
				//---- First Approver selection ----//
				if (inv.Current_Approver__c == null) {

					Approval_Detail__c appObj = new Approval_Detail__c();
					appObj.Invoice__c = inv.Id;
					appObj.Status__c = 'Pending';
					if (DOAConfig.First_Approver_Selection_Field__c != null && DOAConfig.First_Approver_Selection_Field__c != '') {
						appObj.Approver_User__c = String.valueOf(inv.get(DOAConfig.First_Approver_Selection_Field__c));
					}

					if (appList.size() > 0) {
						appObj.Approval_Cycle__c = inv.Current_Cycle__c + 1;
					} else {
						appObj.Approval_Cycle__c = inv.Current_Cycle__c;
					}
					appDetUpdate.add(appObj);

				} else {
					//---- Routing to Supervisor ----//	
					DOA_Matrix__c doa = doaMap.get(inv.Current_Approver__c + '`' + inv.Id);

					if (doa != null) {
						String action = (inv.User_Action__c != null ? (inv.User_Action__c == 'Approve' ? 'Approved' : (inv.User_Action__c == 'Reject' ? 'Rejected' : '')) : '');
						System.debug('action : ' + action);
						System.debug('@@ inv.Current_Approver__c : ' + inv.Current_Approver__c);
						System.debug('@@ inv.Current_Approver__c :- ' + inv.Current_Approver__c + '~' + integer.valueof(inv.Current_Cycle__c));
						Approval_Detail__c appObj = appMap.get(inv.Current_Approver__c + '~' + integer.valueof(inv.Current_Cycle__c));
						system.debug('appObj:' + appObj);

						if (appObj != null) {
							appObj.Status__c = action;
							appObj.Approver_Action__c = inv.User_Action__c;
							appDetUpdate.add(appObj);
						}
						//assigning to supervisor
						Decimal compareAmount = DOAConfig.Amount_Field__c !=null  && inv.get(DOAConfig.Amount_Field__c) != null ? Decimal.valueOf(String.valueOf(inv.get(DOAConfig.Amount_Field__c))) : inv.Total_Amount__c;
						if (inv.User_Action__c == 'Approve' && doa.DOA_Limit__c < compareAmount) {
							System.debug('daofound');
							if (doa.Supervisor__c != null) {
								appObj = new Approval_Detail__c();
								appObj.Invoice__c = inv.Id;
								appObj.Status__c = 'Pending';
								appObj.Approver_User__c = doa.Supervisor__c;
								appObj.Approval_Cycle__c = inv.Current_Cycle__c;
								appDetUpdate.add(appObj);

							} else {
								System.debug('Supervisor Not Found.');
								throw new CustomCRUDFLSException('Supervisor Not Found.');
							}
						}

					} else {
						System.debug('DOA Not Found.');
						throw new CustomCRUDFLSException('DOA Not Found.');
					}
				}
			}

			upsert appDetUpdate;

			Map<String, String> invoiceToStatus = new Map<String, String> ();
			Map<String, String> invoiceToCurrentApp = new Map<String, String> ();
			List<Approval_Detail__c> appDetail = [Select Id, Approval_Cycle__c, Approver_Action__c, Approver_User__c, Invoice__c, Status__c, Invoice__r.Current_Cycle__c From Approval_Detail__c Where Invoice__c IN :InvIdSet];
			for (Approval_Detail__c apd : appDetail) {
				if (apd.Approval_Cycle__c == apd.Invoice__r.Current_Cycle__c)
				if (invoiceToStatus.containsKey(apd.Invoice__c)) {
					if (invoiceToStatus.get(apd.Invoice__c) != 'Rejected' && invoiceToStatus.get(apd.Invoice__c) != 'Pending' && !apd.Status__c.equals('Approved')) {
						invoiceToStatus.put(apd.Invoice__c, apd.Status__c);
						invoiceToCurrentApp.put(apd.Invoice__c, apd.Approver_User__c);
					}
					System.debug('invoiceToStatus-->' + invoiceToStatus);
				} else {
					invoiceToStatus.put(apd.Invoice__c, apd.Status__c);
					invoiceToCurrentApp.put(apd.Invoice__c, apd.Approver_User__c);
					System.debug('invoiceToStatus-->' + invoiceToStatus);
				}
			}
			List<Invoice__c> needToUpdate = new List<Invoice__c> ();
			Set<String> appdInv = new Set<String> ();
			if (!invoiceToStatus.isEmpty()) {
				Invoice__c tmp;
				for (String id : invoiceToStatus.keySet()) {
					if (invoiceToStatus.get(id) == 'Approved') {
						appdInv.add(id);
					}
					tmp = new Invoice__c();
					tmp.Id = id;
					tmp.Approval_Status__c = invoiceToStatus.get(id);
					tmp.Current_Approver__c = invoiceToCurrentApp.get(id);
					needToUpdate.add(tmp);
				}
			}

			System.debug('@@@ needToUpdate : ' + needToUpdate);
			//throw new DMLException();
			if (needToUpdate.size() > 0) {
				update needToUpdate;
				String query = '';
				String selectfields = '';
				Map<string, Schema.sObjectType> describeInfo = Schema.getGlobalDescribe();
				Map<String, Schema.SObjectField> fMap = describeInfo.get('Invoice__c').getDescribe().Fields.getMap();
				if (fMap != null) {
					for (Schema.SObjectField f : fMap.values()) {
						selectfields += f.getDescribe().getName().toLowerCase() + ',';
					}
					selectfields = selectfields.removeEnd(',');
					query = 'select ' + selectfields + ' from Invoice__c where Id in :appdInv ';
					needToUpdate = Database.query(query);
					QCCalculation.checkForQC(needToUpdate, 'Invoice__c');
				}
			}
			// endof code rohan

		} catch(exception ex) {
			System.debug('Exception110');
			system.debug('error' + ex + '----' + ex.getLinenumber());
		}
		return null;
	}


}