global interface InvoiceValidationExceptionInterface{

    Map<object,object> execute(sobject childSobject,String childNameSpace,List<PO_Line_Item__c> poLineItemList,
                            List<Invoice_Line_Item__c> invLineItemList,List<GRN_Line_Item__c> grnLineItemList,Map<Object,Object> validationMap,Map<Object,Object> extraParam);
}