@isTest
public class ComponentConfigRelatedListControllerTest{
    public static testMethod void testFirst(){
     ComponentConfigRelatedListController.saveComponent('Attachment');
     ComponentConfigRelatedListController.getAllComponents();
     ComponentConfigRelatedListController.changecmpState('1');
        ComponentConfigRelatedListController.getObjects();
        ComponentConfigRelatedListController.getAllLayouts();
        ComponentConfigRelatedListController.removeComponent('1');
        //ComponentConfigRelatedListController.getCurrentObjectChild('Invoice__c','Invoice__c');
        ComponentConfigRelatedListController.LayoutInfo LayoutInfoWrap = new ComponentConfigRelatedListController.LayoutInfo();
        LayoutInfoWrap.id = '1';
        LayoutInfoWrap.isActive = false;
        LayoutInfoWrap.LayoutType = 'create';
        LayoutInfoWrap.ObjectName = 'Invoice__c';
        LayoutInfoWrap.PageLayout = null;
        
        ComponentConfigRelatedListController.saveRule(LayoutInfoWrap);
        ComponentConfigRelatedListController.removeLayout('1');
        ComponentConfigRelatedListController.getChildObjectDetails('Invoice__c');
        ComponentConfigRelatedListController.getObjectFields('Invoice__c');
        
        ComponentConfigRelatedListController.PageLayout PageLayoutWrap = new ComponentConfigRelatedListController.PageLayout();
        PageLayoutWrap.objectName= 'Invoice__c';
        PageLayoutWrap.objectLabel = 'invoice';
        PageLayoutWrap.objectNameSpace = 'a';
        
        ComponentConfigRelatedListController.LayoutSection LayoutSectionWrap = new ComponentConfigRelatedListController.LayoutSection();
        LayoutSectionWrap.criteria = 'a';
        LayoutSectionWrap.active = false;
        LayoutSectionWrap.readonly = false;
        LayoutSectionWrap.title = 'abc';
        
        ComponentConfigRelatedListController.column columnWrap = new ComponentConfigRelatedListController.column();
        columnWrap.currentUserSelected = false;
        columnWrap.defaultValue = 'aa';
        columnWrap.currentUserSelected = false;
        columnWrap.excludeCurrentUser = false;
        columnWrap.name = 'a';
        columnWrap.label = 'aa';
        columnWrap.readonly = false;
        columnWrap.required = false;
        
        ComponentConfigRelatedListController.RelatedList RelatedListWrap = new ComponentConfigRelatedListController.RelatedList();
        RelatedListWrap.objectName = 'Invoice__c';
        RelatedListWrap.title = 'aa';
        RelatedListWrap.active = false;
        RelatedListWrap.readonly = false;
        RelatedListWrap.criteria = 'aa';
        RelatedListWrap.relativeField = 'bb';
        RelatedListWrap.whereClause = 'cc';
        RelatedListWrap.orderBy = 'xx';
        RelatedListWrap.groupBy = 'yy';
         
        ComponentConfigRelatedListController.CmpFieldInfo CmpFieldInfoWrap = new ComponentConfigRelatedListController.CmpFieldInfo();
        CmpFieldInfoWrap.apiName = 'aa';
        CmpFieldInfoWrap.label = 'bb';
        CmpFieldInfoWrap.type = 'create';
    }
}