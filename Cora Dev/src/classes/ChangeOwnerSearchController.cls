public class ChangeOwnerSearchController {
    @AuraEnabled
    public static List < User > userDetail(String owner, List < String > caseId, String searchConfigName) {
        try {
            //To fetch
            List < User > userList = new List < User > ();
            String objName = 'CaseManager__c';
            String query = 'Select Id,Name,SmallPhotoUrl from User where isActive = true';
            if (searchConfigName != 'NA') {
                Set < String > UserTypeSet = new Set < String > ();
                SearchConfigWrapper scw = new SearchConfigWrapper();
                scw = ESMSearchController.getSerachConfiguration(searchConfigName);
                objName = scw.ObjectAPIName;
                if (scw.BulkAssignmentUserType != null && scw.BulkAssignmentUserType != '') {
                    for (String UserType: scw.BulkAssignmentUserType.split(',')) {
                        UserTypeSet.add(UserType);
                    }
                    query += ' AND User_Type__c = :UserTypeSet';
                }
            }
            userList = database.query(query);


            //To update 
            List < SObject > caseList = new List < SObject > ();
            System.debug(owner + ' -- ' + searchConfigName);
            if (owner != '' && owner != null) {
                String listType = 'List<' + objName + '>';
                caseList = (List < SObject > ) Type.forName(listType).newInstance();
                List < sObject > castRecords = database.query('Select Id , Owner.Name,Owner.Id from ' + objName + ' where Id in:caseId ');
                for (sObject ca: castRecords) {
                    System.debug('case Manager list' + ca);
                    ca.put('OwnerId', owner);
                    ca.put('User_Action__c', 'Bulk Assignment');
                    caseList.add(ca);
                }
            }
            if (caseList.size() > 0) {
                update caseList;
                System.debug('updated owner' + caseList);
            }
            return userList;
        } catch (Exception e) {
            System.debug('error ' + e);
            throw new AuraHandledException(' Yor Error Message ==>' + e);
        }
    }

    @AuraEnabled
    public static List < User > getUsers(String searchConfigName, String moduelName) {
        try {
            List < User > userList = new List < User > ();
            String query = 'Select Id,Name from User where isActive = true';
            Set < String > userTypeSet = new Set < String > ();
            SearchConfigWrapper scw = ESMSearchController.getSerachConfiguration(searchConfigName);
            String objName = scw.ObjectAPIName;
			String userTypes = '';
			if (moduelName == 'reassignment')
			{
				userTypes = scw.ReassignmentUserType;
			}else{
				userTypes = scw.BulkAssignmentUserType;
			}
            if (userTypes != null && userTypes != '') {
                for (String userType: userTypes.split(',')) {
                    userTypeSet.add(userType);
                }
                query += ' AND User_Type__c = :userTypeSet';
            }
            return database.query(query);
        } catch (Exception e) {
            System.debug('error ' + e);
            throw new AuraHandledException(' Yor Error Message ==>' + e);
        }
    }

    @AuraEnabled
    public static void changeUserAndQueue(String owner, List < String > caseId, String searchConfigName) {

        try {
            List < SObject > caseList = new List < SObject > ();
            SearchConfigWrapper scw = ESMSearchController.getSerachConfiguration(searchConfigName);
            String objName = scw.ObjectAPIName;
            String listType = 'List<' + objName + '>';
            caseList = (List < SObject > ) Type.forName(listType).newInstance();
            List < sObject > castRecords = database.query('Select Id , Owner.Name,OwnerId,User_Action__c from ' + objName + ' where Id in:caseId ');
            for (sObject ca: castRecords) {
                ca.put('OwnerId', owner);
                ca.put('User_Action__c', 'Bulk Assignment');
                caseList.add(ca);
            }
            if (caseList.size() > 0) {
                update caseList;
                System.debug('updated owner' + caseList);
            }
        } catch (Exception e) {
            System.debug('error ' + e);
            throw new AuraHandledException(' Yor Error Message ==>' + e);
        }
    }

    @AuraEnabled
    public static List < QueueSobject > getQueues(String searchConfigName) {
        List < QueueSobject > queueList = new List < QueueSobject > ();
        try {

            SearchConfigWrapper scw = ESMSearchController.getSerachConfiguration(searchConfigName);
            String objName = scw.ObjectAPIName;
            queueList = database.query('select Id, Queue.Name, SobjectType from QueueSobject where SobjectType = :objName');
        } catch (Exception e) {
            System.debug('error ' + e);
            throw new AuraHandledException(' Yor Error Message ==>' + e);

        }
        return queueList;
    }
}