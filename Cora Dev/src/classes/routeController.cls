public with sharing class routeController {
    @AuraEnabled
    public static List<User> userDetail(String sObjectName){
         try{
            List<User> userList  = new List<User>();
			System.debug('sObjectName userDetail routeController :'+sObjectName);
			System_Configuration__c sysConf = [select Value__c from System_Configuration__c where Module__c =: sObjectName and Sub_Module__c ='Route_User_Type' limit 1];
			String scValue = sysConf.Value__c ;
			System.debug('scValue :'+scValue);
			//Invoice_Configuration__c invConf = CommonFunctionController.getInvoiceConfiguration();
			//System.debug('invConf is' +invConf);
			Set < String > userTypeSet = new Set < String > ();
			String query = 'Select Id,Name,SmallPhotoUrl from User where isActive = true AND profile.Name != null';
			String userTypes = '';
			if (sysConf != null) {
				userTypes = sysConf.Value__c;
				if (userTypes != null && userTypes != '') {
					for (String userType: userTypes.split(',')) {
						userTypeSet.add(userType);
					}
					query += ' AND User_Type__c = :userTypeSet';
				}
			}
            userList = database.query(query);
            System.debug('userList is' +userList);
           return userList;            
        }

        
        catch(Exception e){
            System.debug('error ' +e);
            throw new AuraHandledException( ' Yor Error Message ==>'+e);
            
        }
        
    }
    
    @AuraEnabled
    public static List<QueueSobject> queueData(String sObjectName){
        try{
            String currentUserId = UserInfo.getUserId();
           // List<Group> queueList = new List<Group>();
           // List<Group> listOfUserQueue  = new List<Group>();
             System.debug('sobject name is '+sObjectName);
            //queueList = [Select Id,Name from Group where Type='Queue'];
            //System.debug('queue namne is'+owner); 
          	List<QueueSobject> queueList = new List<QueueSobject>();
			queueList = [select Id, Queue.Name, SobjectType from QueueSobject where SobjectType = :sObjectName order by Queue.Name];
           /** for(Group gp:queueList){
                for(GroupMember gpMem :gp.GroupMembers){
                    if(gpMem.UserOrGroupId == currentUserId){
                        listOfUserQueue.add(gp);
                    }
                }
            }*/
         System.debug('list of queue' +queueList);
            return queueList;
        }        
        catch(Exception e){
            System.debug('error ' +e);
            throw new AuraHandledException( ' Yor Error Message ==>'+e);
            
        }
        
        
    }
}