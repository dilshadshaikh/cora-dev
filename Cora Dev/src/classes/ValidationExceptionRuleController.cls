/*
  Authors: Somnath Patil
  Date created: 01/11/2017
  Purpose: Controller class for "QCRule.Page". Its contain static method related to insert,update and delete of QC Rule.
  Dependencies: Called from "QCRule.Page".
  -------------------------------------------------
  Modifications: 1
  Date: 17/11/2017
  Purpose of modification: Added new rule type "form"
  Method/Code segment modified: added new method getQualityCheckFieldset().
 
*/
public with sharing class ValidationExceptionRuleController {

    public static Map<string, List<string>> operatorMap {
        get; set;
    }

    static {
        operatorMap = new Map<string, List<string>> ();
        List<string> textOperator = new List<string> { 'equals', 'not equal to', 'starts with', 'end with', 'contains', 'does not contain', 'contains in list' };
        List<string> numberOperator = new List<string> { 'equals', 'not equal to', 'less than', 'greater than', 'less or equal', 'greater or equal' };
        List<string> pickListOperator = new List<string> { 'equals', 'not equal to', 'contains in list' };
        List<string> booleanOperator = new List<string> { 'equals', 'not equal to' };

        operatorMap.put('TEXT', textOperator);
        operatorMap.put('NUMBER', numberOperator);
        operatorMap.put('CURRENCY', numberOperator);
        operatorMap.put('DOUBLE', numberOperator);
        operatorMap.put('PICKLIST', pickListOperator);
        operatorMap.put('BOOLEAN', booleanOperator);
        operatorMap.put('EMAIL', textOperator);
        operatorMap.put('PHONE', textOperator);
        operatorMap.put('STRING', textOperator);
        operatorMap.put('REFERENCE', new List<string> { 'equals', 'not equal to', 'contains in list' });
        operatorMap.put('TEXTAREA', new List<string> { 'contains', 'does not contain' });
    }

    /*
      Authors: Somnath Patil
      Purpose: Get Case Tracker Fields to create criteria.
      Dependencies: Called from "QCRule.Page"
     */
    @RemoteAction
    public static List<ValidationHelper.FieldInfo> getCaseTrackerFields(String objectName) {
    	System.debug('we are here');
    	List<User> allActiveUser = [select id, Name from User];
		List<ValidationHelper.FieldInfo> fieldNames = new List<ValidationHelper.FieldInfo> ();
		if(objectName != '--None--' && objectName != ''){
        Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
        for (Schema.SObjectField sfield : fieldMap.Values())
        {
            schema.describefieldresult dfield = sfield.getDescribe();

            string type = String.valueOf(dfield.getType());
            if (dField.isCustom() && !dField.isCalculated()) {

                ValidationHelper.FieldInfo fieldInfo = new ValidationHelper.FieldInfo();
                fieldInfo.label = dfield.getLabel();
                fieldInfo.apiName = dfield.getName();
                fieldInfo.pickListValue = new list<ValidationHelper.option> ();
                fieldInfo.type = type;
                fieldInfo.operator = operatorMap.get(fieldInfo.type);

                if (fieldInfo.type == 'REFERENCE') {
                    string relatedObjectName = String.valueOf(dfield.getReferenceTo()).substringBetween('(', ')');
                    if (relatedObjectName.equalsIgnoreCase('USER')) {
                        fieldNames.add(fieldInfo);
                    }
                }
                else {
                    if (fieldInfo.type == 'PICKLIST') {
                        List<Schema.PicklistEntry> picklist = dfield.getPicklistValues();
                        for (Schema.PicklistEntry pickListVal : picklist) {
                            fieldInfo.pickListValue.add(new ValidationHelper.option(pickListVal.getValue(), pickListVal.getLabel()));
                        }
                    }
                    else if (fieldInfo.type == 'BOOLEAN') {
                        fieldInfo.pickListValue.add(new ValidationHelper.option('True', 'True'));
                        fieldInfo.pickListValue.add(new ValidationHelper.option('False', 'False'));
                    }

                    fieldNames.add(fieldInfo);
                	}
            	}
        	}
		}
        return fieldNames;
    }


    /*
      Authors: Somnath Patil
      Purpose: Save QC Rule entry if id is provided, it will create new rule if id is not provided.
      Dependencies: Called from "QCRule.Page"
      */
     @RemoteAction 
     public static List<ValidationHelper.ValidationRule> getAdditionalRuleData(string id){
     	List<ValidationHelper.ValidationRule> qcRules = new List<ValidationHelper.ValidationRule> ();
     	 ValidationHelper.ValidationRule validationConfig = new ValidationHelper.ValidationRule();
     	if(id!=null){
       			List<Validation_Exception_Rule__c> validationList = [select AdditionalRules__c,AdditionalRuleName__c from Validation_Exception_Rule__c where Id=:id];
       			if (validationList.size() > 0) {
           	 for (Validation_Exception_Rule__c valConfigObj : validationList) {
                ValidationHelper.JSONValue jsonField = (ValidationHelper.JSONValue) System.JSON.deserialize(valConfigObj.AdditionalRules__c, ValidationHelper.JSONValue.class);
                 ValidationHelper.ValidationRule valRuleObj = new ValidationHelper.ValidationRule();
                valRuleObj.additionRules = jsonField;
                qcRules.add(valRuleObj);
           	 		}
       			}
     		}
     	return qcRules;
     	}
     @RemoteAction
     public static string saveValidationRule(ValidationHelper.ValidationRule validObj, String objectName,ValidationHelper.ValidationRule newValidObj,ValidationHelper.ValidationRule ruleObj)
     {
     	try{
     		System.debug('newValidObj :'+validObj+','+newValidObj.id);
            System.debug('objectName :'+objectName);
            Validation_Exception_Rule__c validationConfig = new Validation_Exception_Rule__c();
            validationConfig.Rule__c = JSON.serialize(validObj.JSONValue);
            System.debug('----'+newValidObj.isActive+','+newValidObj.errorType);
            System.debug('active'+newValidObj.isActive);
            if(newValidObj.isActive!=null){
            validationConfig.IsActive__c = newValidObj.isActive;
            }
            if( newValidObj.errorType!=null){
            	validationConfig.Error_Type__c = newValidObj.errorType;
            }
            if(newValidObj.errorCode!=null){
            	 validationConfig.Error_Code__c = newValidObj.errorCode;
            }
           	if(newValidObj.isStopExecution!=null){
           		validationConfig.Stop_Excecution__c = newValidObj.isStopExecution;
           	}
            if( newValidObj.isCustom!=null){
           		 validationConfig.isCustom__c = newValidObj.isCustom;	
            }
            if(newValidObj.additionRuleName!=null && newValidObj.additionRuleName!= '--None--' ){
            	validationConfig.AdditionalRuleName__c = newValidObj.additionRuleName;
            }
            if(ruleObj!=null){
            	validationConfig.AdditionalRules__c  = JSON.serialize(ruleObj.JSONValue);
            }
             if (!string.isEmpty(newValidObj.id)) {
                validationConfig.id = newValidObj.id;
            }
            else {
                AggregateResult[] groupedResults = [select MAX(Order__c) from Validation_Exception_Rule__c];
                integer nextOrder = 1;
                if (groupedResults != null && groupedResults.size() > 0) {
                    integer maxOrder = integer.valueOf(groupedResults[0].get('expr0'));
                    if (maxOrder != null) {
                        nextOrder = maxOrder + 1;
                    }
                }
                validationConfig.Order__c = nextOrder;
            }

            upsert validationConfig;
            return 'SUCCESS';
        }
        catch(Exception ex) {
            //ExceptionHandlerUtility.writeException('ValidationExceptionRuleController', ex.getMessage(), ex.getStackTraceString());
            return '';
        }	
     	
     }
    @RemoteAction
    public static string saveRule(QCHelper.QCRule qcRuleObj, String objectName) {
        try {
            System.debug('qcRuleObj :'+qcRuleObj);
            System.debug('objectName :'+objectName);
            QC_Rule_Config__c qcConfig = new QC_Rule_Config__c();
            qcConfig.Rule_Name__c = qcRuleObj.ruleName;
            qcConfig.JSON__c = JSON.serialize(qcRuleObj.JSONValue);
            qcConfig.IsActive__c = qcRuleObj.isActive;
            qcConfig.Type__c = qcRuleObj.type;
            qcConfig.object_Name__c = objectName;
            if (!string.isEmpty(qcRuleObj.id)) {
                qcConfig.id = qcRuleObj.id;
            }
            else {
                AggregateResult[] groupedResults = [select MAX(Order__c) from QC_Rule_Config__c where Type__c = :qcRuleObj.type];
                integer nextOrder = 1;
                if (groupedResults != null && groupedResults.size() > 0) {
                    integer maxOrder = integer.valueOf(groupedResults[0].get('expr0'));
                    if (maxOrder != null) {
                        nextOrder = maxOrder + 1;
                    }
                }
                qcConfig.Order__c = nextOrder;
            }

            upsert qcConfig;
            return 'SUCCESS';
        }
        catch(Exception ex) {
            //ExceptionHandlerUtility.writeException('ValidationExceptionRuleController', ex.getMessage(), ex.getStackTraceString());
            return '';
        }
    }

    /*
      Authors: Somnath Patil
      Purpose: Get all QC Rule.
      Dependencies: Called from "QCRule.Page"
     */
    @RemoteAction
    public static List<ValidationHelper.ValidationRule> getAllRules() {
    	System.debug('Helper class Started');
        List<ValidationHelper.ValidationRule> valRules = new List<ValidationHelper.ValidationRule> ();

        List<Validation_Exception_Rule__c> validationList = [select id, Rule__c, Order__c, isActive__c,isCustom__c,Stop_Excecution__c,Error_Code__c,Error_Type__c from Validation_Exception_Rule__c order by Order__c   limit 999];
        if (validationList.size() > 0) {
            for (Validation_Exception_Rule__c valConfigObj : validationList) {
                ValidationHelper.JSONValue jsonField = (ValidationHelper.JSONValue) System.JSON.deserialize(valConfigObj.Rule__c, ValidationHelper.JSONValue.class);
                ValidationHelper.ValidationRule valRuleObj = new ValidationHelper.ValidationRule();
                valRuleObj.JSONValue = jsonField;
                valRuleObj.order = Integer.valueOf(valConfigObj.Order__c);
                valRuleObj.id = valConfigObj.id;
                valRuleObj.isActive = valConfigObj.IsActive__c;
                valRuleObj.errorCode = valConfigObj.Error_Code__c;
                valRuleObj.errorType = valConfigObj.Error_Type__c;
                valRuleObj.isCustom = valConfigObj.isCustom__c;
                valRuleObj.isStopExecution = valConfigObj.Stop_Excecution__c;
                valRules.add(valRuleObj);
            }
        }
        System.debug('valRules--'+valRules);
        return valRules;
    }

    /*
      Authors: Somnath Patil
      Purpose: Remove QC Rule Config entry based on id provided..
      Dependencies: Called from "QCRule.Page"
     */
    @RemoteAction
    public static void removeRule(string id) {
        try {
            List<Validation_Exception_Rule__c> validRule = [select id from Validation_Exception_Rule__c where id = :id];
            delete validRule;
        }
        catch(Exception ex) {
            //ExceptionHandlerUtility.writeException('ValidationExceptionRuleController', ex.getMessage(), ex.getStackTraceString());
        }
    }

    /*
      Authors: Somnath Patil
      Purpose: Change state of a rule(Active/Inactive).
      Dependencies: Called from "QCRule.Page"
     */
    @RemoteAction
    public static void changeState(string id, boolean isActive) {
        try {
            Validation_Exception_Rule__c rule = new Validation_Exception_Rule__c();
            rule.id = id;
            rule.IsActive__c = isActive;
            update rule;
        }
        catch(Exception ex) {
            // ExceptionHandlerUtility.writeException('ValidationExceptionRuleController', ex.getMessage(), ex.getStackTraceString());
        }
    }

    /*
      Authors: Somnath Patil
      Purpose: Change order of a rule.
      Dependencies: Called from "QCRule.Page"
     */
    @RemoteAction
    public static void changeOrder(integer currentOrder, string orderType) {
        try {
        	List<Validation_Exception_Rule__c> updateValidation = new List<Validation_Exception_Rule__c>();
            integer previousNextCount = 0;
            if (orderType == 'UP') {
                previousNextCount = currentOrder - 1;
            }
            else {
                previousNextCount = currentOrder + 1;
            }
            List<Validation_Exception_Rule__c> valRules = [select id, Order__c from Validation_Exception_Rule__c where Order__c = :currentOrder OR Order__c = :previousNextCount];
            if (valRules.size() > 0) {
                for (Validation_Exception_Rule__c rule : valRules) {
                    if (rule.Order__c == currentOrder) {
                        rule.Order__c = previousNextCount;
                    }
                    else {
                        rule.Order__c = currentOrder;
                    }
                    updateValidation.add(rule);
                }
            }
            update updateValidation;
        }
        catch(Exception ex) {
            //ExceptionHandlerUtility.writeException('ValidationExceptionRuleController', ex.getMessage(), ex.getStackTraceString());
        }
    }


    /*
      Authors: Somnath Patil
      Purpose: Get Quality Check fieldset to create form.
      Dependencies: Called from "QCRule.Page"
     */
    @RemoteAction
    public static List<QCHelper.Option> getQualityCheckFieldset() {
        List<QCHelper.Option> options = new List<QCHelper.Option> ();

        try {
            Map<String, Schema.FieldSet> fieldSetMap = Schema.getGlobalDescribe().get('QualityCheck__c').getDescribe().fieldSets.getMap();

            for (Schema.FieldSet fieldsetObj : fieldSetMap.values()) {
                options.add(new QCHelper.Option(fieldsetObj.getLabel(), fieldsetObj.getName()));
            }
        }
        catch(Exception ex) {
            //ExceptionHandlerUtility.writeException('ValidationExceptionRuleController', ex.getMessage(), ex.getStackTraceString());
        }
        return options;
    }
    @RemoteAction
    public static List<ValidationHelper.FieldInfo> getAdditionalRuleList(){
    	List<ValidationHelper.FieldInfo> objects = new List<ValidationHelper.FieldInfo> ();
    	ValidationHelper.FieldInfo fieldInfo = new ValidationHelper.FieldInfo();
    	Schema.DescribeFieldResult fieldResult = Validation_Exception_Rule__c.AdditionalRuleName__c.getDescribe();
        System.debug('fieldResult :' + fieldResult);
        List<Schema.PicklistEntry> plvalues = fieldResult.getPicklistValues();
      //  objects.add(new SelectOption('--None--', '--None--'));
        for (Schema.PicklistEntry f : plvalues)
        {
        	fieldInfo = new ValidationHelper.FieldInfo();
        	fieldInfo.label = f.getLabel();
            fieldInfo.apiName = f.getValue();
         	objects.add(fieldInfo);   
        }
        System.debug('objects :' + objects);
        return objects;
    	
    }
    @RemoteAction
    public static List<SelectOption> getErrorTypeData() {
    	List<SelectOption> objects = new List<SelectOption> ();
    	Schema.DescribeFieldResult fieldResult = Validation_Exception_Rule__c.Error_Type__c.getDescribe();
        System.debug('fieldResult :' + fieldResult);
        List<Schema.PicklistEntry> plvalues = fieldResult.getPicklistValues();
        objects.add(new SelectOption('--None--', '--None--'));
        for (Schema.PicklistEntry f : plvalues)
        {
            objects.add(new SelectOption(f.getValue(), f.getLabel()));
        }
        System.debug('objects :' + objects);
        return objects;
    	
    }
    @RemoteAction
    public static List<SelectOption> getErrorData() {
    	List<SelectOption> objects = new List<SelectOption> ();
    	Schema.DescribeFieldResult fieldResult = Validation_Exception_Rule__c.Error_Code__c.getDescribe();
        System.debug('fieldResult :' + fieldResult);
        List<Schema.PicklistEntry> plvalues = fieldResult.getPicklistValues();
        objects.add(new SelectOption('--None--', '--None--'));
        for (Schema.PicklistEntry f : plvalues)
        {
            objects.add(new SelectOption(f.getValue(), f.getLabel()));
        }
        System.debug('objects :' + objects);
        return objects;
    	
    }
    @RemoteAction
    public static List<SelectOption> getAllObjectsFields() {
        List<SelectOption> objects = new List<SelectOption> ();
        objects.add(new SelectOption('--None--', '--None--'));
        objects.add(new SelectOption('Invoice__c', 'Invoice'));
        System.debug('objects :' + objects);
        return objects;
    }
}