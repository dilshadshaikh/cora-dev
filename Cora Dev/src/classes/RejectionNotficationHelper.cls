global class RejectionNotficationHelper { 
    public static void sendCaseRejectionNotification(List<CaseManager__c> caseTrackeListNew, Map<Id, CaseManager__c> caseTrackerOldMap) {
        List<CaseManager__c> notficationList = new List<CaseManager__c> ();

        for (CaseManager__c caseTrackerNew : caseTrackeListNew) {
            CaseManager__c caseTrackerOld = caseTrackerOldMap.get(caseTrackerNew.Id);
            if (caseTrackerNew.Status__c != caseTrackerOld.Status__c && caseTrackerNew.Status__c == 'Rejected') {
                notficationList.add(caseTrackerNew);
            }
        }

        if(notficationList.size()>0){
            ClosureNotficationHelper.sendNotification(notficationList,'Rejection');
        }
    }
    @InvocableMethod(label = 'Send Rejection Notification' description = 'Send Rejection Notification based on Rejection Rules.')
    global static void sendCaseRejectionNotification(List<CaseManager__c> caseManagers) {
        if(caseManagers.size()>0){
            ClosureNotficationHelper.sendNotification(caseManagers,'Rejection');
        }
    }
}