@isTest
public class ThrowErrorOnScreenTest{

    public static testMethod void testFirst(){
        Invoice__c inv = new Invoice__c ();
         inv.Invoice_No__c = 'INV-0001';
         inv.Invoice_Date__c = Date.Today();
         inv.Net_Due_Date__c = Date.Today().addDays(60);    
         inv.Total_Amount__c = 500.00;
         inv.Comments__c = 'test comment';
         //inv.User_Actions__c = 'Validate';
         inv.Current_State__c = 'Ready For Processing';        
         insert inv;
       	system.debug('-----inv'+inv);
       		List<Invoice__c> invList = new List<Invoice__c> ();
        	invList.add(inv);
       ThrowErrorOnScreen.errorError(invList);
    }
}