public with sharing class SearchDownloadControllerESM {

    public Boolean limitReached{get;set;}
    public string objectTmp{get;set;}
    public String selectedRowIDs{get;set;}  
    public String ObjCode{get;set;}
    public String searchResultFiledSetName{get;set;}
    public String contentType { get; set; }
    
    public List<Object> fieldNames {get; set; }
    Boolean isData {get; set; }
    public List<List<List<Object>>> lstTransactions {get; set; }
    public ApexPages.StandardSetController con {get; set; }
    public String esmNamespace{get;set;}
    public string xmlheader {get;set;}
    public string endfile{get;set;}

    
    public SearchDownloadControllerESM() {
        try {
        //--- Changed by Bhupinder mankotia | JIRA:ESMPROD-275 | 09-08-2016 | Starts ---//
        esmNamespace = UtilityController.esmNameSpace;
        //--- Changed by Bhupinder mankotia | JIRA:ESMPROD-275 | 09-08-2016 | Ends---//
         String invdatetime = datetime.now().format('yyMMddHHmmss');
             system.debug('invdatetime =>'+invdatetime);
         limitReached = false;
         Set<String> invalidField = new set<String>();
         invalidField.add(esmNamespace+'PDF__c');
           
            String selectedRowIDs ;
            if(ApexPages.currentPage().getParameters().get('key') != null)
                selectedRowIDs = ApexPages.currentPage().getParameters().get('key');
                system.debug('selectedRowIDs =>'+selectedRowIDs);
            String searchConfigName ;
            if(ApexPages.currentPage().getParameters().get('searchConfigName') != null)
                searchConfigName = ApexPages.currentPage().getParameters().get('searchConfigName');
system.debug('searchConfigName =>'+searchConfigName);
            SearchConfiguration__c objSearchConfig = [SELECT Id,ResultFieldSetName__c,ObjectAPIName__c FROM SearchConfiguration__c where Name=:searchConfigName];

           if(objSearchConfig != null){
            objectTmp = objSearchConfig.ObjectAPIName__c != null && objSearchConfig.ObjectAPIName__c != '' ? objSearchConfig.ObjectAPIName__c : '';
            searchResultFiledSetName = objSearchConfig.ResultFieldSetName__c != null && objSearchConfig.ResultFieldSetName__c != '' ? objSearchConfig.ResultFieldSetName__c : '';
            }
            contentType ='application/vnd.ms-excel#'+String.escapeSingleQuotes(objectTmp)+'_'+Userinfo.getFirstName()+'_'+invdatetime+'.xls';
            xmlheader ='<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?>';
            endfile = '</Workbook>';
                system.debug('objectTmp =>'+objectTmp);
            lstTransactions = new List<List<List<Object>>>();
        
             List<String> invStr = new List<String>();
                invStr = selectedRowIDs.split(',');
             
            
               Set<ID> selectedIDs = new Set<Id>();
             if(invStr != null){
                 for(String str : invStr ){
                 if(str != null && str != ''){
                     selectedIDs.add(str.substring(0,str.trim().length()));
                 }
                 }
             }
             
        
             
            fieldNames = new List<String>{};
            String str='';
            
                for(Schema.FieldSetMember f : this.getFieldsSetMembers(searchResultFiledSetName)){
                if(invalidField.contains(f.getFieldPath())){
                    continue;
                }
                //changed by Ronak | JIRA:ESMPROD-325 | 20-06-2016 | Starts
                 //changed by Ronak | JIRA:ESMPROD-372 | 23-08-2016 | Starts
                if(String.valueOf(f.getType()) == 'Reference' && f.getFieldPath().split('\\.').size() < 2){
                    if(f.getType() == Schema.DisplayType.REFERENCE  &&  f.getFieldPath()  != 'OwnerId' &&  f.getFieldPath()  != 'LastModifiedById' &&  f.getFieldPath()  != 'CreatedById') {
                        String refFieldName = f.getFieldPath().subString(0,f.getFieldPath().length()-1)+ 'r.Name';
                        str =str +   ',' + refFieldName  ;
                    }else if(f.getType() == Schema.DisplayType.REFERENCE  && (f.getFieldPath()  == 'OwnerId' ||  f.getFieldPath()  == 'LastModifiedById' ||  f.getFieldPath()  == 'CreatedById')){
                        String refFieldName = f.getFieldPath().subString(0,f.getFieldPath().length()-2)+ '.Name';
                        str =str +   ',' + refFieldName  ;
                    }
                }
                 //changed by Ronak | JIRA:ESMPROD-372 | 23-08-2016 | Ends
                if(f.getFieldPath()  == 'OwnerId' ||  f.getFieldPath()  == 'LastModifiedById' ||  f.getFieldPath()  == 'CreatedById'){
                    fieldNames.add(f.getLabel().subString(0,f.getLabel().length()-2));
                }else{
                    fieldNames.add(f.getLabel());
                }    
                str = str  + ',' + f.getFieldPath();

            }
           
            String queryStr = 'Select Id' + str+ ' from '+String.escapeSingleQuotes(objectTmp)+' Where Id IN :selectedIDs';
            queryStr = queryStr + ' order by CreatedDate DESC';
             system.debug('queryStr=>'+queryStr);
             
            con = new ApexPages.StandardSetController(Database.getQueryLocator(queryStr));
            con.setPageSize(900);
			 system.debug('con.getResultSize()=>'+con.getResultSize());
           Integer i=1;
           isData = true;
               if( con.getResultSize()>0 ) {

                while(isData) {
                    if(i >1) {
                        con.next();
                    }
                    List<SObject> nextTransactions = new List<SObject>();
                    nextTransactions = (List<SObject>)con.getRecords();
                    List<List<Object>> obj = new List<List<Object>>();
                    for(SObject nextTr : nextTransactions ) {

                        List<Object>  tempList = new List<Object>();

                        String noteTitle=''; String noteBody='';
                        String createdDate=''; String createdBy='';
                        for(Schema.FieldSetMember fieldObj : this.getFieldsSetMembers(searchResultFiledSetName))
                        {
                            //changed by Ronak | JIRA:ESMPROD-372 | 23-08-2016 | Starts
                            String field = fieldObj.getFieldPath();
                            String[] temp = field.split('\\.'); 
                            if(invalidField.contains(fieldObj.getFieldPath())){
                                continue;
                            }
                            if((temp.size() > 1 && nextTr.getSObject(temp[0]) != null && nextTr.getSObject(temp[0]).get(temp[1]) != null && nextTr.getSObject(temp[0]).get(temp[1]) != '') || (temp.size() < 2  && nextTr.get(field) != null)){

                            if(fieldObj.getType() ==Schema.DisplayType.DATE) {                       
                                    Date dtVar =  temp.size() > 1?((Date)(nextTr.getSObject(temp[0]).get(temp[1]))):(Date) nextTr.get(field);
                                  //vish  String strDtVar = CommonFunctionController.htmlencodeascii(String.valueOf(dtVar.format()));
                                    system.debug('strDtVar=>'+dtVar);
                                    tempList.add(dtVar);
                                
                            }
                            else if(fieldObj.getType() ==Schema.DisplayType.CURRENCY) {
                                String formattedAmt = '';
                                Decimal amnt = temp.size() > 1?((Decimal)(nextTr.getSObject(temp[0]).get(temp[1]))).SetScale(2):((Decimal)nextTr.get(field)).SetScale(2);
                                if(amnt !=null)
                                {
                                    formattedAmt =  String.valueOf(amnt);

                                }

                                tempList.add(formattedAmt);
                            }
                            else if(fieldObj.getType() ==Schema.DisplayType.DOUBLE) {
                                String formattedAmt = '';
                                Decimal amnt = temp.size() > 1?((Decimal)(nextTr.getSObject(temp[0]).get(temp[1]))).SetScale(2):((Decimal)nextTr.get(field)).SetScale(2);
                                if(amnt !=null)
                                {
                                    formattedAmt =  String.valueOf(amnt);

                                }
                                tempList.add(formattedAmt);
                            }
                           
                           //changed by Ronak | JIRA:ESMPROD-325 | 20-06-2016 | Starts
                           else if((fieldObj.getType() == Schema.DisplayType.REFERENCE || temp.size() > 1) && field != 'OwnerId' &&  field != 'LastModifiedById' &&  field != 'CreatedById') {
                               
                                if(temp.size() > 1){
                                    tempList.add(String.valueOf(((SObject)nextTr.getSobject(temp[0])).get(temp[1])).escapeHtml4());
                                }else{
                                    String refFieldName = field.subString(0,field.length()-1) + 'r';
                                    tempList.add(String.valueOf(((SObject)nextTr.getSobject(refFieldName)).get('name')).escapeHtml4());
                                }
                                   

                                
                                
                            }
                             else if(field == 'OwnerId' ||  field == 'LastModifiedById' ||  field == 'CreatedById') {
                                 String refFieldName = field.subString(0,field.length()-2);
                                if(nextTr.getSobject(refFieldName) != null && ((SObject)nextTr.getSobject(refFieldName)).get('name') != null) {
                                   tempList.add(String.valueOf(((SObject)nextTr.getSobject(refFieldName)).get('name')).escapeHtml4());

                                }else{
                                    tempList.add(' ');
                                }
                                //changed by Ronak | JIRA:ESMPROD-325 | 20-06-2016 | Ends
                            }
                        
                            else if((temp.size() > 1 && nextTr.getSObject(temp[0]) != null && nextTr.getSObject(temp[0]).get(temp[1]) != null && nextTr.getSObject(temp[0]).get(temp[1]) != '') || (temp.size() < 2  && nextTr.get(field) != null)){
                                string nrxttrns= temp.size() > 1?(String.valueOf((nextTr.getSObject(temp[0]).get(temp[1])))):String.valueof(nextTr.get(field));
                                tempList.add(nrxttrns.escapeHtml4());
                            }
                            }
                            else
                            {
                                tempList.add(' ');
                            }
                            //changed by Ronak | JIRA:ESMPROD-372 | 23-08-2016 | Ends
                            system.debug('fieldObj.getFieldPath=>'+fieldObj.getFieldPath());
                        }
                     

                        /*if(i==0) {
                            List<Object>  labelList = new List<Object>();
                            for(String nextLabel : fieldNames) {
                                      labelList.add(nextLabel);
                            }
                           
                            obj.add(labelList);
                        }

                        i=i+1;*/
                       obj.add(tempList);
                        Integer myDMLLimit = Limits.getLimitCpuTime();
                        Integer myDMLLimit1 = Limits.getCpuTime();
                        if(myDMLLimit1 > myDMLLimit - 1000){
                          Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, 'DML Error'));
                          limitReached = true;
                          contentType = '';
                          return;
                        }
                        
                    
                    }
                    lstTransactions.add(obj);
                    system.debug('fieldNames=>lstTransactions=>'+lstTransactions);
                    isData =con.getHasNext();
                }
            }
        }catch(Exception ex){
            system.debug('ex=>'+ex.getStackTraceString()+ex);
        }
    
    }
    public List<Schema.FieldSetMember> getFieldsSetMembers(String objFieldSetName) {
        System.Debug('--objectTmp--'+objectTmp);
        System.Debug('--objFieldSetName--'+objFieldSetName);
        Map<String,Schema.FieldSet> fieldsetMap = Schema.getGlobalDescribe().get(objectTmp).getDescribe().FieldSets.getMap();
        System.Debug('--fieldsetMap--'+fieldsetMap);
        List<String> fldSetName = String.valueOf(objFieldSetName).split(',');
        if( fldSetName != null && fldSetName.size() > 0 ){
            Schema.FieldSet fieldsetValue = fieldsetMap.get(fldSetName[0]);
            if(fieldsetValue != null){
                return fieldsetValue.getFields();
            }
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Fieldset for search criteria not defined.'));
        }
        return null;
    }
}