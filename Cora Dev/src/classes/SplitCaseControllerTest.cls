@isTest
public with sharing class SplitCaseControllerTest {
    @testSetup
    static void setupTestData() {
        CaseManager__c caseManager = new CaseManager__c();
        caseManager.WorkType__c = 'PO Invoice';
        caseManager.Status__c = 'Start';
        insert caseManager;
        SplitCaseController.getSplitCaseFields(caseManager,caseManager.id);
        TestDataGenerator.createContent(caseManager);
    }
    
    
    private static testmethod void testSplit() {
        CaseManager__c caseManager = [select Id, name, Process__c, WorkType__c, Status__c, RecordTypeId, RecordType.Name from CaseManager__c];
         Map<String, Object> responseObj=SplitCaseController.getAttachments(caseManager.id);

        List<SplitCaseController.SplitCase> splitList = SplitCaseController.CloneCase(caseManager, 3);

        For (SplitCaseController.SplitCase splitCase : splitList) {
            splitCase.SelectedAttachments=(List<ContentDocumentLink>) responseObj.get('attachments');
        }

        SplitCaseController.performSplitCase(caseManager, JSON.serialize(splitList), 'Test');
       
    }
}