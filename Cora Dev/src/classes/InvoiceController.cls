public with sharing class InvoiceController {
	@AuraEnabled
	public static Invoice__c getInvoice(string invId) {
		System.debug('invId:' + invId);
		string query = 'SELECT Id,Name,Invoice_No__c, Amount__c FROM Invoice__c WHERE Id =:invId Limit 1';

		Invoice__c invObj = Database.query(query);
		List<invoice_Line_Item__c> invl = [SELECT Id, Name FROM Invoice_Line_Item__c];
		system.debug('invObj:' + invObj);
		return invObj;
	}
	@AuraEnabled
	public static sObject saveInvoiceData(sObject invObj) {
		update invObj;
		return invObj;
	}
	public string validateInvoice() {
		return null;
	}
	public static Map<String, String> saveInvoice(sObject invObj, List<Invoice_Line_Item__c> invLineItemList, List<Purchase_Order__c> poList, List<Invoice_Line_Item__c> otherChargesList, String pageMode, String primaryDocJson, List<ContentVersion> NewAttachments, QualityCheck__c qcObject) {
		Map<String, String> mp = new Map<String, String> ();
		Savepoint sp = Database.setSavepoint();
		try {
			System.debug('invObj:' + invObj);
			System.debug('invLineItemList:' + invLineItemList);
			System.debug('poList:' + poList);
			System.debug('otherChargesList:' + otherChargesList);
			System.debug('pageMode:' + pageMode);
			System.debug('primaryDocJson:' + primaryDocJson);
			System.debug('NewAttachments:' + NewAttachments);
			System.debug('qcObject:' + qcObject);

			String returnMessage = '';
			string esmNamespace = UtilityController.esmNameSpace;
			Map<String, string> sysConfMap = new Map<String, string> ();

			Invoice_Configuration__c invConf = CommonFunctionController.getInvoiceConfiguration();
			String allPO = '';

			for (Purchase_Order__c po : poList) {
				allPO += po.get(invConf.PO_Number_Field_for_Search__c) + ',';
			}
			if (allPO != null && allPO != '')
			{
				allPO.removeEnd(',');
			}
			System.debug('allPO-----' + allPO);
			invObj.put('PO_Numbers__c', allPO);

			system.debug('UserInfo.getUserId()=====' + UserInfo.getUserId());
			String oldOwner = String.valueOf(UserInfo.getUserId());

			System.debug('oldOwner ------::' + oldOwner);

			//String oldCurrentApprover = (invObj.get(esmNamespace + 'Current_Approver__c') != null ? String.valueOf(invObj.get(esmNamespace + 'Current_Approver__c')) : '');

			if (!pageMode.equalsIgnoreCase('new')) {
				system.debug('OwnerId:: if' + oldOwner);
				if (invObj.get(esmNamespace + 'User_Action__c') != null && !String.ValueOf(invObj.get(esmNamespace + 'User_Action__c')).equalsIgnoreCase('Route')) {
					invObj.put('OwnerId', oldOwner);
					//invObj.put(esmNamespace + 'Current_Approver__c', oldCurrentApprover);
				}
			} else {
				invObj.put('Id', null);
				invObj.put('OwnerId', oldOwner);
			}
			if (invObj.get(esmNamespace + 'User_Action__c') != null && (String.ValueOf(invObj.get(esmNamespace + 'User_Action__c')).equalsIgnoreCase(Label.Document_Reclassify) || String.ValueOf(invObj.get(esmNamespace + 'User_Action__c')).equalsIgnoreCase(Label.Reset))) {
				invLineItemList.clear();
				otherChargesList.clear();
			}

			List<Invoice_Line_Item__c> invLineItemToDelete = new List<Invoice_Line_Item__c> ();
			for (System_Configuration__c sysConf :[SELECT Id, Name, Module__c, Sub_Module__c, Value__c FROM System_Configuration__c]) {
				sysConfMap.put(sysConf.Module__c + '|' + sysConf.Sub_Module__c, sysConf.Value__c);
			}


			List<User_Master__c> UserMasterList = new List<User_Master__c> ();
			if (User_Master__c.getSObjectType().getDescribe().isAccessible()
			    && Schema.sObjectType.User_Master__c.fields.User_Name__c.isAccessible()) {
				UserMasterList = [SELECT Id, Name FROM User_Master__c WHERE User_Name__c = :UserInfo.getUserName()];
			} else {
				throw new CustomCRUDFLSException(UtilityController.getAccessExceptionMessage(esmNamespace + 'User_Master__c'));
			}
			if (UserMasterList.size() > 0) {
				invObj.put(esmNamespace + 'Last_Modified_By__c', UserMasterList.get(0).Id);
			}
			//--- Attachment Required on Invoice ---//
			//--------------------------------------//
			Boolean isSupplierUser = false;
			//--- check is supplier ---//

			// Savepoint sp = Database.setSavepoint();
			if (invObj.get(esmNamespace + 'User_Action__c') != null && (String.valueOf(invObj.get(esmNamespace + 'User_Action__c')).equalsIgnoreCase(Label.Reset_Invoice) || String.valueOf(invObj.get(esmNamespace + 'User_Action__c')).equalsIgnoreCase(Label.Document_Reclassify))) {
				invLineItemList.clear();
				otherChargesList.clear();
				poList.clear();
			}
			datetime myDateTime = datetime.now();
			if (isSupplierUser) {
				invObj.put(esmNamespace + 'Input_Source__c', 'Manual - Supplier Portal');
			} else {
				invObj.put(esmNamespace + 'Input_Source__c', 'Manual');
			}
			//--- QC reject without any value in sampling component - check ---//

			//--- Setting custom Last Modified By ---//

			//isSupplierUser

			if (otherChargesList != null && otherChargesList.size() > 0 && sysConfMap.get('Invoice_Processing|Approval_Required_For_Additional_Charges').equalsIgnoreCase('true')) {
				invObj.put('Approval_Required_For_Additional_Charges__c', true);
			} else {
				invObj.put('Approval_Required_For_Additional_Charges__c', false);
			}

			//--- Get Invoice Line Item to delete ---//
			Set<String> invLineItemIdSet = new Set<String> ();

			//--- Configuration Missing for Rejection Complete ---//
			if (sysConfMap.get('Quantity_Calculation|Enable_Quantity_Calculation') != null && sysConfMap.get('Quantity_Calculation|Invoice_Reject_Current_State') == null) {
				returnMessage = 'Configuration Missing: Current State for rejection complete not configured.';
				ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, returnMessage));
			}
			/*if (invObj.User_Action__c == 'Validate') {
			  invObj.Exception_Reason__c = '';
			  } else {
			  }*/


			//--- Saving Invoice record ---//

			System.debug('invObj.Id:' + invObj);
			if (UtilityController.isFieldCreateable(esmNamespace + 'Invoice__c', null)
			    && UtilityController.isFieldUpdateable(esmNamespace + 'Invoice__c', null)) {
				EsmHelper.DataMetadataWrapper wppCc = MainPageController.callOnLoadCustomClasscallOnChangeManager(invObj, invLineItemList, new List<Purchase_Order__c> (), 'On Save Custom Class','Invoice__c');
				invObj = wppCc.currentObject;
				if (wppCc.displayError !=null)
				{
					mp.put('displayError',wppCc.displayError);
					return mp;
				}
				upsert invObj;
			}
			insertAttachentData(primaryDocJson, NewAttachments, invObj.Id);
			list<Invoice_PO_Detail__c> lstInvPoToInsert = new list<Invoice_PO_Detail__c> ();
			list<Invoice_PO_Detail__c> lstInvPoToDelete = new list<Invoice_PO_Detail__c> ();
			Invoice_PO_Detail__c invPoObj;
			Set<String> SetPoId = new Set<String> ();
			for (Purchase_Order__c po : poList) {
				SetPoId.add(String.ValueOf(po.get('Id')));
				invPoObj = new Invoice_PO_Detail__c();
				invPoObj.put(esmNamespace + 'Invoice__c', (Id) invObj.get('id')); //invPoObj.Invoice__c = (Id)childSobject.get('id');
				invPoObj.put(esmNamespace + 'Purchase_Order__c', po.id); //invPoObj.Purchase_Order__c = po.id;
				system.debug('owner id======' + oldOwner);
				if (oldOwner != '' && oldOwner != null) {
					invPoObj.put('OwnerId', oldOwner);
				}

				lstInvPoToInsert.add(invPoObj);
			}
			if (pageMode.equalsIgnoreCase('edit') && UtilityController.isFieldAccessible(esmNamespace + 'Invoice_PO_Detail__c', null)) {
				for (Invoice_PO_Detail__c invl :[SELECT Id, Purchase_Order__c FROM Invoice_PO_Detail__c WHERE Invoice__c = :invObj.Id and Invoice__c != null]) {
					if (!SetPoId.contains(invl.Purchase_Order__c)) {
						lstInvPoToDelete.add(invl);
					}
				}
			}
			if (UtilityController.isFieldCreateable(esmNamespace + 'Invoice_PO_Detail__c', null)
			    && UtilityController.isFieldUpdateable(esmNamespace + 'Invoice_PO_Detail__c', null)
			    && lstInvPoToInsert != null && lstInvPoToInsert.size() > 0) {
				upsert lstInvPoToInsert;
			}
			List<Invoice_Line_Item__c> invLineUpdate = new List<Invoice_Line_Item__c> ();
			for (Invoice_Line_Item__c invl : invLineItemList) {
				invLineItemIdSet.add(invl.Id);
				invl.Invoice__c = invObj.Id;
				invl.put('OwnerId', oldOwner);
				invLineUpdate.add(invl);
			}
			for (Invoice_Line_Item__c invl : otherChargesList) {
				invLineItemIdSet.add(invl.Id);
				invl.Is_Other_Charge__c = true;
				invl.Invoice__c = invObj.Id;
				if (oldOwner != null && oldOwner != '') {
					invl.put('OwnerId', oldOwner);
				}

				invLineUpdate.add(invl);
			}
			if (pageMode.equalsIgnoreCase('edit') && UtilityController.isFieldAccessible(esmNamespace + 'Invoice_Line_Item__c', 'Id,' + esmNamespace + 'PO_Line_Item__c,' + esmNamespace + 'GRN_Line_Item__c,' + esmNamespace + 'Quantity__c,' + esmNamespace + 'Old_Quantity__c')) {
				for (Invoice_Line_Item__c invl :[SELECT Id, PO_Line_Item__c, GRN_Line_Item__c, Quantity__c, Old_Quantity__c FROM Invoice_Line_Item__c WHERE Invoice__c = :invObj.Id]) {
					if (!invLineItemIdSet.contains(invl.Id)) {
						invLineItemToDelete.add(invl);
					}
				}
			}
			if (invLineUpdate != null && invLineUpdate.size() > 0 &&
			    UtilityController.isFieldCreateable(esmNamespace + 'Invoice_Line_Item__c', null)
			    && UtilityController.isFieldUpdateable(esmNamespace + 'Invoice_Line_Item__c', null)) {
				System.debug('invLineUpdate545--' + invLineUpdate);
				upsert invLineUpdate;
			}
			if (qcObject != null && UtilityController.isFieldCreateable(esmNamespace + 'QualityCheck__c', null)
			    && UtilityController.isFieldUpdateable(esmNamespace + 'QualityCheck__c', null)) {
				upsert qcObject;
			}
			//system.debug('invLineUpdate77--' + invLineUpdate);
			if (invLineItemToDelete != null && invLineItemToDelete.size() > 0 &&
			    UtilityController.isDeleteable(esmNamespace + 'Invoice_Line_Item__c', null)
			    && UtilityController.isDeleteable(esmNamespace + 'Invoice_Line_Item__c', null)) {
				System.debug('invLineItemToDelete---' + invLineItemToDelete);
				delete invLineItemToDelete;
			}
			if (lstInvPoToDelete != null && lstInvPoToDelete.size() > 0 &&
			    UtilityController.isDeleteable(esmNamespace + 'Invoice_PO_Detail__c', null)
			    && UtilityController.isDeleteable(esmNamespace + 'Invoice_PO_Detail__c', null)) {
				delete lstInvPoToDelete;
			}

		} catch(Exception ex) {
			mp.put('failure', ExceptionHandlerUtility.customDebug(ex, 'InvoiceController'));
			Database.rollback(sp);
			return mp;
		}
		if (pageMode.equalsIgnoreCase('new') )
		{
			Id idInsObj = invObj.Id;
			invObj = Database.Query('SELECT ' + CommonFunctionController.getAllFieldsOfObject('Invoice__c') + ' FROM  Invoice__c where id= :idInsObj');
		}
		mp.put('success', Label.Invoice_Save +String.valueOf(invObj.get('Name')));
		return mp;

	}

	public static void insertAttachentData(String primaryDocJson, List<ContentVersion> NewAttachments, String parentId) {
		System.debug('parentId' + parentId);

		System.debug('attachments777' + NewAttachments.size());
		System.debug('primaryDocJson777 ' + primaryDocJson);

		if (primaryDocJson != null && primaryDocJson != '')
		{
			Map<String, Boolean> content = (Map<String, Boolean>) JSON.deserialize(primaryDocJson, Map<String, Boolean>.class);
			system.debug('#####' + content);
			List<ContentVersion> cv = new List<ContentVersion> ();

			for (string s : content.keySet()) {
				System.debug('flag ' + content.get(s));

				cv.add(new ContentVersion(Id = s, Is_Primary_Doc__c = content.get(s)));
			}
			if (UtilityController.isFieldUpdateable('ContentVersion', null))
			{
				upsert cv;
			}
		}
		List<ContentDocumentLink> contentDocumentLinkList = new List<ContentDocumentLink> ();
		for (Integer i = 0; i < NewAttachments.size(); i++)
		{
			System.debug('attachments[i].ContentDocumentId' + NewAttachments[i].ContentDocumentId);
			contentDocumentLinkList.add(new ContentDocumentLink(
			                                                    ContentDocumentId = NewAttachments[i].ContentDocumentId,
			                                                    LinkedEntityId = parentId,
			                                                    ShareType = 'V',
			                                                    Visibility = 'AllUsers'));
		}
		System.debug('ContentDocumentLinks  before' + contentDocumentLinkList);

		if (UtilityController.isFieldUpdateable('ContentDocumentLink', null) && UtilityController.isFieldCreateable('ContentDocumentLink', null))
		{
			upsert contentDocumentLinkList;
		}

		System.debug('ContentDocumentLinks  after' + contentDocumentLinkList);

		//return getAttachments(parentId);
	}
	public static Map<String, String> saveData(sObject customObject, String pageMode, String primaryDocJson, List<ContentVersion> NewAttachments, QualityCheck__c qcObject,String objectName) {

		//System.debug('customObject -----'+customObject);		
		//System.debug('pageMode ------'+pageMode);
		//System.debug('primaryDocJsonAtt ------'+primaryDocJson);
		//System.debug('NewAttachmentsAtt ------'+NewAttachments);
		//System.debug('qcObject --------'+qcObject);

		Map<String, String> mp = new Map<String, string> ();
		Savepoint sp = Database.setSavepoint();
		try {
			upsert customObject;
		} catch(Exception ex) {
			Database.rollback(sp);
			mp.put('failure', ExceptionHandlerUtility.customDebug(ex, 'InvoiceController'));
			return mp;
		}
		if (pageMode.equalsIgnoreCase('new') )
		{
			Id idInsObj = customObject.Id;
			customObject = Database.Query('SELECT ' + CommonFunctionController.getAllFieldsOfObject(objectName) + ' FROM  '+objectName +' where id= :idInsObj');
		}
		mp.put('success', Label.Record_Save +String.valueOf(customObject.get('Name')));
		return mp;
	}
}