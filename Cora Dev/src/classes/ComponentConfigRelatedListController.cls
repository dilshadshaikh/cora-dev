public class ComponentConfigRelatedListController {

    public class LayoutInfo {
        public string ObjectName {
            get; Set;
        }
        public string LayoutType {
            get; Set;
        }
        public Boolean isActive {
            get; Set;
        }
        public string id {
            get; Set;
        }
        public PageLayout PageLayout { get; set; }

    }
    public class PageLayout {
        public string objectName {
            get; Set;
        }
        public string objectLabel {
            get; Set;
        }
        public string objectNameSpace {
            get; Set;
        }
        public List<LayoutSection> layoutDetailSections {
            get; Set;
        }
        public List<LayoutSection> layoutHeaderSections {
            get; Set;
        }

        public List<RelatedList> headerLevelComponents {
            get; Set;
        }

        public List<RelatedList> lineItemComponents {
            get; Set;
        }
    }

    public class LayoutSection {
        public string criteria {
            get; Set;
        }
        public string title {
            get; Set;
        }
        public Boolean readonly {
            get; Set;
        }
        public Boolean active {
            get; Set;
        }
        public List<column> columns {
            get; Set;
        }
    }
    public class column {

        public string name {
            get; Set;
        }
        public string label {
            get; Set;
        }
        public string defaultValue {
            get; Set;
        }
        public boolean readonly {
            get; Set;
        }
        public boolean required {
            get; Set;
        }
        public boolean currentUserSelected {
            get; Set;
        }
        public boolean excludeCurrentUser {
            get; Set;
        }
        /*public string fromfield {
          get; Set;
          }
          public string tofield {
          get; Set;
          }*/
    }

    public class RelatedList {
        public string objectName {
            get; Set;

        }
        public string title {
            get; Set;
        }
        public boolean active {
            get; Set;
        }
        public boolean readonly {
            get; Set;
        }
        public string criteria {
            get; Set;
        }
        public List<column> columns {
            get; Set;
        }

        public string relativeField {
            get; Set;
        }
        public String whereClause {
            get; Set;
        }
        public string groupBy {
            get; Set;
        }
        public String orderBy {
            get; Set;
        }
    }
    /*
      Authors: Chandresh Koyani
      Purpose: Wrapper class to store option like value,text.
      Dependencies: TATRuleController.cls,TATTriggerHelper.cls.
     */
    public class Option {
        public string value { get; set; }
        public string text { get; set; }

        public Option() {
        }
        public Option(string value, string text) {
            this.value = value;
            this.text = text;
        }
    }

    public class FieldInfo {
        public string label {
            get; set;
        }
        public string apiName {
            get; set;
        }
        public string type {
            get; set;
        }
    }

//-----------------------------------------------------Added by hitashi start

public class ComponentInfo {
        public string ObjectName {
            get; Set;
        }
        public string ComponentType {
            get; Set;
        }
        public Boolean isActive {
            get; Set;
        }
        public string id {
            get; Set;
        }
        public PageComponent PageComponent { get; set; }

    }
    public class PageComponent {
        public string catagory {
            get; Set;
        }
        public JsonConfiguration configuration {
            get; Set;
        }
    }

    public class JsonConfiguration {
        public Integer allowedSize {
            get; Set;
        }
        public String allowedExt {
            get; Set;
        }
        public Boolean allowAttachPrime {
            get; Set;
        }
        public String allowedExtForPrime {
            get; Set;
        }
    }


    public class CmpFieldInfo {
        public string label {
            get; set;
        }
        public string apiName {
            get; set;
        }
        public string type {
            get; set;
        }
    }

    @RemoteAction
    public static void saveComponent(string cmpname) {
        List<Option> options = new List<Option> ();
        System.debug('cmpname' + cmpname);
        String cmptype = cmpname;

        System.debug('cmptype' + cmptype);
        Component_Config__c cmpconfig = new Component_Config__c();
        cmpconfig.Component_Type__c = cmptype;
        
        PageComponent jsonField = new PageComponent();
        jsonField.catagory = cmptype;
        jsonField.configuration = new JsonConfiguration();
        jsonField.configuration.allowedSize = 0;
        jsonField.configuration.allowedExt = '';
        jsonField.configuration.allowAttachPrime = false;
        jsonField.configuration.allowedExtForPrime = '';
        System.debug('jsonField:' + jsonField);

        cmpconfig.Config_Json__c = JSON.serialize(jsonField);
        System.debug('cmpconfig:' + cmpconfig);
        insert cmpconfig;

    }

    @RemoteAction
    public static List<ComponentInfo> getAllComponents() {
        try {
            System.debug('---');
            List<ComponentInfo> cmpinfo = new List<ComponentInfo> ();

            List<Component_Config__c> cmpconfig = [SELECT Id, Name, Object_Name__c, Component_Type__c, IsActive__c, Title__c, Config_Json__c FROM Component_Config__c order by Object_Name__c];
            System.debug('cmpconfig' + cmpconfig);
            for (Component_Config__c cmpconfigObj : cmpconfig) {
                PageComponent jsonField = (PageComponent) System.JSON.deserialize(cmpconfigObj.Config_Json__c, PageComponent.class);
                ComponentInfo cmpObj = new ComponentInfo();
                cmpObj.PageComponent = jsonField;
                System.debug('cmpObj.PageComponent'+cmpObj.PageComponent);
                cmpObj.ObjectName = cmpconfigObj.Object_Name__c;
                cmpObj.ComponentType = cmpconfigObj.Component_Type__c;
                cmpObj.isActive = cmpconfigObj.IsActive__c;
                cmpObj.id = cmpconfigObj.id;
                cmpinfo.add(cmpObj);
            }
            System.debug('cmpinfo' + cmpinfo);
            return cmpinfo;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('TATRuleController', ex.getMessage(), ex.getStackTraceString());
            return null;
        }
    }

    @RemoteAction
    public static void removeComponent(string id) {
        List<Component_Config__c> cmpdel = [select id from Component_Config__c where id = :id];
        System.debug('-----cmpdel-----' + cmpdel);

        delete cmpdel;
    }
    @RemoteAction
    public static void changecmpState(string recid) {
        List<Component_Config__c> qcRule = [select id, IsActive__c from Component_Config__c where id = :recid];
        System.debug('-----qcRule is active-----' + qcRule);
        for (Component_Config__c cmpconfig : qcRule)
        {
        Boolean isact=cmpconfig.IsActive__c;

            if (isact == true)
            {
                cmpconfig.IsActive__c = false;              
            }
            else if (isact == false)
            {
                cmpconfig.IsActive__c = true;
            }

            System.debug('cmpconfig' + cmpconfig);
            update cmpconfig;
        }


    }


    //@RemoteAction
    //public static List<CmpFieldInfo> getCmptObjectFields(string cmpid) {


        //List<CmpFieldInfo> fieldNames = new List<CmpFieldInfo> ();

        //Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
        //for (Schema.SObjectField sfield : fieldMap.Values())
        //{
            //schema.describefieldresult dfield = sfield.getDescribe();

            //string type = String.valueOf(dfield.getType());
            //CmpFieldInfo fieldInfo = new CmpFieldInfo();
            //CmpFieldInfo.label = dfield.getLabel();
            //CmpFieldInfo.apiName = dfield.getName();
            //CmpFieldInfo.type = type;

            //fieldNames.add(CmpFieldInfo);

        //}

        //return fieldNames;
    //}

    /*
      @RemoteAction
    public static List<TATHelper.objectsAvailaible> getAllObjects() {
          List<TATHelper.objectsAvailaible> objects = new List<TATHelper.objectsAvailaible> ();
        Schema.DescribeFieldResult fieldResult = Component_Config__c.Object_Name__c.getDescribe();
        //System.debug('fieldResult :' + fieldResult);
        List<Schema.PicklistEntry> plvalues = fieldResult.getPicklistValues();
        TATHelper.objectsAvailaible obj =new TATHelper.objectsAvailaible();
        for (Schema.PicklistEntry f : plvalues)
        {
            obj =new TATHelper.objectsAvailaible();
            obj.label = f.getLabel();
            obj.apiName = f.getValue();
            objects.add(obj);

        }
        //System.debug('objects :' + objects);
        return objects;
    }
    */

/*  @RemoteAction
    public static List<SelectOption> getallObjectList(){
   List<SelectOption> options = new List<SelectOption>();
   Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();

    for(Schema.SObjectType thisObj : gd.values()) {
        options.add(new SelectOption(thisObj, thisObj));
    }

   return options;
}*/
//----------------------------------------------Added by Hitakshi end
    @RemoteAction
    public static List<Option> getObjects() {
        List<Option> options = new List<Option> ();

        Schema.DescribeFieldResult fieldResult = Layout_Config__c.Object_Name__c.getDescribe();
        List<Schema.PicklistEntry> pickListValues = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry valueEntry : pickListValues)
        {
            options.add(new Option(valueEntry.getValue(), valueEntry.getLabel()));
        }
        return options;
    }


    @RemoteAction
    public static List<LayoutInfo> getAllLayouts() {
        try {
            System.debug('---');
            List<LayoutInfo> qcRules = new List<LayoutInfo> ();

            List<Layout_Config__c> tatConfiges = [select id, Name, Object_Name__c, Page_Layout__c, Is_Active__c, Config_Json__c from Layout_Config__c order by Object_Name__c];

            for (Layout_Config__c tatConfigObj : tatConfiges) {
                PageLayout jsonField = (PageLayout) System.JSON.deserialize(tatConfigObj.Config_Json__c, PageLayout.class);
                LayoutInfo tatRuleObj = new LayoutInfo();
                tatRuleObj.PageLayout = jsonField;
                //tatRuleObj.ruleName = Name;
                tatRuleObj.ObjectName = tatConfigObj.Object_Name__c;
                tatRuleObj.LayoutType = tatConfigObj.Page_Layout__c;
                tatRuleObj.isActive = tatConfigObj.Is_Active__c;
                tatRuleObj.id = tatConfigObj.id;
                qcRules.add(tatRuleObj);
            }
            return qcRules;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('TATRuleController', ex.getMessage(), ex.getStackTraceString());
            return null;
        }
    }


    @RemoteAction
    public static List<ChildObjectDetail> getCurrentObjectChild(String objectName, String currentObject) {

        List<ChildObjectDetail> childObjectDetails = new List<ChildObjectDetail> ();

        Schema.DescribeSObjectResult describeSobjectResult = Schema.getGlobalDescribe().get(objectName).getDescribe();

        for (Schema.ChildRelationship childRelationshipObj : describeSobjectResult.getChildRelationships()) {
            for (Schema.SObjectField sfield : describeSobjectResult.fields.getMap().Values())
            {
                schema.describefieldresult dfield = sfield.getDescribe();
                System.debug('String.valueOf(dfield.getType() ' + String.valueOf(dfield.getType()) + ',' + dfield.getName());
                if (String.valueOf(dfield.getType()) == 'REFERENCE' && dfield.getName().equals(currentObject)) {
                    ChildObjectDetail objectDetails = new ChildObjectDetail();
                    objectDetails.objectName = dfield.getName();
                    objectDetails.label = dfield.getLabel();
                    childObjectDetails.add(objectDetails);
                }
            }
        }
        System.debug('childObjectDetails' + childObjectDetails);
        return childObjectDetails;
    }
    @RemoteAction
    public static string saveRule(LayoutInfo layout) {
        try {
            Layout_Config__c tatConfig = new Layout_Config__c();
            System.debug('layout.PageLayout' + layout.PageLayout);
            tatConfig.Config_Json__c = JSON.serialize(layout.PageLayout);
            tatConfig.Is_Active__c = layout.isActive;
            tatConfig.Object_Name__c = layout.ObjectName;
            tatConfig.Page_Layout__c = layout.LayoutType;
            if (!string.isEmpty(layout.id)) {
                tatConfig.id = layout.id;
            }
            System.debug('tatConfig' + tatConfig);
            upsert tatConfig;
            return 'SUCCESS';
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('TATRuleController', ex.getMessage(), ex.getStackTraceString());
            return '';
        }

    }


    @RemoteAction
    public static List<FieldInfo> getObjectFields(string objectName) {


        List<FieldInfo> fieldNames = new List<FieldInfo> ();

        Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
        for (Schema.SObjectField sfield : fieldMap.Values())
        {
            schema.describefieldresult dfield = sfield.getDescribe();

            string type = String.valueOf(dfield.getType());
            FieldInfo fieldInfo = new FieldInfo();
            fieldInfo.label = dfield.getLabel();
            fieldInfo.apiName = dfield.getName();
            fieldInfo.type = type;

            fieldNames.add(fieldInfo);

        }

        return fieldNames;
    }
    @RemoteAction
    public static void removeLayout(string id) {
        List<Layout_Config__c> qcRule = [select id from Layout_Config__c where id = :id];
        delete qcRule;
    }


    public class ChildObjectDetail {
        public string objectName {
            get; set;
        }
        public string label {
            get; set;
        }
        public List<FieldInfo> columns {
            get; set;
        }
    }
    @RemoteAction
    public static List<ChildObjectDetail> getChildObjectDetails(string objectName) {
        List<ChildObjectDetail> childObjectDetails = new List<ChildObjectDetail> ();

        Schema.DescribeSObjectResult describeSobjectResult = Schema.getGlobalDescribe().get(objectName).getDescribe();

        for (Schema.ChildRelationship childRelationshipObj : describeSobjectResult.getChildRelationships())
        {
            Schema.SObjectType sobjType = childRelationshipObj.getChildSObject();
            System.debug('sobjType' + sobjType);
            Schema.DescribeSObjectResult describResult = sobjType.getDescribe();
            System.debug('describResult' + describResult);
            if (describResult.isCustom()) {
                ChildObjectDetail objectDetails = new ChildObjectDetail();
                objectDetails.objectName = describResult.getName();
                objectDetails.label = describResult.getLabel();

                objectDetails.columns = new List<FieldInfo> ();

                for (Schema.SObjectField sfield : describResult.fields.getMap().Values())
                {
                    schema.describefieldresult dfield = sfield.getDescribe();

                    string type = String.valueOf(dfield.getType());
                    FieldInfo fieldInfo = new FieldInfo();
                    fieldInfo.label = dfield.getLabel();
                    fieldInfo.apiName = dfield.getName();
                    fieldInfo.type = type;

                    objectDetails.columns.add(fieldInfo);
                    System.debug('objectDetails' + objectDetails);
                }

                childObjectDetails.add(objectDetails);
            }
        }

        return childObjectDetails;
    }
}