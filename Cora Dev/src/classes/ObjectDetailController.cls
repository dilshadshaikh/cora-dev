public class ObjectDetailController {
	@AuraEnabled
	public static EsmHelper.dataHelperRecord getCaseManagerDataFromServer(String objName, List<String> jsonObj) {
	  System.debug('objName :'+objName);
		List<EsmHelper.Columns> cols = new List<EsmHelper.Columns> ();
		
		for (String s : jsonObj) {
			List<EsmHelper.Columns> tmp = UtilityController.getHelperColumnsForWrapper(s);
			for (EsmHelper.Columns t : tmp) {
				cols.add(t);
			}
		}
         System.debug('cols :'+cols);
		sObject currentObject = null;
		EsmHelper.dataHelperRecord wrap = UtilityController.getWrapperFromJson(currentObject, objName, cols);
        System.debug('wrap :'+wrap);
		return wrap;
	}
}