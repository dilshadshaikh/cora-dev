@IsTest public class CustomLoginControllerTest {
    @IsTest(SeeAllData = true) public static void testCustomLoginController() {
    	System_Configuration__c sc =new System_Configuration__c();
    	sc.Module__c ='Supplier_Login_Page';
    	sc.Sub_Module__c ='Supplier_Portal_Logo';
    	sc.Value__c ='ffdf';
        CustomLoginController c =new CustomLoginController();
        c.forwardToCustomAuthPage();
        c.login();
    }
}