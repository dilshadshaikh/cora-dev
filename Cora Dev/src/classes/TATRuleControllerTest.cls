/**
  * Authors     : Chandresh Koyani
  * Date created : 17/11/2017 
  * Purpose      : Test Class for TATRuleTest 
  * Dependencies : TATHelper.cls,TATCalculation.cls,TATRuleController.cls
  * -------------------------------------------------
  *Modifications:
                Date:   
                Purpose of modification:  
                Method/Code segment modified: 

*/
@isTest
private class TATRuleControllerTest { 
    @testSetup 
    static  void  setupTestData() {
        
        CreateRuleTat();

		System_Configuration__c appobj = new System_Configuration__c();
        //appobj.Name = 'SYSC-00000002';
        appobj.Module__c = 'Custom_Record_History';
		appobj.Sub_Module__c = 'Custom_Objects';
		appobj.Value__c = 'Invoice__c';
        insert appobj;

        Invoice__c caseObj=new Invoice__c();
        caseObj.Previous_State__c='Start';
		caseObj.Current_State__c='Start';
        caseObj.Next_State__c='Ready For Processing';
        caseObj.Document_Type__c='PO Invoice';
        caseObj.Amount__c=2000;
		caseObj.User_Action__c='Create';
        insert caseObj;

        Invoice__c caseObjNew=new Invoice__c();
        caseObjNew.Previous_State__c='Ready For Processing';
		caseObjNew.Current_State__c='Ready For Processing';
        caseObjNew.Next_State__c='On Hold';
        caseObjNew.Document_Type__c='PO Invoice';
        caseObjNew.Amount__c=2000;
		caseObjNew.User_Action__c='On Hold';
        insert caseObjNew;
    }
    @isTest
    public static void CreateRuleTat(){
        TATHelper.TATRule tatRuleObj=new TATHelper.TATRule();
        tatRuleObj.JSONValue=new TATHelper.JSONValue();
        List<BusinessHours> bhs=[select id from BusinessHours where IsDefault=true];

        tatRuleObj.JSONValue.criterias=new List<TATHelper.Criteria>();
        tatRuleObj.JSONValue.tatTime=100;
        tatRuleObj.JSONValue.calendar=bhs[0].id;
        tatRuleObj.JSONValue.tatTimeUnit='Minutes';
        tatRuleObj.JSONValue.notificationTime=20;
        tatRuleObj.JSONValue.notificationTimeUnit='Minutes';
        tatRuleObj.JSONValue.template='';
        tatRuleObj.JSONValue.additionalEmails='sonam.patel@sftpl.com';
        tatRuleObj.JSONValue.sendNotification=true;
		
        TATHelper.Criteria cre=null;
        cre=new TATHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Current_State__c');
        cre.Operator='equals';
        cre.Value='Start';
        cre.Type='PICKLIST';
        
        tatRuleObj.JSONValue.Criterias.add(cre);
        

        //cre=new TATHelper.Criteria();
        //cre.Field=ObjectUtil.getPackagedFieldName('Document_Type__c');
        //cre.Operator='contains';
        //cre.Value='PO Invoice';
        //cre.Type='PICKLIST';
        
        //tatRuleObj.JSONValue.Criterias.add(cre);

        //cre=new TATHelper.Criteria();
        //cre.Field=ObjectUtil.getPackagedFieldName('Amount__c');
        //cre.Operator='greater or equal';
        //cre.Value='10000';
        //cre.Type='NUMBER';
        
        //tatRuleObj.JSONValue.Criterias.add(cre);
        
        TAT_Rule_Config__c tatConfig = new TAT_Rule_Config__c();
        tatConfig.Rule_Name__c = 'Test';
        tatConfig.JSON__c = JSON.serialize(tatRuleObj.JSONValue);
        tatConfig.IsActive__c=true;
        tatConfig.Order__c = 1;
		tatConfig.Object_Name__c = 'Invoice__c';
        
        insert tatConfig;

		TATHelper.TATRule tatRuleObj1=new TATHelper.TATRule();
        tatRuleObj1.JSONValue=new TATHelper.JSONValue();

        tatRuleObj1.JSONValue.criterias=new List<TATHelper.Criteria>();
        tatRuleObj1.JSONValue.tatTime=100;
        tatRuleObj1.JSONValue.calendar=bhs[0].id;
        tatRuleObj1.JSONValue.tatTimeUnit='Minutes';
        tatRuleObj1.JSONValue.notificationTime=20;
        tatRuleObj1.JSONValue.notificationTimeUnit='Minutes';
        tatRuleObj1.JSONValue.template='';
        tatRuleObj1.JSONValue.additionalEmails='sonam.patel@sftpl.com';
        tatRuleObj1.JSONValue.sendNotification=true;
		
        cre=null;
        cre=new TATHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Amount__c');
        cre.Operator='greater or equal';
        cre.Value='1000';
        cre.Type='NUMBER';
        
        tatRuleObj1.JSONValue.Criterias.add(cre);

		TAT_Rule_Config__c tatConfig1 = new TAT_Rule_Config__c();
        tatConfig1.Rule_Name__c = 'Test';
        tatConfig1.JSON__c = JSON.serialize(tatRuleObj1.JSONValue);
        tatConfig1.IsActive__c=true;
        tatConfig1.Order__c = 1;
		tatConfig1.Object_Name__c = 'Invoice__c';
        
        insert tatConfig1;

		TATHelper.TATRule tatRuleObj2=new TATHelper.TATRule();
        tatRuleObj2.JSONValue=new TATHelper.JSONValue();

        tatRuleObj2.JSONValue.criterias=new List<TATHelper.Criteria>();
        tatRuleObj2.JSONValue.tatTime=100;
        tatRuleObj2.JSONValue.calendar=bhs[0].id;
        tatRuleObj2.JSONValue.tatTimeUnit='Minutes';
        tatRuleObj2.JSONValue.notificationTime=20;
        tatRuleObj2.JSONValue.notificationTimeUnit='Minutes';
        tatRuleObj2.JSONValue.template='';
        tatRuleObj2.JSONValue.additionalEmails='sonam.patel@sftpl.com';
        tatRuleObj2.JSONValue.sendNotification=true;
		
        cre=null;
        cre=new TATHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Pending_GRN__c');
        cre.Operator='equals';
        cre.Value='true';
        cre.Type='BOOLEAN';
        
        tatRuleObj2.JSONValue.Criterias.add(cre);

		TAT_Rule_Config__c tatConfig2 = new TAT_Rule_Config__c();
        tatConfig2.Rule_Name__c = 'Test';
        tatConfig2.JSON__c = JSON.serialize(tatRuleObj2.JSONValue);
        tatConfig2.IsActive__c=true;
        tatConfig2.Order__c = 1;
		tatConfig2.Object_Name__c = 'Invoice__c';
        
        insert tatConfig2;
    }
    @isTest
    public static void CreateRuleQCPerecentage(){
        TATHelper.TATRule tatRuleObj=new TATHelper.TATRule();
        tatRuleObj.JSONValue=new TATHelper.JSONValue();
        List<BusinessHours> bhs=[select id from BusinessHours where IsDefault=true];

        tatRuleObj.JSONValue.criterias=new List<TATHelper.Criteria>();
        tatRuleObj.JSONValue.tatTime=100;
        tatRuleObj.JSONValue.calendar=bhs[0].id;
        tatRuleObj.JSONValue.tatTimeUnit='Minutes';
        tatRuleObj.JSONValue.notificationTime=20;
        tatRuleObj.JSONValue.notificationTimeUnit='Minutes';
        tatRuleObj.JSONValue.template='';
        tatRuleObj.JSONValue.additionalEmails='sonam.patel@sftpl.com';
        tatRuleObj.JSONValue.sendNotification=true;

        TATHelper.Criteria cre=null;
        cre=new TATHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Process__c');
        cre.Operator='contains';
        cre.Value='AP';
        cre.Type='PICKLIST';
        
        tatRuleObj.JSONValue.Criterias.add(cre);
        

        cre=new TATHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Worktype__c');
        cre.Operator='contains';
        cre.Value='PO Invoices';
        cre.Type='PICKLIST';
        
        tatRuleObj.JSONValue.Criterias.add(cre);

        cre=new TATHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Amount__c');
        cre.Operator='greater or equal';
        cre.Value='1000';
        cre.Type='NUMBER';
        
        tatRuleObj.JSONValue.Criterias.add(cre);

        cre=new TATHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Escalation__c');
        cre.Operator='equals';
        cre.Value='True';
        cre.Type='BOOLEAN';
        
        tatRuleObj.JSONValue.Criterias.add(cre);
        
        TAT_Rule_Config__c tatConfig = new TAT_Rule_Config__c();
        tatConfig.Rule_Name__c = 'Test';
        tatConfig.JSON__c = JSON.serialize(tatRuleObj.JSONValue);
        tatConfig.IsActive__c=true;
        tatConfig.Order__c = 1;
		tatConfig.Object_Name__c = 'Invoice__c';
        
        insert tatConfig;
    }
    public static testMethod void testTATCalculation(){
        List<Invoice__c> caseObjList=[select id,Name,Previous_State__c,Current_State__c,Next_State__c,Document_Type__c,Amount__c,Current_Approver__c,Target_TAT_Time__c,Tat_Rule__c,OwnerId from Invoice__c];
        TATCalculation.calculateStateChange(caseObjList, 'Invoice__c');

        Map<Id,Invoice__c> caseTrackerMap=new Map<Id,Invoice__c>();

        for(Invoice__c caseObj : caseObjList){
            
            Invoice__c caseObjNew=new Invoice__c();
            caseObjNew.Current_State__c='Start';
            caseObjNew.Next_State__c='Ready For Processing';
            caseObjNew.Document_Type__c='PO Invoice';
            caseObjNew.Amount__c=2000;
            caseObjNew.Id=caseObj.id;
            
            caseTrackerMap.put(caseObj.id,caseObjNew);
        }

        //TATCalculation.calculateStateChange(caseObjList,caseTrackerMap);

        TATCalculation.setActualTatTime(caseObjList, 'Invoice__c',caseObjList);
		TATCalculation.isSameMonth(Date.today(), Date.today());
		TATCalculation.isSameWeek(Date.today(), Date.today());
    }
    public static testMethod void testIsMatchMethod(){
        
        TATHelper.Criteria cre=new TATHelper.Criteria();
        cre.Field=ObjectUtil.getPackagedFieldName('Current_State__c');
        cre.Operator='equals';
        cre.Value='True';
        cre.Type='BOOLEAN';

        //Boolean check
        
        TATCalculation.IsMatch(cre,true);
        cre.Operator='not equal to';
        TATCalculation.IsMatch(cre,true);

        //Integer Check
        cre.Value='5';

        cre.Operator='not equal to';
        TATCalculation.IsMatch(cre,1);

        cre.Operator='equals';
        TATCalculation.IsMatch(cre,1);

        cre.Operator='less than';
        TATCalculation.IsMatch(cre,2);

        cre.Operator='greater than';
        TATCalculation.IsMatch(cre,2);

        cre.Operator='less or equal';
        TATCalculation.IsMatch(cre,2);

        cre.Operator='greater or equal';
        TATCalculation.IsMatch(cre,2);

        //String Check

        cre.Value='TEst';

        cre.Operator='not equal to';
        TATCalculation.IsMatch(cre,'Test');

        cre.Operator='equals';
        TATCalculation.IsMatch(cre,'Test');

        cre.Operator='starts with';
        TATCalculation.IsMatch(cre,'Test');

        cre.Operator='end with';
        TATCalculation.IsMatch(cre,'Test');

        cre.Operator='contains';
        TATCalculation.IsMatch(cre,'Test');

        cre.Operator='does not contain';
        TATCalculation.IsMatch(cre,'Test');

        cre.Operator='contains in list';
        TATCalculation.IsMatch(cre,'Test');

        List<BusinessHours> bhs=[select id from BusinessHours where IsDefault=true];
        TATCalculation.getDateDiffByBusinessHours(bhs[0].id,Datetime.Now(),Datetime.Now());
    }
    public static testMethod void testQCRuleController(){
        TATRuleController ruleController=new TATRuleController();
		TATRuleController.getAllObjects();
        TATRuleController.getAllRules('Invoice__c');
        List<TATHelper.FieldInfo> fieldList= TATRuleController.getObjectFields('Invoice__c');

        TAT_Rule_Config__c tatRule = [select id,Name,JSON__c,Object_Name__c from TAT_Rule_Config__c limit 1];
        
        TATRuleController.ChangeState(tatRule.id,true);
        TATRuleController.ChangeOrder(1,'UP');
        TATRuleController.ChangeOrder(4,'Down');
        
        TATRuleController.ChangeOrder(3,'UP');
        

        TATHelper.TATRule tatRuleObj=new TATHelper.TATRule();
        tatRuleObj.Id=tatRule.Id;
        tatRuleObj.RuleName='Test1';
        tatRuleObj.Order=1;
        tatRuleObj.IsActive=true;
		tatRuleObj.objectName ='Invoice__c';
        tatRuleObj.JSONValue=(TATHelper.JSONValue) System.JSON.deserialize(tatRule.JSON__c, TATHelper.JSONValue.class);

        TATRuleController.SaveRule(tatRuleObj,'Invoice__c');

        tatRuleObj.Id='';

        TATRuleController.SaveRule(tatRuleObj,'Invoice__c');

        TATRuleController.RemoveRule(tatRule.id);

        TATRuleController.getBusinessHours();

        TATRuleController.getAllEmailTemplate();
    }
}