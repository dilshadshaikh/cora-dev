@isTest
public class RejectionNotficationHelperTest{
    public static testMethod void testFirst(){
     CaseManager__c cm = new CaseManager__c();
     cm.Current_State__c = 'Rejected';  
     insert cm;
        
     CaseManager__c cm1 = new CaseManager__c();
     cm1.Current_State__c = 'Start';  
     insert cm1;
        
     System.debug('cm :' + cm);
     list<CaseManager__c> ListCm = new list<CaseManager__c>();
     ListCm.add(cm);  
     RejectionNotficationHelper.sendCaseRejectionNotification(ListCm);
     Map<Id, CaseManager__c> caseTrackerOldMap = new Map<Id, CaseManager__c>();
     caseTrackerOldMap.put(cm.id,cm1);
     RejectionNotficationHelper.sendCaseRejectionNotification(ListCm,caseTrackerOldMap);
    }
}