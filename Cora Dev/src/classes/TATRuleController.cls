public with sharing class TATRuleController {
    @RemoteAction
    public static List<TATHelper.objectsAvailaible> getAllObjects() {
          List<TATHelper.objectsAvailaible> objects = new List<TATHelper.objectsAvailaible> ();
        Schema.DescribeFieldResult fieldResult = TAT_Rule_Config__c.Object_Name__c.getDescribe();
        //System.debug('fieldResult :' + fieldResult);
        List<Schema.PicklistEntry> plvalues = fieldResult.getPicklistValues();
		TATHelper.objectsAvailaible obj =new TATHelper.objectsAvailaible();
        for (Schema.PicklistEntry f : plvalues)
        {
            obj =new TATHelper.objectsAvailaible();
			obj.label = f.getLabel();
			obj.apiName = f.getValue();
			objects.add(obj);

        }
        //System.debug('objects :' + objects);
        return objects;
    }
    @RemoteAction
    public static List<TATHelper.FieldInfo> getObjectFields(String objName) {
        try {
            System.debug('objName:'+objName);
            List<User> allActiveUser = [select id, Name from User where isActive=true limit 999];

            List<TATHelper.FieldInfo> fieldNames = new List<TATHelper.FieldInfo> ();

            Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap();
            System.debug('fieldMap :'+fieldMap );
            for (Schema.SObjectField sfield : fieldMap.Values())
            {
                schema.describefieldresult dfield = sfield.getDescribe();
				System.debug('dfield :'+dfield );
                string type = String.valueOf(dfield.getType());
				System.debug('type :'+type );
                if (dField.isCustom() && !dField.isCalculated() && !type.equalsIgnoreCase('REFERENCE')) {

                    TATHelper.FieldInfo fieldInfo = new TATHelper.FieldInfo();
                    fieldInfo.label = dfield.getLabel();
                    fieldInfo.apiName = dfield.getName();
                    fieldInfo.pickListValue = new list<TATHelper.option> ();
                    fieldInfo.type = type;
                    fieldInfo.operator = EmailToCaseSettingHelper.operatorMap.get(fieldInfo.type);


                    if (fieldInfo.type == 'PICKLIST') {
                        List<Schema.PicklistEntry> picklist = dfield.getPicklistValues();
                        for (Schema.PicklistEntry pickListVal : picklist) {
                            fieldInfo.pickListValue.add(new TATHelper.option(pickListVal.getValue(), pickListVal.getLabel()));
                        }
                    }
                    else if (fieldInfo.type == 'BOOLEAN') {
                        fieldInfo.pickListValue.add(new TATHelper.option('True', 'True'));
                        fieldInfo.pickListValue.add(new TATHelper.option('False', 'False'));
                    }

                    fieldNames.add(fieldInfo);

                }
            }
            return fieldNames;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('TATRuleController', ex.getMessage(), ex.getStackTraceString());
            return null;
        }
    }
    /*
      Purpose: Get all business hours.
      Dependencies: Called from "TATRule.Page"
     */
    @RemoteAction
    public static List<TATHelper.Option> getBusinessHours() {
        try {
            System.debug('Business Hours');
            List<TATHelper.Option> businessHrsList = new List<TATHelper.Option> ();
            List<businesshours> businessHrs = [select id, Name from businesshours order by IsDefault limit 999];

            for (businesshours bh : businessHrs) {
                businessHrsList.add(new TATHelper.Option(bh.id, bh.Name));
            }
            return businessHrsList;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('TATRuleController', ex.getMessage(), ex.getStackTraceString());
            return null;
        }
    }

    /*
      Purpose: Geat all templates to send notfication.
      Dependencies: Called from "TATRule.Page"
     */
    @RemoteAction
    public static List<EmailTemplate> getAllEmailTemplate() {
        List<EmailTemplate> emailTemplates = [Select Id, Name from EmailTemplate where IsActive = true AND Folder.Name=:ESMConstants.TAT_NOTIFICATION_TEMPLATE_FOLDER order by Name];
        return emailTemplates;
    }

    /*
      Purpose: Save TAT Rule entry if id is provided, it will create new rule if id is not provided.
      Dependencies: Called from "TATRule.Page"
     */
    @RemoteAction
    public static string saveRule(TATHelper.TATRule tatRuleObj,String objName) {
        try {
            System.debug('ObjName saveRule:'+ObjName);
            System.debug('tatRuleObj :'+tatRuleObj);
            Tat_Rule_Config__c tatConfig = new Tat_Rule_Config__c();
            tatConfig.Rule_Name__c = tatRuleObj.ruleName;
            tatconfig.Object_Name__c= objName ;
            tatConfig.JSON__c = JSON.serialize(tatRuleObj.JSONValue);
            tatConfig.IsActive__c = tatRuleObj.isActive;
            if (!string.isEmpty(tatRuleObj.id)) {
                tatConfig.id = tatRuleObj.id;
            }
            else {
                AggregateResult[] groupedResults = [select MAX(Order__c) from Tat_Rule_Config__c];
                integer nextOrder = 1;
                if (groupedResults != null && groupedResults.size() > 0) {
                    integer maxOrder = integer.valueOf(groupedResults[0].get('expr0'));
                    if (maxOrder != null) {
                        nextOrder = maxOrder + 1;
                    }
                }
                tatConfig.Order__c = nextOrder;
            }

            upsert tatConfig;
            return 'SUCCESS';
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('TATRuleController', ex.getMessage(), ex.getStackTraceString());
            return '';
        }

    }

    /*
      Purpose: Get all TAT Rule.
      Dependencies: Called from "TATRule.Page"
     */
    @RemoteAction
    public static List<TATHelper.TATRule> getAllRules(String objectName) {
        try {
            List<TATHelper.TATRule> qcRules = new List<TATHelper.TATRule> ();

            List<Tat_Rule_Config__c> tatConfiges = [select id,Object_Name__c, Rule_Name__c, JSON__c, Order__c, IsActive__c from Tat_Rule_Config__c where Object_Name__c=:objectName order by Order__c limit 999];

            for (Tat_Rule_Config__c tatConfigObj : tatConfiges) {
                TATHelper.JSONValue jsonField = (TATHelper.JSONValue) System.JSON.deserialize(tatConfigObj.JSON__c, TATHelper.JSONValue.class);
                TATHelper.TATRule tatRuleObj = new TATHelper.TATRule();
                tatRuleObj.JSONValue = jsonField;
                tatRuleObj.ruleName = tatConfigObj.Rule_Name__c;
                tatRuleObj.objectName = tatConfigObj.Object_Name__c;
                tatRuleObj.order = Integer.valueOf(tatConfigObj.Order__c);
                tatRuleObj.id = tatConfigObj.id;
                tatRuleObj.isActive = tatConfigObj.IsActive__c;
                qcRules.add(tatRuleObj);
            }
            return qcRules;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('TATRuleController', ex.getMessage(), ex.getStackTraceString());
            return null;
        }

    }

    /*
      Purpose: Remove TAT Rule Config entry based on id provided..
      Dependencies: Called from "TATRule.Page"
     */
    @RemoteAction
    public static void removeRule(string id) {
        try {
            List<Tat_Rule_Config__c> qcRule = [select id from Tat_Rule_Config__c where id = :id];
            delete qcRule;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('TATRuleController', ex.getMessage(), ex.getStackTraceString());
        }
    }

    /*
      Purpose: Change state of a rule(Active/Inactive).
      Dependencies: Called from "TATRule.Page"
     */
    @RemoteAction
    public static void changeState(string id, boolean isActive) {
        try {
            Tat_Rule_Config__c rule = new Tat_Rule_Config__c();
            rule.id = id;
            rule.IsActive__c = isActive;
            update rule;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('TATRuleController', ex.getMessage(), ex.getStackTraceString());
        }
    }

    /*
      Purpose: Change order of a rule.
      Dependencies: Called from "TATRule.Page"
     */
    @RemoteAction
    public static void changeOrder(integer currentOrder, string orderType) {
        try {
            integer previousNextCount = 0;
            if (orderType == 'UP') {
                previousNextCount = currentOrder - 1;
            }
            else {
                previousNextCount = currentOrder + 1;
            }
            List<Tat_Rule_Config__c> qcRules = [select id, Order__c from Tat_Rule_Config__c where(Order__c = :currentOrder OR Order__c = :previousNextCount)];
            for (Tat_Rule_Config__c rule : qcRules) {
                if (rule.Order__c == currentOrder) {
                    rule.Order__c = previousNextCount;
                }
                else {
                    rule.Order__c = currentOrder;
                }
            }
            update qcRules;
        }
        catch(Exception ex) {
            ExceptionHandlerUtility.writeException('TATRuleController', ex.getMessage(), ex.getStackTraceString());
        }

    }
}