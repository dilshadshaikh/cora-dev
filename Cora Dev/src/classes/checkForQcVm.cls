public class checkForQcVm {
	@InvocableMethod(label = 'Check For QC VM' description = 'Check for QC based on QC Rules definied')
	public static void checkForQC(List<Vendor_Maintenance__c> vm) {
		QCCalculation.checkForQC(vm, 'Vendor_Maintenance__c');
	}
}