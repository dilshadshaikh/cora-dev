public class MainPageController {

	@AuraEnabled
	public static Map<String, Object> getUserInfo() {
		Map<String, Object> userDetails = new Map<String, Object> ();
		try {
			userDetails.put('UserName', userinfo.getName());
			userDetails.put('InstanceName', [SELECT Id, InstanceName FROM Organization].InstanceName);
		} catch(Exception ex) {
			System.debug(ex.getStackTraceString());
		}
		return userDetails;
	}

	@AuraEnabled
	public static EsmHelper.DataMetadataWrapper getSObjectDetails(String layout, String recId, String sObjectName, sObject dataModal) {
		EsmHelper.DataMetadataWrapper wrapper = new EsmHelper.DataMetadataWrapper();
		wrapper.isAccesible = true;
		/*try 
		  {*/

		//Check supplier profile user
		System_Configuration__c sysConf = [SELECT Value__c FROM System_Configuration__c where Module__c = 'Profile' and Sub_Module__c = 'Supplier_Profile_ID'];
		Boolean isSupplierUser = false;
		List<Supplier_Profile__c> suppList = new List<Supplier_Profile__c> ();
		if (sysConf.Value__c != null && sysConf.Value__c != '' && UserInfo.getProfileId().contains(sysConf.Value__c)) {
			isSupplierUser = true;
		}
		JSONGenerator finalJson = JSON.createGenerator(true);
		finalJson.writeStartObject();
		//try {
		List<Layout_Configuration__c> lst = new List<Layout_Configuration__c> ();
		if (isSupplierUser) {
			lst = [SELECT Config_Json__c FROM Layout_Configuration__c where Is_Active__c = true and Object_Name__c = :sObjectName and System__c = 'Supplier Portal' and Page_Layout__c = :layout];
		}
		else
		{
			lst = [SELECT Config_Json__c FROM Layout_Configuration__c where Is_Active__c = true and Object_Name__c = :sObjectName and System__c = 'Processor Portal' and Page_Layout__c = :layout];
		}

		finalJson.writeObjectField('layoutJson', lst);
		if (layout == 'EDIT' || layout == 'VIEW')
		{
			dataModal = Database.query(CommonFunctionController.getCreatableFieldsSOQL(sObjectName, 'Id = :recId', '', true));
			if (layout == 'EDIT') {
				try
				{
					dataModal.put('OwnerId', UserInfo.getUserId());
					upsert dataModal;
					dataModal = Database.query(CommonFunctionController.getCreatableFieldsSOQL(sObjectName, 'Id = :recId', '', true));
					System.debug('dataModal --------' + dataModal);
					List<Invoice_Line_Item__c> invList = new List<Invoice_Line_Item__c> ();
					if (sObjectName != null && sObjectName.equalsIgnoreCase('Invoice__c'))
					{
						invList.addAll(CommonFunctionController.fillOtherChargesList(UtilityController.esmNameSpace, dataModal.ID));
						invList.addAll(CommonFunctionController.fillInvLineItem(UtilityController.esmNameSpace, dataModal.ID));
						for (Invoice_Line_Item__c invl : invList) {
							invl.put('OwnerId', UserInfo.getUserId());
						}
						upsert invList;

						List<sobject> InvoicePODetail = CommonFunctionController.getInvoicePODetail(UtilityController.esmNameSpace, dataModal.ID);
						for (sobject invPO : InvoicePODetail) {
							invPO.put('OwnerId', UserInfo.getUserId());
						}
						upsert InvoicePODetail;
					}

					EsmHelper.DataMetadataWrapper wppCc = callOnLoadCustomClasscallOnChangeManager(dataModal, invList, new List<Purchase_Order__c> (), 'On Load Custom Class','Invoice__c');
					dataModal = wppCc.currentObject;
					System.debug('dataModal-----' + dataModal);
					wrapper.isAccesible = true;
				}
				catch(Exception e)
				{
					wrapper.isAccesible = false;
				}
			}
		} else {

			EsmHelper.DataMetadataWrapper wppCc = callOnLoadCustomClasscallOnChangeManager(dataModal, new List<Invoice_Line_Item__c> (), new List<Purchase_Order__c> (), 'On Load Custom Class',sObjectName);
			dataModal = wppCc.currentObject;
		}
		System.debug('sObjectName545--' + sObjectName);
		if (sObjectName != null)
		{
			finalJson.writeObjectField('sObjectName', sObjectName);
			finalJson.writeObjectField('sObjectMetadata', UtilityController.getObjectMetadata(sObjectName));
			finalJson.writeObjectField('sObjectFieldMetadata', UtilityController.getFieldsMetadata(sObjectName));
		}

		wrapper.currentObject = dataModal;
		wrapper.layoutJson = finalJson.getAsString();
		wrapper.esmNameSpace = UtilityController.esmNameSpace;
		System.debug('wrapper ----' + wrapper);
		/*}
	   catch (Exception e)
	   {
	   ExceptionHandlerUtility.customDebug(e,'ddd');
	   }*/
	return wrapper;
}
@AuraEnabled
public static EsmHelper.DataMetadataWrapper callOnChangeManager(String className, sObject childSobject, List<Invoice_Line_Item__c> invLineItemList, List<Purchase_Order__c> puchaseOrder, String field, String value) {
	return callOnChangeManager(className, childSobject, invLineItemList, puchaseOrder, field, value, 'Invoice__c');
}
@AuraEnabled
public static EsmHelper.DataMetadataWrapper callOnChangeManager(String className, sObject childSobject, List<Invoice_Line_Item__c> invLineItemList, List<Purchase_Order__c> puchaseOrder, String field, String value, String objName) {
	try {
		OnChangeConfig obj;
		try {
			if (className != null && !String.valueOf(className).equals('')) {
				obj = (OnChangeConfig) Type.forName(className).newInstance();
			}
		} catch(Exception e) {
			EsmHelper.DataMetadataWrapper wrapper = new EsmHelper.DataMetadataWrapper();
			wrapper.currentObject = childSobject;
			wrapper.error = 'Exception while getting class to run onchnage logic : ' + e.getMessage() + ':' + e.getStackTraceString();
			return wrapper;
		}
		if (obj != null) {
			String childNameSpace = UtilityController.esmNameSpace;
			Set<String> poId = new Set<String> ();
			Map<object, object> extraParamMap = new Map<object, object> ();
			List<PO_Line_Item__c> poLineItemList = new List<PO_Line_Item__c> ();
			List<GRN_Line_Item__c> grnLineItemList = new List<GRN_Line_Item__c> ();
			if (objName != null && objName.equals('Invoice__c'))
			{
				for (Purchase_Order__c po : puchaseOrder) {
					poId.add(po.id);
				}
				poLineItemList = CommonFunctionController.getPOline(poId);
				extraParamMap.put('POSet', poId);
				//extraParamMap.put('INVCONF', invConf);
				extraParamMap.put('lstPO', puchaseOrder);
				//grnLineItemList = CommonFunctionController.getGRNline(poId);
			}
			obj.execute(childSobject, invLineItemList, poLineItemList, grnLineItemList, field, value, childNameSpace, extraParamMap);
			EsmHelper.DataMetadataWrapper wrapper = new EsmHelper.DataMetadataWrapper();
			if (extraParamMap!=null && extraParamMap.get('alertmessage')!=null)
			{
				wrapper.displayError = String.valueOf(extraParamMap.get('alertmessage'));
			}
			wrapper.currentObject = childSobject;
			return wrapper;
		}
		return null;
	} catch(Exception ex) {
		ExceptionHandlerUtility.customDebug(ex, 'callOnChangeManager');
		EsmHelper.DataMetadataWrapper wrapper = new EsmHelper.DataMetadataWrapper();
		wrapper.error = ExceptionHandlerUtility.customDebug(ex, 'callOnChangeManager');
		wrapper.currentObject = childSobject;
		return wrapper;
	}
}

//--- Validate Invoice data ---//
@AuraEnabled
public static EsmHelper.ExceptionValidationWrapper validateInvoiceData(sObject invoiceObject, List<Invoice_Line_Item__c> invLineItemList, List<sObject> otherChargesList, List<Purchase_Order__c> poList, String childNameSpace) {
	try {
		String jsonValidationExceptionMsg = '';
		invoiceObject.put(childNameSpace + 'Exception_Reason__c', '');

		System_Configuration__c sysConf = [SELECT Value__c FROM System_Configuration__c where Module__c = 'Invoice_Processing' and Sub_Module__c = 'Approval_Required_For_Additional_Charges'];

		if (otherChargesList != null && otherChargesList.size() > 0 && sysConf.Value__c != null && sysConf.Value__c != '' && sysConf.Value__c.equalsIgnoreCase('true')) {
			invoiceObject.put(childNameSpace + 'Approval_Required_For_Additional_Charges__c', true);
		}
		else {
			invoiceObject.put(childNameSpace + 'Approval_Required_For_Additional_Charges__c', false);
		}

		EsmHelper.ExceptionValidationWrapper wrapper = new EsmHelper.ExceptionValidationWrapper();
		Map<String, String> validMap = new Map<String, String> ();
		validMap = InvoiceValidationExceptionController.executeValidationException(invoiceObject, invLineItemList, otherChargesList, poList);
		if (validMap != null && validMap.get('sytemExceptionOccur') == null) {
			String tempExceptionReason = validMap.get('tempExceptionReason');
			invoiceObject.put(childNameSpace + 'Exception_Reason__c', tempExceptionReason);
			wrapper.currentObject = invoiceObject;
			wrapper.invLines = invLineItemList;
			wrapper.validationMap = validMap;
			return wrapper;
		} else {
			wrapper.error = validMap.get('sytemExceptionOccur');
			return wrapper;
		}
	}
	catch(Exception ex) {
		ExceptionHandlerUtility.customDebug(ex, 'MainPageController');
	}
	return null;
}

//--- Save Invoice and its related data ---//
@AuraEnabled
public static Map<String, String> SaveInvoiceData(Invoice__c invoiceObject, List<Invoice_Line_Item__c> invLineItemList, List<Purchase_Order__c> poList, Invoice_Line_Item__c[] otherChargesList,
                                                   String pageMode, String primaryDocJsonAtt, List<ContentVersion> NewAttachmentsAtt, QualityCheck__c qcObject) {
	if (pageMode.equals('new')) {
		invoiceObject.Id = null;
	}
	return InvoiceController.saveInvoice(invoiceObject, invLineItemList, poList, otherChargesList, pageMode, primaryDocJsonAtt, NewAttachmentsAtt, qcObject);
}
@AuraEnabled
public static cloneWrapper getInvoiceDetails(String invoiceId) {
	cloneWrapper cw = new cloneWrapper();
	cw.Invoice = Database.query(CommonFunctionController.getSOQLQuery('Invoice__c', 'Id=:invoiceId'));
	cw.attachment = getAttachment(invoiceId);
	return cw;
}

public static List<ContentVersion> getAttachment(String invoiceId) {
	List<ContentDocumentLink> contentDocumentLinks = new List<ContentDocumentLink> ();
	List<ContentVersion> contentVersionList = new List<ContentVersion> ();
	List<ContentVersion> contentVersionListToBeSent = new List<ContentVersion> ();
	try {
		if (UtilityController.isFieldAccessible('ContentDocumentLink', null))
		{
			contentDocumentLinks = [SELECT ContentDocumentId FROM ContentDocumentLink where LinkedEntityId = :invoiceId];

		}
		for (Integer i = 0; i < contentDocumentLinks.size(); i++)
		{

			ContentVersion cv = [SELECT Title, PathOnClient, VersionData, Origin, IsMajorVersion, Is_Primary_Doc__c FROM ContentVersion where ContentDocumentId = :contentDocumentLinks[i].ContentDocumentId limit 1];
			contentVersionList.add(cv.clone(false, true));

		}

		insert contentVersionList;
		String contentVersionIds = '';
		for (Integer i = 0; i < contentVersionList.size(); i++) {
			contentVersionIds = contentVersionIds + '\'' + contentVersionList[i].id + '\',';

		}

		contentVersionIds = contentVersionIds.removeEnd(',');
		String query = 'SELECT id, ContentDocumentId, Title, LastModifiedDate ,FileType,ContentSize,FileExtension,Is_Primary_Doc__c FROM ContentVersion where Id in (' + contentVersionIds + ')';
		contentVersionListToBeSent = Database.query(query);
	}
	catch(Exception e) {
		System.debug('msg :' + e.getMessage());
		System.debug('trace :' + e.getStackTraceString());

	}
	/*  lead l = [select id, firstname, lastname, company from lead where id = '00Q3000000aKwVN' limit 1][0];
	  lead l2 = l.clone(false, true);
	  insert l2;*/
	return contentVersionListToBeSent;

}
public class cloneWrapper {
	@AuraEnabled
	public Invoice__c Invoice { get; set; }
	@AuraEnabled
	public List<ContentVersion> attachment { get; set; }
}
@AuraEnabled
public static EsmHelper.DataMetadataWrapper callOnLoadCustomClasscallOnChangeManager(sObject childSobject,
                        List<Invoice_Line_Item__c> invLineItemList, List<Purchase_Order__c> puchaseOrder, String actionType,String objectName) {
	EsmHelper.DataMetadataWrapper wp = new EsmHelper.DataMetadataWrapper();
	try
	{
		System.debug('On change Manager method called');
		System.debug('childobj :' + childSobject);
		System.debug('actionType -----' + actionType);
		System.debug('objectName266 -----' + objectName);
		OnChangeConfig obj;
		System_Configuration__c sysConf = Database.Query('SELECT Value__c FROM System_Configuration__c where Module__c = :objectName and Sub_Module__c = :actionType');
		wp = callOnChangeManager(sysConf.Value__c, childSobject, invLineItemList, puchaseOrder, null, null);
		return wp;
	}
	catch(Exception e)
	{
		ExceptionHandlerUtility.customDebug(e, 'callOnLoadCustomClasscallOnChangeManager');
		wp.currentObject = childSobject;
		return wp;
	}
}
@AuraEnabled
public static Map<String, string> SaveObjectData(sObject customObject, String pageMode, String primaryDocJsonAtt, List<ContentVersion> NewAttachmentsAtt, QualityCheck__c qcObject,String objectName) {

	//System.debug('customObject -----'+customObject);		
	//System.debug('pageMode ------'+pageMode);
	//System.debug('primaryDocJsonAtt ------'+primaryDocJsonAtt);
	//System.debug('NewAttachmentsAtt ------'+NewAttachmentsAtt);
	//System.debug('qcObject --------'+qcObject);

	if (pageMode.equals('new')) {
		customObject.Id = null;
	}
	EsmHelper.DataMetadataWrapper wppCc = callOnLoadCustomClasscallOnChangeManager(customObject, new List<Invoice_Line_Item__c> (), new List<Purchase_Order__c> (), 'On Save Custom Class',objectName);
	customObject = wppCc.currentObject;
	if (wppCc.displayError !=null)
	{
		Map<String,String> mp =new Map<String,String>();
		mp.put('displayError',wppCc.displayError);
		return mp;
	}
	return InvoiceController.saveData(customObject, pageMode, primaryDocJsonAtt, NewAttachmentsAtt, qcObject,objectName);

}
@AuraEnabled
public static cloneWrapper getCloneSobjectDetails(String recordId, String objName) {
	cloneWrapper cw = new cloneWrapper();
	cw.Invoice = Database.query(CommonFunctionController.getSOQLQuery(objName, 'Id=:recordId'));
	//cw.attachment = getAttachment(recordId);
	return cw;
}
}