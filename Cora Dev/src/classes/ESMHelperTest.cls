@isTest
public class ESMHelperTest{

 public static testMethod void testEsmHelperMethod(){
    ESMHelper.errorResult wrapperField = new ESMHelper.errorResult();
    wrapperField.ValExecMsgKey = 'abc';
    wrapperField.errorMessage = 'xyz';
    wrapperField.type = 'mno';
    wrapperField.isCustom = True;
 }
    
    public static testMethod void testCriteriaMethod(){
    ESMHelper.Criteria wrapperCriteria = new ESMHelper.Criteria();
    wrapperCriteria.field = 'User_Action__c';
    wrapperCriteria.operator = '==';
    wrapperCriteria.value = 'Reject';
    wrapperCriteria.type = 'String';
    wrapperCriteria.refField = 'Invoice__c';
 }
    
    public static testMethod void testRuleMethod(){
    ESMHelper.Rule wrapperRule = new ESMHelper.Rule();
    wrapperRule.JSONValue = null;
    wrapperRule.ruleName = 'TestRule' ;
    wrapperRule.order = 1 ;
    wrapperRule.id = '';
    wrapperRule.isActive = false;
    wrapperRule.type = 'RelatedList' ;
    wrapperRule.objectName = 'Invoice__c' ;
 }
 //   public static testMethod void testJSONValueMethod(){
    /*ESMHelper.JSONValue wrapperJSONValue = new ESMHelper.JSONValue();
    wrapperJSONValue.criterias = 'abc';
    wrapperCriteria.operator = 'ana';
    wrapperCriteria.value = 'a';
    wrapperCriteria.type = 'b';
    wrapperCriteria.refField = 'c';*/
        
 /*   Validation_Exception_Rule__c validationRuleConfig =new Validation_Exception_Rule__c();
    validationRuleConfig.Rule__c = '{"criterias":[{"value":"121","type":"CURRENCY","refField":"","operator":"equals","field":"AMT__c"},{"value":"11","type":"CURRENCY","refField":"","operator":"not equal to","field":"AMT__c"},{"value":"56","type":"CURRENCY","refField":"","operator":"less than","field":"AMT__c"},{"value":"21","type":"CURRENCY","refField":"","operator":"greater than","field":"AMT__c"},{"value":"122","type":"CURRENCY","refField":"","operator":"less or equal","field":"AMT__c"},{"value":"3234","type":"CURRENCY","refField":"","operator":"greater or equal","field":"AMT__c"},{"value":"True","type":"BOOLEAN","refField":"","operator":"equals","field":"Approval_Required_For_Additional_Charges__c"},{"value":"False","type":"BOOLEAN","refField":"","operator":"not equal to","field":"Approval_Required_For_Additional_Charges__c"},{"value":"121","type":"STRING","refField":"","operator":"equals","field":"Error_Message__c"},{"value":"12","type":"STRING","refField":"","operator":"not equal to","field":"Error_Message__c"},{"value":"12","type":"STRING","refField":"","operator":"starts with","field":"Error_Message__c"},{"value":"232","type":"STRING","refField":"","operator":"contains","field":"Error_Message__c"},{"value":"23","type":"STRING","refField":"","operator":"end with","field":"Error_Message__c"},{"value":"123","type":"STRING","refField":"","operator":"does not contain","field":"Error_Message__c"},{"value":"12","type":"STRING","refField":"","operator":"contains in list","field":"Error_Message__c"}],"attributes":null}';
    validationRuleConfig.Error_Type__c = 'Validation'; 
    validationRuleConfig.isActive__c = true;  
    validationRuleConfig.isCustom__c = true;
    validationRuleConfig.Error_Code__c = 'DUPLICATE_INVOICE'; 
    validationRuleConfig.Stop_Excecution__c = true;    
    validationRuleConfig.Order__c= 19;
    validationRuleConfig.AdditionalRules__c = '{"criterias":null,"attributes":[{"additionalRuleValue":"525","additionalRuleField":"Tolerance_In_Percent"},{"additionalRuleValue":"51","additionalRuleField":"Tolerance_In_Amount"}]}'; 
   
    insert validationRuleConfig;
    Invoice__c obj = new Invoice__c();
        obj.Document_Type__c = 'PO Invoice';
        obj.Invoice_No__c = 'INV-0001';
        obj.Invoice_Date__c = Date.Today();
        obj.Amount__c = 121;
        obj.Comments__c = 'test comment';
        obj.User_Action__c = 'Validate';
        obj.Current_State__c = 'Ready For Processing';
        obj.Approval_Required_For_Additional_Charges__c = true;
        obj.Error_Message__c = '121';
        
        insert obj;
    EsmHelper.Rule esmRule = new EsmHelper.Rule();
    esmRule.id = validationRuleConfig.id;
    esmRule.JSONValue = (EsmHelper.JSONValue) System.JSON.deserialize(validationRuleConfig.Rule__c, EsmHelper.JSONValue.class);
    EsmHelper.checkRuleMatch(esmRule,obj);
        
        
        obj.Amount__c = 11;
        obj.Approval_Required_For_Additional_Charges__c = false;
        obj.Error_Message__c = '12';
        
        update obj;
 
    esmRule.id = validationRuleConfig.id;
    esmRule.JSONValue = (EsmHelper.JSONValue) System.JSON.deserialize(validationRuleConfig.Rule__c, EsmHelper.JSONValue.class);
    EsmHelper.checkRuleMatch(esmRule ,obj);
        
        
   
        obj.Document_Type__c = 'PO Invoice';
        obj.Invoice_No__c = 'INV-0001';
        obj.Invoice_Date__c = Date.Today();
        obj.Amount__c = 56;
        obj.Comments__c = 'test comment';
        obj.User_Action__c = 'Validate';
        obj.Current_State__c = 'Ready For Processing';
        obj.Approval_Required_For_Additional_Charges__c = true;
        obj.Error_Message__c = '232';
        
        insert obj;
    
    esmRule.id = validationRuleConfig.id;
    esmRule.JSONValue = (EsmHelper.JSONValue) System.JSON.deserialize(validationRuleConfig.Rule__c, EsmHelper.JSONValue.class);
    EsmHelper.checkRuleMatch(esmRule,obj);
        
       
        obj.Amount__c = 21;
        obj.Approval_Required_For_Additional_Charges__c = false;
        obj.Error_Message__c = '23';
        
        update obj;
    esmRule.id = validationRuleConfig.id;
    esmRule.JSONValue = (EsmHelper.JSONValue) System.JSON.deserialize(validationRuleConfig.Rule__c, EsmHelper.JSONValue.class);
    EsmHelper.checkRuleMatch(esmRule ,obj);
        
     obj.Document_Type__c = 'PO Invoice';
        obj.Invoice_No__c = 'INV-0001';
        obj.Invoice_Date__c = Date.Today();
        obj.Amount__c = 122;
        obj.Comments__c = 'test comment';
        obj.User_Action__c = 'Validate';
        obj.Current_State__c = 'Ready For Processing';
        obj.Approval_Required_For_Additional_Charges__c = true;
        obj.Error_Message__c = '123';
        
        insert obj;
    
    esmRule.id = validationRuleConfig.id;
    esmRule.JSONValue = (EsmHelper.JSONValue) System.JSON.deserialize(validationRuleConfig.Rule__c, EsmHelper.JSONValue.class);
    EsmHelper.checkRuleMatch(esmRule,obj);
        
       
        obj.Amount__c = 3234;
        obj.Approval_Required_For_Additional_Charges__c = false;
        obj.Error_Message__c = '123';
        
        update obj;
    esmRule.id = validationRuleConfig.id;
    esmRule.JSONValue = (EsmHelper.JSONValue) System.JSON.deserialize(validationRuleConfig.Rule__c, EsmHelper.JSONValue.class);
    EsmHelper.checkRuleMatch(esmRule ,obj);
         
 } */
}