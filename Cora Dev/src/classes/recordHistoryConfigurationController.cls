public class recordHistoryConfigurationController {
	@RemoteAction
	public static List<records> getAllObjectsFields() {
		Map<String, Schema.SobjectField> fields = Schema.SobjectType.Record_History__c.fields.getMap();
		System_Configuration__c sc = [select Value__c from System_Configuration__c where Module__c = 'Custom_Record_History' limit 1];
		List<records> rec = new List<records> ();

		for (String fieldsLp : fields.keySet()) 
		{
			String fval =fields.get(fieldsLp).getDescribe().getName();
			if (String.ValueOf(fields.get(fieldsLp).getDescribe().getType()).containsIgnoreCase('REFERENCE') &&
			!(fval.equals('User__c') || fval.equals('LastModifiedById') || fval.equals('CreatedById__c') || fval.equals('OwnerId')
				 || fval.equals('CreatedById')))
			{
				records r = new records();
				r.name = fval;
				r.label = fields.get(fieldsLp).getDescribe().getLabel();
				if (sc.Value__c != null && sc.Value__c.containsIgnoreCase(fval))
				{
					r.active = true;
				} else {
					r.active = false;
				}
				rec.add(r);
			}
		}
		return rec;
	}

	public class records {
		public string name { get; set; }
		public string label { get; set; }
		public Boolean active { get; set; }
	}
	@RemoteAction
	public static String changeState(string name, boolean isActive) {
		try {
			System_Configuration__c sc = [select Value__c from System_Configuration__c where Module__c = 'Custom_Record_History' limit 1];
			System.debug('condi43---' + (isActive && (sc.Value__c == null || !sc.Value__c.containsIgnoreCase(name))));
			if (isActive)
			{
				if (sc.Value__c == null)
				{
					sc.Value__c = name;
				} else {
					sc.Value__c = sc.Value__c + ',' + name;
				}
			} else {
				// iterate over list and replace
				String scv = '';
				for (String val : sc.Value__c.split(',')) {
					if (!val.equals(name))
					{
						scv = scv + val + ',';
					}
				}
				sc.Value__c = scv.removeEnd(',');
			}
			update sc;
		}
		catch(Exception ex) {
			ExceptionHandlerUtility.writeException('QCRuleController', ex.getMessage(), ex.getStackTraceString());
		}
		return 'sd';
	}
}