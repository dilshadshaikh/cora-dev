trigger PurchaseOrderMasterTrigger on Purchase_Order__c(before insert, before update, after insert, after update) {
	try {
		//--- Share Purchase Order with Vendor ---//
		List<System_Configuration__c> sysconfig = [SELECT id, Name, Module__c, Sub_Module__c, Value__c FROM System_Configuration__c where Module__c = 'Purchase_Order' AND Sub_Module__c = 'Share_PO_With_Vendor'];
		if (sysconfig != null && sysconfig.size() > 0 && sysconfig.get(0).Value__c == 'true') {
			if ((Trigger.isUpdate || Trigger.isInsert) && (Trigger.isAfter)) {

				List<SObject> insertShareObj = new List<SObject> ();
				Set<Id> parentId = Trigger.NewMap.keySet();

				List<Purchase_Order__share> deleteShareObj = [SELECT Id FROM Purchase_Order__share WHERE ParentId IN :parentId AND AccessLevel != 'All'];

				set<string> vendorId = new Set<String> ();

				for (Purchase_Order__c obj : Trigger.New) {

					if (obj.Vendor__c != null) {
						vendorId.add(obj.Vendor__c);
					}
				}

				Map<String, String> mapUserNameVendorId = new Map<String, String> ();

				if (Account.getSObjectType().getDescribe().isAccessible() && Schema.sObjectType.Account.fields.Id.isAccessible() && Schema.sObjectType.Account.fields.User_Name__c.isAccessible()) {

					for (Account sup :[SELECT Id, User_Name__c FROM Account WHERE Id IN :vendorId]) {
						mapUserNameVendorId.put(sup.Id, sup.User_Name__c);
					}
				}

				Map<String, String> mapUserNameUId = new Map<String, String> ();

				if (UtilityController.isFieldAccessible('User', 'Id,FirstName,LastName,IsActive,UserName')) {
					for (User u :[Select Id, FirstName, LastName, IsActive, UserName FROM User WHERE UserName IN :mapUserNameVendorId.values()]) {
						mapUserNameUId.put(u.UserName, u.id);
					}
				}

				for (Purchase_Order__c obj : Trigger.New) {
					if (obj.Vendor__c != null && mapUserNameVendorId.get(obj.Vendor__c) != null && mapUserNameUId.get(mapUserNameVendorId.get(obj.Vendor__c)) != null) {

						Purchase_Order__Share shareObj = new Purchase_Order__Share();
						shareObj.put('ParentId', obj.id);
						shareObj.put('UserOrGroupId', mapUserNameUId.get(mapUserNameVendorId.get(obj.Vendor__c)));
						shareObj.put('AccessLevel', 'Edit');
						shareObj.put('RowCause', Schema.Purchase_Order__Share.RowCause.Manual);
						insertShareObj.add(shareObj);

					}
				}

				if (deleteShareObj != null && deleteShareObj.size() > 0) {
					List<Database.DeleteResult> sr = Database.delete(deleteShareObj, false);
				}
				if (insertShareObj != null && insertShareObj.size() > 0) {
					List<Database.SaveResult> sr = Database.insert(insertShareObj, false);
				}
			}
		}
	}
	catch(Exception e) {
		Trigger.New[0].addError(UtilityController.customDebug(e, 'PurchaseOrderMasterTrigger'));
	}
}