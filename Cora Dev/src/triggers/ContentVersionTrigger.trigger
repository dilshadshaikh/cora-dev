trigger ContentVersionTrigger on ContentVersion (before insert) {
    List<System_Configuration__c> sysconfig = [SELECT id, Name, Module__c, Sub_Module__c, Value__c FROM System_Configuration__c where Module__c = 'Content_Version'];

	if (sysconfig != null && sysconfig.size() > 0 && sysconfig.get(0).Value__c == 'true') {
	    if ((Trigger.isBefore && Trigger.isInsert)) { 
	       // To update flag of attachment
	       ContentVerionHandler.updateFlag(trigger.new, trigger.old);
	    }
	}
}