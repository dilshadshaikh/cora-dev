trigger EmailMessageTrigger on EmailMessage(before insert, after insert, after update, after delete, after undelete) {
    /*
    Authors: Hiren SONI
    */
     List<System_Configuration__c> sysconfig = [SELECT id, Name, Module__c, Sub_Module__c, Value__c FROM System_Configuration__c where Module__c = 'Email_Message'];

	if (sysconfig != null && sysconfig.size() > 0 && sysconfig.get(0).Value__c == 'true') {
	    if (Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate || Trigger.isDelete || Trigger.isUndelete)) {
	        EmailMessageHandler.calUnreadEmailCntAndUpdateCase(trigger.new);
	    }
	    //PUX-333
	    if(Trigger.isbefore && Trigger.isInsert){
	        EmailMessageHandler.markOutboundMessageAsRead(trigger.new);
	    } 
	}
}