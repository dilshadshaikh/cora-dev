trigger InsertUpdateUserMaster on User (before insert, before update,after Insert,after update) {
    try{
    system.debug('------------------Trigger Call----------------');
    String prefix = UtilityController.esmNameSpace;
    if(Trigger.isBefore){
        set<String> uniqueUserName = new set<String>();
        Map<String,String> mapNewOldUserName = new Map<String,String>();
        for(User u: Trigger.new){
            if(!uniqueUserName.add(u.Username)){
                u.addError(Label.User_Name_is_already_exist_in_User_Masters);
            }
            if(trigger.isUpdate){
                mapNewOldUserName.put(u.Username,trigger.oldMap.get(u.id).Username);
               
            }else{ 
                mapNewOldUserName.put(u.Username,u.Username);
            }
        } 
        
        system.debug('uniqueUserName=>'+uniqueUserName);
        map<String, User_Master__c> userMap = new map<String, User_Master__c>([SELECT Id,User_Name__c FROM User_Master__c where User_Name__c IN :uniqueUserName]);
        system.debug('userMap=>'+userMap);
        if(Trigger.isInsert){
            for(User newBu : Trigger.new){
                for(User_Master__c newBuTemp : userMap.Values()){
                    if(newBuTemp.User_Name__c == newBu.Username){
                        newBu.addError(Label.User_Name_is_already_exist_in_User_Masters);
                    }
                }
            }
        }else{
            for(User newBu : Trigger.new){
                if(mapNewOldUserName.containsKey(newBu.Username) && newBu.Username != mapNewOldUserName.get(newBu.Username)){
                    for(User_Master__c newBuTemp : userMap.Values()){
                        if(newBuTemp.User_Name__c == newBu.Username){
                            newBu.addError(Label.User_Name_is_already_exist_in_User_Masters);
                        }
                    }
                }
            }
        }
    }
    
    if(Trigger_Configuration__c.getOrgDefaults().Enable_User_Usermaster_Sync__c && Trigger.isAfter){
        Map<String,String> mapNewOldUserName = new Map<String,String>();
        for(User u: trigger.newMap.Values()){
            if(trigger.isUpdate){
                mapNewOldUserName.put(u.Username,trigger.oldMap.get(u.id).Username);
               
            }else{ 
                mapNewOldUserName.put(u.Username,u.Username);
            }

        } 
        System.debug('=mapNewOldEmail===='+mapNewOldUserName);
        if(!mapNewOldUserName.isEmpty()){
            System.debug('==in==');
            UserMasterManager hu = new UserMasterManager(mapNewOldUserName); 
            ID jobID = System.enqueueJob(hu);
        }
    }
    
    if(Trigger_Configuration__c.getOrgDefaults().Enable_Sharing_To_Delegate_User__c && Trigger.isAfter){
        Map<Id,User> userMap = new Map<Id,User>();
        for(User usr : Trigger.New) {
            userMap.put(usr.Id,usr);
        }
        system.debug('userMap:'+userMap);
        system.debug('userMap keySet:'+userMap.keySet());
        
        
        Set<Id> invSet = new Set<Id>();
        Set<Id> smSet = new Set<Id>();
        List<SObject> insertShareObj = new List<SObject>();
        //--- Added by Mekyush | JIRA:ESMPROD-222 | 24-02-2016 | Starts ---//
        List<Invoice__c> invList = [SELECT id,name,OwnerId from Invoice__c WHERE OwnerId =: userMap.keySet()];
        List<Vendor_Maintenance__c> smList = [SELECT id,name,OwnerId from Vendor_Maintenance__c WHERE OwnerId =: userMap.keySet()];
        //--- Added by Mekyush | JIRA:ESMPROD-222 | 24-02-2016 | Ends ---//
        for(Invoice__c inv:invList){
            invSet.add(inv.Id);
        }
        system.debug('invList:'+invList);
        system.debug('invList Size:'+invList.size());
        
        for(Vendor_Maintenance__c sm:smList){
            smSet.add(sm.Id);
        }
        system.debug('Supplier_Maintainance List:'+smList);
        system.debug('Supplier_Maintainance set:'+smSet);
        
        String shareReasonInv = Schema.Invoice__Share.RowCause.Manual;
        String shareReasonSM = Schema.Vendor_Maintenance__Share.RowCause.Manual;
        Set<String> prevSharedWith = new Set<String>();
        for(User usr : Trigger.New) {
            Id DelegateUserId = usr.Delegate_User__c;
            if(Trigger.isUpdate && Trigger.oldMap.get(usr.Id).get('Delegate_User__c') != null){
                prevSharedWith.add(String.ValueOf(Trigger.oldMap.get(usr.Id).get('Delegate_User__c')));
            }
            if(usr.Enable_Delegation__c == true && usr.Delegate_User__c != null){
                for(Invoice__c obj:invList){
                    if(obj.OwnerId != usr.Id) {
                        continue;
                    }
                    Invoice__Share shareObj = New Invoice__Share();
                    shareObj.put('ParentId',(Id)obj.get('Id'));
                    shareObj.put('UserOrGroupId',DelegateUserId);
                    shareObj.put('AccessLevel','Edit');
                    shareObj.put('RowCause',shareReasonInv);
                    insertShareObj.add(shareObj);
                }
                for(Vendor_Maintenance__c sm:smList){
                    if(sm.OwnerId != usr.Id) {
                        continue;
                    }
                    Vendor_Maintenance__Share shareObj = new Vendor_Maintenance__Share();
                    shareObj.put('ParentId',(Id)sm.get('Id'));
                    shareObj.put('UserOrGroupId',DelegateUserId);
                    shareObj.put('AccessLevel','Edit');
                    shareObj.put('RowCause',shareReasonSM);
                    insertShareObj.add(shareObj);
                }
            }
        }
        List<SObject> deleteShareObj = Database.query('Select Id From '+prefix+'Invoice__Share Where ParentId In :invSet AND UserOrGroupId IN: prevSharedWith And AccessLevel != \'All\'');
        if(deleteShareObj != null && deleteShareObj.size() > 0) {
            List<Database.DeleteResult> sr = Database.delete(deleteShareObj,false);
        }
        
        deleteShareObj = Database.query('Select Id From '+prefix+'Vendor_Maintenance__Share Where ParentId In :smSet AND UserOrGroupId IN: prevSharedWith And AccessLevel != \'All\'');
        system.debug('deleteShareObj SM:'+deleteShareObj);
        system.debug('deleteShareObj SM Size:'+deleteShareObj.size());
        if(deleteShareObj != null && deleteShareObj.size() > 0) {
            List<Database.DeleteResult> sr = Database.delete(deleteShareObj,false);
        }
        system.debug('insertShareObj:'+insertShareObj);
        system.debug('insertShareObj Size:'+insertShareObj.size());
        if(insertShareObj != null && insertShareObj.size() > 0) {
            List<Database.SaveResult> sr = Database.insert(insertShareObj,false);
        }
        
    }
    }
    catch(Exception e)
    {
        system.debug('------------------e----------------'+e.getcause()+':'+e.getlinenumber());
    }
}