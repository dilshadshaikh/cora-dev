trigger InvoiceMasterTrigger on Invoice__c(before insert, before update, after insert, after update) {
    try {
       //Added by Neha Tailor for setting Sharing for TPRM-934
        Trigger_Configuration__c tc;
        if (Trigger_Configuration__c.getInstance(Userinfo.getUserid()) != null) {
            tc = Trigger_Configuration__c.getInstance(Userinfo.getUserid());
        } else {
            tc = Trigger_Configuration__c.getOrgDefaults();
        }
        if (tc.Enable_Invoice_master_Trigger__c)
        {
        //Ended Here
            String esmNameSpace = UtilityController.esmNameSpace;
            Map<String, string> sysConfMap = new Map<String, string> ();

            Map<Boolean, string> errorMap = new Map<Boolean, string> ();
            Set<String> po_ids = new Set<String> ();
            Map<String, Purchase_Order__c> poMap = new Map<String, Purchase_Order__c> ();

            Invoice_Configuration__c invConf;
            if (Invoice_Configuration__c.getInstance(Userinfo.getUserid()) != null) {
                invConf = Invoice_Configuration__c.getInstance(Userinfo.getUserid());
            } else {
                invConf = Invoice_Configuration__c.getOrgDefaults();
            }

            String po_no_field = invConf.PO_Number_Field_for_Search__c;
            System.debug('po_no_field :' + po_no_field);

            if (Trigger.isBefore) {
                for (System_Configuration__c sysConf :[SELECT Id, Name, Module__c, Sub_Module__c, Value__c FROM System_Configuration__c]) {
                    sysConfMap.put(sysConf.Module__c + '|' + sysConf.Sub_Module__c, sysConf.Value__c);
                }

                // -------- Added by Varsha | 11-04-2018 | JIRA ESMPROD-1031 | Start ------ //
                //--- Capture Invoice Comments ---//
                for (Invoice__c obj : Trigger.New) {
                    if (obj.Comments__c != null) {
                        UtilityController.InvFieldMap = new Map<String, string> ();
                        UtilityController.InvFieldMap.put(String.valueOf(obj.get('Invoice_No__c')), String.valueOf(obj.get('Comments__c')));
                        obj.put('Comments__c', null);
                    }
                }

                // -------- Added by Varsha | 11-04-2018 | JIRA ESMPROD-1031 | End --------- //

                //--- Capture Exception History ---//
                if (!UtilityController.exceptionHistoryCaptured) {
                    errorMap = CommonFunctionController.InsertExceptionHistory(Trigger.New, esmNameSpace, sysConfMap);
                    UtilityController.exceptionHistoryCaptured = true;
                }
                if (errorMap.keySet().contains(true)) {
                    Trigger.New[0].addError(errorMap.get(true));
                }
            }
            //--- Capture Invoice History ---//
            triggerHelper.insertUpdateHistory(Trigger.New, Trigger.old, Trigger.isInsert, Trigger.isUpdate, Trigger.newMap, Trigger.oldMap, 'Invoice__c');

            //--- Start | Entrying data in Invoice PO Detail ---//
            if (Trigger.isBefore && Trigger.isInsert) {
                UtilityController.InvPOMap = new Map<String, string> ();
                for (Invoice__c obj : Trigger.New) {
                    if (obj.Purchase_Order__c != null) {
                        po_ids.add(obj.Purchase_Order__c);
                    }
                }

                System.debug('po_ids :' + po_ids);

                String query = 'Select id,' + po_no_field + ' from Purchase_Order__c where id = :po_ids';
                list<Purchase_Order__c> poList = database.query(query);
                System.debug('poList :' + poList);
                for (Purchase_Order__c po : poList) {
                    poMap.put(po.id, po);
                }
                System.debug('poMap :' + poMap);
                for (Invoice__c obj : Trigger.New) {
                    if (obj.Purchase_Order__c != null) {
                        if (obj.PO_Numbers__c != null) {
                            obj.PO_Numbers__c = obj.PO_Numbers__c + ',' + (poMap.get(obj.Purchase_Order__c)).get(po_no_field);
                        } else {
                            obj.PO_Numbers__c = (poMap.get(obj.Purchase_Order__c)).get(po_no_field) + '';
                        }
                        System.debug('obj.PO_Numbers__c :' + obj.PO_Numbers__c);
                        UtilityController.InvPOMap.put(obj.Invoice_No__c + '~' + obj.Vendor__c + '~' + obj.Total_Amount__c, obj.Purchase_Order__c);
                        obj.Purchase_Order__c = null;
                    }
                }
            }

            if (Trigger.isAfter && Trigger.isInsert && UtilityController.InvPOMap != null && UtilityController.InvPOMap.size() > 0) {
                List<Invoice_PO_Detail__c> InvPODetailToInsert = new List<Invoice_PO_Detail__c> ();

                for (Invoice__c invObj : Trigger.New) {
                    Invoice_PO_Detail__c invPOObj = new Invoice_PO_Detail__c();
                    invPOObj.Invoice__c = invObj.id;
                    invPOObj.Purchase_Order__c = UtilityController.InvPOMap.get(invObj.Invoice_No__c + '~' + invObj.Vendor__c + '~' + invObj.Total_Amount__c);
                    InvPODetailToInsert.add(invPOObj);

                }
                if (InvPODetailToInsert.size() > 0) {
                    if (Invoice_PO_Detail__c.getSObjectType().getDescribe().isCreateable()
                        && Schema.sObjectType.Invoice_PO_Detail__c.fields.Invoice__c.isCreateable()
                        && Schema.sObjectType.Invoice_PO_Detail__c.fields.Purchase_Order__c.isCreateable()) {
                        if (Invoice_PO_Detail__c.getSObjectType().getDescribe().isUpdateable()
                            && Schema.sObjectType.Invoice_PO_Detail__c.fields.Invoice__c.isUpdateable()
                            && Schema.sObjectType.Invoice_PO_Detail__c.fields.Purchase_Order__c.isUpdateable()) {
                            upsert InvPODetailToInsert;
                        } else {
                            Trigger.New[0].addError(UtilityController.getUpdateExceptionMessage(esmNameSpace + 'Invoice_PO_Detail__c'));
                        }
                    } else {
                        Trigger.New[0].addError(UtilityController.getCreateExceptionMessage(esmNameSpace + 'Invoice_PO_Detail__c'));
                    }
                }
            }
            //--- Ends | Entrying data in Invoice PO Detail ---//   

            if ((Trigger.isUpdate || Trigger.isInsert) && (Trigger.isAfter)) {
                System.debug('sysConfMap:' + sysConfMap);

                // -------- Added by Varsha | 11-04-2018 | JIRA ESMPROD-1031 | Start ------ //
                //--- Capture Invoice Comments ---//
                if (!UtilityController.invoiceHistoryCaptured) {
                    errorMap = CommonFunctionController.insertCommentHistory(Trigger.New, esmNameSpace + 'Invoice__c', esmNameSpace, true);
                    UtilityController.invoiceHistoryCaptured = true;
                }
                // -------- Added by Varsha | 11-04-2018 | JIRA ESMPROD-1031 | End --------- //
                //--- Share Invoice with Vendor ---//
                System.debug('---------out if----------------' + tc.Enable_Sharing_to_vendor__c);
                //if (sysConfMap.get('Vendor_Master|Share_Invoice_With_Vendor') != null && sysConfMap.get('Vendor_Master|Share_Invoice_With_Vendor') == 'true') {
                if (tc.Enable_Sharing_to_vendor__c) {
                    System.debug('---------in if----------------');
                    List<SObject> insertShareObj = new List<SObject> ();
                    Set<Id> parentId = Trigger.NewMap.keySet();

                    List<Invoice__share> deleteShareObj = Database.query('SELECT Id FROM Invoice__share WHERE ParentId IN :parentId AND AccessLevel != \'All\'');

                    set<string> vendorId = new Set<String> ();

                    for (Invoice__c obj : Trigger.New) {

                        if (obj.Vendor__c != null) {
                            vendorId.add(obj.Vendor__c);
                        }
                    }
                    System.debug('--------vendorId' + vendorId);
                    Map<String, String> mapUserNameVendorId = new Map<String, String> ();
                    if (Account.getSObjectType().getDescribe().isAccessible() && Schema.sObjectType.Account.fields.Id.isAccessible() && Schema.sObjectType.Account.fields.User_Name__c.isAccessible()) {

                        for (Account sup :[SELECT Id, User_Name__c FROM Account WHERE Id IN :vendorId]) {
                            mapUserNameVendorId.put(sup.Id, sup.User_Name__c);
                        }
                    }
                    System.debug('--------mapUserNameVendorId' + mapUserNameVendorId);
                    Map<String, String> mapUserNameUId = new Map<String, String> ();
                    if (UtilityController.isFieldAccessible('User', 'Id,FirstName,LastName,IsActive,UserName')) {
                        for (User u :[Select Id, FirstName, LastName, IsActive, UserName FROM User WHERE UserName IN :mapUserNameVendorId.values()]) {
                            mapUserNameUId.put(u.UserName, u.id);
                        }
                    }
                    System.debug('--------mapUserNameUId' + mapUserNameUId);
                    //Code added by Dhwanil | ESMPROD-974
                    Invoice_Configuration__c IC=[select Pay_Me_Early_State__c from Invoice_Configuration__c];
                    
                    for (Invoice__c obj : Trigger.New) {
                        system.debug('----Shareing Rule before if----:'+obj.Vendor__c+':'+mapUserNameVendorId.get(obj.Vendor__c)+':'+mapUserNameUId.get(mapUserNameVendorId.get(obj.Vendor__c)));
                        if (obj.Vendor__c != null && mapUserNameVendorId.get(obj.Vendor__c) != null && mapUserNameUId.get(mapUserNameVendorId.get(obj.Vendor__c)) != null) {
                            Invoice__Share shareObj = new Invoice__Share();
                            shareObj.put('ParentId', obj.id);
                            shareObj.put('UserOrGroupId', mapUserNameUId.get(mapUserNameVendorId.get(obj.Vendor__c)));
                            system.debug('----Shareing Rule----:'+IC.Pay_Me_Early_State__c.split(',').contains(obj.current_state__c));
                            system.debug('----Shareing Rule----:'+IC.Pay_Me_Early_State__c.split(',')+':'+obj.current_state__c);
                            shareObj.put('AccessLevel', 'Read');
                            for(String str:IC.Pay_Me_Early_State__c.split(','))
                            {
                                if(str.equalsignorecase(obj.current_state__c))
                                {
                                    system.debug('Inside If');
                                    shareObj.put('AccessLevel', 'Edit');
                                    break;
                                }
                            }
                            shareObj.put('RowCause', Schema.Invoice__Share.RowCause.Manual);
                            system.debug('----Shareing Rule shareObj----:'+shareObj);
                            insertShareObj.add(shareObj);

                        }
                        
                    }
                    System.debug('--------insertShareObj' + insertShareObj);
                    System.debug('--------insertShareObj' + deleteShareObj);
                    if (deleteShareObj != null && deleteShareObj.size() > 0) {
                        List<Database.DeleteResult> sr = Database.delete(deleteShareObj, false);
                    }

                    if (insertShareObj != null && insertShareObj.size() > 0) {
                        List<Database.SaveResult> sr = Database.insert(insertShareObj, false);
                    }
                    //Code Update end by Dhwanil | ESMPROD-974

                }
            }

        
        
          //--- Added by Vishwa | JIRA:ESMPROD-1263 | 5-31-2018 | Starts ---//
  // Added For Accuracy Report
  if (Trigger.isAfter && (Trigger.isUpdate || Trigger.isInsert)) {
      
    Map<String, string> APFieldADHfieldMap = new Map<String, string> ();
      
    for (OCR_Accuracy_Report_Mapping__c OCRConf :[SELECT Id, FieldName_on_Object__c, FieldName_on_OCR__c, Object_Name__c FROM OCR_Accuracy_Report_Mapping__c]) {
    if(OCRConf.Object_Name__c=='Invoice__c')
        APFieldADHfieldMap.put(OCRConf.FieldName_on_Object__c, OCRConf.FieldName_on_OCR__c);
    }
       System.debug('--------APFieldADHfieldMap ' + APFieldADHfieldMap);
  
    list<OCR_Accuracy_Report_HeaderLevel__c> lstAccuracyForInsert = new list<OCR_Accuracy_Report_HeaderLevel__c> ();
  //  list<OCR_Accuracy_Report_HeaderLevel__c> lstAccuracyForUpdate = new list<OCR_Accuracy_Report_HeaderLevel__c> ();
    OCR_Accuracy_Report_HeaderLevel__c objAccuracyReport;
    Set<Id> idList = Trigger.newMap.keySet();
    List<OCR_Accuracy_Report_HeaderLevel__c> existingData = new List<OCR_Accuracy_Report_HeaderLevel__c> ();
    String query = CommonFunctionController.getCreatableFieldsSOQL('OCR_Accuracy_Report_HeaderLevel__c', esmNameSpace + 'Invoice__c IN :idList', false);
    if (UtilityController.isFieldAccessible('OCR_Accuracy_Report_HeaderLevel__c', null)) {
      existingData = database.query(query);
    }
    Map<Id, OCR_Accuracy_Report_HeaderLevel__c> existingDataMap = new Map<Id, OCR_Accuracy_Report_HeaderLevel__c> ();
    if (existingData != null && existingData.size() > 0) {
      for (OCR_Accuracy_Report_HeaderLevel__c obj : existingData) {
        if (obj.get(esmNameSpace + 'Invoice__c') != null) {
          existingDataMap.put((Id) obj.get(esmNameSpace + 'Invoice__c'), obj);
        }
      }
    }
        System.debug('--------existingDataMap ' + existingDataMap);


/*    for (akritivtlm__Accuracy_Data_History_Config__c adhc : lstAccrHist) {
      APFieldADHfieldMap.put(adhc.Name, adhc.akritivtlm__Accuracy_Report_Field__c);
    }*/
     System_Configuration__c sysConf=[SELECT Id, Name, Module__c, Sub_Module__c, Value__c FROM System_Configuration__c where Sub_Module__c='OCR_Accuracy_Current_State'];
     String currentStateFromConfig = sysConf.Value__c;
      
  //  if (lstAccrHist != null && lstAccrHist.size() > 0) {
   if (APFieldADHfieldMap != null && APFieldADHfieldMap.size() > 0) {
      for (Invoice__c childobj : Trigger.new) {
        String currentState = '';
        currentState = childobj.Previous_State__c;
        
      
       System.debug('--------currentState ' + currentState);
      System.debug('--------currentStateFromConfig ' + currentStateFromConfig);
          

     //   akritivtlm__App_Logic__c appLogic = akritivtlm__App_Logic__c.getInstance(objCode + '.CurrentStateForAccuracyReport');
       
        if (currentStateFromConfig != null && currentStateFromConfig != '' && currentState != null && currentState != '' && currentState.equalsIgnoreCase(currentStateFromConfig)) 
        {
 System.debug('--------childobj.Id ');
          if (!existingDataMap.containsKey(childobj.Id))  {
            objAccuracyReport = new OCR_Accuracy_Report_HeaderLevel__c();
          }
 System.debug('--------objAccuracyReport ---- ' + objAccuracyReport);
            
          objAccuracyReport.put(esmNameSpace + 'Invoice__c', childobj.get('Id'));
          for (string s : APFieldADHfieldMap.keyset()) {
            if (string.valueof(childobj.get(s)) != null && string.valueof(childobj.get(s)) != '')
            objAccuracyReport.put(APFieldADHfieldMap.get(s), childobj.get(s));
            else
            objAccuracyReport.put(APFieldADHfieldMap.get(s), null);
          }
            
             System.debug('--------objAccuracyReport ' + objAccuracyReport);
            
          if (!existingDataMap.containsKey(childobj.Id))  {
            lstAccuracyForInsert.add(objAccuracyReport);
          }
            
          
        }
      }
    }

    if (lstAccuracyForInsert.size() > 0) {
        
            System.debug('--------lstAccuracyForInsert ' + lstAccuracyForInsert);

      if (UtilityController.isFieldCreateable('OCR_Accuracy_Report_HeaderLevel__c', null)) {
        Database.insert(lstAccuracyForInsert, false);
      }
    }
   /* if (lstAccuracyForUpdate.size() > 0) {
          System.debug('--------lstAccuracyForUpdate ' + lstAccuracyForUpdate);
      if (UtilityController.isFieldUpdateable('OCR_Accuracy_Report_HeaderLevel__c', null)) {
        Database.update(lstAccuracyForUpdate, false);
      }
    }*/

  }
  //--- Added by Vishwa | JIRA:ESMPROD-1263 | 5-31-2018 | Starts ---//
        }
    }
    catch(Exception e) {
          System.debug('--------Exception ' + e);
        ExceptionHandlerUtility.customDebug(e, 'InvoiceMasterTrigger');
        Trigger.New[0].addError(UtilityController.customDebug(e, 'InvoiceMasterTrigger'));
    }

}