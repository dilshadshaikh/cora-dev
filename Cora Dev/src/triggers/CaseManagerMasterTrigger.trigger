trigger CaseManagerMasterTrigger on CaseManager__c(before insert, before update, after insert, after update) {
  try {
      
    String esmNameSpace = UtilityController.esmNameSpace;
    //--- Capture Invoice Comments ---//
    Map<Boolean,string> errorMap = new Map<Boolean,string>();
    if (Trigger.isBefore) {

      errorMap = CommonFunctionController.insertCommentHistory(Trigger.New, esmNameSpace+'CaseManager__c', esmNameSpace,true);
      if(errorMap.keySet().contains(true)){
        Trigger.New[0].addError(errorMap.get(true));
      }
    }
    //--- Share CaseManager with Vendor ---//
    
      
      if (Trigger.isBefore && Trigger.isUpdate) {        
            CaseManagerHandler.updatePrevQueue(trigger.new, trigger.old);
            //To populate report related fields
            CaseManagerHandler.populateReportRelatedFields(trigger.new, trigger.old);        
           // QCCalculation.checkForQC(Trigger.New, Trigger.OldMap);
        }
        if (Trigger.isAfter && Trigger.isUpdate) {
            TATTriggerHelper.calculateStateChange(Trigger.New, Trigger.OldMap);
           // ReminderAndEscalationHelper.removeReminderObject(Trigger.New, Trigger.OldMap);
            //TATTriggerHelper.removeTatNotification(Trigger.New, Trigger.OldMap);
            TATTriggerHelper.setActualTatTime(Trigger.New, Trigger.OldMap);
        //    ReminderAndEscalationHelper.setReminderAndEscalation(Trigger.New, Trigger.OldMap);
        //ClosureNotficationHelper.sendCaseClosureNotification(Trigger.New, Trigger.OldMap);
        //RejectionNotficationHelper.sendCaseRejectionNotification(Trigger.New, Trigger.OldMap);
        }
        if (Trigger.isAfter && Trigger.isInsert) {
            TATTriggerHelper.CreateCaseTransition(trigger.new);
            //TATTriggerHelper.createTATNotification(trigger.new);
        }
        
        
    
    if ((Trigger.isUpdate || Trigger.isInsert) && (Trigger.isAfter)) {

      List<SObject> insertShareObj = new List<SObject> ();
      Set<Id> parentId = Trigger.NewMap.keySet();
      List<CaseManager__share> deleteShareObj = Database.query('SELECT Id FROM CaseManager__share WHERE ParentId IN :parentId AND AccessLevel != \'All\'');
      set<string> vendorId = new Set<String> ();

      for (CaseManager__c obj : Trigger.New) {
        if (obj.Vendor__c != null) {
          vendorId.add(obj.Vendor__c);
        }
      }

      Map<String, String> mapUserNameVendorId = new Map<String, String> ();
      if (Account.getSObjectType().getDescribe().isAccessible() && Schema.sObjectType.Account.fields.Id.isAccessible() && Schema.sObjectType.Account.fields.User_Name__c.isAccessible()) {
        for (Account sup :[SELECT Id, User_Name__c FROM Account WHERE Id IN :vendorId]) {
          mapUserNameVendorId.put(sup.Id, sup.User_Name__c);
        }
      }
      Map<String, String> mapUserNameUId = new Map<String, String> ();
      if (UtilityController.isFieldAccessible('User', 'Id,FirstName,LastName,IsActive,UserName')) {
        for (User u :[Select Id, FirstName, LastName, IsActive, UserName FROM User WHERE UserName IN :mapUserNameVendorId.values()]) {
          mapUserNameUId.put(u.UserName, u.id);
        }

      }
      for (CaseManager__c obj : Trigger.New) {
        if (obj.Vendor__c != null && mapUserNameVendorId.get(obj.Vendor__c) != null && mapUserNameUId.get(mapUserNameVendorId.get(obj.Vendor__c)) != null) {
          CaseManager__share shareObj = new CaseManager__share();
          shareObj.put('ParentId', obj.id);
          shareObj.put('UserOrGroupId', mapUserNameUId.get(mapUserNameVendorId.get(obj.Vendor__c)));
          shareObj.put('AccessLevel', 'Edit');
          shareObj.put('RowCause', Schema.CaseManager__share.RowCause.Manual);
          insertShareObj.add(shareObj);

        }

      }
        if (deleteShareObj != null && deleteShareObj.size() > 0) {
          List<Database.DeleteResult> sr = Database.delete(deleteShareObj, false);
        }
        
        if (insertShareObj != null && insertShareObj.size() > 0) {
          List<Database.SaveResult> sr = Database.insert(insertShareObj, false);
        }


      }
    

  }
  catch(Exception e) {
    System.debug(ExceptionHandlerUtility.customDebug(e,'55554545'));
    system.debug('================'+e.getlinenumber());
    Trigger.New[0].addError(UtilityController.customDebug(e, 'CaseManagerMasterTrigger'));
  }

}