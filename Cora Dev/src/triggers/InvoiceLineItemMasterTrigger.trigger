trigger InvoiceLineItemMasterTrigger on Invoice_Line_Item__c (before insert, before update, after insert, after update, after delete) {
  try {
    Trigger_Configuration__c tgrConf;
    if(Trigger_Configuration__c.getInstance(Userinfo.getUserid()) != null) {
        tgrConf = Trigger_Configuration__c.getInstance(Userinfo.getUserid());
    } else {
        tgrConf = Trigger_Configuration__c.getOrgDefaults();
    }
    //System.debug('tgrConf --------'+tgrConf);
    /*Invoice_Configuration__c invConf;
    if(Invoice_Configuration__c.getInstance(Userinfo.getUserid()) != null) {
        invConf = Invoice_Configuration__c.getInstance(Userinfo.getUserid());
    } else {
        invConf = Invoice_Configuration__c.getOrgDefaults();
    }*/
    //System.debug('invConf --------'+invConf);
    //List<System_Configuration__c> sysConfigs = new List<System_Configuration__c>();
    //sysConfigs = [select id, Name, Module__c, Sub_Module__c, Value__c from System_Configuration__c where Module__c = 'Quantity_Calculation' and (Sub_Module__c = 'Enable_Quantity_Calculation' or Sub_Module__c = 'Invoice_Reject_Current_State') limit 999];
    //System.debug('sysConfigs --------'+sysConfigs);
    if(tgrConf.Enable_Invoice_Line_Item_Trigger__c){
        String esmNameSpace = UtilityController.esmNameSpace;
        
        if(Trigger.IsBefore){
          
          for(Invoice_Line_Item__c invl : Trigger.New){
                system.debug('----Line Id------'+invl);
                invl.Old_Quantity__c = invl.Quantity__c;
                invl.Old_Quantity__c = invl.Old_Quantity__c != null ? invl.Old_Quantity__c : 0;
                invl.Old_GRN_Line_Item__c = invl.GRN_Line_Item__c;
                if(Trigger.IsInsert){
                  invl.Invoice_Line_Item_No__c = UtilityController.getUniqueRamdomId('INVL-', '');//'INVL-' + Math.round(Math.random()*1000) + DateTime.now().getTime();
                }
            }
        }
        //--- Added by Dilshad | JIRA:ESMPROD-278 | 20-05-2016 | Starts ---//
        //--- Remaining Quantity Updation Logic | Starts ---//
        system.debug('--- Remaining Quantity Updation Logic');
        boolean enableQuantityCalculation = false;
        String rejectedState = '';
        Invoice_Configuration__c invConf = CommonFunctionController.getInvoiceConfiguration();
        enableQuantityCalculation = Boolean.valueOf(invConf.get(esmNameSpace+'Enable_Quantity_Calculation__c'));
        rejectedState = String.valueOf(invConf.get(esmNameSpace+'Invoice_Reject_Current_State__c'));
        /*for(System_Configuration__c sConf : sysConfigs){
            if(sConf.Sub_Module__c.equals('Enable_Quantity_Calculation') && sConf.Value__c.equalsIgnoreCase('true'))
            {
                enableQuantityCalculation = true;
            }
            if(sConf.Sub_Module__c.equals('Invoice_Reject_Current_State'))
            {
                rejectedState = sConf.Value__c;
            }
        }*/
        System.debug('enableQuantityCalculation --------'+enableQuantityCalculation);
        System.debug('rejectedState --------'+rejectedState);
        if(enableQuantityCalculation){
            if(Trigger.IsAfter) {
                Map<Id,String> InvlGRNLQntyMap = new Map<Id,String>();
                Map<Id,String> InvlPolQntyMap = new Map<Id,String>();
                
                if(Trigger.isDelete){ // When Invoice Line Item is DELETED //
                    for(Invoice_Line_Item__c invl : Trigger.Old){
                        if(invl.Quantity_Calculation_Required__c){
                            if(invl.GRN_Line_Item__c != null){
                                if(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c) != null){
                                    system.debug('InvlGRNLQntyMap.get(invl.GRN_Line_Item__c):'+InvlGRNLQntyMap.get(invl.GRN_Line_Item__c));
                                    InvlGRNLQntyMap.put(invl.GRN_Line_Item__c,(0 + Decimal.ValueOf(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c).split('~')[0])) + '~' + ((Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0) + Decimal.ValueOf(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c).split('~')[1])));
                                }
                                else {
                                    InvlGRNLQntyMap.put(invl.GRN_Line_Item__c,0 + '~' + (Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0));
                                }
                            }
                            if(invl.PO_Line_Item__c != null){
                                
                                if(InvlPolQntyMap.get(invl.PO_Line_Item__c) != null){
                                    InvlPolQntyMap.put(invl.PO_Line_Item__c,(0 + Decimal.ValueOf(InvlPolQntyMap.get(invl.PO_Line_Item__c).split('~')[0])) + '~' + ((Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0)+Decimal.ValueOf(InvlPolQntyMap.get(invl.PO_Line_Item__c).split('~')[1])));
                                } else {
                                    InvlPolQntyMap.put(invl.PO_Line_Item__c,0 + '~'+ (Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0));
                                }
                            }
                        }
                    }
                    
                }
                else if(Trigger.isInsert){ // When new Invoice Line Item is INSERTED //
                    List<String> invoicesIds = new List<String>();
                    for(Invoice_Line_Item__c invl : Trigger.New){
                        system.debug('I invl.Invoice__c ------------'+invl.Invoice__c);
                        if(invl.Invoice__c != null)
                        {
                            invoicesIds.add(invl.Invoice__c);
                        }
                    }
                    system.debug('I invoicesIds ------------'+invoicesIds);
                    MAP<ID, Invoice__c> invoices = new MAP<ID, Invoice__c> ([select id, Current_State__c, Previous_State__c from Invoice__c where id in :invoicesIds limit 999]);
                    system.debug('I invoices ------------'+invoices);
                    for(Invoice_Line_Item__c invl : Trigger.New){
                        if(invl.Quantity_Calculation_Required__c){
                            system.debug('I invl:::1::::'+invl);
                            if(invl.Invoice__c != null)
                            {
                                system.debug('I inv.invoice --------------'+invoices.get(invl.Invoice__c));
                                system.debug('I inv.invoice.Current_State__c --------------'+invoices.get(invl.Invoice__c).Current_State__c);
                            }
                            if(invl.Invoice__c != null && invoices.get(invl.Invoice__c) != null && rejectedState != null && invoices.get(invl.Invoice__c).Current_State__c.equals(rejectedState)){
                                system.debug('I IIIFFFFFFFFF');
                                if(invl.GRN_Line_Item__c != null){
                                    if(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c) != null){
                                        InvlGRNLQntyMap.put(invl.GRN_Line_Item__c,(0 + Decimal.ValueOf(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c).split('~')[0])) + '~' + (0 + Decimal.ValueOf(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c).split('~')[1])));
                                    }
                                    else {
                                        InvlGRNLQntyMap.put(invl.GRN_Line_Item__c,0 + '~' + 0);
                                    }
                                }
                                if(invl.PO_Line_Item__c != null){
                                    
                                    if(InvlPolQntyMap.get(invl.PO_Line_Item__c) != null){
                                        InvlPolQntyMap.put(invl.PO_Line_Item__c,(0 + Decimal.ValueOf(InvlPolQntyMap.get(invl.PO_Line_Item__c).split('~')[0])) + '~' + (0 + Decimal.ValueOf(InvlPolQntyMap.get(invl.PO_Line_Item__c).split('~')[1])));
                                    } else {
                                        InvlPolQntyMap.put(invl.PO_Line_Item__c,0 + '~'+ 0);
                                    }
                                }
                            } else {
                                system.debug('I EEELLLLSSSSEEE');
                                if(invl.GRN_Line_Item__c != null){
                                    
                                    if(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c) != null){
                                        system.debug('InvlGRNLQntyMap.get(invl.GRN_Line_Item__c):'+InvlGRNLQntyMap.get(invl.GRN_Line_Item__c));
                                        InvlGRNLQntyMap.put(invl.GRN_Line_Item__c,((invl.Quantity__c != null ? invl.Quantity__c : 0) + Decimal.ValueOf(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c).split('~')[0])) +'~'+0);
                                    }
                                    else {
                                        InvlGRNLQntyMap.put(invl.GRN_Line_Item__c,(invl.Quantity__c != null ? invl.Quantity__c : 0)+'~'+0);
                                    }
                                }
                                
                                if(invl.PO_Line_Item__c != null){
                                    if(InvlPolQntyMap.get(invl.PO_Line_Item__c) != null){
                                        InvlPolQntyMap.put(invl.PO_Line_Item__c,((invl.Quantity__c != null ? invl.Quantity__c : 0) + Decimal.ValueOf(InvlPolQntyMap.get(invl.PO_Line_Item__c).split('~')[0]))+'~'+(0+Decimal.ValueOf(InvlPolQntyMap.get(invl.PO_Line_Item__c).split('~')[1])));
                                    } else {
                                        InvlPolQntyMap.put(invl.PO_Line_Item__c,(invl.Quantity__c != null ? invl.Quantity__c : 0)+'~'+0);
                                    }
                                }
                            }
                        }
                    }
                }
                else if(Trigger.isUpdate){  // When Invoice Line Item is UPDATED //
                    List<String> invoicesIds = new List<String>();
                    for(Invoice_Line_Item__c invl : Trigger.New){
                        system.debug('I invl.Invoice__c ------------'+invl.Invoice__c);
                        if(invl.Invoice__c != null)
                        {
                            invoicesIds.add(invl.Invoice__c);
                        }
                    }
                    system.debug('I invoicesIds ------------'+invoicesIds);
                    MAP<ID, Invoice__c> invoices = new MAP<ID, Invoice__c> ([select id, Current_State__c, Previous_State__c from Invoice__c where id in :invoicesIds limit 999]);
                    system.debug('I invoices ------------'+invoices);
                    for(Invoice_Line_Item__c invl : Trigger.New){
                        system.debug('------Invil---------'+invl);
                        if(invl.Quantity_Calculation_Required__c){
                            if(Trigger.isUpdate && invl.Invoice__c != null && invoices.get(invl.Invoice__c) != null && rejectedState != null && invoices.get(invl.Invoice__c).Current_State__c.equals(rejectedState) && !(invoices.get(invl.Invoice__c).Current_State__c.equals(invoices.get(invl.Invoice__c).Previous_State__c))){
                                system.debug('------if---------'+invl);
                                if(invl.GRN_Line_Item__c != null){
                                    if(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c) != null){
                                        InvlGRNLQntyMap.put(invl.GRN_Line_Item__c,(0 + Decimal.ValueOf(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c).split('~')[0])) + '~' + ((Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0) + Decimal.ValueOf(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c).split('~')[1])));
                                    }
                                    else {
                                        InvlGRNLQntyMap.put(invl.GRN_Line_Item__c,0 + '~' + (Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0));
                                    }
                                }
                                if(invl.PO_Line_Item__c != null){
                                    
                                    if(InvlPolQntyMap.get(invl.PO_Line_Item__c) != null){
                                        InvlPolQntyMap.put(invl.PO_Line_Item__c,(0 + Decimal.ValueOf(InvlPolQntyMap.get(invl.PO_Line_Item__c).split('~')[0])) + '~' + ((Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0) + Decimal.ValueOf(InvlPolQntyMap.get(invl.PO_Line_Item__c).split('~')[1])));
                                    } else {
                                        InvlPolQntyMap.put(invl.PO_Line_Item__c,0 + '~'+ (Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0));
                                    }
                                }
                            } else {
                                system.debug('------else---------');
                                if(invl.GRN_Line_Item__c != null){
                                    
                                    if(invl.GRN_Line_Item__c == Trigger.oldMap.get(invl.Id).GRN_Line_Item__c){
                                        if(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c) != null){
                                            InvlGRNLQntyMap.put(invl.GRN_Line_Item__c,((invl.Quantity__c != null ? invl.Quantity__c : 0) + Decimal.ValueOf(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c).split('~')[0])) +'~'+((Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0) + Decimal.ValueOf(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c).split('~')[1])));
                                        }
                                        else {
                                            InvlGRNLQntyMap.put(invl.GRN_Line_Item__c,(invl.Quantity__c != null ? invl.Quantity__c : 0)+'~'+(Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0));
                                        }
                                    }
                                    else if(invl.GRN_Line_Item__c != Trigger.oldMap.get(invl.Id).GRN_Line_Item__c){
                                        if(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c) != null){
                                            InvlGRNLQntyMap.put(invl.GRN_Line_Item__c,((invl.Quantity__c != null ? invl.Quantity__c : 0) + Decimal.ValueOf(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c).split('~')[0])) +'~'+ (0 + Decimal.ValueOf(InvlGRNLQntyMap.get(invl.GRN_Line_Item__c).split('~')[1])));
                                        }
                                        else {
                                            InvlGRNLQntyMap.put(invl.GRN_Line_Item__c,(invl.Quantity__c != null ? invl.Quantity__c : 0)+'~'+0);
                                        }
                                        
                                        if(Trigger.oldMap.get(invl.Id).GRN_Line_Item__c != null){
                                            if(InvlGRNLQntyMap.get(Trigger.oldMap.get(invl.Id).GRN_Line_Item__c) != null){
                                                InvlGRNLQntyMap.put(Trigger.oldMap.get(invl.Id).GRN_Line_Item__c,(0 + Decimal.ValueOf(InvlGRNLQntyMap.get(Trigger.oldMap.get(invl.Id).GRN_Line_Item__c).split('~')[0])) +'~'+ ((Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0) + Decimal.ValueOf(InvlGRNLQntyMap.get(Trigger.oldMap.get(invl.Id).GRN_Line_Item__c).split('~')[1])));
                                            }
                                            else {
                                                InvlGRNLQntyMap.put(Trigger.oldMap.get(invl.Id).GRN_Line_Item__c,0+'~'+(Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0));
                                            }
                                        }
                                    }
                                    system.debug('----InvlGRNLQntyMap-----'+InvlGRNLQntyMap);
                                }
                                else if(invl.GRN_Line_Item__c == null && Trigger.oldMap.get(invl.Id).GRN_Line_Item__c != null){
                                    if(InvlGRNLQntyMap.get(Trigger.oldMap.get(invl.Id).GRN_Line_Item__c) != null){
                                        InvlGRNLQntyMap.put(Trigger.oldMap.get(invl.Id).GRN_Line_Item__c,0 +'~'+ ((Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0) + Decimal.ValueOf(InvlGRNLQntyMap.get(Trigger.oldMap.get(invl.Id).GRN_Line_Item__c).split('~')[1])));
                                    }
                                    else {
                                        InvlGRNLQntyMap.put(Trigger.oldMap.get(invl.Id).GRN_Line_Item__c,0+'~'+(Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0));
                                    }
                                }
                                
                                if(invl.PO_Line_Item__c != null){
                                    if(InvlPolQntyMap.get(invl.PO_Line_Item__c) != null){
                                        
                                        InvlPolQntyMap.put(invl.PO_Line_Item__c,((invl.Quantity__c != null ? invl.Quantity__c : 0) + Decimal.ValueOf(InvlPolQntyMap.get(invl.PO_Line_Item__c).split('~')[0]))+'~'+((Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0) + Decimal.ValueOf(InvlPolQntyMap.get(invl.PO_Line_Item__c).split('~')[1])));
                                    } else {
                                        InvlPolQntyMap.put(invl.PO_Line_Item__c,(invl.Quantity__c != null ? invl.Quantity__c : 0)+'~'+(Trigger.oldMap.get(invl.Id).Quantity__c != null ? Trigger.oldMap.get(invl.Id).Quantity__c : 0));
                                    }
                                }
                                system.debug('----InvlPolQntyMap-----'+InvlPolQntyMap);
                            }
                        }
                    }
                }
        List<GRN_Line_Item__c> grnLineItemUpdateList = new List<GRN_Line_Item__c>();
                if(InvlGRNLQntyMap.size() > 0){
                    if(GRN_Line_Item__c.getSObjectType().getDescribe().isAccessible()
          && Schema.sObjectType.GRN_Line_Item__c.fields.Id.isAccessible()
          && Schema.sObjectType.GRN_Line_Item__c.fields.Name.isAccessible()
          && Schema.sObjectType.GRN_Line_Item__c.fields.ESM_Remaining_Quantity__c.isAccessible()
          && Schema.sObjectType.GRN_Line_Item__c.fields.PO_Line_Item__c.isAccessible()){
            grnLineItemUpdateList = [SELECT Id,Name,ESM_Remaining_Quantity__c,PO_Line_Item__c FROM GRN_Line_Item__c WHERE Id IN: InvlGRNLQntyMap.keySet()];
                    } else {
            throw new CustomCRUDFLSException(UtilityController.getAccessExceptionMessage(esmNameSpace + 'GRN_Line_Item__c'));
          }
          if(grnLineItemUpdateList != null && grnLineItemUpdateList.size() > 0){
                        for(GRN_Line_Item__c grnl : grnLineItemUpdateList){
                            if(InvlGRNLQntyMap.get(grnl.Id) != null){
                                if(grnl.ESM_Remaining_Quantity__c == null){grnl.ESM_Remaining_Quantity__c = 0;}
                                // -- Added by Shital Rathod | 28-05-2018 | ESMPROD-1090 | Start -- // 
                                // grnl.ESM_Remaining_Quantity__c = grnl.ESM_Remaining_Quantity__c - Decimal.ValueOf(InvlGRNLQntyMap.get(grnl.Id).split('~')[0]) + Decimal.ValueOf(InvlGRNLQntyMap.get(grnl.Id).split('~')[1]);
                                grnl.ESM_Remaining_Quantity__c = grnl.ESM_Remaining_Quantity__c - Decimal.ValueOf(InvlGRNLQntyMap.get(grnl.Id).split('~')[1]);
                                // -- Added by Shital Rathod | 25-05-2018 | ESMPROD-1090 | End -- // 
                            }
                        }
                    }
                }
                List<PO_Line_Item__c> poLineItemUpdateList = new List<PO_Line_Item__c>();
                System.debug('++++++++InvlPolQntyMap'+InvlPolQntyMap);
                if(InvlPolQntyMap.size() > 0){
          if(PO_Line_Item__c.getSObjectType().getDescribe().isAccessible()
          && Schema.sObjectType.PO_Line_Item__c.fields.Id.isAccessible()
          && Schema.sObjectType.PO_Line_Item__c.fields.Name.isAccessible()
          && Schema.sObjectType.PO_Line_Item__c.fields.ESM_Remaining_Quantity__c.isAccessible()){
            poLineItemUpdateList = [SELECT Id,Name,ESM_Remaining_Quantity__c FROM PO_Line_Item__c WHERE Id IN: InvlPolQntyMap.keySet()];
                    } else {
            throw new CustomCRUDFLSException(UtilityController.getAccessExceptionMessage(esmNameSpace + 'PO_Line_Item__c'));
          }
          if(poLineItemUpdateList != null && poLineItemUpdateList.size() > 0){
                        for(PO_Line_Item__c pol : poLineItemUpdateList){
                        System.debug('pol----'+pol);
                            if(InvlPolQntyMap.get(pol.Id) != null){
                                if(pol.ESM_Remaining_Quantity__c == null){pol.ESM_Remaining_Quantity__c = 0;}
                               System.debug('pol.ESM_Remaining_Quantity__c======='+pol.ESM_Remaining_Quantity__c);
                               System.debug('InvlPolQntyMap.get======='+InvlPolQntyMap.get(pol.Id));
                               // -- Added by Shital Rathod | 25-05-2018 | ESMPROD-1090 | Start -- // 
                               pol.ESM_Remaining_Quantity__c = pol.ESM_Remaining_Quantity__c - Decimal.ValueOf(InvlPolQntyMap.get(pol.Id).split('~')[0]) + Decimal.ValueOf(InvlPolQntyMap.get(pol.Id).split('~')[1]);
                                 //pol.ESM_Remaining_Quantity__c = pol.ESM_Remaining_Quantity__c - Decimal.ValueOf(InvlPolQntyMap.get(pol.Id).split('~')[1]);
                                // -- Added by Shital Rathod | 25-05-2018 | ESMPROD-1090 | End -- // 
                            }
                        }
                    }
                }
                system.debug('poLineItemUpdateList:'+poLineItemUpdateList);
                if(grnLineItemUpdateList.size() > 0){
                    if(GRN_Line_Item__c.getSObjectType().getDescribe().isUpdateable()
          && Schema.sObjectType.GRN_Line_Item__c.fields.ESM_Remaining_Quantity__c.isUpdateable()) {
            update grnLineItemUpdateList;
          } else {
            throw new CustomCRUDFLSException(UtilityController.getUpdateExceptionMessage(esmNameSpace + 'GRN_Line_Item__c'));
          }
                }
                
                if(poLineItemUpdateList.size() > 0){
                    if(PO_Line_Item__c.getSObjectType().getDescribe().isUpdateable()
          && Schema.sObjectType.PO_Line_Item__c.fields.ESM_Remaining_Quantity__c.isUpdateable()) {
          System.debug('updated list------'+poLineItemUpdateList);
            update poLineItemUpdateList;
          } else {
            throw new CustomCRUDFLSException(UtilityController.getUpdateExceptionMessage(esmNameSpace + 'PO_Line_Item__c'));
          }
                }
                
            }
        }
        //--- Remaining Quantity Updation Logic | Ends ---//
        if(Trigger.IsBefore && Trigger.isInsert) {
            
            //---- Auto Matching | Starts ---//
            if(invConf.Auto_Match_Flow_Method__c != null && invConf.Auto_Match_Flow_Method__c != ''){
               
                boolean skipAutoMatch = false;
                Set<string> poSet = new set<string>();
                for(Invoice_Line_Item__c invl : Trigger.New){
                    system.debug('invl:'+invl);
                    if(invl.PO_Line_Item__c != null || invl.Purchase_Order__c == null){
                        // When PO Line Item is already mapped (i.e. when it Invoice Line Item is create in ESM) then there is no need of auto match
                        // When Purchase Order is not mapped on Invoice Line Item (i.e. in case of NON PO Invoice) then there is no need of auto match
                        skipAutoMatch = true;
                        break;
                    }
                    poSet.add(invl.Purchase_Order__c);
                }
                if(!skipAutoMatch){
                    Map<String,string> polinvlMap = new Map<String,string>();    
                    Map<string,string> InvMap = new Map<string,string>();
                    Map<string,set<string>> InvPOMap = new Map<string,set<string>>();
                    string polQuery = 'SELECT Id,Name';
                    List<PO_GRN_Line_Invoice_Line_Mappings__c> cusmdt = [select Invoice_Line_Item_Field_Name__c, PO_GRN_Line_Item_Field_Name__c,Used_In_Auto_Match__c from PO_GRN_Line_Invoice_Line_Mappings__c];
    
                   /* for(PO_Line_Item_Invoice_Line_Item_Mapping__c polinvlMapping:PO_Line_Item_Invoice_Line_Item_Mapping__c.getAll().values()){
                        if(polinvlMapping.Used_In_Auto_Match__c){
                            polinvlMap.put(polinvlMapping.PO_Line_Item_Field_Name__c,polinvlMapping.Invoice_Line_Item_Field_Name__c);
                            polQuery += ',' + polinvlMapping.PO_Line_Item_Field_Name__c;
                        }
                    }*/
                    for (PO_GRN_Line_Invoice_Line_Mappings__c cml : cusmdt) {
                        if(cml.Used_In_Auto_Match__c){
                            polinvlMap.put(String.valueOf(cml.PO_GRN_Line_Item_Field_Name__c), String.valueOf(cml.Invoice_Line_Item_Field_Name__c));
                            polQuery += ',' + cml.PO_GRN_Line_Item_Field_Name__c;
                        }
                    }
                    if(!polQuery.containsIgnoreCase('Purchase_Order__c')){
                        polQuery += ','+esmNameSpace+'Purchase_Order__c';
                    }
                    polQuery += ','+esmNameSpace+'GRN_Match_Required_Formula__c,PO_Line_No__c';
                    polQuery += ' FROM '+esmNameSpace+'PO_Line_Item__c WHERE '+esmNameSpace+'Purchase_Order__c IN:poSet'; 
                    
          List<PO_Line_Item__c> polList = new List<PO_Line_Item__c>();
          if(UtilityController.isFieldAccessible(esmNameSpace+'PO_Line_Item__c',null)){
            polList = database.query(polQuery);
                    }
          List<GRN_Line_Item__c> grnLineItemList = new List<GRN_Line_Item__c>();
          if(UtilityController.isFieldAccessible(esmNameSpace+'GRN_Line_Item__c',null)){
            grnLineItemList = [SELECT Id,Name,PO_Line_Item__c,Purchase_Order__c,GRN__c,PO_Line_Item__r.GRN_Match_Required_Formula__c FROM GRN_Line_Item__c WHERE Purchase_Order__c IN:poSet];
                    }
                    
          Map<String,String> GRNLineMap = new Map<String,String>();
                    Map<String,String> GRNMap = new Map<String,String>();
                    if(grnLineItemList != null && grnLineItemList.size() > 0){
                        for(GRN_Line_Item__c grnl : grnLineItemList){
                            if(grnl.PO_Line_Item__r.GRN_Match_Required_Formula__c != null && grnl.PO_Line_Item__r.GRN_Match_Required_Formula__c){
                                if(GRNLineMap.get(grnl.PO_line_Item__c)!= null){
                                    GRNLineMap.put(grnl.PO_line_Item__c,GRNLineMap.get(grnl.PO_line_Item__c)+'~'+grnl.id);
                                    GRNMap.put(grnl.PO_line_Item__c,GRNMap.get(grnl.PO_line_Item__c)+'~'+grnl.GRN__c);
                                }
                                else{
                                    GRNLineMap.put(grnl.PO_line_Item__c,grnl.id);
                                    GRNMap.put(grnl.PO_line_Item__c,grnl.GRN__c);
                                }
                            }
                        }
                    }
                    set<string> cPOSet = new set<string>();
                    for(Invoice_Line_Item__c invl : Trigger.New){
                        if(InvPOMap.get(invl.Invoice__c) != null){
                            cPOSet = InvPOMap.get(invl.Invoice__c);
                            cPOSet.add(invl.Purchase_Order__c);
                            InvPOMap.put(invl.Invoice__c,cPOSet);
                        } else {
                            cPOSet = new set<string>();
                            cPOSet.add(invl.Purchase_Order__c);
                            InvPOMap.put(invl.Invoice__c,cPOSet);
                        }
                        if(invl.PO_Line_Item__c == null){
                            invl.Is_Mismatch__c = true;
                            boolean Matched = true;
                            
                            if(invConf.Auto_Match_Flow_Method__c.equalsIgnoreCase('Sequence')){
                                for(String polFieldName:polinvlMap.keySet()){
                                    String invlFieldName = polinvlMap.get(polFieldName);
                                    for(PO_Line_Item__c pol:polList){
                                        Matched = false;
                                        if(pol.Purchase_Order__c == invl.Purchase_Order__c){
                                            if(pol.get(polFieldName) != null && pol.get(polFieldName).equals(invl.get(invlFieldName))){
                                                Matched = true;                                         
                                            }
                                            if(Matched){
                                                invl.PO_Line_Item__c = pol.Id;
                                                break;                                      
                                            }
                                        }
                                    }
                                    if(invl.PO_Line_Item__c != null){
                                        break;                                      
                                    }
                                }
                            }
                            else if(invConf.Auto_Match_Flow_Method__c.equalsIgnoreCase('Parallel')){
                                for(PO_Line_Item__c pol:polList){
                                    Matched = true;
                                    if(pol.Purchase_Order__c == invl.Purchase_Order__c){
                                        for(String polFieldName:polinvlMap.keySet()){
                                            String invlFieldName = polinvlMap.get(polFieldName);
                                            system.debug('pol.get(polFieldName):'+pol.get(polFieldName));
                                            system.debug('invl.get(invlFieldName):'+invl.get(invlFieldName));
                                            if((pol.get(polFieldName) != null && invl.get(invlFieldName) == null) || (pol.get(polFieldName) == null && invl.get(invlFieldName) != null)){
                                                Matched = false;
                                            }
                                            else if(pol.get(polFieldName) != null && !pol.get(polFieldName).equals(invl.get(invlFieldName))){
                                                Matched = false;
                                            }
                                        }
                                        if(Matched){
                                            invl.PO_Line_Item__c = pol.Id;
                                            if(invl.Invoice_Line_Item_No__c == null){
                                                invl.Invoice_Line_Item_No__c = pol.Purchase_Order__c + '~' + pol.PO_Line_No__c + '~' + CommonFunctionController.getRandomnumber(10);
                                            }
                                            break;                                      
                                        }
                                    }
                                }
                            }
                            if(invl.PO_Line_Item__c != null){
                                invl.Is_Mismatch__c = false;
                                if(InvMap.get(invl.Invoice__c) == null || (InvMap.get(invl.Invoice__c) != null && InvMap.get(invl.Invoice__c) != 'Fail')){
                                    InvMap.put(invl.Invoice__c, 'Pass');
                                } 
                                if(GRNLineMap != null && GRNLineMap.size() > 0 && GRNLineMap.get(invl.PO_Line_Item__c) != null && !GRNLineMap.get(invl.PO_Line_Item__c).contains('~')){
                                    invl.GRN_Line_Item__c = GRNLineMap.get(invl.PO_Line_Item__c);
                                    invl.GRN__c = GRNMap.get(invl.PO_Line_Item__c);
                                }
                                try{
                                    if(invl.Quantity__c != null){
                                        if(UserInfo.isMultiCurrencyOrganization() && invl.Quantity__c != null && invl.Rate_Currency__c != null){
                      invl.Amount_Currency__c = invl.Quantity__c * invl.Rate_Currency__c.setscale(3,System.RoundingMode.HALF_UP);
                    } else if(!UserInfo.isMultiCurrencyOrganization() && invl.Quantity__c != null && invl.Rate__c != null){
                      invl.Amount__c = invl.Quantity__c * invl.Rate__c.setscale(3,System.RoundingMode.HALF_UP);
                    }
                                    }
                                } catch(exception ex){}
                            }
                            else {
                                invl.Exception_Reason__c = 'PO Line Not Found';
                                invl.Is_Mismatch__c = true;
                                InvMap.put(invl.Invoice__c, 'Fail');
                                if(invl.Invoice_Line_Item_No__c == null){
                                    invl.Invoice_Line_Item_No__c = invl.Purchase_Order__c + '~null~' + CommonFunctionController.getRandomnumber(10);
                                }
                            }
                        }
                        
                    }
                    
                    if(InvMap != null && InvMap.size() > 0){
            List<Invoice__c> invList = new List<Invoice__c>();
            if(UtilityController.isFieldAccessible(esmNameSpace+'Invoice__c','Id,Name,'+esmNameSpace+'Exception_Reason__c,'+esmNameSpace+'User_Actions__c,'+esmNameSpace+'Current_State__c')){
              invList = [SELECT Id,Name,Exception_Reason__c,User_Action__c,Current_State__c FROM Invoice__c WHERE Id IN:InvMap.keySet()];
                        }
            if(invList != null && invList.size() > 0){
                            for(Invoice__c inv:InvList){
                                if(InvMap.get(inv.Id).equalsIgnoreCase('Fail') || (inv.Exception_Reason__c != null && inv.Exception_Reason__c.contains('PO Line Not Found'))){
                                    if(inv.Exception_Reason__c != null && !inv.Exception_Reason__c.contains('PO Line Not Found')){
                                        inv.Exception_Reason__c = inv.Exception_Reason__c+';PO Line Not Found';
                                    } else {
                                        inv.Exception_Reason__c = 'PO Line Not Found';
                                    }
                                    inv.User_Action__c = 'Auto Match Fail';
                                } else {
                                    inv.User_Action__c = 'Auto Match Pass';
                                }
                            }
                            system.debug('InvMap:'+InvMap);
              if(Invoice__c.getSObjectType().getDescribe().isUpdateable()
              && Schema.sObjectType.Invoice__c.fields.Exception_Reason__c.isAccessible()
              && Schema.sObjectType.Invoice__c.fields.User_Action__c.isAccessible()){
                update invList;
              } else {
                throw new CustomCRUDFLSException(UtilityController.getUpdateExceptionMessage(esmNameSpace + 'Invoice__c'));
              }
                        }
                    }
                    //--- Update InvoicePOMatrix ---//
                    if(InvPOMap != null && InvPOMap.size() > 0){
                        List<Invoice_PO_Detail__c> invPoList = new List<Invoice_PO_Detail__c>();
                        for(String inv: InvPOMap.keySet()){
                            for(String po: InvPOMap.get(inv)){
                                Invoice_PO_Detail__c invpoObj = new Invoice_PO_Detail__c();
                                invpoObj.Invoice__c = inv;
                                invpoObj.Purchase_Order__c = po;
                                invPoList.add(invpoObj);
                            }
                        }
                        if(invPoList != null && invPoList.size() > 0){
                            if(Invoice_PO_Detail__c.getSObjectType().getDescribe().isCreateable()
              && Schema.sObjectType.Invoice_PO_Detail__c.fields.Invoice__c.isCreateable()
              && Schema.sObjectType.Invoice_PO_Detail__c.fields.Purchase_Order__c.isCreateable()){
                insert invPoList;
              } else {
                throw new CustomCRUDFLSException(UtilityController.getAccessExceptionMessage(esmNameSpace + 'Invoice_PO_Detail__c'));
              }
                        }
                    }
                }
            }
            //---- Auto Matching | Ends ---//
        }
        
        //--- Added by Vishwa | JIRA:ESMPROD-1263 | 5-31-2018 | Starts ---//
  // Added For Accuracy Report
  if (Trigger.isAfter && (Trigger.isUpdate || Trigger.isInsert)) {
      
    Map<String, string> APFieldADHfieldMap = new Map<String, string> ();
      
    for (OCR_Accuracy_Report_Mapping__c OCRConf :[SELECT Id, FieldName_on_Object__c, FieldName_on_OCR__c, Object_Name__c FROM OCR_Accuracy_Report_Mapping__c]) {
    if(OCRConf.Object_Name__c=='Invoice_Line_Item__c')
        APFieldADHfieldMap.put(OCRConf.FieldName_on_Object__c, OCRConf.FieldName_on_OCR__c);
    }
       System.debug('--------APFieldADHfieldMap ' + APFieldADHfieldMap);
 
    list<OCR_Accuracy_Report_LineLevel__c> lstAccuracyForInsert = new list<OCR_Accuracy_Report_LineLevel__c> ();
   // list<OCR_Accuracy_Report_LineLevel__c> lstAccuracyForUpdate = new list<OCR_Accuracy_Report_LineLevel__c> ();
    OCR_Accuracy_Report_LineLevel__c objAccuracyReport;
    Set<Id> idList = Trigger.newMap.keySet();
    List<OCR_Accuracy_Report_LineLevel__c> existingData = new List<OCR_Accuracy_Report_LineLevel__c> ();
    String query = CommonFunctionController.getCreatableFieldsSOQL('OCR_Accuracy_Report_LineLevel__c', esmNameSpace + 'Invoice_Line_Item__c IN :idList', false);
    if (UtilityController.isFieldAccessible('OCR_Accuracy_Report_LineLevel__c', null)) {
      existingData = database.query(query);
    }
    Map<Id, OCR_Accuracy_Report_LineLevel__c> existingDataMap = new Map<Id, OCR_Accuracy_Report_LineLevel__c> ();
    if (existingData != null && existingData.size() > 0) {
      for (OCR_Accuracy_Report_LineLevel__c obj : existingData) {
        if (obj.get(esmNameSpace + 'Invoice_Line_Item__c') != null) {
          existingDataMap.put((Id) obj.get(esmNameSpace + 'Invoice_Line_Item__c'), obj);
        }
      }
    }
        System.debug('--------existingDataMap ' + existingDataMap);


/*    for (akritivtlm__Accuracy_Data_History_Config__c adhc : lstAccrHist) {
      APFieldADHfieldMap.put(adhc.Name, adhc.akritivtlm__Accuracy_Report_Field__c);
    }*/
     System_Configuration__c sysConf=[SELECT Id, Name, Module__c, Sub_Module__c, Value__c FROM System_Configuration__c where Sub_Module__c='OCR_Accuracy_Current_State'];
     String currentStateFromConfig = sysConf.Value__c;
      
      
    Set<String> invIds = new Set<String> ();
    Map<String, Invoice__c> invMap = new Map<String, Invoice__c> ();
    Map<String, OCR_Accuracy_Report_HeaderLevel__c> OCRheaderMap = new Map<String, OCR_Accuracy_Report_HeaderLevel__c> ();
     for (Invoice_Line_Item__c obj : Trigger.new) {
          if (obj.Invoice__c != null) {
                 invIds.add(obj.Invoice__c);
             }
       }
       System.debug('invIds :' + invIds);

       String query1 = 'Select id,Previous_State__c from Invoice__c  where id = :invIds';
       list<Invoice__c> invList = database.query(query1);
       System.debug('invList :' + invList);
       for (Invoice__c inv : invList) {
               invMap.put(inv.id, inv);
         }
                System.debug('invMap :' + invMap);
      
  String query2 = CommonFunctionController.getCreatableFieldsSOQL('OCR_Accuracy_Report_HeaderLevel__c', esmNameSpace + 'Invoice__c IN :invIds', false);
    List<OCR_Accuracy_Report_HeaderLevel__c> OCRheaderList = new List<OCR_Accuracy_Report_HeaderLevel__c> ();
      if (UtilityController.isFieldAccessible('OCR_Accuracy_Report_HeaderLevel__c', null)) {
      OCRheaderList = database.query(query2);
    }
  
    if (OCRheaderList != null && OCRheaderList.size() > 0) {
      for (OCR_Accuracy_Report_HeaderLevel__c obj : OCRheaderList) {
        if (obj.get(esmNameSpace + 'Invoice__c') != null) {
          OCRheaderMap.put((Id) obj.get(esmNameSpace + 'Invoice__c'), obj);
        }
      }
    }
        System.debug('--------OCRheaderMap ' + OCRheaderMap);
      
  //  if (lstAccrHist != null && lstAccrHist.size() > 0) {
   if (APFieldADHfieldMap != null && APFieldADHfieldMap.size() > 0) {
      for (Invoice_Line_Item__c childobj : Trigger.new) {
        
        String currentState = '';
        currentState = (invMap.get(childobj.Invoice__c)).Previous_State__c;
    
        System.debug('--------currentState ' + currentState);
        System.debug('--------currentStateFromConfig ' + currentStateFromConfig);
          
         OCR_Accuracy_Report_HeaderLevel__c OCRheaderObj=OCRheaderMap.get(childobj.Invoice__c);

     //   akritivtlm__App_Logic__c appLogic = akritivtlm__App_Logic__c.getInstance(objCode + '.CurrentStateForAccuracyReport');
       
        if (currentStateFromConfig != null && currentStateFromConfig != '' && currentState != null && currentState != '' && currentState.equalsIgnoreCase(currentStateFromConfig)) 
        {
 
          if (!existingDataMap.containsKey(childobj.Id)){
            objAccuracyReport = new OCR_Accuracy_Report_LineLevel__c();
          }
          System.debug('--------objAccuracyReport ---- ' + objAccuracyReport);
            
          objAccuracyReport.put(esmNameSpace + 'Invoice_Line_Item__c', childobj.get('Id'));
          objAccuracyReport.put(esmNameSpace + 'OCR_Accuracy_Report_HeaderLevel__c', OCRheaderObj.Id);
          for (string s : APFieldADHfieldMap.keyset()) {
            if (string.valueof(childobj.get(s)) != null && string.valueof(childobj.get(s)) != '')
            objAccuracyReport.put(APFieldADHfieldMap.get(s), childobj.get(s));
            else
            objAccuracyReport.put(APFieldADHfieldMap.get(s), null);
          }
            
             System.debug('--------objAccuracyReport ' + objAccuracyReport);
            
          if (!existingDataMap.containsKey(childobj.Id))  {
            lstAccuracyForInsert.add(objAccuracyReport);
          }
            
          
        }
      }
    }

    if (lstAccuracyForInsert.size() > 0) {
        
            System.debug('--------lstAccuracyForInsert ' + lstAccuracyForInsert);

      if (UtilityController.isFieldCreateable('OCR_Accuracy_Report_LineLevel__c', null)) {
        Database.insert(lstAccuracyForInsert, false);
      }
    }
 /*   if (lstAccuracyForUpdate.size() > 0) {
          System.debug('--------lstAccuracyForUpdate ' + lstAccuracyForUpdate);
      if (UtilityController.isFieldUpdateable('OCR_Accuracy_Report_LineLevel__c', null)) {
        Database.update(lstAccuracyForUpdate, false);
      }
    }*/

  }
  //--- Added by Vishwa | JIRA:ESMPROD-1263 | 5-31-2018 | Starts ---//
    }
    
      }
    catch(Exception e) {
          System.debug('--------Exception ' + e);
        ExceptionHandlerUtility.customDebug(e, 'InvoiceMasterTrigger');
        Trigger.New[0].addError(UtilityController.customDebug(e, 'InvoiceMasterTrigger'));
    }

}